��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  f  1?  :   �@  .   �@  6   A  /   9A  �   iA  .   GB  =   vB  1   �B  9   �B  >    C  �   _C  I   D  N   LD  =   �D  8   �D  �   E  �   �E  �   �F     mG     G     �G  "   �G     �G     �G     �G     
H     H  "   'H     JH     aH     gH     |H     �H  E   �H     �H     �H     I     
I     #I     <I     \I     {I     �I     �I     �I     �I  
   �I     �I     J     /J  	   FJ  �   PJ     �J     �J     �J     K  A   !K     cK     }K     �K  $   �K     �K     �K     L     'L  $   -L  �   RL     �L     �L     �L     M     M     "M  	   1M  !   ;M     ]M  	   tM     ~M     �M     �M  '   �M  %   �M  )   �M  !   N  @   ?N  )   �N  d   �N     O     $O     CO     ^O  "   uO  *   �O      �O     �O  /   �O  .   *P  1   YP  *   �P  #   �P     �P     �P  D   Q     PQ     VQ     dQ     jQ     |Q     �Q     �Q  
   �Q  O   �Q      R     R  	   R     R      R  #   :R     ^R  v  oR     �S  &   �S     "T     *T     0T  \   HT  ^   �T     U     U     -U     ?U     WU     uU     |U     �U     �U  9   �U  K   �U  3   .V  +   bV  T   �V     �V     W     W     #W     0W     DW     IW     PW     YW     lW     �W     �W  %   �W  #   �W  
   X     X     ,X  <   JX     �X  	   �X     �X     �X     �X     �X     �X  R   Y  P   WY  S   �Y  L   �Y  %   IZ     oZ  Q   �Z  (   �Z  +   [  F   .[  A   u[  ?   �[  K   �[     C\  (   O\     x\  	   �\  S   �\     �\  	   �\     �\     ]  9   !]     []     h]     ]     �]     �]     �]     �]     �]     �]     �]     ^  #   &^     J^     d^     x^  <   �^  �   �^  G   �_     �_  	   �_      �_      `     -`  	   B`     L`     ``  ;   g`  /   �`  .   �`  .   a     1a  _   >a  ]   �a  `   �a  [   ]b     �b     �b     �b     �b     �b     �b  
   c     c     +c     Kc     hc  7   ~c  6   �c  t   �c  !   bd    �d  9   �e     �e     �e  (    f  "   )f  Q   Lf  t   �f  o   g  Z   �g     �g  -   �g  !    h     Bh     Ih  6   ih  M   �h  )   �h     i  B   .i  '   qi      �i     �i  N   �i     )j  :   Fj      �j     �j     �j     �j  :   �j  5   k     Ek     Zk     jk     �k  
   �k     �k     �k     �k  $   �k  	   l     l  !   l     ;l     Bl     Kl  	   Xl     bl     il     ml  	   tl     ~l     �l     �l  
   �l     �l     �l     �l     �l     �l                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-26 11:48+0000
Last-Translator: Nuno Miguel <nunomgue@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/xfce/exo/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [opções] --build-list [[nome do ficheiro]...]
        exo-open --launch TIPO [PARÂMETROs...]   --build-list      Interpreta pares (nome, ficheiro)
   --extern          Gera os símbolos externos
   --launch TIPO [PARÂMETROS...]       Executar a aplicação preferida do
                                      TIPO com PARÂMETROs opcionais, onde
                                      TIPO é um dos seguintes valores.   --name=identifier nome da variável/macro C
 --output=nomeficheiro Escreve código para ficheiro indicado
   --static          Gera os símbolos estáticos
   --strip-comments  Remove comentários de ficheiros XML
   --strip-content   Remove nós de conteúdo de ficheiros XML
   --working-directory DIRETÓRIO       Diretório de trabalho por omissão para
                                      aplicações ao utilizar a opção --launch.   -?, --help                          Mostra esta mensagem de ajuda e sai   -V, --version                       Mostra as informações da versão e sai   -V, --version     Mostra as informações da versão e sai
   -h, --help        Mostra esta mensagem de ajuda e sai
   WebBrowser       - O navegador web preferido.
  MailReader       - O leitor de correio preferido.
  FileManager      - O gestor de ficheiros preferido.
  TerminalEmulator - O emulador de terminal preferido. %s (Xfce %s)

Direitos de autor (c) 2003-2006
        os-cillation e.K. Todos os direitos reservados.

Desenvolvido por Benedikt Meurer <benny@xfce.org>.

Compilado com Gtk+-%d.%d.%d, executado em Gtk+-%d.%d.%d.

Por favor, reporte os erros em <%s>.
 %s vem SEM QUALQUER GARANTIA,
Pode distribuir cópias de %s nos termos da
GNU LGPL que pode ser encontrada no pacote do código
fonte de %s.

 Ícones de ação Ativo Cor de contorno de item ativo Cor de preenchimento de item ativo Índice de item ativo Cor de texto de item ativo Todos os ficheiros Todos os ícones Animações Botão para escolha da aplicação Ícones de aplicação Balsa Dispositivo de bloco Brave Explorar sistema de ficheiros Explore o sistema de ficheiros para escolher o comando personalizado. Navegar na web C_omentário: C_riar Gestor de ficheiros Caja Dispositivo de carácter Escolha a aplicação preferida Escolha um gestor de ficheiros Escolha um leitor de correio Escolha um emulador de terminal Escolha um navegador web Escolher nome do ficheiro Chromium Claws Mail Limpar campo de pesquisa Espaçamento de coluna Espaçamento de coluna Com_ando: Direitos de autor(c) %s
        os-cillation e.K. Todos os direitos reservados.

Desenvolvido por Benedikt Meurer <benny@xfce.org>.

 Criar diretório Criar lançador Criar lançador <b>%s</b> Criar ligação Criar um novo ficheiro de ambiente de trabalho no diretório dado Cor de contorno do cursor Cor de preenchimento do cursor Cor de texto do cursor Personalizar barra de ferramentas... Navegador Debian Sensible Emulador de terminal Debian X Ícones de dispositivo Dillo _Não voltar a mostrar esta mensagem Arraste um item sobre as barras de ferramentas para o adicionar, das barras de ferramentas para a tabela de itens para o remover. Editar diretório Editar lançador Editar ligação Emblemas Ícones emotivos Ativar procura Encompass Emulador de terminal Enlightened  Navegador web Epiphany Evolution Executáveis FIFO Falha ao criar "%s". Falha ao executar o gestor de ficheiros Falha ao executar o leitor de correio Falhou ao executar o emulador de terminal Falha ao executar o navegador web Falha ao executar a aplicação preferida para a categoria "%s". Falha ao carregar o conteúdo de "%s": %s Falha ao carregar a imagem "%s": Motivo desconhecido, provavelmente um ficheiro de imagem corrompido Falha ao abrir "%s". Falha ao abrir %s para escrita Falha ao abrir o URI "%s". Falha ao abrir o ecrã Falha ao abrir o ficheiro "%s": %s Falha ao processar o conteúdo de "%s": %s Falha ao ler o ficheiro "%s": %s Falha ao gravar "%s". Falha ao definir o gesto de ficheiros preferido Falha ao definir o leitor de correio preferido Falha ao definir o emulador de terminal preferido Falha ao definir o navegador web preferido O ficheiro "%s" não tem chave tipo Gestor de ficheiros Ícones de tipos de ficheiro A localização de ficheiro não é um ficheiro ou diretório normal Pasta Seguir estado GIcon Terminal do GNOME Navegador web Galeon Geary Google Chrome Homogéneo Como o texto e o ícone de cada item são posicionados em relação um ao outro Icecat Icedove Iceweasel Ícone Modelo de barra de ícone Modelo de visualização de ícones Coluna de ícone Se não especificar a opção --launch, exo-open irá abrir todos os URLs
especificadas com seus gestores de URL preferidos. Se especificar
a opção --launch, pode selecionar a aplicação preferida que deseja
executar e transmitir parâmetros adicionais para a aplicação (i.e.para
TerminalEmulator pode transmitir a linha de comandos que deve ser
executada no terminal). Ficheiros de imagens O assistente de tipo "%s" é inválido Jumanji KMail Navegador web Konqueror Iniciar assistente de TIPO com o PARÂMETRO opcional, onde TIPO é um dos seguintes valores. Carregar ficheiros desktop não é suportado se %s for compilado sem funcionalidades GIO-Unix. Modo de esquema Navegador de texto Links Ícones de locais Navegador de texto Lynx Leitor de correio eletrónico Margem Coluna de marcação Ícones de menu Midori A coluna do modelo a pesquisar quando procurando por item Coluna de modelo usada para obter o caminho absoluto dum ficheiro de imagem Coluna de modelo usada para retirar o ícone pixbuf Coluna de modelo usada para retirar o texto A coluna do modelo utilizada para retirar o texto se estiver a usar marcação Pango Modelo para a barra de ícone Navegador Mozilla Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXTerm Nautilus Navegador Netscape Nenhuma aplicação selecionada Nenhum comando especificado Nenhum ficheiro selecionado Nenhum ficheiro/pasta especificado(a) Sem assistente definido para " %s". Sem ícone Número de colunas O número de colunas a exibir Abrir a janela de configuração das
aplicações preferidas Navegador Opera Opções: Orientação Gestor de ficheiros PCMan Gestor de ficheiros PCManFM-Qt "Scripts" Perl Coluna Pixbuf Por favor, escolha o seu gestor de ficheiros
preferido e clique OK para continuar. Por favor, escolha o seu leitor de correio
preferido e clique OK para continuar. Por favor, escolha o seu emulador de terminal
preferido e clique OK para continuar. Por favor, escolha o seu navegador web
preferido e clique OK para continuar. Por favor, reporte os erros em <%s>.
 Aplicações preferidas Aplicações preferidas (Navegador web, Leitor de correio e Emulador de terminal) URL pré-definido ao criar uma ligação Comando pré-definido ao criar um lançador Comentário pré-definido ao criar um ficheiro do ambiente de trabalho Ícone pré-definido ao criar um ficheiro do ambiente de trabalho Nome pré-definido ao criar um ficheiro do ambiente de trabalho Pressione o botão esquerdo do rato para alterar a aplicação selecionada. Antevisão  Mostra as informações da versão e sai "Scripts" Python QTerminal Acede ao assistente de por omissão de TYPE, onde TYPE é um dos seguintes valores. QupZilla ROX-Filer RXVT Unicode Ler o correio eletrónico Renderizar diferentemente baseado no estado da seleção. Reordenável Comando para reiniciar Gestor de ficheiros Rodent Espaçamento de linha Espaçamento de linha "Scripts" Ruby Executar no _terminal ID do SOCKET Sakura Coluna de procura Escolher í_cone de: Selecione um diretório de trabalho Selecione uma aplicação Selecione um ícone Selecione a aplicação Selecione as aplicações por omissão dos vários serviços Selecione esta opção para ativar a notificação de arranque ao executar o comando no gestor de ficheiros ou menu. Nem todas as aplicações têm suporte à notificação de arranque. Selecione esta opção para executar o comando numa janela de terminal. Modo de seleção Separador Comando para reiniciar a sessão Socket do gestor de definições "Scripts" de consola Um clique Duração do clique Socket O espaço inserido nos limites da visualização de ícones O espaço inserido as entre células de um item O espaço inserido entre as grelhas de colunas O espaço inserido entre as grelhas das linhas Espaçamento Especifique a aplicação que pretende utilizar como
gestor de ficheiros preferido para o Xfce: Especifique a aplicação que pretende utilizar como
leitor de correio preferido para o Xfce: Especifique a aplicação que pretende utilizar como
emulador de terminal preferido para o Xfce: Especifique a aplicação que pretende utilizar como
o navegador web preferido para o Xfce: Ícones de estado Ícones padrão Surf Sylpheed TIPO [PARÂMETRO] Emulador de terminal Terminator Coluna de texto Texto para ícones i_mportantes Texto p_ara todos os ícones O GIcon a renderizar. A quantidade de espaço entre duas colunas consecutivas A quantidade de espaço entre duas linhas consecutivas O espaço de tempo após a qual o item sob o ponteiro do rato será selecionado automaticamente no modo de um clique O ficheiro "%s" não possui dados Os seguintes TYPEs são suportados por comandos --launch e --query:

WebBrowser         - O navegador Web preferido.
MailReader         - O leitor de correio preferido.
Gestor de ficheiros - O gestor de ficheiros preferido.
Emulador de terminal - O emulador de terminal preferido. Os seguintes TIPOS são suportados pelo comando --launch: O ícone a renderizar. O modo de esquema O modelo para a visualização de ícone A orientação da barra de ícones O gestor de ficheiros preferido será usado para explorar o conteúdo das pastas. O leitor de correio eletrónico preferido será usado para escrever mensagens ao clicar nos endereços eletrónicos. O emulador de terminal preferido será usado para executar os comandos que necessitem de uma linha de comandos. O navegador web preferido será usado para abrir ligações e exibir o conteúdo da ajuda. O modo de seleção O tamanho, em pixeis, do ícone a renderizar. A largura utilizada por cada item Thunar E_stilo de barra de ferramentas Digite '%s --help' para informações de utilização. O tipo de ficheiro de ambiente de trabalho a criar (aplicação ou ligação) Incapaz de detetar o esquema URI de "%s". Ícones sem categoria O tipo de ficheiro de ambiente de trabalho não é suportado: "%s" Utilização: %s [opções] [ficheiro]
 Utilização: exo-open [URLs...] U_sar notificação de arranque Utilizar uma aplicação especifica e que não está incluída na lista acima. Utilizar a linha de comandos Permite ao utilizador procurar interativamente nas colunas A visualização é reordenável Vimprobable2 Navegador de texto W3M Navegador web Se as janelas dependentes devem ser todas do mesmo tamanho Se os itens na vista podem ser ativados com um clique Largura de cada item Grupo de janela Líder do grupo de janela _Diretório de trabalho: Terminal X Terminal do Xfce Gestor de ficheiros Xfe [FICHEIRO|PASTA] _Adicionar nova barra de ferramentas _Cancelar _Fechar Omissão de a_mbiente de trabalho _Ajuda Ícon_e: Só í_cones _Internet _Nome: _OK _Abrir _Outro... _Remover barra de ferramentas _Gravar Pe_squisar ícone: Só _texto _URL: _Utilitários aterm qtFM tamanho 