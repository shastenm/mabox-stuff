��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  <   A  7   RA  ;   �A  ?   �A  �  B  H   �C  `   �C  I   PD  P   �D  Y   �D  	  EE  Z   OF  i   �F  X   G  I   mG  K  �G    I  7  J     SK     qK  ;   �K  ;   �K  0   �K  9   )L     cL     uL     �L  .   �L  !   �L     �L  #   �L     M  6   M  g   RM  &   �M     �M     �M  &   
N  )   1N  >   [N  0   �N  .   �N  0   �N  "   +O  "   NO     qO  
   zO  &   �O     �O     �O     �O     �O     yP  *   �P  4   �P     �P  Q   Q  &   eQ  &   �Q  $   �Q  ;   �Q     R  *   ,R     WR     wR  A   }R  �   �R     �S  ,   �S     �S     �S     T     T  	   4T  /   >T     nT  	   �T  !   �T     �T  *   �T  `   �T  b   JU  b   �U  T   V  z   eV  B   �V  �   #W  *   �W  =   �W  .   ;X  1   jX  6   �X  F   �X  :   Y  .   UY  b   �Y  d   �Y  d   LZ  V   �Z  D   [  !   M[  $   o[  D   �[     �[  %   �[     \     #\     :\     W\     ]\     k\  �   �\     ]     ]  	   $]     .]  (   ;]  6   d]     �]  �  �]  !   f`  P   �`     �`     �`     �`    a  �   b  !   �b  '   �b     �b  &   c  #   2c     Vc     _c     c     �c  �   �c  �   d  c   �d  \   .e  �   �e  /   !f     Qf     hf     xf     �f     �f     �f     �f     �f  (   �f  "   �f     g  0   .g  Y   _g     �g  #   �g  <   �g  k   .h     �h     �h     �h  '   �h  ,    i     -i     Ci  �   Yi  �   �i  �   �j  �   2k  2   �k  1   �k  �   'l  ?   �l  W   �l  O   Um  E   �m  ?   �m  l   +n  /   �n  C   �n     o  	   $o  �   .o     p  	   p     #p  .   0p  [   _p  )   �p  %   �p  (   q     4q     Nq     hq  )   ~q     �q     �q     �q  "   �q  ,   �q     +r     Kr     cr  b   �r  J  �r  o   1t     �t     �t  2   �t  .   	u     8u     Ou  0   ou  
   �u  -   �u  0   �u  2   
v  $   =v     bv  �   ov  �   -w  �   �w  �   �x     Wy  #   sy     �y     �y     �y  #   �y  
   �y  !   �y  .   z  *   ?z  .   jz  E   �z  C   �z  �   #{  2   �{  �  |  X   �}  &   !~  !   H~  =   j~  0   �~  �   �~  �     �   V�  �   3�     ݁  I   ��  M   E�     ��  1   ��  p   ̂  L   =�  B   ��     ̓  A   �  ?   )�  .   i�  B   ��  ^   ۄ  8   :�  e   s�  D   م     �  %   +�     Q�  L   g�  S   ��  ,   �     5�  '   K�     s�     ��     ��  &   ��     �  B   ��     @�     R�  0   b�     ��     ��     ��     ͈     ߈     �     �     ��  5   �     E�     Y�     u�     ��     ��     ��     ��     ��                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-25 22:42+0000
Last-Translator: Igor <f2404@yandex.ru>
Language-Team: Russian (http://www.transifex.com/xfce/exo/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
        %s [параметры] --build-list [[файл]...]
        exo-open --launch ТИП [ПАРАМЕТРы...]   --build-list      Разбор пар (имя, файл)
   --extern          Создать внешне символы
   --launch ТИП [ПАРАМЕТРы...]         Запустить предпочитаемое приложение
                                      выбранного ТИПа с необязательными
                                      ПАРАМЕТРами, где ТИП может принимать одно
                                      из следующих значений.   --name=identifier Имя переменной или макроса C
   --output=filename Записать созданный код C в указанный файл
   --static          Создать статические символы
   --strip-comments  Удалить комментарии из файлов XML
   --strip-content   Удалить содержимое узлов из файлов XML
   --working-directory КАТАЛОГ         Рабочий каталог для приложений
                                      по умолчанию при использовании
                                      параметра --launch.   -?, --help                          Показать эту справку и выйти   -V, --version                       Показать информацию о версии и выйти   -V, --version     Показать информацию о версии и выйти
   -h, --help        Показать эту справку и выйти
   WebBrowser       - предпочитаемый веб-браузер.
  MailReader       - предпочитаемая почтовая программа.
  FileManager      - предпочитаемый файловый менеджер.
  TerminalEmulator - предпочитаемый эмулятор терминала. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Все Все права защищены.

Автор: Benedikt Meurer <benny@xfce.org>.

Собрано с Gtk+-%d.%d.%d, используется Gtk+-%d.%d.%d.

Об ошибках сообщайте на <%s>.
 %s распространяется БЕЗ КАКОЙ-ЛИБО ГАРАНТИИ,
разрешено распространение копий %s на условиях
GNU Lesser General Public License, полный текст лицензии
находится в пакете исходных кодов %s.

 Значки действий Активно Цвет обводки активного элемента Цвет заливки активного элемента Индекс активных элементов Цвет текста активного элемента Все файлы Все значки Анимация Кнопка выбора приложения Значки приложений Balsa Блочное устройство Brave Просмотреть файловую систему Просмотреть файловую систему для выбора нужной команды. Смотреть в Интернете _Комментарий: _Создать Файловый-менеджер Caja Символьное устройство Выбор предпочитаемого приложения Выбор файлового менеджера Выбор почтовой программы Выбор эмулятора терминала Выбор веб-браузера Выберите имя файла Chromium Claws Mail Очистить поле поиска Отступы колонок Отступы колонок К_оманда: Copyright (c) %s
        os-cillation e.K. Все права защищены.

Автор: Benedikt Meurer <benny@xfce.org>.

 Создать каталог Создать значок запуска Создать значок запуска <b>%s</b> Создать ссылку Создать новый desktop-файл в указанном каталоге Цвет обводки курсора Цвет заливки курсора Цвет текста курсора Настроить панель инструментов... Debian Sensible Browser Эмулятор терминала Debian Значки устройств Dillo Больше _не показывать это сообщение Для добавления перетащите элемент на панель. Для удаления перетащите элемент с панели в таблицу с элементами. Изменить каталог Изменить значок запуска Изменить ссылку Эмблемы Эмотиконы Разрешить поиск Encompass Эмулятор терминала Enlightened Веб-браузер Epiphany Evolution Исполняемые файлы FIFO Не удалось создать «%s». Не удалось запустить файловый менеджер по умолчанию Не удалось запустить почтовую программу по умолчанию Не удалось запустить эмулятор терминала по умолчанию Не удалось запустить веб-браузер по умолчанию Не удалось запустить предпочитаемое приложение для категории «%s». Не удалось открыть содержимое «%s»: %s Не удалось загрузить изображение «%s»: причина неизвестна, возможно файл изображения повреждён Не удалось открыть «%s». Не удалось открыть «%s» для записи Не удалось открыть URI «%s». Не удалось открыть дисплей Не удалось открыть файл «%s»: %s Не удалось разобрать содержимое «%s»: %s Не удалось прочитать файл «%s»: %s Не удалось сохранить «%s». Не удалось установить файловый менеджер по умолчанию Не удалось установить почтовую программу по умолчанию Не удалось установить эмулятор терминала по умолчанию Не удалось установить веб-браузер по умолчанию В файле «%s» отсутствует ключ типа (Type) Файловый менеджер Значки типов файлов Путь не указывает на файл или каталог Каталог Учитывать состояние Значок (GIcon) Терминал GNOME Веб-браузер Galeon Geary Google Chrome Однородный Как текст и значок каждого элемента будут располагаться относительно друг друга Icecat Icedove Iceweasel Значок Модель панели значков Модель представления значков Колонка значков Если вы не укажете параметр --launch, exo-open откроет все указанные адреса
URL с помощью установленных для них обработчиков. Если вы укажете параметр
--launch, то вы сможете выбрать приложения, которые должны быть запущены, и
передать дополнительные параметры этим приложениям (например, вы можете
передать эмулятору терминала команду, которая должна быть запущена в
командной строке). Файлы изображений Неверный тип вспомогательной программы «%s» Jumanji KMail Веб-браузер Konqueror Запустить вспомогательное предпочитаемое приложение выбранного ТИПа c необязательным ПАРАМЕТРом, где ТИП может принимать одно из следующих значений. Запуск файлов на рабочем столе невозможен, если %s собран без поддержки GIO-Unix. Режим отображения Текстовый браузер Links Значки места Текстовый браузер Lynx Почтовая программа Поле Колонка разметки Значки меню Midori Колонка модели, в которой осуществляется поиск при поиске в элементах Колонка модели, используемая для получения абсолютного пути файла отображаемого изображения Колонка модели, используемая для получения pixbuf значка Колонка модели, используемая для получения текста Колонка модели, используемая для получения текста при использовании разметки Pango Модель для панели значков Браузер Mozilla Mozilla Firefox Почта Mozilla Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Приложение не выбрано Не указана команда Файл не выбран Не указан файл или каталог Отсутствует вспомогательное приложение для «%s». Без значка Количество колонок Количество отображаемых колонок Открыть диалоговое окно выбора
предпочитаемых приложений Браузер Opera Параметры: Ориентация Файловый менеджер PCMan Файловый менеджер PCManFM-Qt Сценарии Perl Колонка pixbuf Пожалуйста, выберите предпочитаемый файловый
менеджер и щёлкните OK для продолжения. Пожалуйста, выберите предпочитаемую почтовую
программу и щёлкните OK для продолжения. Пожалуйста, выберите предпочитаемый эмулятор
терминала  и щёлкните OK для продолжения. Пожалуйста, выберите предпочитаемый
веб-браузер и щёлкните OK для продолжения. Об ошибках сообщайте на <%s>.
 Предпочитаемые приложения Предпочитаемые приложения (веб-браузер, почтовая программа и эмулятор терминала) Задать URL-адрес создаваемой ссылки Задать команду для создаваемого значка запуска Задать комментарий создаваемого desktop-файла Задать значок создаваемого desktop-файла Задать имя создаваемого desktop-файла Нажмите левую кнопку мыши для смены выбранного приложения. Предварительный просмотр Показать информацию о версии и выйти Сценарии Python QTerminal Запросить предпочитаемое вспомогательное приложение указанного ТИПа, где ТИП может принимать одно из следующих значений. QupZilla ROX-Filer RXVT Unicode Читать электронную почту Отображать по-разному в зависимости от состояния. Можно переупорядочить Команда перезапуска Файловый менеджер Rodent Отступы строк Отступы строк Сценарии Ruby Запускать в _терминале ID сокета Sakura Колонка поиска Выбрать _значок из: Выбор рабочего каталога Выбор приложения Выбор значка Выбор приложения Выберите приложения по умолчанию для различных задач Отметьте этот параметр, чтобы включить уведомление о запуске при запуске команды из файлового менеджера или меню. Учтите, что не все приложения поддерживают уведомление о запуске. Отметьте этот параметр для запуска команды в окне терминала. Режим выделения Разделитель Команда перезапуска сессии Сокет менеджера настроек Сценарии Shell Одиночный щелчок Таймаут одиночного щелчка Сокет Отступы от краёв значков Расстояние между ячейками Расстояние между колонками Отступы между строк Отступ Укажите приложение, которое вы хотите использовать
в качестве файлового менеджера по умолчанию для Xfce: Укажите приложение, которое вы хотите использовать
в качестве почтовой программы по умолчанию для Xfce: Укажите приложение, которое вы хотите использовать
в качестве эмулятора терминала по умолчанию для Xfce: Укажите приложение, которое вы хотите использовать
в качестве веб-браузера по умолчанию для Xfce: Значки статуса Стандартные значки Surf Sylpheed ТИП [ПАРАМЕТР] Эмулятор терминала Terminator Текстовая колонка Текст для в_ажных значков Текст для _всех значков Отображаемый значок (GIcon). Расстояние между соседними колонками Расстояние между соседними строками Промежуток времени в режиме одиночного щелчка, после которого элемент под курсором мыши будет выбран Файл «%s» не содержит данных Для параметров --launch и --query поддерживаются следующие ТИПы:

  WebBrowser       - предпочитаемый веб-браузер.
  MailReader       - предпочитаемая почтовая программа.
  FileManager      - предпочитаемый файловый менеджер.
  TerminalEmulator - предпочитаемый эмулятор терминала. Для команды --launch поддерживаются следующие ТИПы: Отображаемый значок. Режим отображения Модель для представления значков Ориентация панели значков Предпочитаемый файловый менеджер будет использован для просмотра содержимого каталогов. Предпочитаемая почтовая программа будет использована для составления писем при щелчке по адресу электронной почты. Предпочитаемый эмулятор терминала будет использован для запуска команд, которым требуется окружение командной строки. Предпочитаемый веб-браузер будет использован для открытия гиперссылок и просмотра справки. Режим выделения Размер отображаемого значка в пикселях. Ширина, используемая для каждого элемента Thunar _Стиль панели инструментов Выполните «%s --help» для получения информации об использовании. Тип создаваемого desktop-файла (Application или Link) Не удалось распознать схему URI в «%s». Прочие значки Неподдерживаемый тип desktop-файла «%s» Использование: %s [параметры] [файл]
 Использование: exo-open [URLs...] Использовать _уведомление о запуске Использовать другое приложение, не из этого списка. Использовать командную строку Разрешить пользователю интерактивный поиск в колонках Представление можно переупорядочить Vimprobable2 Текстовый браузер W3M Веб-браузер Должны ли все потомки быть одного размера Активируются ли объекты при одиночном щелчке Ширина каждого элемента Группа окон Главное окно в группе Рабочий _каталог: X-терминал Терминал Xfce Файловый менеджер Xfce [ФАЙЛ|КАТАЛОГ] _Добавить новую панель инструментов О_тменить _Закрыть _Рабочий стол по умолчанию _Справка З_начок: Только _значки _Интернет _Имя: О_к _Открыть _Другой... _Удалить панель инструментов _Сохранить _Искать значок: Только _текст URL-_адрес: _Утилиты aterm qtFM размер 