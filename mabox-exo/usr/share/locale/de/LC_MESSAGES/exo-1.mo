��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  a  1?  5   �@  -   �@  A   �@  .   9A    hA  $   �B  B   �B  0   �B  9   C  <   TC  �   �C  K   %D  J   qD  9   �D  :   �D  �   1E  �   �E  �   �F     wG     �G     �G      �G     �G     �G     	H     H     #H     /H     OH     aH     gH     �H  ?   �H  ?   �H     I     I  
   +I     6I     KI     fI  -   �I  .   �I  '   �I  0   J     <J     PJ  
   YJ     dJ     vJ     �J     �J  z   �J     K     -K      =K     ^K  /   sK  (   �K  )   �K  (   �K     L  *   ;L  !   fL     �L     �L  "   �L  i   �L     *M     AM     TM     lM  	   tM     ~M  	   �M     �M     �M  	   �M     �M     �M  $   �M  =   	N  =   GN  6   �N  ?   �N  K   �N  5   HO  g   ~O  %   �O  2   P  1   ?P  %   qP  .   �P  8   �P  ,   �P  '   ,Q  ?   TQ  ?   �Q  8   �Q  A   R  )   OR     yR     �R  6   �R     �R     �R     �R     �R     S     S     S     ,S  T   :S     �S     �S  	   �S     �S     �S     �S     �S  j  �S     PU     \U     {U     �U  	   �U  o   �U  r   V     vV     �V  
   �V     �V     �V     �V     �V     �V     �V  )   �V  S   W  7   lW  +   �W  L   �W     X     :X     JX     ZX     gX     {X     �X     �X     �X     �X     �X     �X  &   �X  #   Y     8Y     DY      RY  9   sY     �Y  	   �Y     �Y     �Y     �Y     Z     Z  U   Z  U   sZ  N   �Z  Y   [     r[     �[  H   �[  1   �[  *   "\     M\     j\     �\  D   �\     �\  $   �\     ]  	   ]  S   $]     x]  	   �]     �]     �]  &   �]     �]     �]     �]     ^     ^     !^     .^     F^     T^  
   [^     f^  !   w^     �^     �^     �^  <   �^  �   _  _   �_     F`  
   S`     ^`  8   ~`     �`     �`  '   �`     �`  C   a  =   Ha  6   �a  5   �a     �a  V   �a  W   Rb  P   �b  Y   �b     Uc     cc     sc     xc     �c     �c  
   �c  
   �c  $   �c      �c     �c  6   d  5   Ed  �   {d  %   e     .e  >   Of     �f     �f  !   �f      �f  U   �f  m   Sg  i   �g  _   +h     �h  A   �h  (   �h     i     i  F   #i  :   ji  8   �i     �i  $   �i      j     ?j  .   _j  L   �j     �j  C   �j     6k     Sk     `k     rk  1   �k  P   �k     l     !l     /l     Ol  
   dl     ol     }l     �l  %   �l  
   �l     �l     �l     �l     �l     �l  	   m     m     #m     'm     0m     <m  
   Vm     am     mm  	   �m  
   �m     �m     �m     �m                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-27 15:32+0000
Last-Translator: Vinzenz Vietzke <vinz@vinzv.de>
Language-Team: German (http://www.transifex.com/xfce/exo/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [Optionen] --build-list [[Name Datei] …]
         exo-open --launch TYP [PARAMETER …]   --build-list      Paare nach dem Schema (Name, Datei) einlesen
   --extern          Externe Symbole erstellen
   --launch TYP [PARAMETER …]         Die bevorzugte Anwendung für den TYP
                                      mit den optionalen PARAMETERn starten,
                                      wobei TYP einen der folgenden Werte
                                      annehmen kann.   --name=identifier C-Variablenname
 --output=Dateiname erstellte csource in angegeben Datei schreiben
   --static          Statische Symbole erstellen
   --strip-comments  Kommentare aus XML-Dateien entfernen
   --strip-content   Knoteninhalte aus XML-Dateien entfernen
   --working-directory VERZEICHNIS     Arbeitsverzeichnis für Anwendungen, die
                                      mit --launch gestartet werden.   -?, --help                          Diesen Hilfetext anzeigen und beenden   -V, --version                       Programmversion anzeigen und beenden   -V, --version     Programmversion anzeigen und beenden
   -h, --help        Diesen Hilfetext anzeigen und beenden
 WebBrowser - Der bevorzugte Internetbrowser.
MailReader - Das bevorzugte E-Mail-Programm.
FileManager - Die bevorzugte Dateiverwaltung.
TerminalEmulator - Das bevorzugte Terminal. %s (Xfce %s)

Urheberrecht (c) 2003-2006
        os-cillation e.K. Alle Rechte vorbehalten.

Entwickelt von Benedikt Meurer <benny@xfce.org>.

Kompiliert mit Gtk+-%d.%d.%d, momentan laufend unter Gtk+-%d.%d.%d.

Bitte Fehler an <%s> melden.
 %s kommt ABSOLUT OHNE GEWÄHR.
Sie können im Rahmen der GNU Lesser General Public License, die im Quellpaket von %s enthalten ist, Kopien von %s verbreiten.

 Aktionssymbole Aktiv Randfarbe des aktiven Elementes Füllfarbe des aktiven Elementes Index des aktiven Elementes Textfarbe des aktiven Elementes Alle Dateien Alle Symbole Animationen Knopf zur Auswahl der Anwendung Anwendungssymbole Balsa Blockorientiertes Gerät Brave Das Dateisystem durchsuchen, um einen eigenen Befehl zu wählen Das Dateisystem durchsuchen, um einen eigenen Befehl zu wählen Das Internet durchsuchen _Kommentar: _Erstellen Caja-Dateiverwaltung Zeichenorientiertes Gerät Bevorzugte Anwendung auswählen Benutzerdefinierte Dateiverwaltung auswählen Benutzerdefiniertes E-Mail-Programm auswählen Benutzerdefiniertes Terminal auswählen Benutzerdefinierten Internetnavigator auswählen Dateinamen eingeben Chromium Claws Mail Suchfeld löschen Spaltenabstand Spaltenabstand _Befehl: Urheberrecht (c) %s
        os-cillation e.K. Alle Rechte vorbehalten.

Entwickelt von Benedikt Meurer <benny@xfce.org>.

 Neuen Ordner anlegen Starter anlegen Starter für <b>%s</b> erstellen Verknüpfung anlegen Eine neue Datei im angegebenen Ordner erstellen Randfarbe des Elementes unter dem Zeiger Füllfarbe des Elementes unter dem Zeiger Textfarbe des Elementes unter dem Zeiger Werkzeugleiste anpassen … Debians voreingestellter Internetnavigator Debians voreingestelltes Terminal Gerätesymbole Dillo Diese Meldung _nicht mehr anzeigen Elemente lassen sich auf die oberen Werkzeugleisten, durch ablegen und ziehen, hinzufügen und entfernen. Verzeichnis bearbeiten Starter bearbeiten Verknüpfung bearbeiten Embleme Emoticons Suche aktivieren Encompass Enlightened Terminal-Emulator Epiphany Evolution Ausführbare Dateien FIFO »%s« konnte nicht erstellt werden. Die vorgegebene Dateiverwaltung konnte nicht gestartet werden Das vorgegebene E-Mail-Programm konnte nicht gestartet werden Das vorgegebene Terminal konnte nicht gestartet werden Der vorgegebene Internetnavigator konnte nicht gestartet werden Die bevorzugte Anwendung für den Typ »%s« konnte nicht gestartet werden. Der Inhalt aus »%s« konnte nicht geladen werden: %s Das Bild »%s« konnte aus unbekanntem Grund nicht geladen werden. Vermutlich ist die Datei beschädigt »%s« konnte nicht geöffnet werden. »%s« konnte nicht zum Schreiben geöffnet werden Die Adresse »%s« konnte nicht geöffnet werden. Anzeige konnte nicht geöffnet werden Datei »%s« konnte nicht geöffnet werden: %s Der Inhalt aus »%s« konnte nicht analysiert werden: %s Datei »%s« konnte nicht gelesen werden: %s »%s« konnte nicht gespeichert werden. Die vorgegebene Dateiverwaltung konnte nicht eingestellt werden Das vorgegebene E-Mail-Programm konnte nicht eingestellt werden Das vorgegebene Terminal konnte nicht eingestellt werden Der vorgegebene Internetnavigator konnte nicht eingestellt werden Die Datei »%s« hat keinen Typschlüssel Dateiverwaltung Dateitypensymbole Dateiziel ist ist keine normale Datei oder Verzeichnis Ordner Zustand folgen Symbol (GIcon) GNOME-Terminal Galeon Webbrowser Geary Google Chrome Gleichmäßig Legt fest, wie Text und Symbol jedes Elementes relativ zueinandner angeordnet werden IceCat Icedove Iceweasel Symbol Symbolleistenmodell Symbolansichtsmodell Symbolspalte Falls Sie die --launch-Option nicht angeben, wird exo-open alle angegebenen
Adressen mit Ihrem bevorzugten Programm öffnen. Ansonsten können Sie auswählen,
welche bevorzugte Anwendung Sie starten wollen, und können dieser dann
zusätzliche Parameter übergeben (z.B. kann man bei TerminalEmulator den
Befehl angeben, der im Terminal ausgeführt werden soll). Bilddateien Ungültiger Programmtyp »%s« Jumanji KMail Konqueror Das bevorzugte Programm für TYP mit dem optionalen PARAMETER starten, wobei TYP einer der folgenden Werte ist. Das Ausführen von Schreibtischdateien wird nicht unterstützt, wenn %s ohne GIO-Unix-Funktionen kompiliert wurde. Anordnungsstil Links (textbasiert) Ortsymbole Lynx (textbasiert) E-Mail-Programm Rand Markup-Spalte Menüsymbole Midori Spalte, die für die Suche verwenden soll Spalte, aus welcher der absolute Pfad einer Bilddatei zum Darstellen entnommen wird Spalte, aus welcher die Pixbuf-Symbole entnommen werden Spalte, aus welcher der Text entnommen wird Spalte, aus welcher der Text entnommen wird, falls Pango-Markup benutzt wird Modell für die Symbolleiste Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Keine Anwendung ausgewählt Kein Befehl angegeben Keine Datei ausgewählt Keine Datei bzw. kein Ordner angegeben Kein Helfer für »%s« festgelegt. Kein Symbol Spaltenanzahl Die Anzahl anzuzeigender Spalten Die bevorzugten Anwendungen über einen Dialog einstellen Opera Browser Optionen: Ausrichtung PCMan-Dateiverwaltung PCManFM-Qt Dateiverwaltung Perl-Skripte Pixbuf-Spalte Bitte Ihre bevorzugte Dateiverwaltung
auswählen und auf OK klicken, um fortzufahren. Bitte Ihr bevorzugtes E-Mail-Programm
auswählen und auf OK klicken, um fortzufahren. Bitte Ihr bevorzugtes Terminal
auswählen und auf OK klicken, um fortzufahren. Bitte Ihren bevorzugten Internetnavigator
auswählen und auf OK klicken, um fortzufahren. Bitte Fehler an <%s> melden.
 Bevorzugte Anwendungen Bevorzugte Anwendungen (Internetnavigator, E-Mail-Programm und Terminal) Voreingestellte Adresse (nur für Verknüpfungen) Voreingestellter Befehl (nur für Starter) Voreingestellter Kommentator Voreingestelltes Symbol Voreingestellter Name Die linke Maustaste drücken, um eine andere Anwendung auszuwählen. Vorschau Programmversion anzeigen und beenden Python-Skripte QTerminal Den vorgegebenen Helfer für TYP abfragen, wobei TYP einer der folgenden Werte ist. QupZilla ROX-Filer Unicode-RXVT E-Mails lesen Je nach Auswahlzustand anders rendern. Umsortierbar Befehl zum Neustart Rodent-Dateiverwaltung Zeilenabstand Zeilenabstand Ruby-Skripte Im _Terminal ausführen SOCKET-NUMMER Sakura Suchspalte Symbol_wahl aus: Ein Arbeitsverzeichnis auswählen Anwendung auswählen Ein Symbol auswählen Anwendung auswählen Standardanwendungen für unterschiedliche Dienste auswählen Diese Option auswählen, um eine Startbenachrichtigung zu erhalten, wenn die Anwendung aus der Dateiverwaltung oder dem Menü heraus gestartet wird. Nicht jede Anwendung unterstützt Startbenachrichtigungen. Diese Option auswählen, um den oben angegebenen Befehl in einem Terminal-Fenster auszuführen. Auswahlmodus Trennlinie Befehl zum Neustart der Sitzung Socket zum Einbinden des Dialogs in ein anderes Programm Shell-Skripte Einfacher Klick Auswahlverzögerung bei einfachem Klick Socket Zwischenraum, der an den Rändern der Symbolansicht eingefügt wird Zwischen den Zellen eines Elementes eingefügter Zwischenraum Der Abstand zwischen zwei aufeinanderfolgenden Spalten Der Abstand zwischen zwei aufeinanderfolgenden Zeilen Abstand Bitte die Anwendung angeben, die Sie als
vorgegebene Dateiverwaltung verwenden wollen: Bitte die Anwendung angeben, die Sie als
vorgegebenes E-Mail-Programm verwenden wollen: Bitte die Anwendung angeben, die Sie als
vorgegebenes Terminal verwenden wollen: Bitte die Anwendung angeben, die Sie als
vorgegebenen Internetnavigator verwenden wollen: Statussymbole Standardsymbole Surf Sylpheed TYP [PARAMETER] Terminal Terminator Textspalte Text für _wichtige Symbole anzeigen Text für _alle Symbole anzeigen Das zu rendernde GIcon. Der Abstand zwischen zwei aufeinanderfolgenden Spalten Der Abstand zwischen zwei aufeinanderfolgenden Zeilen Die Zeitspanne, nach der das Element unter dem Mauszeiger automatisch ausgewählt wird, wenn Aktivierung durch einfachen Klick aktiviert ist Die Datei »%s« enthält keine Daten Die folgenden TYPen werden von den --launch und --query Befehlen unterstützt:

 WebBrowser       - Der bevorzugte Webbrowser.
 MailReader        - Das bevorzugte E-Mail-Programm.
 FileManager        - Die bevorzugte Dateiverwaltung.
 TerminalEmulator - Der bevorzugte Terminal-Emulator. Die folgenden TYPen für --launch werden derzeit unterstützt: Das zu rendernde Symbol. Der Anordnungsstil Das Modell für die Symbolansicht Die Ausrichtung der Symbolleiste Der bevorzugte Dateimanager wird verwendet, um den Inhalt von Ordnern zu durchsuchen. Das bevorzugte E-Mail-Programm wird verwendet, um E-Mails zu erstellen, wenn Sie auf E-Mail-Adressen klicken. Der bevorzugte Terminalemulator wird verwendet, um Befehle auszuführen, die eine CLI-Umgebung erfordern. Der bevorzugte Webbrowser wird verwendet, um Hyperlinks zu öffnen und Hilfsinhalte anzuzeigen. Der Auswahlmodus Die Größe in Pixeln, in der das Symbol dargestellt werden soll. Die verwendete Breite für jedes Element Thunar Werkzeugleisten_stil »%s --help« eingeben, um Hilfe zum Aufruf des Programms zu erhalten. Typ der zu erstellenden Datei (Anwendung der Verknüpfung) Das Adressschema von »%s« konnte nicht erkannt werden. Nicht kategorisierte Symbole Nicht unterstützter Dateityp »%s« Aufruf: %s [Optionen] [Datei]
 Aufruf: exo-open [Adressen …] _Visuelle Rückmeldung über den Programmstart Eine eigene Anwendung verwenden, die in der obigen Liste nicht enthalten ist Befehlszeile verwenden Die Ansicht erlaubt dem Benutzer, Spalten interaktiv zu durchsuchen Die Ansicht ist umsortierbar Vimprobable2 W3M (textbasiert) Internetnavigator Legt fest, ob alle Kinder gleichgroß sein sollen Legt fest, ob Elemente bereits mit einem einfachen Klick aktiviert werden sollen Breite für jedes Element Fenstergruppe Repräsentant der Fenstergruppe Arbeits_verzeichnis: X-Terminal Xfce-Terminal Xfe-Dateiverwaltung [DATEI|ORDNER] Eine _neue Werkzeugleiste hinzufügen _Abbrechen S_chließen System_vorgabe _Hilfe S_ymbol: Nur _Symbole anzeigen _Internet _Name: _OK _Öffnen _Andere … Werkzeugleiste _entfernen _Speichern _Suchsymbol Nur _Text anzeigen A_dresse: _Werkzeuge aterm qtFM Größe 