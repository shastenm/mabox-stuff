��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  f  1?  5   �@  ,   �@  7   �@  +   3A  �   _A  +   8B  >   dB  +   �B  6   �B  <   C  �   CC  D   �C  A   D  4   XD  7   �D  �   �D  �   }E  �   QF     �F  	   G     G     )G     EG     XG     tG     �G     �G     �G     �G     �G  	   �G     �G     �G  -   �G     H     (H  
   6H     AH     VH     cH     |H     �H     �H     �H     �H     I     I     I     .I     5I     <I  q   JI     �I     �I     �I     �I  0    J     1J     MJ     iJ     �J     �J     �J     �J     �J     �J  K   �J     AK     NK     ^K     kK     rK     yK  	   �K     �K     �K  	   �K     �K     �K     �K  !   �K  !   L  !   AL  !   cL  2   �L  $   �L  H   �L     &M     ?M     YM     vM      �M  $   �M      �M     �M  !   	N  !   +N  !   MN  !   oN  %   �N     �N     �N  '   �N  	   O     O     O      O     -O     DO     JO  	   XO  3   bO     �O     �O  	   �O     �O     �O     �O  	   �O  K  �O     /Q  !   <Q     ^Q     fQ     lQ  i   �Q  F   �Q     7R     DR     ZR     gR     |R     �R  	   �R     �R     �R  !   �R  3   �R  &   S     8S  4   WS     �S     �S     �S     �S     �S     �S     �S     �S     �S     T     (T     8T     HT      _T  	   �T     �T     �T  (   �T     �T  	   �T     �T     �T     U     U  
   (U  @   3U  @   tU  @   �U  @   �U  "   7V     ZV  K   mV     �V  !   �V  $   �V  $   W  $   BW  0   gW     �W     �W     �W  	   �W  Q   �W     %X  	   .X     8X     EX  '   ^X     �X     �X     �X     �X     �X     �X     �X     �X     �X  	   �X     Y      Y     3Y     LY     YY  '   lY  �   �Y  9   Z     YZ  	   fZ     pZ     �Z     �Z     �Z     �Z  	   �Z  '   �Z  !   �Z     [     -[     I[  @   P[  @   �[  @   �[  @   \     T\     a\     n\     s\     |\     �\  
   �\  	   �\     �\     �\     �\     ]     ]  <   9]     v]  �   �]  *   �^     �^     �^     �^     �^  6   �^  K   4_  E   �_  E   �_     `  !   `     ;`     Q`     h`  &   |`  6   �`  %   �`      a  '   a     8a      Wa     xa  6   �a     �a  *   �a     b     b     !b     5b  !   Eb  *   gb     �b  	   �b     �b     �b     �b     �b     �b     �b     c  
   &c  
   1c     <c  
   Mc     Xc     fc     tc     �c  
   �c  
   �c     �c     �c  
   �c     �c     �c     �c     d     d     d     "d                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-26 05:47+0000
Last-Translator: 玉堂白鹤 <yjwork@qq.com>
Language-Team: Chinese (China) (http://www.transifex.com/xfce/exo/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
         %s [选项] --build-list [[名称文件]…]
         exo-open --launch 类型 [参数…]   --build-list          解析（名称，文件）对
   --extern              生成外部符号
   --launch 类型 [参数…]       用可选“参数”启动
                                      “类型”的首选应用程序，
                                      “类型”可以是下列值之一。   --name=标识符         C 宏/变量名
 --output=文件名 将生成的 csource 写入指定的文件
   --static              生成静态符号
   --strip-comments      移除 XML 文件中的注释
   --strip-content       移除 XML 文件中的节点内容
   --working-directory 目录             使用 --launch 选项时
                                      应用程序的默认工作目录。   -?, --help                          显示此帮助信息后退出   -V, --version                       显示版本信息后退出   -V, --version         显示版本信息后退出
   -h, --help            显示此帮助信息后退出
   WebBrowser       - 首选网络浏览器。
  MailReader       - 首选邮件阅读器。
  FileManager      - 首选文件管理器。
  TerminalEmulator - 首选终端模拟器。 %s(Xfce %s)

版权 (c) 2003-2006
        os-cillation e.K. 版权所有。

由 Benedikt Meurer <benny@xfce.org> 编写。

用 Gtk+-%d.%d.%d 构建，运行 Gtk+-%d.%d.%d。

请向 <%s> 报告程序缺陷。
 %1$s 绝对没有任何担保，
您可以在 %3$s 的源码包中找到 GNU 宽通用公共
许可协议并在此协议的约束下重新发布 %2$s 的副本。

 动作图标 当前的 当前项目的边框颜色 当前项目的填充颜色 当前项目索引 当前项目的文字颜色 所有文件 所有图标 动画 应用程序选择器按钮 应用程序图标 Balsa 块设备 Brave 浏览文件系统 浏览文件系统以选择自定义命令。 浏览网络 注释(_O)： 创建(_R) Caja 文件管理器 字符设备 选择首选应用程序 选择自定义文件管理器 选择自定义邮件阅读器 选择自定义终端模拟器 选择自定义网络浏览器 选择文件名 Chromium Claws 邮件 清除搜索框 列距 栏距 命令(_A)： 版权 (c) %s
          os-cillation e.K. 版权所有。

代码由 Benedikt Meurer <benny@xfce.org> 编写。

 创建目录 创建启动器 创建启动器 <b>%s</b> 创建链接 在给定目录中创建一个新的桌面文件 光标项目的边框颜色 光标项目的填充颜色 光标项目的文字颜色 定制工具栏… Debian 实用浏览器 Debian X 终端模拟器 设备图标 Dillo 不再显示此信息(_N) 将一个项目拖到工具栏中来添加，从工具栏拖走来移除。 编辑目录 编辑启动器 编辑链接 徽标 表情 启用搜索 Encompass Enlightened 终端模拟器 Epiphany 网络浏览器 Evolution 可执行文件 FIFO 无法创建 “%s”。 无法执行默认文件管理器 无法执行默认邮件阅读器 无法执行默认终端模拟器 无法执行默认网络浏览器 无法启动类别“%s”的首选应用程序。 无法从 “%s” 载入内容：%s 无法载入图片 “%s”：未知原因，图片文件可能已损坏 无法打开 “%s”。 无法打开 %s 以写入 无法打开 URI “%s”。 无法打开显示 无法打开文件 “%s”：%s 无法解析 “%s” 的内容：%s 无法读取文件 “%s”：%s 无法保存 “%s”。 无法设置默认文件管理器 无法设置默认邮件阅读器 无法设置默认终端模拟器 无法设置默认网络浏览器 文件 “%s” 没有类型关键字 文件管理器 文件类型图标 文件位置不是常规文件或目录 文件夹 跟随状态 图标 GNOME 终端 Galeon 网络浏览器 Geary 谷歌 Chrome 均匀的 如何排放各项目文字与图标的相对位置 Icecat Icedove Iceweasel 图标 图标栏模板 图标视图模板 图标栏 如果您没有指定 --launch 选项，exo-open 会用首选 URL 处理器
打开所有指定 URL链接。而如果您指定 --launch选项，您可以
选择要运行的首选应用程序，并可以传额外的参数给该应用程序
（例如对于终端模拟器，您可以传递一个应在终端中运行的命令行）。 图片文件 无效辅助程序类型 “%s” Jumanji KMail Konqueror 网络浏览器 用可选“参数”来启动“类型”的默认辅助程序，“类型”可以是以下值之一。 %s 未编译支持 GIO-Unix 功能时，不支持启动桌面文件。 布局模式 Links 文本浏览器 位置图标 Lynx 文本浏览器 邮件阅读器 边缘 标记栏 菜单图标 Midori 浏览器 以项目进行搜索的模板栏 获取所需渲染图像的绝对路径的模态列 用来获取图标 pixbuf 的模板栏 用来获取文字的模板栏 使用 Pango 标记时用来获取文字的模板栏 图标栏所用的模板 Mozilla 浏览器 Mozilla Firefox Mozilla 邮件 Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator 未选中应用程序 未指定命令 未选中文件 未指定文件/目录  "%s"的辅助程序未定义。 无图标 列数 显示的列数 打开首选应用程序
配置对话框 Opera 浏览器 选项： 方向 PCMan 文件管理器 PCManFM-Qt 文件管理器 Perl 脚本 Pixbuf 栏 现在请选择您的首选文件管理器
并按确认继续。 现在请选择您的首选邮件阅读器
并按确定继续。 现在请选择您的首选终端模拟器
并按确定继续。 现在请选择您的首选网络浏览器
并按确定继续。 请向 <%s> 报告程序缺陷。
 首选应用程序 首选应用程序（网络浏览器、邮件阅读器和终端模拟器） 创建链接时的预设 URL 创建启动器时的预设命令 创建桌面文件时的预设注释 创建桌面文件时的预设图标 创建桌面文件时的预设名称 按鼠标左键来更改选中的应用程序。 预览 显示版本信息后退出 Python 脚本 QTerminal 查询“类型”的默认辅助程序，“类型”可以是以下值之一。 QupZilla ROX-Filer RXVT Unicode 阅读您的电子邮件 根据不同的选择状态来显示。 可重排序 重启命令 Rodent 文件管理器 行距 行距 Ruby 脚本 在终端中运行(_T) 套接字 ID Sakura 搜索栏 从此选取图标(_I)： 选择工作目录 选择一个应用程序 选择图标 选择应用程序 为各种服务选择默认应用程序 选中此项后，从文件管理器或菜单中运行此命令时会启用启动通知。不是所有应用程序都支持启动通知。 选中此项后，此命令会在终端窗口中运行。 选择模式 分隔符 会话重启命令 设置管理器套接字 Shell 脚本 单击 单击时效 套接字 图标视图中插入到边缘的空间 插入项目单元之间的间距 插入两列之间的间距 插入两行之间的间距 间距 指定您要用作 Xfce 默认
文件管理器的应用程序： 指定您要用作 Xfce 默认
邮件阅读器的应用程序： 指定您要用作 Xfce 默认
终端模拟器的应用程序： 指定您要用作 Xfce 默认
网络浏览器的应用程序： 状态图标 原始图标 Surf Sylpheed 类型 [参数] 终端模拟器 Terminator 文字栏 重要图标显示文字(_M) 所有图标显示文字(_A) 要显示的图标。 相邻两栏之间的间距 相邻两行之间的间距 单击模式中，自动选中鼠标光标下项目的时间 文件 “%s” 未包含数据 --launch 和 --query 命令支持以下类型：

  WebBrowser       - 首选网络浏览器。
  MailReader       - 首选邮件阅读器。
  FileManager      - 首选文件管理器。
  TerminalEmulator - 首选终端模拟器。 --launch 命令支持以下“类型”： 要显示的图标。 布局模式 图标视图所用的模板 图标栏的方向 首选文件管理器将用于浏览文件夹内容。 首选邮件阅读器将用于您点击电子邮件地址时撰写邮件。 首选终端模拟器将用于运行需要命令行环境的命令。 首选网络浏览器将用于打开超链接及显示帮助内容。 选择模式 所显示图标的像素大小。 各项目所用宽度 Thunar 文件管理器 工具栏样式(_S) 输入 ‘%s --help’ 了解用法。 创建的桌面文件类型（应用程序或链接） 无法检测“%s”的 URI-scheme。 未归类图标 不支持的桌面文件类型 “%s” 用法：%s [选项] [文件]
 用法：exo-open [URL链接…] 使用启动通知(_S) 使用不在以上列表中的自定义应用程序。 使用命令行 允许用户通过栏交互搜索的视图 视图可重排序 Vimprobable2 W3M 文本浏览器 网络浏览器 子部件是否应大小皆一致 是否可用单击激活视图中的项目 各项目宽度 窗口组 窗口组之首 工作目录(_D)： X 终端 Xfce 终端 Xfe 文件管理器 [文件|文件夹] 添加新的工具栏(_A) 取消(_C) 关闭(_C) 桌面默认(_D) 帮助(_H) 图标(_I)： 仅图标(_I) 互联网(_I) 名称(_N)： 确定(_O) 打开(_O) 其它(_O)… 移除工具栏(_R) 保存(_S) 搜索图标(_S)： 仅文字(_T) URL链接(_U)： 实用程序(_U) aterm qtFM 大小 