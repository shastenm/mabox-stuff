��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  [  1?  1   �@  ,   �@  2   �@  0   A  �   PA  *   8B  G   cB  -   �B  3   �B  4   C  �   BC  H   �C  F   !D  5   hD  7   �D  �   �D  �   �E  �   ~F  	   +G     5G     ;G     XG     mG     G     �G  
   �G     �G     �G     �G     �G     �G     �G     �G  1   H  
   =H  
   HH     SH     YH     mH     �H     �H     �H     �H     �H      I     I     I     'I     AI     SI  
   eI  �   pI     �I      J     J  	   &J  4   0J     eJ     }J     �J     �J     �J     �J     �J     K     K  j   'K     �K     �K     �K     �K  
   �K     �K  	   �K     �K     L  	   %L     /L     GL     LL  )   `L  (   �L  ,   �L  '   �L  8   M     AM  V   `M     �M     �M     �M     N     N      7N     XN     vN  %   �N  $   �N  (   �N  #    O  &   $O     KO     ZO  1   kO     �O     �O     �O     �O     �O     �O     �O     �O  V   �O     MP     TP  	   \P     fP     kP     }P  
   �P  �  �P     !R     /R     LR     TR     ZR  i   qR  S   �R     /S     ?S     SS     _S     rS     �S     �S  	   �S     �S  2   �S  X   �S  ;   1T  4   mT  M   �T     �T     U     U     )U     7U     KU     PU     WU     `U     sU     �U     �U      �U  .   �U     V      V     -V  )   KV     uV     �V  	   �V     �V     �V  
   �V     �V  R   �V  Q   ,W  U   ~W  P   �W     %X     EX  E   WX      �X  '   �X  ,   �X  (   Y  (   <Y  <   eY  	   �Y      �Y     �Y  	   �Y  S   �Y     8Z  	   AZ     KZ     `Z  *   pZ     �Z     �Z     �Z     �Z     �Z  
    [     [     ![     *[  
   1[     <[     N[     d[  
   s[     ~[  -   �[  �   �[  >   e\     �\     �\     �\     �\     �\     �\     ]     ]  4   ]  4   S]  >   �]  >   �]     ^  T   ^  S   a^  W   �^  R   _     `_  	   l_     v_     {_     �_     �_  
   �_  
   �_     �_     �_     �_     `      `  e   ?`     �`  �   �`  .   �a     �a     �a     b     "b  K   =b  g   �b  x   �b  ]   jc     �c  &   �c  $   �c     !d     (d  #   :d  5   ^d  )   �d     �d  &   �d     �d     e     :e  ?   Ze     �e  J   �e     �e     f     'f     9f  A   Ff  <   �f     �f     �f     �f     g  
   g     #g     1g     Dg     Kg     dg     kg     rg     �g     �g     �g  	   �g     �g     �g     �g     �g     �g     �g  
   �g     �g     �g  	   �g     h     h     h                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-27 08:53+0000
Last-Translator: Ardjuna <Asyura.x@gmail.com>
Language-Team: Indonesian (http://www.transifex.com/xfce/exo/language/id/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id
Plural-Forms: nplurals=1; plural=0;
        %s [opsi] --build-list [[nama berkas]...]
        exo-open --launch TIPE [PARAMETER...]   --build-list      Parse (nama, berkas) pasangan
   --extern          Bangkitkan simbol eksternal
   --launch TIPE [PARAMETER...]       Jalankan aplikasi kesukaan dari
                                       TIPE dengan opsi tambahan PARAMETERs,dimana
                                       TIPE merupakan salah satu nilai berikut.   --name=identifier nama makro/variabel C
   --output=filename Menulis csource yang dihasilkan ke berkas tertentu
   --static          Bangkitkan simbol statik
   --strip-comments  Hapus komentar dari berkas XML
    --strip-content   Hapus isi node dari berkas XML
   --working-directory DIREKTORI       Direktori kerja standar untuk aplikasi
                                       ketika menggunakan opsi --launch.   -?, --help                          Cetak pesan bantuan ini dan keluar   -V, --version                       Cetak informasi versi dan keluar   -V, --version     Cetak informasi versi dan keluar
   -h, --help        Cetak pesan bantuan ini dan keluar
   WebBrowser       - Peramban Web kesukaan.
  MailReader       - Pembaca Surat kesukaan.
  FileManager      - Manajer Berkas kesukaan.
  TerminalEmulator - Emulator Terminal kesukaan. %s (Xfce %s)

Hak Cipta (c) 2003-2006
        os-cillation e.K. Hak cipta dilindungi undang-undang.

Ditulis oleh Benedikt Meurer <benny@xfce.org>.

Dibangun dengan Gtk+-%d.%d.%d, menjalankan Gtk+-%d.%d.%d.

Silakan laporkan kutu ke <%s>.
 %s hadir dengan SAMA SEKALI TANPA GARANSI,
Anda dapat mendistribusikan salinan dari %s di bawah
GNU Lesser General Public License yang dapat ditemukan di
paket sumber %s.

 Ikon Aksi Aktif Warna garis batas item aktif Warna isi item aktif Indeks item aktif Warna teks item aktif Semua Berkas Semua Ikon Animasi Tombol Pemilih Aplikasi Ikon Aplikasi Balsa Perangkat Blok Brave Ramban sistem berkas Ramban sistem berkas untuk memilih perintah suai. Ramban web K_omentar: B_uat Manajer Berkas Caja Perangkat Karakter Pilih Aplikasi Kesukaan Pilih Manajer Berkas suai Pilih Pembaca Surat suai Pilih Emulator Terminal suai Pilih Peramban Web suai Pilih nama berkas Chromium Surat Claws Bersihkan ruang pencarian Jarak Antar Kolom Jarak antar kolom Perint_ah: Hak Cipta (c) %s
        os-cillation e.K. Hak cipta dilindungi undang-undang.

Ditulis oleh Benedikt Meurer <benny@xfce.org>.

 Buat Direktori Buat Peluncur Buat Peluncur <b>%s</b> Buat Taut Buat berkas destop baru di direktori yang ditentukan Warna batas item kursor Warna isi item kursor Warna teks item kursor Suaikan Batang Alat... Peramban Sensitif Debian Emulator Terminal X Debian Ikon Divais Dillo Janga_n tampilkan info ini lagi Tarik item ke batang alat di atas untuk menambahkannya, dari batang alat di tabel item untuk menghapusnya. Sunting Direktori Sunting Peluncur Sunting Taut Emblem Ikon Emosi Aktifkan Pencarian Encompass Emulator Terminal Enlightened Peramban Web Epiphany Evolution Berkas Dapat Dieksekusi FIFO Gagal membuat "%s". Gagal mengeksekusi Manajer Berkas standar Gagal mengeksekusi Pembaca Surat standar Gagal mengeksekusi Emulator Terminal standar Gagal mengeksekusi Peramban Web standar Gagal menjalankan aplikasi kesukaan untuk kategori "%s". Gagal memuat isi dari "%s": %s Gagal memuat gambar "%s": Alasan tidak diketahui, kemungkinan berkas gambar yang korup Gagal membuka "%s". Gagal membuka %s untuk menulis Gagal membuka URI "%s". Gagal membuka tampilan Gagal membuka berkas "%s": %s Gagal mengurai isi dari "%s": %s Gagal membaca berkas "%s": %s Gagal menyimpan "%s". Gagal mengatur Manajer Berkas standar Gagal mengatur Pembaca Surat standar Gagal mengatur Emulator Terminal standar Gagal mengatur Peramban Web standar Berkas "%s" tidak mempunyai kunci tipe Manajer Berkas Ikon Tipe Berkas Lokasi berkas bukan berkas reguler atau direktori Folder Ikuti status GIcon Terminal GNOME Peramban Web Galeon Geary Google Chrome Homogen Cara teks dan ikon untuk setiap item diletakkan secara relatif terhadap satu sama lain Icecat Icedove Iceweasel Ikon Model Batang Ikon Model Tampilan Ikon Kolom ikon Jika anda tidak menentukan opsi --launch, exo-open akan membuka semua
URL yang ditentukan dengan penanganan URL kesukaan mereka. Atau, jika
anda menentukan opsi --launch, anda dapat memilih aplikasi kesukaan yang
ingin anda jalankan, dan meneruskan parameter tambahan ke aplikasi (misalnya
untuk TerminalEmulator anda dapat meneruskan baris perintah yang seharusnya
berjalan di terminal). Berkas Gambar Tipe pembantu "%s" tidak sah Jumanji KMail Peramban Web Konqueror Jalankan pembantu standar dari TIPE dengan PARAMETER opsional, TIPE adalah salah satu dari nilai berikut. Menjalankan berkas desktop tidak didukung jika %s dikompilasi tanpa fitur GIO-Unix. Mode tata letak Peramban Teks Links Ikon Lokasi Peramban Teks Lynx Pembaca Surat Marjin Kolom markah Ikon Menu Midori Kolom model tempat mencari saat mencari dalam item Kolom model yang digunakan untuk mengambil jalur absolut dari berkas gambar untuk render Kolom model yang digunakan untuk mengambil pixbuf ikon dari Kolom model yang digunakan untuk mengambil teks dari Kolom model yang digunakan untuk mengambil teks jika menggunakan markah Pango Model untuk batang ikon Peramban Mozilla Mozilla Firefox Surat Mozilla Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Tak ada aplikasi terpilih Tidak ada perintah ditentukan Tak ada berkas terpilih Tak ada berkas/folder ditentukan Tidak ada pembantu yang ditentukan untuk "%s". Tak ada ikon Jumlah kolom Jumlah kolom yang ditampilkan Buka dialog konfigurasi
Aplikasi Kesukaan Peramban Opera Opsi: Orientasi Manajer Berkas PCMan PCManFM Qt File Manager Skrip Perl Kolom pixbuf Silakan pilih Manajer Berkas kesukaan anda
sekarang dan klik OK untuk melanjutkan. Silakan pilih Pembaca Surat kesukaan anda
sekarang dan klik OK untuk melanjutkan. Silakan pilih Emulator Terminal kesukaan anda
sekarang dan klik OK untuk melanjutkan. Silakan pilih Peramban Web kesukaan anda
sekarang dan klik OK untuk melanjutkan. Silakan laporkan kutu ke <%s>.
 Aplikasi Kesukaan Aplikasi Kesukaan (Peramban Web, Pembaca Surat dan Emulator Terminal) Preset URL ketika membuat tautan Preset perintah ketika membuat peluncur Preset komentar ketika membuat berkas destop Preset ikon ketika membuat berkas destop Preset nama ketika membuat berkas destop Tekan tombol tetikus kiri untuk mengganti aplikasi terpilih. Pratayang Cetak informasi versi dan keluar Skrip Python QTerminal Kueri pembantu bawaan dari TYPE, di mana TYPE adalah salah satu dari nilai berikut. QupZilla ROX-Filer Sandi Universal RXVT Baca email anda Render berbeda berdasarkan status pilihan. Dapat diurut kembali Perintah start ulang  Manajer Berkas Rodent Jarak Antar Baris Jarak antar baris Skrip Ruby Jalankan di _terminal ID SOKET Sakura Kolom Cari Pilih _ikon dari: Pilih direktori aktif Pilih Aplikasi Pilih ikon Pilih aplikasi Pilih aplikasi standar untuk berbagai layanan Pilih opsi ini untuk mengaktifkan pemberitahuan hidupkan ketika perintah dijalankan dari manajer berkas atau menu. Tidak semua aplikasi mendukung pemberitahuan hidupkan. Pilih opsi ini untuk menjalankan perintah di jendela terminal. Mode seleksi Pemisah Perintah start ulang sesi Soket manajer pengaturan Skrip Shell Klik Tunggal Waktu Klik Tunggal Soket Ruangan yang akan disisipkan pada sisi tampilan ikon Ruangan yang akan disisipkan di antara sel pada item Ruangan yang akan disisipkan di antara baris dalam kotak-kotak Ruangan yang akan disisipkan di antara baris dalam kotak-kotak Spasi Tentukan aplikasi yang ingin anda gunakan
sebagai Manajer Berkas standar untuk Xfce: Tentukan aplikasi yang ingin anda gunakan
sebagai Pembaca Surat standar untuk Xfce: Tentukan aplikasi yang ingin anda gunakan
sebagai Emulator Terminal standar untuk Xfce: Tentukan aplikasi yang ingin anda gunakan
sebagai Peramban Web standar untuk Xfce: Ikon Status Stok Ikon Surf Sylpheed TIPE [PARAMETER] Emulator Terminal Terminator Kolom teks Teks untuk I_kon Penting Teks untuk _Semua Ikon GIcon untuk render. Besar ruangan antara dua kolom Besar ruangan antara dua baris Jumlah waktu yang dibutuhkan kursor tetikus untuk secara otomatis memilih item pada mode klik tunggal Berkas "%s" tidak berisi data TYPE berikut didukung untuk perintah --launch dan --query:

  WebBrowser       - Peramban Web pilihan.
  MailReader       - Pembaca Surel pilihan.
  FileManager      - Pengelola Berkas pilihan.
  TerminalEmulator - Emulator Terminal pilihan . TIPE berikut didukung untuk perintah --launch: Ikon untuk render. Mode tata letak Model untuk tampilan ikon Orientasi dari batang ikon Manajer File yang dipilih akan digunakan untuk menjelajah isi suatu folder. Pembaca Surel yang dipilih akan digunakan untuk menyusun surat ketika anda mengklik suatu alamat surel. Emulator Terminal yang dipilih akan dipakai untuk menjalankan perintah yang membutuhkan lingkungan perintah baris (CLI). Peramban Web yang dipilih akan digunakan untuk membuka tautan dan menampilkan konten bantuan. Mode seleksi Ukuran ikon untuk render dalam piksel. Lebar yang digunakan untuk tiap item Thunar Gaya _Batang Alat Ketik '%s --help' untuk penggunaan. Tipe berkas destop untuk membuat (Aplikasi atau Taut) Tak dapat mendeteksi skema URI dari "%s". Ikon Tak Terkategorikan Tipe berkas destop "%s" tidak didukung Penggunaan: %s [opsi] [berkas]
 Penggunaan: exo-open [URL..] Gunakan _pemberitahuan hidupkan Pilih aplikasi suai yang tidak disertakan pada senarai di atas. Gunakan baris perintah Tampilan memungkinkan pengguna untuk mencari dalam kolom secara interaktif Tampilan dapat diurut kembali Vimprobable2 Peramban Teks W3M Peramban Web Menentukan apakah semua anak memiliki ukuran yang sama atau tidak Apakah item di tampilan dapat diaktivasi dengan klik tunggal Lebar untuk tiap item Kelompok jendela Pemimpin kelompok jendela _Direktori Aktif: Terminal X Terminal Xfce Manajer Berkas Xfe FOLDER _Tambah batang alat baru _Batal _Tutup _Destop Standar _Bantuan _Ikon: _Hanya ikon _Internet _Nama: _OK _Buka _Lainnya... Hapus Batang _Alat _Simpan Ikon cari: Hanya _teks _URL: _Utilitas aterm qtFM ukuran 