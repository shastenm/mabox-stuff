��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  o  1?  A   �@  =   �@  =   !A  O   _A  G  �A  ;   �B  h   3C  K   �C  Y   �C  R   BD  �   �D  g   aE  `   �E  O   *F  V   zF  #  �F  ;  �G  �   1I  )   )J     SJ  /   dJ  3   �J  .   �J  3   �J     +K  !   GK     iK  :   �K  -   �K     �K     �K     L  $   L  c   8L     �L     �L     �L  *   �L     M  <   (M  O   eM  I   �M  M   �M  A   MN  "   �N     �N  
   �N  &   �N     �N     O     -O  �   >O     �O  !   �O  +   	P     5P  Z   SP  1   �P  5   �P  5   Q      LQ     mQ  ,   �Q  +   �Q     �Q  B   �Q  �   'R     �R  #   �R     S     %S     :S      OS  	   pS  /   zS      �S  	   �S  %   �S     �S      T  c   T  ]   �T  a   �T  U   BU  p   �U  I   	V  �   SV  *   �V  9   W  3   BW  7   vW  2   �W  <   �W  2   X     QX  a   qX  [   �X  _   /Y  S   �Y  .   �Y  %   Z  2   8Z  J   kZ     �Z     �Z     �Z     �Z     �Z     [     [     )[  S   :[     �[     �[  	   �[     �[      �[  %   �[  !   �[  %  \     E^  %   a^     �^     �^  !   �^  �   �^  �   ~_     `  '   8`  -   ``  &   �`     �`     �`     �`  !   �`     a  R   a  �   pa  *   b  &   =b  V   db      �b     �b     �b     c     c     &c     +c     2c     ;c  %   Nc  )   tc  0   �c  7   �c  4   d     <d     Vd  2   pd  P   �d     �d     e     e  /   #e  0   Se     �e     �e  v   �e  t   'f  |   �f  p   g  @   �g  )   �g  �   �g  O   �h  [   �h  _   /i  Y   �i  Q   �i  ~   ;j     �j  :   �j     k  	   ,k  �   6k     �k  	   �k     �k     �k  L   	l     Vl  &   vl  ,   �l  $   �l  *   �l     m  $   2m  	   Wm     am     hm  (   �m  *   �m  %   �m  #   �m  %   n  c   Dn    �n  e   �o     -p     Ip  7   \p  6   �p     �p     �p  $   �p  
   !q  G   ,q  F   tq  A   �q  3   �q     1r  �   @r  �   �r  �   �s  �   !t  (   �t  %   �t     u     u     u  #   -u  
   Qu     \u  8   tu  6   �u     �u  :   �u  6   9v  �   pv  +   w  �  =w  ]   �x  "   3y     Vy  ,   vy  8   �y  �   �y  �   pz  �   9{  �   |  !   �|  L   �|  ?   &}     f}     m}  :   �}  ]   �}  H   ~  %   h~  4   �~  7   �~  .   �~  5   *  Z   `  .   �  �   �  ,   s�     ��  %   ��     Ӏ  J   �  f   6�     ��     ��  0   ԁ     �     �     4�     B�     S�  4   g�     ��  	   ��  )   ��     �     �      �     �     /�     8�     >�     F�     U�     r�     ��     ��     ��     ��     σ     Ճ     ڃ                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-14 11:22+0000
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh (http://www.transifex.com/xfce/exo/language/kk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kk
Plural-Forms: nplurals=2; plural=(n!=1);
        %s [опциялар] --build-list [[аты файл]...]
        exo-open --launch ТҮРІ [ПАРАМЕТРЛЕР...]   --build-list      (аты, файл) жұбын өндеу
   --extern          Сыртқы таңбаларды генерациялау
   --launch ТҮР [ПАРАМЕТРлер...]    ТҮРдің таңдамалы қолданбасын
                                      міндетті емес ПАРАМЕТРмен жөнелту, бұл жердегі
                                      ТҮР - төмендегі мәндердің біреуі.   --name=identifier C макрос/айнымалы аты
   --output=filename Генерацияланған csource көрсетілген файлға жазу
   --static          Ішкі таңбаларды генерациялау
   --strip-comments  XML файлдарынан түсіндірмелерді өшіру
   --strip-content   XML файлдарынан node құрамасын өшіру
   --working-directory БУМА         --launch опциясымен қолданылатын қолданбалар
                                      үшін негізгі жұмыс бумасы.   -?, --help                          Осы көмек ақпаратын шығару мен шығу   -V, --version                       Нұсқа ақпаратын шығару мен шығу   -V, --version     Нұсқа ақпаратын шығару мен шығу
   -h, --help        Осы көмек ақпаратын шығару мен шығу
   WebBrowser       - Таңдамалы веб браузер.
  MailReader       - Таңдамалы пошта қолданбасы.
  FileManager      - Таңдамалы файлдар басқарушысы.
  TerminalEmulator - Таңдамалы терминал эмуляторы. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Барлық құқықтары қорғалған.

Benedikt Meurer <benny@xfce.org> жазған.

Gtk+-%d.%d.%d жиналып, Gtk+-%d.%d.%d көмегімен қосылған.

Ақаулықтар жөнінде хабарласыңыз: <%s>.
 %s ЕШҚАНДАЙ КЕПІЛДЕМЕСІЗ таратылады,
Сіз %s көшірмелерін %s  бастапқы код дестесінен табылатын
GNU Lesser General Public License аясында тарата аласыз.

 Әрекеттер таңбашалары Белсенді Белсенді элемент шет түсі Белсенді элемент құйма түсі Белсенді элемент индексі Белсенді элемент мәтін түсі Барлық файлдар Барлық таңбашалар Анимациялар Қолданбаларды таңдау батырмасы Қолданбалар таңбашалары Balsa Блоктық құрылғы Brave Файлдық жүйені шолу Таңдауыңызша команда орнату үшін файлдық жүйені шолу. Интернетті шолу А_нықтамасы: Ж_асау Caja файлдар басқарушысы Символық құрылғы Таңдамалы қолданбаны көрсетіңіз Таңдауыңызша файлдар басқарушысын көрсету Таңдауыңызша пошта қолданбасын көрсету Таңдауыңызша терминал эмуляторын көрсету Таңдауыңызша веб браузерді көрсету Файл атын таңдаңыз Chromium Claws Mail Іздеу өрісін тазарту Бағандар аралығы Бағандар аралығы Ко_манда: Copyright (c) %s
        os-cillation e.K. Барлық құқықтары қорғалған.

Benedikt Meurer <benny@xfce.org> жазған.

 Буманы жасау Жөнелткішті жасау <b>%s</b> жөнелткішін жасау Сілтемені жасау Көрсетілген бумада жаңа жұмыс үстел файлын жасау Курсордың элемент шет түсі Курсордың элемент құйма түсі Курсордың элемент мәтін түсі Панельді баптау... Debian Sensible Browser Debian X терминал эмуляторы Құрылғылар таңбашалары Dillo Бұл хабарламаны ке_лесіде көрсетпеу Элементті қосу үшін оны жоғарыдағы панельге тартып апарыңыз, өшіру үшін кері тартыңыз. Буманы түзету Жөнелткішті түзету Сілтемені түзету Эмблемалар Смайликтер Іздеуді іске қосу Encompass Enlightened терминал эмуляторы Epiphany веб браузері Evolution Орындалатын файлдар FIFO "%s" жасау сәтсіз. Негізгі файлдар басқарушысын жөнелту сәтсіз аяқталды Негізгі пошта қолданбасын жөнелту сәтсіз аяқталды Негізгі терминал эмуляторын жөнелту сәтсіз аяқталды Негізгі веб браузерді жөнелту сәтсіз аяқталды "%s" санаты үшін таңдамалы қолданбаны жөнелту сәтсіз аяқталды. "%s" ішінен құраманы жүктеу мүмкін емес: %s "%s" суретін жүктеу мүмкін емес: Себебі белгісіз, мүмкін, ол файл зақымдалған "%s" ашу сәтсіз аяқталды. Жазу үшін %s ашу сәтсіз аяқталды "%s" URI-ын ашу сәтсіз аяқталды. Дисплейді ашу сәтсіз аяқталды "%s" файлын ашу мүмкін емес: %s "%s" құрамасын өндеу мүмкін емес: %s "%s" файлын оқу мүмкін емес: %s "%s" сақтау сәтсіз. Негізгі файлдар басқарушысын орнату сәтсіз аяқталды Негізгі пошта қолданбасын орнату сәтсіз аяқталды Негізгі терминал эмуляторын орнату сәтсіз аяқталды Негізгі веб браузерді орнату сәтсіз аяқталды "%s" файлында түр кілті жоқ Файлдар басқарушысы Файл түрлердің таңбашалары Файл орналасуы кәдімгі файл не бума емес Бума Күйін сақтау GIcon GNOME терминалы Galeon веб браузері Geary Google Chrome Біркелкі Мәтін мен таңбаша өзара орналасуын көрсетеді Icecat Icedove Iceweasel Таңбаша Таңба панель түрі Таңбашалар көрінісі Таңбашалар бағаны Егер сіз --launch опциясын көрсетпесеңіз, exo-open көрсетілген URL-ды
сәйкес URL өндеуіштермен ашады. Немесе, --launch опциясы көрсетілсе,
жөнелтілетін таңдамалы қолданбаны таңдай аласыз, және қосымша
параметрлерді көрсете аласыз (мыс. терминал эмуляторы үшін терминалда
орындалатын командалық жол болуы мүмкін). Сурет файлдары Қате көмекші түрі "%s" Jumanji KMail Konqueror веб браузері ТҮР үшін негізгі көмекшіні жөнелту, міндетті емес ПАРАМЕТРмен, бұл жердегі ТҮР - төмендегі мәндердің біреуі. %s нұсқасы GIO-Unix мүмкіндіктерінсіз жиналған кезде desktop файлдарын жөнелтуге болмайды. Парақ бағдарлауы Links мәтіндік браузері Орналасудың таңбашалары Lynx мәтіндік браузері Пошта қолданбасы Шет Белгі бағаны Мәзір таңбашалары Midori Элемент іздегенді қолданылатын модель баған Өңдеу үшін сурет файлының абсолютті орналасу жолын алу үшін қолданылатын модель бағаны Таңбаша алынатын баған Мәтін алынатын баған Pango markup қолданып мәтінді алу үшін модель бағаны Таңба панель түрі Mozilla браузері Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Қолданба таңдалмады Команда көрсетілмеген Ешбір файл ерекшеленбеген Ешбір файл/бума көрсетілмеген "%s" үшін көмекші қолжетерсіз. Таңбашасы жоқ Бағандар саны Көрсетілетін бағандар саны Таңдамалы қолданбалар сұхбат
терезесін ашу Opera Browser Опциялар: Бағдары PCMan файлдарды басқарушысы PCManFM-Qt файлдар басқарушысы Perl скриптері Pixbuf бағаны Таңдамалы файлдар басқарушысын көрсетіп,
ОК батырмасын басыңыз. Таңдамалы пошта қолданбаңызды көрсетіп,
ОК батырмасын басыңыз. Таңдамалы терминал эмуляторыңызды көрсетіп,
ОК батырмасын басыңыз. Таңдамалы веб браузеріңізді көрсетіп,
ОК батырмасын басыңыз. Ақаулықтар жөнінде хабарлаңыз: <%s>.
 Таңдамалы қолданбалар Таңдамалы қолданбалар (веб браузер, пошта қолданбасы мен терминал эмуляторы) Сілтемені жасауда алдын-ала орнатылған URL-ы Жөнелткішті жасауда алдын-ала орнатылған команда Desktop файлын жасауда алдын-ала орнатылған түсіндірме Desktop файлын жасауда алдын-ала орнатылған таңбаша Desktop файлын жасауда алдын-ала орнатылған аты Ерекшеленген қолданбаны өзгерту үшін тышқанның сол жақпен шертіңіз. Алдын-ала қарау Нұсқа ақпаратын шығару мен шығу Python скриптері QTerminal ТҮРІ түріндегі негізгі көмекшісін сұрау, бұл жерде ТҮРІ - келесі мәндердің біреуі. QupZilla ROX-Filer RXVT Unicode Поштаңызды оқу Ерекшеленгеннің күйіне байланысты өндеу. Қайта реттелетін Қайта қосу командасы Rodent файлдар басқарушысы Жол арасындағы орын Жолдар арасындағы орын Ruby скриптері Т_ерминалда орындау SOCKET ID Sakura Іздеу бағаны Таңба_шаны қайдан алу: Жұмыс бумасын таңдаңыз Қолданбаны таңдаңыз Таңбашаны таңдаңыз Қолданбаны таңдаңыз Кейбір қызметтер үшін негізгі қолданбаларды таңдаңыз Бұл опцияны таңдасаңыз, команда файлдар басқарушы не мәзірден орындалған кезде хабарлама көрсетіледі. Қосылу хабарламасын әрбір қолданба қолдай бермейді. Бұл опцияны таңдасаңыз, команда терминалда орындалады. Ерекшелеу түрі Ажыратқыш Сессияны қайта қосу командасы Баптаулар басқарушысы сокеті Shell скриптері Бірлік шерту Бірлік шерту уақыты Сокет Таңбаша көрінісінде бұрыштардағы орын Элемент ұяшықтары арасындағы бос орын Тор бағандарының арасындағы аралық Тор жолдары арасындағы орын Аралығы Xfce үшін негізгі файлдар басқарушысы ретінде қолданғыңыз
келетін қолданбаны көрсетіңіз: Xfce үшін негізгі пошта қолданбасы ретінде қолданғыңыз
келетін қолданбаны көрсетіңіз: Xfce үшін негізгі терминал эмуляторы ретінде қолданғыңыз
келетін қолданбаны көрсетіңіз: Xfce үшін негізгі веб браузер ретінде қолданғыңыз
келетін қолданбаны көрсетіңіз: Қалып-күй таңбашалары Әдеттегі таңбашалар Surf Sylpheed ТҮРІ [ПАРАМЕТР] Терминал эмуляторы Terminator Мәтін бағаны Маң_ызды таңбашалар үшін мәтін Ба_рлық таңбашалар үшін мәтін Өнделетін GIcon. Екі қатар баған арасындағы орын Екі қатар жол арасындағы орын Бірлік шерту кезіндегі тышқан курсорының астындағы элементті автобелсендірілу уақыты "%s" файлында мәлімет жоқ Келесі ТҮРлерге ғана --launch және --query командаларында қолдау бар:

  WebBrowser       - Таңдамалы веб браузер.
  MailReader       - Таңдамалы пошта қолданбасы.
  FileManager      - Таңдамалы файлдар басқарушысы.
  TerminalEmulator - Таңдамалы терминал эмуляторы. Келесі ТҮРлерге ғана --launch командасында қолдау бар: Өнделетін таңбаша. Парақ бағдарлауы Таңбашалар көрініс түрі Таңбаша жолағының бағдарлануы Таңдамалы файлдар басқарушысы бумалар құрамасын шолу үшін қолданылатын болады. Таңдамалы пошта қолданбасы эл. пошта адрестері шертілген кезде хаттарды құрастыру үшін қолданылатын болады. Таңдамалы терминал эмуляторы командалық жол ортасын талап ететін командаларды жөнелту үшін қолданылатын болады. Таңдамалы веб браузері гиперсілтемелерді ашу және көмекті көрсету үшін қолданылатын болады. Ерекшелеудің түрі Өнделетін таңбашаның пиксельдегі өлшемі. Әр элемент ені ретінде алынатын ен Thunar Панель _стилі Көмек алу үшін '%s --help' енгізіңіз. Жасалынатын desktop файлдың түрі (қолданба не сілтеме) "%s" үшін URI-схемасын анықтау мүмкін емес. Санатсыз таңбашалар Қолдауы жоқ desktop "%s" файл түрі Қолданылуы: %s [опциялар] [файл]
 Қолданылуы: exo-open [URL-дер...] Қос_ылу хабарламасын қолдану Жоғарыдағы тізімде жоқ басқа қолданбаны қолдану. Командалық жолды қолдану Көрініс пайдаланушыға бағандардан интерактивті іздеуге мүмкіндік береді Көрініс қайта реттеледі Vimprobable2 W3M мәтіндік браузері Веб браузері Ұрпақ өлшемдері дәл сондай болу керек пе Көріністегі элементтер бірлік шертумен белсендіріледі Әр элемент ені Терезелер тобы Терезелер тобындағы озушы Жұмыс _бумасы: X терминалы Xfce Terminal Xfe File Manager [ФАЙЛ|БУМА] Ж_аңа саймандар панелін қосу Ба_с тарту _Жабу _Жұмыс үстел негізгісі Кө_мек Таңба_ша: _Тек таңбашалар _Интернет _Аты: _ОК _Ашу _Басқа... _Панельді өшіру _Сақтау _Таңбашаны іздеу: Т_ек мәтін _URL: _Утилиталар aterm qtFM өлшемі 