��         	    �      �  2   �  -   ,  -   Z  ,   �  �   �  *   �  ,   �  3   �  8   %  �   ^  F   �  H   <  7   �  5   �  �   �  �   �  �   �     =     J     Q     j     �     �  	   �  	   �  
   �     �     �     �     �     	       2   &     Y  	   h     r     z     �     �     �     �  !   �           1      A   
   J      U      h      w   	   �   o   �       !     !     !!     ;!  0   G!     x!     �!     �!     �!     �!     �!     "     "  b   "     }"     �"  	   �"     �"  	   �"     �"  	   �"     �"     �"  	   #     #     #     !#  &   8#  %   _#  +   �#  %   �#  9   �#  %   $  H   7$     �$     �$     �$     �$     �$  $    %     %%     B%  "   W%  !   z%  '   �%  !   �%     �%      &     &  0   &     N&     U&     b&     h&     w&     �&     �&     �&  H   �&     �&     �&  	   '     '     '      '     0'  h  <'     �(     �(     �(     �(     �(  i   �(  W   X)     �)     �)     �)     �)     �)     �)     *  
   *     *  :   #*  J   ^*  2   �*  +   �*  <   +     E+     \+     l+     |+     �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     8,     @,     R,  4   o,     �,     �,     �,     �,     �,     �,  F   �,  E   <-  K   �-  E   �-     .     1.  G   H.     �.  '   �.  +   �.  (   /  (   -/  ;   V/     �/  "   �/     �/  L   �/  	   0     #0     00  0   @0     q0     }0     �0     �0     �0     �0     �0  	   �0     �0     �0     �0     	1     $1     :1     I1  0   \1  �   �1  ;   /2     k2  	   z2     �2     �2     �2     �2     �2     �2  5   �2  0   !3  +   R3  )   ~3     �3  I   �3  H   �3  N   C4  H   �4     �4     �4     �4     5     5      5     :5     N5  3   c5  0   �5  r   �5     ;6  ;   Z6     �6     �6     �6     �6     �6  )   7     27     O7     V7     e7  4   �7  (   �7     �7  "   �7     8     28     L8  A   f8     �8  8   �8     �8     
9     9  0   '9  A   X9     �9     �9     �9     �9  
   �9     �9     �9     :     :     .:     6:     =:     N:     T:     [:  	   g:     q:     x:     |:  	   �:     �:     �:     �:  
   �:     �:  
   �:     �:     �:  �  �:  2   �<  -   �<  :   �<  5   &=  �   \=  *   H>  3   s>  B   �>  F   �>  �   1?  Z   �?  \   /@  J   �@  K   �@    #A    ,B  �   IC     0D     PD  '   WD  %   D      �D  #   �D     �D     �D     E      %E     FE     dE     jE     �E     �E  <   �E     �E     �E  	   F     F     +F  $   AF  -   fF  @   �F  /   �F  )   G     /G     FG  
   OG     ZG      sG      �G     �G  �   �G     KH     ]H     oH     �H  B   �H  %   �H  #   I  !   *I  %   LI     rI  "   �I     �I     �I  �   �I     }J     �J     �J     �J     �J     �J     �J  !   �J  &   K     EK  !   XK     zK     K  <   �K  O   �K  >   $L  8   cL  >   �L  2   �L  Z   M     iM  ;   }M      �M     �M  !   �M  )   N  %   FN     lN  <   �N  O   �N  >   O  3   LO  *   �O     �O     �O  6   �O     P     P     9P     IP     ]P     xP     �P     �P  P   �P     �P     �P  	   Q     Q  &   Q  &   EQ     lQ  '  �Q     �S  *   �S     �S     �S  $   �S  �   T  y   �T     9U     OU  !   mU     �U  *   �U     �U     �U     �U     V  L   )V  m   vV  L   �V  G   1W  s   yW  (   �W     X     .X  *   NX     yX     �X     �X     �X     �X  '   �X     �X     Y  !   )Y  7   KY     �Y     �Y  6   �Y  @   �Y     'Z     ?Z  
   MZ     XZ     vZ     �Z  v   �Z  t   [  x   �[  p   \  9   r\     �\  �   �\  0   K]  0   |]  @   �]  B   �]  >   1^  S   p^     �^  6   �^     _  �    _     �_     �_  (   �_  S   �_  $   C`      h`     �`  "   �`     �`     �`      a      a     8a     Ea     Ya     va     �a     �a     �a  H   �a  �   b  I   c     Oc     gc  )   pc  $   �c     �c     �c  $   �c     d  M   d  @   cd  4   �d  6   �d  
   e  p   e  r   �e  t   �e  l   tf     �f     �f     g     g     4g  #   Bg     fg  !   �g  >   �g  3   �g  �   h  3   �h  E   �h     i     1i  ?   Gi  &   �i     �i  .   �i  +   �i  
   #j     .j  @   Mj  [   �j  3   �j      k  8   ?k  $   xk  "   �k  .   �k  Y   �k  "   Il  ]   ll  /   �l     �l     m  I   *m  g   tm     �m     �m  $   n     2n     Hn     \n     rn     �n  &   �n     �n  	   �n  '   �n     o     o     "o     9o     Go     Po     \o     do     qo     �o     �o     �o     �o     �o     �o     �o     �   �   �   �   �   o   K                l   9   	     V                       n   �   �   R   b     �   �   �   �   E       �       �            �         �       3   �       �   �   �         �                &             :      6   $       u   �       >   �   H   }       ?   Y   �   Z   0     �   �   �           	               �         �          �       �     M      7       Q   L   �       �   "   �      I       �   8   /   �   �   �       �   ;       �   <   O   �          ,   �   �   �   �   �       j     S       T   ^   �   �     �   *   |       �   {   �   [   �   �   �   �   \   w       ~               �   f   �   �   
    �   t   �       �   �   �       �   h       �     ]                            P          �   v   q          �   )   �   �   N       �   A       G   �   2              �   g   �   �       W           �              �   a          �       �   J   �   �                         1   �   �   X                 D     �   =           �              _       �   �       �   m   �      (   F              �   5       �       @   �     �   �   �   x   �   y   `   z   i   �      �           �   U   �   4   �   s       �   '       �   �   �                 -   �   �         �   B   �          �       �           �       C       �   �   �   �   �   �   k   �       #   �         c   
       �   p   e   .   �       %   d     �   r         !   �   �       �       �         �       �   �               +           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts Query the default helper of TYPE, where TYPE is one of the following values. ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Sylpheed TYPE [PARAMETER] Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Arabic (http://www.transifex.com/xfce/exo/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
        %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      تحليل (إسم,ملف) أزواج
   --extern          توليد رموز خارجية
   --launch TYPE [PARAMETERs...]       إطلاق التطبيق المفضل لـ
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          توليد رموز تابثة
   --strip-comments  حذف التعليقات من ملفات XML
   --strip-content   حذف محتويات العقدة لمفات XML
   --working-directory DIRECTORY       مجلد العمل الإفتراضي
                                      عند إستعمال خيار --launch option.   -?, --help                          طباعة رسالة المساعة و الخروج   -V, --version                       طباعة معلومات النسخة و الخروج   -V, --version     طباعة معلومات النسخة والخروج
   -h, --help        طباعة رسالة المساعدة و الخروج
   متصفح الوب       - متصفح الوب المفضل.
  قارئ البريد       - قارئ البريد المفضل.
  مدير الحزم      - مدير الحزم المفضل.
  محاكي الطرفية - محاكي الطرفية المفضل. %s (إكسفس %s)

حقوق الطبع (c) 2003-2006
        كل الحقوق محفوظة. os-cillation e.K.

كتب من طرف Benedikt Meurer <benny@xfce.org>.

مبني على جتك+-%d.%d.%d, running Gtk+-%d.%d.%d.

المرجوا الإبلاغ. عن الخلل إلى<%s>.
 %s يأتي بدون ضمان على الإطلاق,
قد تكون حصلت على نسخة من %s بموجب
رخصة جنو العمومية الصغرى التي يمكن العثور عليها
في حزمة المصدر%s.

 أيقونة الإجراءات نشط تنشيط لون عنصر الحدود تنشيط لون عنصر الملء تنشيط عنصر الفهرس تنشيط لون عنصر النص كل الملفات كل الأيقونات تحريكات زر إختيار التطبيق تطبيق الأيقونات Balsa كتلة الجهاز بريف تصفح ملف النظام تصفح ملف النظام لإختيار امر مخصص. تصفح الوِب تعلي_ق: أ_نشئ مدير الملفات Caja جهاز الأحرف إختر التطبيق المفضل إختر مدير الملفات المخصص إختر قارئ البريد الإلكتروني المخصص إختر محاكي الطرفية المخصص إختر متصفح الوب المخصص إختر إسم ملف Chromium Claws Mail محو حقل البحث مسافة بين الأعمدة مسافة بين الأعمدة أ_مر: حقوق الطبع (c) %s
        os-cillation e.K. كل الحقوق محفوظة.

كتب من طرف Benedikt Meurer <benny@xfce.org>.

 أنشئ مجلد انشئ مطلق انشئ مطلق <b>%s</b> أنشئ وصلة أنشئ ملف سطح المكتب في المجلد المعطى مؤشر لون عنصر الحدود مؤشر لون عنصر الملء مؤشر لون عنصر النص تخصيص شريط الأدوات... متصفح ديبيان محاكي طرفية ديبيان أيقونات الأجهزة Dillo اسحب عنصر إلى أشرطة الأدوات المذكورة أعلاه لإضافته ومن أشرطة الأدوات في جدول العناصر لإزالته. تحرير المجلد تحرير مطلق تحرير الوصلة شعارات تعابير تمكّين البحث يشمل محاكي ترفية Enlightened متصفّح الوِب إبِفَني إيفُليوشن الملفات التنفيذية FIFO فشل إنشاء "%s". فشل تشغيل مدير الملفات الإفتراضي فشل تشغيل قارئ البريد الإلكتروني الإفتراضي فشل تشغيل محاكي الطرفية الإفتراضي فشل تشغيل متصفح الوب الإفتراضي فشل إطلاق التطبيق المفضل للصنف "%s". فشل تحميل المحنويات من "%s": %s فشل تحميل الصورة "%s": سبب مجهول, ربما الصورة معطوبة فشل فتح "%s". فشل في فتح الملف %s من أجل الكتابة فشل فتح المسار "%s". فشل في فتح الشاشة فشل فتح الملف "%s": %s فشل تحليل محتويات "%s": %s فشل قراءة الملف "%s": %s فشل حفظ "%s". فشل تعيين مدير الملفات الإفتراضي فشل تعيين قارئ البريد الإلكتروني الإفتراضي فشل تعيين محاكي الطرفية الإفتراضي فشل تعيين المتصفح الإفتراضي ملف "%s" بدون مفتاح الصنف مدير الملفات نوع الأيقونات موقع ملف ليس ملف أو مجلد نظامي مجلد الحالة التالية أيقونة ج طرفية جنوم Galeon متصفح الوب جيري جوجل كروم متجانس كيفية وضع نص و أيقون كل عنصر بالنسبة لبعضهما أيس كات Icedove Iceweasel أيقونة نموذج مشهد الأيفونات نموذج مظهر الأيفونات عمود الرمز إذا لم تحدد خيار --launch، سيتم فتح المسارات
المحددة مع كافة محددات المواقع المعلومة مع مُطلقها
المفضل. وإلا، إذا قمت بتحديد الخيار --launch، يمكنك
تحديد التطبيق المفضل الذي تريد تشغيله، وتمرير المعلمات
الإضافية للتطبيق (أي بالنسبة لتطبيقات الطرفية يمكنك تمرير سطر الأوامر الذي يجب تشغيلها في المحطة الطرفية). ملفات صور نوع المساعد غير سليم "%s" جومتجي KMail متصفح الوب كونكيرور إطلاق المساعد الافتراضي للنوع مع المعلمة الاختيارية، حيث سيكون النوع أحد القيم التالية. إطلاق ملفات سطح المكتب ليس مدعوما عند %s تم تجميعه بواسطة GIO-Unix features. نمط التصميم Links متصفح النصوص الأيقونات المحلية Lynx متصفح النصوص قارء البريد الالكتروني الهامش عمود العلامات أيقونات القائمة ميدوري نموذج عمود يتم البحث فيه عند البحث في عنصر نموذج لعمود يستخدم لجلب المسار المطلق لملف الصورة ليتم عرضه عمود النموذج المستخدم لاسترداد رمز pixbuf من عمود النموذج المستخدم لاسترداد النص من نموذج العمود المستخدم لاستعادة النص اذا تم استخدام ترميز بانغو نموذج لشريط الأيقونات متصفح موزيلا موزيلا فَيَرفُكس بريد الإلكتوني لموزيلا موزيلا تندربيرد Mutt NXterm ناوتلس Netscape متصفح ﻻ يوجد أي برنامج محدد بدون أوامر محددة لم يتم تحديد ملف بدون ملف/مجلد محدد لا يوجد مساعد مُعرَف من أجل "%s". بدون أيقونة عدد الأعمدة عدد الأعمدة التي سيتم إظهارها فتح التطبيقات المفضلة
حوار الإعداد متصفح أوبيرا خيارات: توجيه PCMan مدير الملفات بيرل سكريبت Pixbuf عمود المرجوا تحديد مدير
ملفاتك المفضل الأن و الضغط على نعم للمضي قدما. المرجوا تحديد قارء
بريدك المفضل الأن و الضغط على نعم للمضي قدما. المرجوا تحديد محاكي
طرفيتك المفضل الأن و الضغط على نعم للمضي قدما. المرجوا تحديد متصفح
وب المفضل الأن و الضغط على نعم للمضي قدما. الرجاء التبليغ عن الخلل إلى <%s>.
 تطبيقات المفضلة التطبيقات المفضلة (متصفح الوب, قارئ البريد الإلكتروني و محاكي الطرفية) إختر مسار قبل إنشاء الوصلة إختر أمرا قبل إنشاء المطلق إختر تعليق قبل إنشاء ملف سطح المكتب إختر أيقونة قبل إنشاء ملف سطح المكتب إختر إسما قبل إنشاء ملف سطح المكتب إضغط بالزر أيمر للفأرة لتغيير التطبيق المحدد. معاينة طباعة معلومات النسخة و الخروج بيثون سكريبت استفهم من المساعد الافتراضي عن النوع، حيث سيكون النوع أحد القيم التالية. مرشح ROX RXVT يونيكود إقرء بريدك الإلكتروني التقديم بشكل مختلف استناداً إلى حالة التحديد. قابل لإعادة الترتيب أمر إعادة التشغيل مدير الملفات Rodent المسافة بين الصفوف مسافة بين الصفوف روبي سكريبت تنفيذ في ال_طرفية مُعرف المقبس ساكورا عمود البحث إ_ختر أيقونة من: إختر مسار العمل إختر تطبيقا إختر أيقونة إختر تطبيقا إختر التطبيقات المفضلة لخدمات المتنوعة حدد هذا الخيار لتمكين إعلام بدء التشغيل عند تشغيل الأمر من إدارة الملفات أو من القائمة. لا تدعم كل التطبيق الإعلام بدء التشغيل. إختر هذا الخيار لتشغيل الأمر في الطرفية. وضع الإنتقاء فاصل أمر إعادة تشغيل الجلسة مقبس مدير الإعدادات شيل سكريبت نقرة واحدة مهلة النقرة الواحدة مقبس المساحة المتروكة على أطراف مظهر الأيقونات المسافات الموضوعة بين خلايا العنصر الفراغ المدرج بين صفوف أعمدة الفراغ المدرج بين صفوف الشبكة مسافة حدد التطبيق الذي تريد إستعماله كـ
مدير ملفات إفتراضي لـإكسفس: حدد التطبيق الذي تريد إستعماله كـ
قارء البريد إفتراضي لـإكسفس: حدد التطبيق الذي تريد إستعماله
كمحاكي الطرفية إفتراضي لـ إكسفس: حدد التطبيق الذي تريد إستعماله كـ
متصفح وب إفتراضي لـإكسفس: حالة الأيقونات Sylpheed نوع [ثابتة] محاكي الطرفية عمود نص نص لأ_يقونات الهامة نص _لكل الأيقونات أيقونة ج لتقديمها. مقدار المسافة بين عمودين متتاليين مقدار المسافة صفين متتاليين مقدار وقت المرور تحت مؤشر الفأرة لتحديد العنصر تلقائياً في وضع نقرة واحدة الملف "%s" لا يحنوي غلى بيانات الانواع التالية مدعومة لأمر --launch command: رمز لتقديم. نمط التصميم النموذج المستخدم لإظهار الأيقونات توجيه شريط الأيقونات وضعٌ للإنتقاء حجم أيقونة العرض بالبكسل. العرض المستخدم لكل عنصر تونار شكل شريط الأدوات اكتب '%s --help' للمساعدة على الاستخدام. نوع ملف سطج المكتب الذي سيتم إنشائه (تطبيق أو وصلة) غير قادر على كشف URI-scheme لـ "%s". أيقونات غير مصنفة نوع ملف سطح المكتب غير مدعوم "%s" إستعمال: %s [options] [file]
 إستعمال: exo-open [URLs...] أستخدم _تنبية بدأ التشغيل إستعمل تطبيق مخصص عند عدم وجوده في اللائحة أعلاه. إستعمل سطر الأوامر مشهد يتيح للمستخدم البحث بين الأعمدة بصورة تفاعلية إظهار قابل لإعادة الترتيب W3M متصفح النصوص متصفّح وِب إذا كان ينبغي أن يكون الأطفال بنفس الحجم إذا كان يمكن تنشيط العناصر الموجودة في العرض بنقرة مفردة عرض كل عنصر مجموعة النافذة مشرف مجموعة النافذة م_سار العمل: طرفيّة إكس طرفية إكسفس مدير الملفات Xfe [ملف|مجلد] _أضِف شريط أدوات جديد _إلغاء _أغلق _سطح المكتب الإفتراضي _مساعدة _أيقونة: _أيقونات فقط _إنترنت _اسم: _موافق _فتح _أخرى... _حذف شريط الأدوات _حفظ _ابحث عن أيقونة: _نص فقط _المسار: _أدوات aterm حجم 