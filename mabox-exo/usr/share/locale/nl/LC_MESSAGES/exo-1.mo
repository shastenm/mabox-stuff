��    ,     |	  �  �         2   !  -   T  -   �  ,   �  �   �  *   �  >   �  ,   +  3   X  8   �  �   �  F   \  H   �  7   �  5   $  �   Z  �   !  �   �     �     �     �     �     �     �  	      	      
   %      0      K      ]      c      p      v   2   �      �   	   �      �      �      �      !     !!     >!  !   Z!     |!     �!     �!  
   �!     �!     �!     �!  	   �!  o   �!     g"     x"     �"     �"  0   �"     �"     �"     #     &#     ;#     S#     n#     {#  b   �#     �#     �#  	   $     $  	   $     $  	   +$     5$     S$  	   h$     r$     �$     �$  &   �$  %   �$  +   �$  %   %  9   >%  %   x%  H   �%     �%     �%     &     3&     J&  $   g&     �&     �&  "   �&  !   �&  '   '  !   +'     M'     g'     t'  0   �'     �'     �'     �'     �'     �'     �'     �'     (  H   (     Z(     a(  	   i(     s(     x(     �(     �(  h  �(     *     *     1*     9*     ?*  i   U*  W   �*     +     #+     6+     E+     W+     c+     j+  
   x+     �+  :   �+  J   �+  2   ,  +   C,  <   o,     �,     �,     �,     �,     �,     -     	-     -     -     ,-     D-     Y-     j-     �-     �-     �-     �-  4   �-     .     .     ".     ..     A.     Y.     f.  F   t.  E   �.  K   /  E   M/     �/     �/  G   �/     0  '   /0  +   W0  (   �0  (   �0  ;   �0     1  "   1     <1  	   K1  L   U1     �1  	   �1     �1     �1  0   �1     2     2     2     32     ?2     K2     X2  	   i2     s2     z2     �2     �2     �2     �2     �2  0   �2  �   3  ;   �3     �3  	   4     4     .4     F4     T4     a4     v4  5   }4  0   �4  +   �4  )   5     :5  I   B5  H   �5  N   �5  H   $6     m6     z6     �6     �6     �6     �6  
   �6     �6     �6     �6     �6  3   7  0   E7  r   v7     �7    8  ;   9     U9     i9     y9     �9  J   �9  [   �9  \   [:  T   �:     ;  )    ;     J;     g;     n;     };  4   �;  (   �;     �;  "   <     .<     J<     d<  A   ~<     �<  8   �<     =     "=     /=     @=  0   L=  A   }=     �=     �=     �=     �=  
   >     >     !>     2>     @>     S>     [>     b>     s>     y>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   �>     �>  
   �>     �>     �>     �>  `  ?  4   b@  -   �@  4   �@  .   �@  �   )A  +   B  J   -B  0   xB  <   �B  @   �B  �   'C  J   �C  N   D  :   TD  7   �D  �   �D  �   wE  �   gF     G     #G     *G     GG     cG     |G     �G     �G  	   �G     �G     �G     �G     �G     
H     H  D   0H     uH     �H     �H     �H     �H     �H  $   �H  "   �H  "   I     ?I     ^I     oI  
   xI     �I     �I     �I  
   �I  }   �I     BJ     KJ     XJ     oJ  4   ~J     �J     �J      �J     K     )K     AK     ZK     oK  �   uK  
   �K     L     L     &L     /L     BL  	   UL     _L     {L  	   �L     �L     �L     �L  0   �L  /   �L  0   *M  *   [M  9   �M  %   �M  `   �M     GN  '   ]N     �N     �N      �N  -   �N     O     %O  0   <O  0   mO  0   �O  *   �O  !   �O     P     -P  .   FP     uP     yP  
   �P     �P     �P     �P     �P     �P  P   �P     Q     &Q  	   .Q  	   8Q     BQ     UQ     mQ  h  Q     �R     �R     S     S     !S  g   6S  w   �S     T     &T     9T     MT     _T     oT     uT     �T     �T  :   �T  U   �T  F   *U  ;   qU  Y   �U     V     &V     6V     FV     SV     gV     lV     sV     |V     �V     �V     �V     �V     �V     W     'W     7W  A   UW     �W     �W     �W     �W     �W     �W     �W  G   X  D   NX  E   �X  @   �X     Y     8Y  F   MY  9   �Y  <   �Y  F   Z  E   RZ  A   �Z  B   �Z     [  %   /[     U[  	   d[  J   n[     �[  	   �[     �[     �[  7   �[     \     ,\     =\     U\     e\     u\     �\  	   �\     �\  	   �\     �\     �\     �\     �\     ]  5   ]  �   J]  I   ^     K^     Y^     i^  &   �^     �^     �^     �^     �^  A   �^  ;   ._  0   j_  0   �_     �_  P   �_  P   )`  P   z`  K   �`     a     *a     @a     Ea     Na     _a     oa  
   wa  $   �a     �a     �a  8   �a  5   b  l   Rb  $   �b  1  �b  ?   d     Vd     sd  "   �d  &   �d  ]   �d  s   .e  p   �e  k   f     f  5   �f  %   �f     �f     �f     g  <   g  -   \g     �g  ,   �g     �g     �g     h  G   ,h     th  8   �h     �h     �h     �h  
   �h  7   i  D   @i     �i     �i     �i  	   �i  
   �i     �i     �i     �i     j  	   #j     -j     6j     Mj     Sj     _j     tj     }j     �j     �j     �j     �j     �j     �j     �j     �j     �j     �j     �j     �j                 7   Y   G             �   �   �   "   �   �           )   �   r              	     p   m      �     �     =   5       �     �           �           �       �   '  Z   �   �     J     c   @   �               -         �   �   �   �   �   �       v   �   Q   '             j           t   u     �      �   y   �           P       �   �   �   �         �   �           |   �      �   �       �   �   +      �   *       �   �                      T   �   �             s     N      �         ;   g      �   �   �       �         X   )          W   �                   B           h   \           �   l   3   �   �      U       �   �   &  �   �   #   C   �   �   k       �   �   M   9              ~          �       O   �   �   2     ]       4   D   �   1       �      �             	  �   d             E   �   �      �   i   R      S   �     �   A   a       $   e   z     0   
  H   �      .      �       �   L       �       �   :     q            �   b   _       (       �       *          >   ?   �   F   �   �   �   �           }       {   �   <   �       "  $  V           %   �   �     �   �   (      8   �   �   %      �   �   �   �       !  �         ^   �   `             �   x           �   !       �     �   �   �       f          �   �              w   �   #      �   �   �     [       +   �       �       �         /       &   �     �   �   K   n   �           I   �   �       
   ,         �   �   ,  �       6               �             �   o          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-24 18:44+0000
Last-Translator: Pjotr <pjotrvertaalt@gmail.com>
Language-Team: Dutch (http://www.transifex.com/xfce/exo/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [opties] --build-list [[naam-bestand]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Verwerk (naam-, bestand-) paren
   --extern          Genereer externe symbolen
   --launch TYPE [PARAMETERs...]       Start de voorkeurtoepassing voor
                                      TYPE met extra opties, waarbij TYPE 
                                      een van de volgende waarden is.   --name=identifier C macro/variabele-naam
   --output=bestandnaam Schrijf aangemaakte csource naar opgegeven bestand
   --static          Genereer statische symbolen
   --strip-comments  Verwijder commentaren uit XML-bestanden
   --strip-content   Verwijder node-informatie uit XML-bestanden
   --working-directory MAP       Standaard werkmap voor toepassingen
                                      indien de --launch optie wordt gebruikt.   -?, --help                         Deze hulptekst afdrukken en afsluiten   -V, --version                       Versie-informatie afdrukken en afsluiten   -V, --version     Druk versie-informatie af en sluit af
   -h, --help        Druk deze hulptekst af en sluit af
 WebBrowser - De voorkeurs-webbladeraar.
 MailReader - De voorkeurs-emaillezer.
 FileManager - De voorkeurs-bestandbeheerder.
 TerminalEmulator - Het voorkeurs-terminalvenster. %s (Xfce %s)

Auteursrecht (c) 2003-2006
 os-cillation e.K. Alle rechten voorbehouden.

Geschreven door Benedikt Meurer <benny@xfce.org>.

Gebouwd met Gtk+-%d.%d.%d, met gebruikmaking van Gtk+-%d.%d.%d.

Gelieve fouten te melden aan <%s>.
 Op %s zit GEEN ENKELE GARANTIE,
U mag %s herdistribueren onder de voorwaarden van
de GNU Lesser General Public License, welke gevonden kan worden
in het %s bronpakket.

 Actiepictogrammen Actief Randkleur van actief element Vulkleur van actief element Index van actief element Tekstkleur van actief element Alle bestanden Alle pictogrammen Animaties Knop voor toepassingkeuze Toepassingspictogrammen Balsa Blokapparaat Brave Blader door het bestandssysteem Blader door het bestandssysteem om een aangepaste opdracht te kiezen Verken het web _Opmerking: M_aken Caja bestandbeheerder Tekenapparaat Kies voorkeurstoepassing Kies een aangepaste bestandbeheerder Kies een aangepast e-mailprogramma Kies een aangepast terminalvenster Kies een aangepaste webbrowser Kies bestandnaam Chromium Claws Mail Maak zoekveld leeg Kolom-spatiëring Kolomspatiëring Opdr_acht: Auteursrecht (c) %s
        os-cillation e.K. Alle rechten voorbehouden.

Geschreven door Benedikt Meurer <benny@xfce.org>.

 Maak map Maak starter Maak starter <b>%s</b> Maak koppeling Maak een nieuw bureaubladbestand in de opgegeven map Randkleur van aanwijzer-element Vulkleur van aanwijzer-element Tekstkleur van aanwijzer-element Werkbalk aanpassen... Debian Sensible Browser Debian X Terminalvenster Apparaatpictogrammen Dillo Sleep een element naar de werkbalken erboven om het toe te voegen, en van de werkbalken naar de elemententabel om het te verwijderen. Bewerk map Bewerk starter Bewerk koppeling Emblemen Emotiepictogrammen Zoeken inschakelen Encompass Enlightened Terminalvenster Epiphany Webbrowser Evolution Uitvoerbare bestanden FIFO Kon '%s' niet maken. Kon de standaard-bestandbeheerder niet uitvoeren Kon het standaard-emailprogramma niet uitvoeren Kon het standaard-terminalvenster niet uitvoeren Kon de standaard-webbrowser niet uitvoeren Kon voorkeurstoepassing voor categorie '%s' niet starten. Kon de inhoud van '%s' niet laden: %s Kon afbeelding '%s' niet laden: onbekende reden, waarschijnlijk een beschadigd afbeeldingbestand Kon '%s' niet openen. Kon %s niet openen om naar te schrijven Kon URI '%s' niet openen. Kon scherm niet openen Kon bestand '%s' niet openen: %s Kon de inhoud van bestand '%s' niet lezen: %s Kon bestand '%s' niet lezen: %s Kon '%s' niet opslaan. Kon de standaard-bestandbeheerder niet instellen Kon het standaard-e-mailprogramma niet instellen Kon het standaard-terminalvenster niet instellen Kon de standaard-webbrowser niet instellen Bestand '%s' heeft geen typetoets Bestandbeheerder Bestandtype-pictogrammen Bestandlocatie is geen regulier bestand of map Map Volg status GPictogram GNOME Terminal Galeon Webbrowser Geary Google Chrome Homogeen Hoe de tekst en pictogram van elk element ten opzichte van elkaar zijn geplaatst Icecat Icedove Iceweasel Pictogram Pictogrambalkmodel Pictogramweergave-model Pictogrammenkolom Als u de --launch optie niet opgeeft, zal exo-open al de opgegeven URL's
met hun voorkeurs-URL-afhandelaars openen. Als u de --launch optie wel
opgeeft, dan kunt u kiezen welke voorkeurtoepassing u wil gebruiken en
kunt u extra parameters aan de toepassing doorgeven (bijvoorbeeld: voor
TerminalEmulator kunt u de in de terminal uit te voeren opdracht opgeven) Afbeeldingbestanden Ongeldig hulptype '%s' Jumanji KMail Konqueror Webbrowser Start de standaardhulp van TYPE met de optionele PARAMETER, waar TYPE één van de volgende waarden is. Het starten van bureaubladbestanden wordt niet ondersteund wanneer %s wordt gecompileerd zonder GIO-Unix-eigenschappen. Vormgevingmodus Links Tekstbrowser Locatiepictogrammen Lynx Tekstbrowser E-mailprogramma Marge Opmaakkolom Menupictogrammen Midori Modelkolom om te doorzoeken indien element wordt doorzocht Modelkolom om het absolute pad te verkrijgen naar een weer te geven afbeeldingbestand Modelkolom die gebruikt wordt voor het ophalen van de pictogram-pixbuf Modelkolom die gebruikt wordt voor het ophalen van de tekst Modelkolom die gebruikt wordt voor het ophalen van de tekst bij gebruik van Pango-opmaak. Model voor de pictogrammenbalk Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXTerm Nautilus Netscape Navigator Geen toepassing geselecteerd Geen opdracht opgegeven Geen bestand geselecteerd Geen bestand/map opgegeven Geen helper bepaald voor '%s'. Geen pictogram Aantal kolommen Aantal weer te geven kolommen Open het dialoogscherm voor configuratie
van voorkeurtoepassingen Opera Browser Opties: Oriëntatie PCMan Bestandbeheerder PCManFM-Qt Bestandbeheerder Perl scripts Pixbuf-kolom Kies nu uw voorkeurs-bestandbeheerder 
en klik op Oké om door te gaan. Kies nu uw voorkeurs-emailprogramma
en klik op Oké om door te gaan. Kies nu uw voorkeurs-terminalvenster
en klik op Oké om door te gaan. Kies nu uw voorkeurs-webbrowser
en klik op Oké om door te gaan. Meld fouten a.u.b. aan <%s>.
 Voorkeurtoepassingen Voorkeurstoepassingen (webbrowser, e-mailprogramma en terminalvenster) Vooringestelde URL bij het maken van een nieuwe koppeling Vooringestelde opdracht bij het maken van een nieuwe starter Vooringestelde opmerking bij het maken van een nieuw bureaubladbestand Vooringesteld pictogram bij het maken van een nieuw bureaubladbestand Vooringestelde naam bij het maken van een nieuw bureaubladbestand Klik met de linker muistoets om de gekozen toepassing te wijzigen. Voorbeeldweergave Druk versie-informatie af en sluit af Python scripts QTerminal Zoek de standaardhelper van TYPE, waar TYPE één der volgende waarden is. QupZilla ROX-Filer RXVT Unicode Lees uw email Geef verschillend weer, gebaseerd op de selectiestatus. Herschikbaar Herstartopdracht Rodent bestandbeheerder Rij-spatiëring Rij-spatiëring Ruby scripts In _terminal uitvoeren SOCKET ID Sakura Zoekkolom Kies p_ictogram vanuit: Kies een werkmap Kies een toepassing Kies een pictogram Kies toepassing Selecteer standaardtoepassingen voor diverse diensten Selecteer deze optie om opstartmelding in te schakelen wanneer deze opdracht wordt uitgevoerd vanuit de bestandbeheerder of het menu. Niet elke toepassing ondersteunt opstartmelding. Selecteer deze optie om de opdracht in een terminalvenster uit te voeren. Selectiemodus Scheidingsteken Herstartopdracht voor sessie Contactpunt voor instellingenbeheerder Shell scripts Enkele klik Enkele-klik-blokkeertijd Contactpunt Ruimte die aan de randen van de pictogramweergave geplaatst wordt Ruimte die tussen de cellen van een element geplaatst wordt Ruimte die tussen rasterkolommen geplaatst wordt Ruimte die tussen de rasterrijen geplaatst wordt Spatiëring Kies de toepassing die u wil gebruiken
als standaard-bestandbeheerder voor Xfce: Kies de toepassing die u als standaard-
e-mailprogramma wil gebruiken voor Xfce: Kies de toepassing die u als standaard-
terminalvenster wil gebruiken voor Xfce: Kies de toepassing die u als standaard-
webbrowser wil gebruiken voor Xfce: Statuspictogrammen Standaardpictogrammen Surf Sylpheed TYPE [PARAMETER] Terminalvenster Afmaker Tekstkolom Tekst voor belangrijke pictogra_mmen Tekst voor _alle pictogrammen Het weer te geven Gpictogram. De hoeveelheid ruimte tussen twee opeenvolgende kolommen De hoeveelheid ruimte tussen twee opeenvolgende rijen De hoeveelheid tijd waarna een element onder de muispijl automatisch wordt geselecteerd in enkele-klik-modus Het bestand '%s' bevat geen gegevens De volgende TYPEs worden ondersteund voor de opdrachten --launch en --query:

  WebBrowser       - De webverkenner van uw voorkeur.
  MailReader       - Het e-mailprogramma van uw voorkeur.
  FileManager      - De bestandverkenner van uw voorkeur.
  TerminalEmulator - Het terminalvenster van uw voorkeur. De volgende TYPEs worden ondersteund voor de --launch opdracht: Het weer te geven pictogram. De vormgevingmodus Het model van de pictogramweergave De oriëntatie van de pictogrammenbalk De bestandbeheerder van uw voorkeur zal worden gebruikt om de inhoud van mappen te verkennen. Het e-mailprogramma van uw voorkeur zal worden gebruikt om e-mails op te stellen wanneer u op e-mailadressen klikt. Het terminalprogramma van uw voorkeur zal worden gebruikt om opdrachten uit te voeren die een terminal vereisen. De webverkenner van uw voorkeur zal worden gebruikt om webkoppelingen te openen en om hulpteksten te tonen. De selectiemodus De grootte van het weer te geven pictogram in pixels. De gebruikte breedte voor elk element Thunar Werkbalk_stijl Tik '%s --help' voor gebruik Het te maken bureaubladbestandtype (toepassing of koppeling) Kon het URI-schema van '%s' niet vaststellen. Ongecategoriseerde pictogrammen Niet-ondersteund type bureaubladbestand '%s' Gebruik: %s [opties] [bestand]
 Gebruik: exo-open [URL's...] Gebruik op_startmelding Gebruik een aangepaste toepassing die niet in bovenstaande lijst staat. Gebruik de opdrachtregel Weergave laat gebruikers kolommen interactief doorzoeken Weergave is herschikbaar Vimprobable2 W3M tekstbrowser Webbrowser Of de kinderen allemaal dezelfde grootte moeten hebben. Of de elementen in de weergave te activeren zijn met een enkele klik Breedte voor elk element Venstergroep Venstergroep-leider _Werkmap: X Terminal Xfce Terminal Xfe bestandbeheerder [BESTAND|MAP] Voeg een nieuwe werkb_alk toe _Afbreken _Sluiten Werkomgeving-standaard _Hulp _Pictogram: Alleen p_ictogrammen Internet _Naam: O_ké _Openen _Overige... _Verwijder werkbalk O_pslaan _Zoek pictogram: Alleen _tekst _URL: Hulpprogramma's aterm qtFM grootte 