��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  h  1?  :   �@  0   �@  /   A  ,   6A  �   cA  0   =B  J   nB  0   �B  6   �B  >   !C  �   `C  I   �C  D   BD  3   �D  8   �D  �   �D  �   �E  �   �F     `G  
   rG  "   }G      �G     �G      �G     �G     H  
   !H     ,H     DH     VH  
   \H     gH     mH  >   �H     �H     �H     �H     �H     I     I  $   (I  &   MI  !   tI     �I     �I     �I     �I     �I     �I     J  	   "J  {   ,J  
   �J     �J     �J  
   �J  ,   �J     K     4K     QK     nK     �K     �K     �K     �K  (   �K  O   �K     JL     [L     rL     �L     �L  
   �L  	   �L     �L     �L  	   �L     �L     �L  #   �L  1   "M  3   TM  )   �M  '   �M  ?   �M  /   N  R   JN  $   �N  4   �N  "   �N     O  )   :O  4   dO  )   �O  (   �O  1   �O  3   P  1   RP  '   �P  &   �P     �P     �P  .   �P     ,Q     3Q     @Q     FQ     UQ     iQ     oQ     }Q  A   �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     R     R     5S     CS     aS     iS     oS  i   �S  h   �S     YT     fT     yT     �T     �T  
   �T     �T     �T     �T  <   �T  Q    U  /   rU  %   �U  L   �U     V     (V     7V     GV     ]V     qV     vV     }V     �V     �V     �V     �V  "   �V  3   W     8W     EW  #   ZW  $   ~W     �W  	   �W     �W     �W     �W     �W     X  ?   X  A   RX  <   �X  :   �X  1   Y     >Y  @   PY  "   �Y  ,   �Y  1   �Y  .   Z  ,   BZ  9   oZ  
   �Z     �Z     �Z  	   �Z  Q   �Z     ?[  	   H[     R[     _[  -   w[     �[     �[     �[  
   �[  	    \     
\     \     /\     7\  
   >\     I\     d\     w\     �\     �\  2   �\  �   �\  8   �]     �]     �]  $   �]     ^      ^     7^     F^     Z^  2   b^  %   �^  )   �^  $   �^     
_  ;   _  5   M_  1   �_  .   �_     �_     �_     `     `     `     !`  
   0`     ;`  #   H`     l`     �`  /   �`  *   �`  g   �`  !   aa    �a  +   �b     �b     �b     �b     �b  Q   c  l   Wc  V   �c  \   d     xd  ,   �d     �d     �d     �d  $   �d  7   e  (   Ee     ne  '   �e  %   �e     �e  #   �e  D   f     `f  =   vf  $   �f     �f     �f     �f  *   g  2   /g     bg     sg     �g     �g  	   �g     �g     �g     �g     �g     �g     h     h     !h     'h     0h  	   Ch     Mh     Th     Xh     ^h     fh  	   }h     �h     �h     �h  
   �h     �h     �h     �h                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-21 14:55+0000
Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>
Language-Team: Finnish (http://www.transifex.com/xfce/exo/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
         %s [valitsimet] --build-list [[nimi tiedosto]...]
         exo-open --launch TYYPPI [PARAMETRIT...]   --build-list      Lue (nimi, tiedosto)-parit
   --extern          Generoi extern-symbolit
   --launch TYYPPI [PARAMETRIT...]     Suorita TYYPIN oletussovellus
                                      käyttäen valinnaisia PARAMETREJA,
                                      kun TYYPPI on joku alla luetelluista.   --name=tunniste   C-makron tai muuttujan nimi
   --output=tiedostonnimi Kirjoita luotu csource määritettyyn tiedostoon
   --static          Generoi staattiset symbolit
   --strip-comments  Poista kommentit XML-tiedostoista
   --strip-content   Poista noodien sisältö XML-tiedostoista
   --working-directory KANSIO          Sovellusten oletusarvoinen työkansio
                                      käytettäessä --launch -valitsinta.   -?, --help                          Näytä tämä ohjeteksti ja poistu   -V, --version                       Näytä versiotiedot ja poistu   -V, --version     Näytä versiotiedot ja poistu
   -h, --help        Näytä tämä ohjeteksti ja poistu
   WebBrowser       - Oletusarvoinen web-selain.
  MailReader       - Oletusarvoinen sähköpostiohjelma.
  FileManager      - Oletusarvoinen tiedostonhallinta.
  TerminalEmulator - Oletusarvoinen pääteohjelma. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Kaikki oikeudet pidätetään.

Käännettäessä Gtk+-%d.%d.%d, nyt käytössä Gtk+-%d.%d.%d.

Ilmoita ohjelmavirheistä osoitteeseen <%s>.
 %s ei ole MINKÄÄNLAISEN TAKUUN ALAINEN.
Voit levittää ja kopioida %s -ohjelmistoa GNU Lesser General
Public License -lisenssin ehtojen mukaisesti. Lisenssin löydät
%s -lähdekoodipakkauksesta.

 Toimintokuvakkeet Aktiivinen Aktiivisen kohteen reunuksen väri Aktiivisen kohteen täyttöväri Aktiivisen kuvakkeen indeksi Aktiivisen kohteen tekstin väri Kaikki tiedostot Kaikki kuvakkeet Animaatiot Sovellusvalitsinpainike Sovelluskuvakkeet Balsa Lohkolaite Brave Selaa tiedostojärjestelmää Selaa tiedostojärjestelmää valitaksesi mukautetun komennon. Selaa verkkosivuja Ko_mmentti: _Luo Caja-tiedostonhallinta Merkkilaite Valitse oletussovellus Valitse mukautettu tiedostonhallinta Valitse mukautettu sähköpostiohjelma Valitse mukautettu pääteohjelma Valitse mukautettu verkkoselain Valitse tiedostonimi Chromium Claws-sähköposti Tyhjennä hakukenttä Sarakkeiden välit Sarakkeiden väli _Komento: Copyright (c) %s
        os-cillation e.K. Kaikki oikeudet pidätetään.

Kirjoittanut Benedikt Meurer <benny@xfce.org>.

 Luo kansio Luo käynnistin Luo käynnistin <b>%s</b> Luo linkki Luo uusi desktop-tiedosto valittuun kansioon Osoitinkohteen reunuksen väri Osoitinkohteen täyttöväri Osoitinkohteen tekstin väri Mukauta työkalupalkki... Debianin sensible-browser Debianin X-pääteohjelma Laitekuvakkeet Dillo _Älä näytä tätä viestiä uudelleen Lisää ja poista yllä olevissa työkalupalkeissa olevia kohteita raahaamalla. Muokkaa kansiota Muokkaa käynnistintä Muokkaa linkkiä Tunnuskuvat Hymiöt Salli haku Encompass ETerm-pääteohjelma Epiphany-verkkoselain Evolution Suoritettavat tiedostot FIFO Kohteen "%s" luominen epäonnistui. Oletustiedostonhallinnan käynnistys epäonnistui Oletussähköpostiohjelman käynnistys epäonnistui Oletuspäätteen käynnistys epäonnistui Oletusselaimen käynnistys epäonnistui Oletussovelluksen suorittaminen kategorialle "%s" epäonnistui. Kohteen "%s" sisällön lataus epäonnistui: %s Kuvan "%s" lataus epäonnistui: Syy tuntematon, kuvatiedosto luultavasti viallinen Kohteen "%s" avaaminen epäonnistui. Tiedoston %s avaaminen kirjoitettavaksi epäonnistui URI:n "%s" avaaminen epäonnistui. Näytön avaaminen epäonnistui Tiedoston "%s" avaaminen epäonnistui: %s Tiedoston "%s" sisällön jäsennys epäonnistui: %s Tiedoston "%s" lukeminen epäonnistui: %s Kohteen "%s" tallentaminen epäonnistui. Oletustiedostonhallinnan asettaminen epäonnistui Oletussähköpostiohjelman asettaminen epäonnistui Oletuspäätesovelluksen asettaminen epäonnistui Oletusselaimen asettaminen epäonnistui Tiedostolla "%s" ei ole Type-kenttää Tiedostonhallinta Tiedostotyyppikuvakkeet Sijainti ei ole tavallinen tiedosto tai kansio Kansio Seuraa tilaa GIcon Gnomen pääte Galeon-verkkoselain Geary Google Chrome Tasamittainen Kuinka kohteiden teksti ja kuvake sijoitetaan suhteessa toisiinsa Icecat Icedove Iceweasel Kuvake Kuvakepalkkimalli Kuvakenäkymämalli Kuvakesarake Jos et määritä --launch -valitsinta, exo-open avaa kaikki määritetyt URL:t
niiden oletuskäsittelijöillä. Muussa tapauksessa voit valita suoritettavan
sovelluksen ja antaa sille lisäparametreja (voit esimerkiksi määrittää
päätteessä suoritettavan komentorivin). Kuvatiedostot Sovellustyyppi "%s" ei kelpaa Jumanji KMail Konqueror-verkkoselain Käynnistä TYYPIN oletussovellus käyttäen valinnaista VALITSINTA. TYYPPI on yksi seuraavista arvoista. Työpöytätiedostojen käynnistystä ei tueta, kun %s on käännetty ilman GIO-Unix -toiminnallisuutta. Asettelutapa Links-tekstiselain Sijaintikuvakkeet Lynx-tekstiselain Sähköpostiohjelma Marginaali Merkkaussarake Valikkokuvakkeet Midori Mallin sarake, josta etsitään kohteen kautta etsittäessä Mallisarake, jota käytetään näytettävän kuvan absoluuttista polkua hakiessa Mallin sarake, josta kuvakkeen pixbuf noudetaan Mallin sarake, josta teksti noudetaan Mallin sarake, josta teksti noudetaan käytettäessä Pango-merkintäkieltä Kuvakepalkin malli Mozilla-selain Mozilla Firefox Mozillan sähköposti Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ei sovellusta valittuna Ei komentoa määritetty Ei tiedostoa valittuna Tiedostoa/kansiota ei määritetty Tyypille "%s" ei ole määritelty oletussovellusta. Ei kuvaketta Sarakkeiden määrä Näytettävien sarakkeiden määrä Avaa oletussovellusten
asetusdialogi Opera-selain Valinnat: Suunta PCMan-tiedostonhallinta PCManFM-Qt-tiedostonhallinta Perl-skriptit Pixbuf-sarake Valitse haluamasi tiedostonhallinta ja
napsauta OK jatkaaksesi. Valitse haluamasi sähköpostiohjelma ja
napsauta OK jatkaaksesi. Valitse haluamasi pääteohjelma ja
napsauta OK jatkaaksesi. Valitse haluamasi verkkoselain ja
napsauta OK jatkaaksesi. Ilmoitathan ohjelmistovirheet osoitteeseen <%s>.
 Oletussovellukset Oletussovellukset (verkkoselain, sähköposti ja pääteohjelma) Esiasetettu URL luotaessa linkkiä Esiasetettu komento luotaessa käynnistintä Esiasetettu kommentti luotaessa desktop-tiedostoa Esiasetettu kuvake luotaessa desktop-tiedostoa Esiasetettu nimi luotaessa desktop-tiedostoa Paina hiiren vasenta painiketta vaihtaaksesi sovelluksen. Esikatselu Näytä versiotiedot ja poistu Python-skriptit QTerminal Tiedustele tyypin TYPE oletussovellusta, jossa TYPE on yksi seuraavista arvoista. QupZilla ROX-Filer RXVT Unicode Käytä sähköpostiasi Piirrä eri tavoin valinnan tilasta riippuen. Uudelleen järjestettävä Uudelleenkäynnistyskomento Rodent-tiedostonhallinta Rivivälit Riviväli Ruby-skriptit Suorita _päätteessä PISTOKE Sakura Hakusarake Valitse _kuvake kohteesta: Valitse työkansio Valitse sovellus Valitse kuvake Valitse sovellus Valitse oletussovellukset erinäisille palveluille Valitse tämä ottaaksesi käyttöön käynnistyksen huomautuksen, kun komento suoritetaan tiedostonhallinnasta tai valikosta. Kaikki sovellukset eivät tue käynnistyksen huomautusta. Valitse tämä suorittaaksesi komennon pääteikkunassa. Valintatapa Erotin Istunnon uudelleenkäynnistyskomento Asetustenhallinnan pistoke Komentotulkin skriptit Kertanapsautus Kertanapsautusviive Pistoke Kuvakenäkymän reunoille lisättävä tyhjä tila Kohteen lohkojen välinen tyhjä tila Ruudukon sarakkeiden välinen tyhjä tila Ruudukon rivien välinen tyhjä tila Välit Määritä Xfce:n oletusarvoinen
tiedostonhallintasovellus: Määritä Xfce:n oletusarvoinen
sähköpostiohjelma: Määritä Xfce:n oletusarvoinen
päätesovellus: Määritä Xfce:n oletusarvoinen verkkoselain: Tilakuvakkeet Vakiokuvakkeet Surf Sylpheed TYYPPI [VALITSIN] Pääteohjelma Terminator Tekstisarake Teksti vain t_ärkeille kuvakkeille Teksti k_aikille kuvakkeille Piirrettävä GIcon. Vierekkäisten sarakkeiden välinen tyhjä tila Peräkkäisten rivien välinen tyhjä tila Viive, jonka jälkeen hiiren osoittimen alla oleva kohde valitaan automaattisesti kertanapsautustilassa Tiedosto "%s" ei sisällä tietoa Seuraavat TYPE-argumentit ovat tuettuja --launch ja --query -komennoille:

  WebBrowser       - Oletus verkon selaamiseen.
  MailReader       - Oletussähköpostisovellus.
  FileManager      - Oletus tiedostonhallintaan.
  TerminalEmulator - Oletuspäätesovellus. Komento --launch tukee seuraavia TYYPPEJÄ: Piirrettävä kuvake. Asettelutapa Kuvakenäkymän malli Kuvakepalkin suunta Oletusarvoista tiedostonhallintaa käytetään kansioiden sisällön selaamiseen. Oletussähköpostiohjelmaa käytetään sähköpostien kirjoittamiseen sähköpostiosoitteita napsauttaessa. Oletuspääteohjelmaa käytetään komentoriviä vaativien sovellusten suorittamiseen. Oletusverkkoselainta käytetään linkkien avaamiseen ja ohjeiden sisällön näyttämiseen. Valintatapa Piirrettävän kuvakkeen koko kuvapisteinä. Ruudukon kohteiden leveys Thunar Työkalupalkin t_yyli Saat ohjeita komennolla "%s --help". Luotavan desktop-tiedoston tyyppi (sovellus tai linkki) Kohteen "%s" URI-skeemaa ei tunnistettu. Luokittelemattomat kuvakkeet Tyypin "%s" desktop-tiedostoja ei tueta Käyttö: %s [valitsimet] [tiedosto]
 Käyttö: exo-open [URL:t...] Käytä käynnistyksen _huomautusta Valitse mukautettu sovellus, jota ei löydy yläpuoliselta listalta. Käytä komentoriviä Käyttäjä voi hakea näkymän sarakkeista interaktiivisesti Näkymän voi järjestää uudelleen Vimprobable2 W3M-tekstiselain Verkkoselain Ovatko kaikki lapsielementit samankokoisia Voiko näkymän kohteet valita kertanapsautuksilla Kohteiden leveys Ikkunaryhmä Ikkunaryhmän johtaja _Työkansio: X-pääte Xfce-pääte Xfe-tiedostonhallinta [TIEDOSTO|KANSIO] _Lisää uusi työkalupalkki _Peru _Sulje Työpöydän _oletus O_hje Ku_vake: Pelkät _kuvakkeet _Internet _Nimi: _OK _Avaa _Muu... _Poista työkalupalkki _Tallenna _Etsi kuvaketta: Pelkkä t_eksti _URL: _Työkalut aterm qtFM koko 