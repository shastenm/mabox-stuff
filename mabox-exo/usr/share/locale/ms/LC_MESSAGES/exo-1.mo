��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  S  1?  2   �@  -   �@  0   �@  '   A  �   ?A  .   B  @   MB  '   �B  /   �B  6   �B  �   C  M   �C  J   �C  9   CD  8   }D  �   �D  �   oE  �   SF     �F      G     G      G     7G     IG  
   _G  
   jG     uG     }G     �G     �G     �G     �G     �G  .   �G     �G     H     H     H     0H     ?H     WH     pH     �H     �H     �H     �H  	   �H     �H     �H     I  
   I  r   )I     �I     �I     �I     �I  5   �I     J     2J     JJ     aJ     xJ     �J     �J     �J     �J  t   �J     OK     aK     rK     �K     �K     �K  	   �K     �K     �K  	   �K     �K     L     L  !   L     ?L  %   _L  $   �L  >   �L  '   �L  P   M     bM     vM     �M     �M     �M  "   �M     N     N  "   4N      WN  &   xN  %   �N  %   �N     �N     �N  .   	O     8O     ?O     LO     RO     aO     yO     O     �O  P   �O     �O     �O  	   �O     �O     P     P  
   )P  n  4P  	   �Q     �Q     �Q     �Q     �Q  i   �Q  M   ^R     �R     �R     �R     �R     �R     �R     �R  	   S     S  :   S  P   XS  8   �S  .   �S  I   T     [T     sT     �T     �T     �T     �T     �T     �T     �T     �T     �T     U     U  "   <U  
   _U     jU     yU  *   �U     �U     �U  	   �U     �U     �U  
   V     V  K   'V  I   sV  O   �V  N   W     \W     |W  G   �W  !   �W  (   �W  *   !X  (   LX  (   uX  7   �X  	   �X     �X      Y  	   Y  O   Y     gY  	   pY     zY     �Y  5   �Y     �Y     �Y     �Y     Z     Z  
   .Z     9Z  	   OZ     YZ     `Z     oZ     �Z     �Z     �Z     �Z  0   �Z  �   �Z  C   �[     �[     �[     �[     \     .\  	   :\     D\     ]\  ,   c\  -   �\  &   �\  &   �\     ]  M   ]  K   `]  Q   �]  P   �]     O^  	   [^     e^     j^     s^     �^  
   �^  
   �^     �^     �^     �^  .   �^  )    _  `   J_      �_  �   �_  .   �`     �`     a     a     ,a  D   Ba  U   �a  c   �a  W   Ab     �b  $   �b  &   �b     �b     �b  "   c  6   /c  !   fc     �c  &   �c      �c     �c     d  <   !d     ^d  G   qd     �d     �d     �d     �d  -   e  <   4e     qe     �e     �e     �e  
   �e     �e     �e     �e      f     f     "f     )f     8f     Af     Hf  	   Uf     _f     ff     jf     pf     ~f     �f     �f     �f     �f     �f     �f     �f     �f                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-04 18:54+0000
Last-Translator: abuyop <abuyop@gmail.com>
Language-Team: Malay (http://www.transifex.com/xfce/exo/language/ms/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ms
Plural-Forms: nplurals=1; plural=0;
        %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Hurai pasangan (nama, fail)
   --extern          Jana simbol extern
   --launch TYPE [PARAMETERs...]       Lancar aplikasi digemari bagi
                                      TYPE dengan PARAMETER pilihan, yang
                                      mana TYPE adalah salah satu nilai berikut.   --name=identifier nama makro/pembolehubah C
   --output=filename Tulis csource terjana untuk fail dinyatakan
   --static          Jana simbol statik
   --strip-comments  Buang ulasan dari fail XML
   --strip-content   Buang kandungan nod dari fail XML
   --working-directory DIRECTORY       Direktori kerja lalai untuk aplikasi bila
                                      guna pilihan --launch.   -?, --help                          Cetak mesej bantuan ini kemudian keluar   -V, --version                       Cetak maklumat versi kemudian keluar   -V, --version     Cetak maklumat versi kemudian keluar
   -h, --help        Cetak mesej bantuan kemudian keluar
   WebBrowser       - Pelayar sesawang digemari.
  MailReader       - Pembaca Mel digemari.
  FileManager      - Pengurus Fail digemari.
  TerminalEmulator - Emulator Terminal digemari. %s (Xfce %s)

Hakcipta (c) 2003-2006
        os-cillation e.K. Semua hak terpelihara.

Ditulis oleh Benedikt Meurer <benny@xfce.org>.

Dibina dengan Gtk+-%d.%d.%d, menjalankan Gtk+-%d.%d.%d.

Sila laporkan pepijat kepada <%s>.
 %s disertakan TANPA JAMINAN,
Anda boleh mengedar semula salinan %s dibawah terma
GNU Lesser General Public License yang dapat ditemui dalam
pakej sumber %s.

 Ikon Tindakan Aktif Warna sempadan item aktif Warna isian item aktif Indeks item aktif Warna teks item aktif Semua Fail Semua Ikon Animasi Butang Pemilih Aplikasi Ikon Aplikasi Balsa Peranti Sekat Brave Layar sistem fail Layar sistem fail untuk memilih perintah suai. Layar sesawang _Ulasan: C_ipta Pengurus Fail Caja Peranti Aksara Pilih Aplikasi Digemari Pilih Pengurus Fail suai Pilih Pembaca Mel suai Pilih Emulator Terminal suai Pilih Pelayar Sesawang suai Pilih nama fail Chromium Mel Claws Kosongkan medan gelintar Penjarakan Lajur Penjarakan lajur _Perintah: Hakcipta (c) %s
        os-cillation e.K. Semua hak terpelihara.

Ditulis oleh Benedikt Meurer <benny@xfce.org>.

 Cipta Direktori Cipta Pelancar Cipta Pelancar <b>%s</b> Cipta Pautan Cipta fail dekstop baharu dalam direktori yang diberi Warna sempadan item kursor Warna isian item kursor Warna teks item kursor Suaikan Palang Alat... Pelayar Serasi Debian Emulator Terminal X Debian Ikon Peranti Dillo _Jangan tunjuk mesej ini lagi Seret satu item ke dalam palang alat diatas untuk menambahnya, dari palang alat dalam jadual item untuk membuangnya. Sunting Direktori Sunting Pelancar Sunting Pautan Lambang Emotikon Benarkan Gelintar Encompass Emulator Terminal Enlightened Pelayar Sesawang Epiphany Evolution Fail Boleh Laksana FIFO Gagal mencipta "%s". Gagal lakukan Pengurus Fail lalai Gagal lakukan Pembaca Mel lalai Gagal lakukan Emulator Terminal lalai Gagal lakukan Pelayar Sesawang lalai Gagal untuk melancarkan aplikasi digemari untuk kategori "%s". Gagal memuatkan kandungan dari "%s": %s Gagal memuatkan imej "%s": Sebab tidak diketahui, berkemungkinan fail imej rosak Gagal membuka "%s". Gagal membuka %s untuk ditulis Gagal membuka URI "%s". Gagal membuka paparan Gagal membuka fail "%s": %s Gagal menghurai kandungan "%s": %s Gagal membaca fail "%s": %s Gagal menyimpan "%s". Gagal tetapkan Pengurus Fail lalai Gagal tetapkan Pembaca Mel lalai Gagal tetapkan Emulator Terminal lalai Gagal tetapkan Pelayar Sesawang lalai Fail "%s" tidak mempunyai kunci jenis Pengurus Fail Ikon Jenis Fail Lokasi fail bukanlah fail atau direktori nalar Folder Ikut keadaan GIcon Terminal GNOME Pelayar Sesawang Galeon Geary Google Chrome Seragam Bagaimana teks dan ikon bagi setiap item diletak relatif diantara satu sama lain Icecat Icedove Iceweasel Ikon Model Palang Ikon Model Paparan Ikon Lajur ikon Jika anda tidak nyatakan pilihan --launch, exo-open akan buka semua URL
dinyatakan dengan pengendali URL digemari mereka. Melainkan, jika anda
nyatakan pilihan --launch, anda boleh pilih aplikasi digemari yang hendak
dijalankan, dan lulusi parameter tambahan ke aplikasi (iaitu TerminalEmulator
anda boleh lulusi beris perintah yang patut dijalankan dalam terminal). Fail Imej Jenis pembantu "%s" tidak sah Jumanji KMail Pelayar Sesawang Konqueror Lancar pembantu lalai TYPE dengan PARAMETER pilihan, yang mana TYPE adalah salah satu dari nilai berikut. Pelancaran fail desktop tidak disokong bila %s dikompil tanpa fitur GIO-Unix. Mod bentangan Pelayar Teks Links Ikon Lokasi Pelayar Teks Lynx Pembaca Mel Margin Lajur tandaan Ikon Menu Midori Lajur model untuk gelintar bila menggelintar menerusi item Lajur model yang digunakan untuk memperoleh laluan mutlak fail imej yang diterap Lajur model yang digunakan untuk mendapatkan ikon pixbuf Lajur model yang digunakan untuk dapatkan teks Lajur model yang digunakan untuk mendapatkan teks jika guna penanda Pango Model untuk palang ikon Pelayar Mozilla Mozilla Firefox Mel Mozilla Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Tiada aplikasi dipilih Tiada perintah dinyatakan Tiada fail dipilih Tiada fail/folder dinyatakan Tiada pembantu ditakrif untuk "%s" Tiada ikon Bilangan lajur Bilangan lajur untuk dipapar Buka dialog konfigurasi Aplikasi 
Digemari Pelayar Opera Pilihan: Orientasi Pengurus Fail PCMan Pengurus Fail PCManFM-Qt Skrip Perl Lajur Pixbuf Sila pilih Pengurus Fail digemari anda
sekarang dan klik OK untuk teruskan. Sila pilih Pembaca Mel digemari anda
sekarang dan klik OK untuk teruskan. Sila pilih Emulator Terminal digemari anda
sekarang dan klik OK untuk teruskan. Sila pilih Pelayar Sesawang digemari
anda sekarang dan klik OK untuk
teruskan. Sila laporkan pepijat ke <%s>.
 Aplikasi Digemari Aplikasi Digemari (Pelayar Sesawang, Pembaca Mel dan Emulator Terminal) URL praset ketika mencipta pautan Perintah praset ketika mencipta pelancar Ulasan praset ketika mencipta fail desktop Ikon praset ketika mencipta fail desktop Nama praset ketika mencipta fail desktop Tekan butang tetikus kiri untuk ubah aplikasi terpilih. Pratonton Papar maklumat versi dan keluar Skrip Python QTerminal Tanya pembantu lalai TYPE, yang mana TYPE adalah salah satu dari nilai berikut. QupZilla ROX-Filer RXVT Unicode Baca emel anda Terap secara berlainan berdasarkan keadaan pemilihan. Boleh Ditertib Semula Perintah mula semula Pengurus Fail Rodent Penjarakan Baris Penjarakan baris Skrip Ruby Jalan dalam _terminal SOCKET ID Sakura Lajur Gelintar Pilih _ikon dari: Pilih satu direktori kerja Pilih Aplikasi Pilih satu ikon Pilih aplikasi Pilih aplikasi lalai untuk pelbagai perkhidmatan Pilih pilihan ini untuk benarkan pemberitahuan permulaan bila perintah dijalankan dari pengurus fail atau menu. Tidak semua aplikasi menyokong pemberitahuan permulaan. Pilih pilihan ini untuk jalankan perintah dalam tetingkap terminal. Mod pemilihan Pemisah Perintah mula semula sesi Soket pengurus tetapan Skrip Shell Satu Klik Had Masa Tamat Satu Klik Soket Jarak yang disisip pada pinggir paparan ikon Jarak yang disisip diantara sel sesebuah item Jarak yang disisip diantara lajur grid Jarak yang disisip diantara baris grid Jarak Nyatakan aplikasi yang anda mahu guna
sebagai Pengurus Fail Lalai untuk Xfce: Nyatakan aplikasi yang anda mahu guna
sebagai Pembaca Mel Lalai untuk Xfce: Nyatakan aplikasi yang anda mahu guna
sebagai Emulator Terminal Lalai untuk Xfce: Nyatakan aplikasi yang anda mahu guna
sebagai Pelayar Sesawang Lalai untuk Xfce: Ikon Status Ikon Stok Surf Sylpheed TYPE [PARAMETER] Emulator Terminal Terminator Lajur teks Teks untuk Ikon P_enting Teks untuk Semu_a Ikon GIcon untuk diterap. Amaun jarak diantara dua lajur yang berturutan Amaun jarak diantara dua baris berturutan Amaun masa selepas item dibawah kursor tetikus akan dipilih secara automatik dalam mod satu klik Fail "%s" tidak mengandungi data TYPE berikut disokong untuk perintah --launch dan --querry:

  WebBrowser       - Pelayar sesawang digemari.
  MailReader       - Pembaca Mel digemari.
  FileManager      - Pengurus Fail digemari.
  TerminalEmulator - Emulator Terminal digemari. TYPE berikut disokong untuk perintah --launch: Ikon untuk diterap. Mod bentangan Model untuk paparan ikon Orientasi palang ikon Pengurus Fail digemari akan digunakan untuk layari kandungan folder. Pembaca Mel digemari akan digunakan untuk gubah emel bila anda klik pada alamat emel. Emulator Terminal digemari akan digunakan untuk jalankan perintah yang memerlukan persekitaran CLI. Pelayar Sesawang digemari akan digunakan untuk buka pautan dan papar kandungan bantuan. Mod pemilihan Saiz ikon unuk diterap dalam piksel. Lebar yang digunakan untuk setiap item Thunar _Gaya Palang Alat Taip '%s --help' untuk penggunaan. Jenis fail desktop yang dicipta (Aplikasi atau Pautan) Tidak boleh kesan skema-URI "%s". Ikon Tidak Dikenali Jenis fail desktop "%s" tidak disokong Penggunaan: %s [options] [file]
 Penggunaan: exo-open [URLs...] Guna pemberitahuan pe_rmulaan Guna aplikasi suai yang tidak tersedia dari senarai di atas. Guna perintah suai Lihat pengguna yang dibenarkan gelintar menerus lajur secara interaktif Paparan boleh ditertib semula Vimprobable2 Pelayar Teks W3M Pelayar Sesawang Sama ada anak seharusnya semua bersaiz serupa Sama ada item dalam paparan boleh diaktifkan denga satu klik Lebar untuk setiap item Kumpulan tetingkap Ketua kumpulan tetingkap _Direktori Kerja: Terminal X Terminal Xfce Pengurus Fail Xfe [FAIL|FOLDER] T_ambah palang alat baharu _Batal _Tutup Lalai _Desktop Ba_ntuan _Ikon: _Ikon sahaja _Internet _Nama: _OK B_uka _Lain-lain... _Buang Palang Alat _Simpan _Gelintar ikon: _Teks sahaja _URL: _Utiliti aterm qtFM saiz 