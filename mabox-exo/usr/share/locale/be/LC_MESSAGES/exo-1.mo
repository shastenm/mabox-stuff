��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  I   &A  7   pA  ?   �A  ?   �A  Z  (B  Q   �C  c   �C  K   9D  L   �D  W   �D  �   *E  ^   �E  m   HF  \   �F  M   G    aG  $  �H  2  �I     �J     �J  7   K  =   >K  0   |K  ;   �K     �K     �K     L  *    L     KL     gL     mL     �L  .   �L  c   �L  &   $M     KM     ^M  "   pM  #   �M  2   �M  ,   �M  .   N  0   FN  "   wN  "   �N     �N  
   �N  &   �N     �N     O     8O  �   EO     �O  '   �O  3   P  !   CP  k   eP  "   �P  (   �P  &   Q  "   DQ     gQ     Q     �Q     �Q  G   �Q  �   R  #   �R  -   �R  %   S     =S     LS     _S  	   {S     �S     �S  	   �S      �S     �S  0   �S  e   #T  k   �T  k   �T  ]   aU  r   �U  F   2V  �   yV  .   W  8   7W  2   pW  7   �W  :   �W  H   X  >   _X  )   �X  m   �X  m   6Y  m   �Y  _   Z  9   rZ     �Z  $   �Z  D   �Z     4[     C[     ][     c[     z[     �[     �[     �[  l   �[     -\     4\  	   <\     F\  (   S\  6   |\     �\  �  �\     W_  <   k_     �_     �_     �_  �   �_  �   �`  %   na  %   �a  #   �a  %   �a  !   b     &b     /b     Mb     cb  k   jb  �   �b  o   gc  h   �c  �   @d  /   �d     e      e     0e     =e     Qe     Ve     ]e     fe  $   ye  "   �e     �e  :   �e  G   f  !   _f  !   �f  @   �f  J   �f     /g     Kg     _g  #   vg  (   �g     �g     �g  �   �g  �   zh  �   i  �   �i  O   +j  %   {j  �   �j  I   'k  Q   qk  a   �k  c   %l  a   �l  f   �l  !   Rm  G   tm     �m  	   �m  �   �m     �n  	   �n     �n  .   �n  <   (o  /   eo  !   �o  $   �o     �o     �o     p  )   ,p  	   Vp     `p     gp     �p  ,   �p     �p     �p     q  Y   %q  �   q  n   ur     �r     s  .   s  (   Js     ss  %   �s  4   �s  
   �s  1   �s  ,    t  ,   Mt  *   zt     �t  �   �t  �   mu  �   &v  �   �v     �w  !   �w     �w     �w     �w  #   �w  
   x     #x  .   Ax  *   px  -   �x  ?   �x  =   	y  �   Gy  ,   z  �  1z  S   �{  '   |  %   ;|  6   a|  0   �|  �   �|  �   a}  �   0~  �   �~     �  M   �  3   �     5�     <�  5   U�  f   ��  <   �  &   /�  W   V�  =   ��  ,   �  H   �  m   b�  6   Ђ  c   �  J   k�     ��  #   Ã     �  L   ��  a   J�  ,   ��     ل  '   �  !   �  
   7�     B�  "   X�     {�  %   ��     ��     υ  5   ߅     �     %�     4�     O�     a�     n�     z�     ��     ��     ��     ʆ     �     ��     �     �     �  
    �                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-08 07:58+0000
Last-Translator: Zmicer Turok <nashtlumach@gmail.com>
Language-Team: Belarusian (http://www.transifex.com/xfce/exo/language/be/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: be
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
        %s [параметры] --build-list [[назва файла]...]
        exo-open --launch ТЫП [ПАРАМЕТРы...]   --build-list      Разбор пар (назва, файл)
   --extern          Генераваць сімвалы extern
  --launch ТЫП [ПАРАМЕТРы...]       запусціць пераважную праграму
                                      абранага тыпу з неабавязковымі ПАРАМЕТРАМі, дзе
                                      тып можа быць адным з наступных значэнняў.   --name=identifier назва пераменнай альбо функцыі С
   --output=filename Запісаць згенераваны csource у зададзены файл
   --static          Генераваць статычныя сімвалы
   --strip-comments  Выдаліць каментары з файлаў XML
   --strip-content   Выдаліць змесціва вузлоў з файлаў XML
   --working-directory DIRECTORY       Працоўны каталог для праграм
                                      пры выкарыстанні параметра --launch.   -?, --help                          Вывесці гэтую даведку і выйсці   -V, --version                       Вывесці інфармацыю пра версію і выйсці   -V, --version     Вывесці інфармацыю пра версію і выйсці
   -h, --help        Вывесці гэтую даведку і выйсці
   WebBrowser       - Пераважны вэб-браўзер.
  MailReader       - Пераважная паштовая праграма.
  FileManager      - Пераважны кіраўнік файлаў.
  TerminalEmulator - Пераважны эмулятар тэрмінала. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Усе правы абароненыя.

Аўтар: Benedikt Meurer <benny@xfce.org>.

Сабрана з Gtk+-%d.%d.%d, выкарыстоўваецца Gtk+-%d.%d.%d.

Пра памылкі паведамляйце на <%s>.
 %s распаўсюджваецца без АНІЯКІХ ГАРАНТЫЙ,
дазволена распаўсюджванне копій %s на ўмовах
GNU Lesser General Public License, поўны тэкст ліцэнзіі
знаходзіцца ў пакунку зыходнага коду %s.

 Значкі дзеянняў Актыўна Колер мяжы актыўнага элемента Колер заліўкі актыўнага элемента Індэкс актыўнага элемента Колер тэксту актыўнага элемента Усе файлы Усе значкі Анімацыі Кнопка выбару элемента Значкі праграм Balsa Блочная прылада Brave Прагляд файлавай сістэмы Агляд файлавай сістэмы для пошуку неабходнага загаду. Праглядзець у сеціве Ка_ментар: Ст_варыць Кіраўнік файлаў Caja Сімвальная прылада Абраць пераважную праграму Абраць кіраўніка файлаў Абраць паштовую праграму Абраць эмулятар тэрмінала Абраць вэб-браўзер Абраць назву файла Chromium Claws Mail Ачысціць поле пошуку Водступы слупкоў Водступы слупкоў За_гад: Copyright (c) %s
        os-cillation e.K. Усе правы абаронены.

Аўтар: Benedikt Meurer <benny@xfce.org>.

 Стварыць каталог Стврыць запускальнік Стварыць запускальнік <b>%s</b> Стварыць спасылку Стварыць новы файл працоўнага стала ў зададзеным каталозе Колер мяжы курсора Колер заліўкі курсора Колер тэксту курсора Наладзіць панэль... Debian Sensible Browser Debian X Terminal Emulator Значкі прылад Dillo Больш _не паказваць гэтае паведамленне Каб дадаць, перацягніце элемент на панэль. Каб выдаліць, перацягніце элемент з панэлі ў табліцу элементаў. Рэдагаваць каталог Рэдагаваць запускальнік Рэдагаваць спасылку Эмблемы Эмаціконы Уключыць пошук Encompass Enlightened Terminal Emulator Вэб-браўзер Epiphany Evolution Файлы для запуску FIFO Не атрымалася стварыць "%s". Не атрымалася запусціць прадвызначаны кіраўнік файлаў Не атрымалася запусціць прадвызначаную паштовую праграму Не атрымалася запусціць прадвызначаны эмулятар тэрмінала Не атрымалася запусціць прадвызначаны вэб-браўзер Не атрымалася запусціць пераважную праграму для катэгорыі  "%s" Не атрымалася загрузіць змесціва "%s": %s Не атрымалася загрузіць выяву "%s": невядомая прычына. Магчыма, файл пашкоджаны Не атрымалася адкрыць "%s". Немагчыма адкрыць %s для запісу Не атрымалася адкрыць URL "%s". Не атрымалася адкрыць дысплей Не атрымалася адкрыць файл "%s": %s Не атрымалася распазнаць змесціва "%s": %s Не атрымалася прачытаць файл "%s": %s Немагчыма захаваць "%s". Не атрымалася прызначыць прадвызначанага кіраўніка файлаў Не атрымалася прызначыць прадвызначаную паштовую праграму Не атрымалася прызначыць прадвызначаны эмулятар тэрмінала Не атрымалася прызначыць прадвызначаны вэб-браўзер У файле "%s" адсутнічае ключ тыпу Кіраўнік файлаў Значкі тыпаў файлаў Шлях не ўказвае на файл альбо каталог Каталог Улічваць стан GIcon Тэрмінал GNOME Вэб-браўзер Galeon Geary Google Chrome Аднастайны Як тэкст і значок будуць размяшчацца адносна адзін другога Icecat Icedove Iceweasel Значок Мадэль панэлі значкоў Мадэль адлюстравання значкоў Слупок значкоў Калі вы не зададзіце параметр --launch, exo-open адкрые ўсе ўведзеныя
адрасы URL с дапамогай пераважных апрацоўшчыкаў. Калі вы
зададзіце параметр --launch, то зможаце абраць праграмы, якія
мусяць быць запушчаныя, і адправіць дадатковыя параметры гэтым
праграмам (напрыклад, вы можаце адправіць эмулятару тэрмінала
загад, які павінен быць запушчаны ў загадным радку). Файлы выяў Хібны тып дапаможнай праграмы "%s" Jumanji KMail Вэб-браўзер Konqueror Запусціць прадвызначаную дапаможную праграму для абранага ТЫПу c неабавязковым ПАРАМЕТРам, дзе ТЫП можа быць адным з наступных значэнняў. Запуск файлаў на працоўным стале немагчымы калі %s скампіляваны без падтрымкі GIO-Unix. Рэжым адлюстравання Тэкставы браўзер Links Значкі размяшчэння Тэкставы браўзер Links Паштовая праграма Поле Слупок разметкі Значкі меню Midori Слупок мадэлі, дзе адбываецца пошук пры пошуку ў элементах Слупок мадэлі, выкарыстоўваемы для атрымання абсалютнага шляху да файла выявы Слупок мадэлі,што выкарыстоўваецца для атрымання значка pixbuf Слупок мадэлі,што выкарыстоўваецца для атрымання тэксту Слупок мадэлі,што выкарыстоўваецца для атрымання тэксту пры выкарыстанні разметкі Pango Мадэль для панэлі значкоў Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Праграма не абраная Загад не вызначаны Файл не абраны Файл альбо каталог не вызначаны Дапаможная праграма для "%s" адсутнічае. Значок адсутнічае Колькасць слупкоў Колькасць адлюстроўваемых слупкоў Адкрыць акно наладкі
пераважных праграм Вэб-браўзер Opera Параметры: Размяшчэнне Кіраўнік файлаў PCMan Кіраўнік файлаў PCManFM-Qt Скрыпты Perl Слупок Pixbuf Калі ласка, абярыце пераважны кіраўнік файлаў
і націсніце "Добра" для працягу. Калі ласка, абярыце пераважную паштовую праграму
і націсніце "Добра" для працягу. Калі ласка, абярыце пераважны эмулятар тэрмінала
і націсніце "Добра" для працягу. Калі ласка, абярыце пераважны вэб-браўзер
і націсніце "Добра" для працягу. Калі ласка, паведамляйце пра памылкі на <%s>.
 Пераважныя праграмы Пераважныя праграмы (Вэб-браўзер, паштовая праграма, эмулятар тэрмінала) Прызначыць URL-адрас ствараемай спасылкі Прызначыць загад ствараемага запускальніка Прызначыць загад ствараемага файла працоўнага стала Прызначыць значок ствараемага файла працоўнага стала Прызначыць назву ствараемага файла працоўнага стала Націсніце левую кнопку мышы для змены абранай праграмы. Папярэдні прагляд Вывесці інфармацыю пра версію і выйсці Скрыпты Python QTerminal Запытаць прадвызначаную дапаможную праграму для абранага ТЫПу c неабавязковым ПАРАМЕТРам, дзе ТЫП можа быць адным з наступных значэнняў. QupZilla ROX-Filer RXVT Unicode Чытаць электронную пошту Адлюстраванне залежыць ад стану. Магчыма пераўпарадкаваць Загад перазапуску Кіраўнік файлаў Rodent Водступы радкоў Водступы радкоў Скрыпты Ruby Запускаць у _тэрмінале SOCKET ID Sakura Слупок пошуку Абраць _значок з: Абраць працоўны каталог Абярыце праграму Абраць значок Абраць праграму Абраць прадвызначаныя праграмы для пэўных задач Абярыце параметр, каб уключыць інфармаванне пра запуск праграм у кіраўніку файлаў ці праз меню. Не кожная праграма гэта падтрымлівае. Абярыце гэты параметр, каб выконваць загад у акне тэрмінала. Рэжым вылучэння Падзяляльнік Загад перазапуску сеансу Сокет кіраўніка налад Скрыпты Shell Адзіночная пстрычка Таймаўт адзіночнай пстрычкі Сокет Водступы ад вуглоў значкоў Прамежак паміж ячэйкамі Прамежак паміж слупкамі Водступы паміж радкамі Прастора Прызначце прадвызначаную праграму, якую вы хочаце выкарыстоўваць
у якасці кіраўніка файлаў для Xfce: Прызначце прадвызначаную праграму, якую вы хочаце выкарыстоўваць
у якасці паштовай праграмы для Xfce: Прызначце прадвызначаную праграму, якую вы хочаце выкарыстоўваць
у якасці эмулятара тэрмінала для Xfce: Прызначце прадвызначаную праграму, якую вы хочаце выкарыстоўваць
у якасці вэб-браўзера для Xfce: Значкі стану Стандартны значок Surf Sylpheed ТЫП [ПАРАМЕТР] Эмулятар тэрмінала Terminator Тэкставы слупок Тэкст для _важных значкоў Тэкст для _ўсіх значкоў Значок GIcon для адмалёўкі. Прастора паміж суседнімі слупкамі Прастора паміж суседнімі радкамі Прамежак часу ў рэжыме адзіночнай пстрычкі, пасля якога элемент пад курсорам будзе абраны аўтаматычна Файл "%s" не змяшчае даных Загадамі --launch і --query commands:

 падтрымліваюцца наступныя ТЫПы:

  WebBrowser       - Пераважны вэб-браўзер.
  MailReader       - Пераважная паштовая праграма.
  FileManager      - Пераважны кіраўнік файлаў.
  TerminalEmulator - Пераважны эмулятар тэрмінала. Загадам --launch падтрымліваюцца наступныя ТЫПы: Значок для адмалёўкі. Рэжым адлюстравання Мадэль адлюстравання значкоў Арыентацыя панэлі значкоў Пераважны кіраўнік файлаў будзе выкарыстоўвацца для прагляду змесціва каталогаў. Пераважны паштовы кліент будзе выкарыстоўвацца для напісання лістоў пры націсканні на адрас электроннай пошты. Пераважны эмулятар тэрмінала будзе выкарыстоўвацца для запуску загадаў, якім патрабуецца асяроддзе CLI. Пераважны вэб-браўзер будзе выкарыстоўвацца для адкрыцця гіперспасылак і паказу даведкі. Рэжым вылучэння Памер адлюстроўваемага значка ў пікселях. Шырыня для кожнага элемента Thunar Стыль _панэлі Увядзіце "%s --help" для дапамогі. Тып ствараемага файла працоўнага стала (Application альбо Link) Не атрымалася выявіць URI-scheme у "%s". Значкі без катэгорыі Непадтрымліваемы тып файла працоўнага стала "%s" Выкарыстанне: %s [параметры] [файл]
 Выкарыстанне: exo-open [URLs...] Выкарыстоўваць _апавяшчэнне пра запуск Выкарыстоўваць адвольную праграму, якой няма ў гэтым спісе. Выкарыстоўваць загадны радок Дазволіць карыстальніку інтэрактыўны пошук у слупках Адлюстраванне магчыма пераўпарадкаваць Vimprobable2 Тэкставы браўзер W3M Вэб-браўзер Ці мусяць усе нашчадкі быць аднаго памеру Ці мусяць элементы актывавацца адзіночнай пстрычкай Шырыня кожнага элемента Група акон Галоўнае акно ў групе Працоўны _каталог: X Terminal Тэрмінал Xfce Кіраўнік файлаў Xfce [ФАЙЛ|КАТАЛОГ] _Дадаць новую панэль _Скасаваць _Закрыць _Прадвызначаны працоўны стол _Даведка _Значок: _Толькі значкі _Інтэрнэт _Назва: _Добра _Адкрыць _Іншае... _Выдаліць панэль _Захаваць _Шукаць значок: _Толькі тэкст _URL: _Утыліты aterm qtFM памер 