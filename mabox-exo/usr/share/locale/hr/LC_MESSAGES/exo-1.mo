��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  -   �@  $   A  -   0A  $   ^A  �   �A  ,   B  F   ?B  %   �B  2   �B  :   �B  Y   C  .   tC  4   �C  :   �C  0   D  �   DD  �   �D  �   �E     vF     �F     �F     �F     �F     �F     �F  	   �F  	   G     G     %G     4G     :G     PG     VG  ;   rG     �G  
   �G     �G     �G     �G     �G  ,   H  #   CH  )   gH  #   �H     �H     �H     �H     �H     �H     I  	   I  g    I     �I     �I     �I     �I  5   �I     J     0J     OJ     nJ     �J     �J     �J     �J     �J  b   �J  
   YK     dK     tK     �K  	   �K     �K  	   �K     �K     �K  	   �K     �K     L     L  3   ,L  ,   `L  0   �L  ,   �L  ;   �L  .   'M  R   VM     �M  !   �M     �M     N  '   #N  *   KN  &   vN     �N  5   �N  .   �N  2   O  .   OO     ~O     �O     �O  4   �O     �O     P     P     P     +P     AP     GP     UP  <   ^P     �P     �P  	   �P     �P     �P     �P     �P  f  �P     TR      ZR     {R     �R     �R  t   �R  d   S     |S     �S     �S     �S     �S     �S     �S     �S     T  @   T  V   PT  '   �T      �T  H   �T     9U     NU     aU     qU     �U     �U     �U     �U     �U     �U     �U     �U     V  "   #V  
   FV     QV     ^V  1   uV     �V     �V     �V     �V      �V     
W     W  W   %W  Q   }W  P   �W  K    X  "   lX     �X  G   �X  (   �X  .   Y  ?   DY  =   �Y  @   �Y  :   Z     >Z  &   FZ     mZ  	   |Z  \   �Z     �Z  	   �Z     �Z     [  "   [     :[     U[     n[     �[     �[     �[     �[  	   �[     �[     �[     �[     �[     \     *\     :\  )   J\  �   t\  ?   ']     g]  
   v]     �]     �]     �]     �]  )   �]     	^  +   ^  &   C^  &   j^  %   �^     �^  Q   �^  K   _  N   \_  L   �_     �_     `     `     `     $`     4`  
   G`     R`     ``     w`     �`  "   �`     �`  r   �`      Na  �   oa  3   ]b     �b     �b     �b     �b  F   �b  `   )c  n   �c  a   �c     [d  %   jd  '   �d     �d     �d      �d  >   �d  +   2e     ^e  /   ue  !   �e     �e     �e  ?   �e     ?f  >   Wf  #   �f     �f     �f     �f  ,   �f  ;   g     Yg     ng     |g     �g  
   �g     �g     �g     �g     �g     h     h     h     ,h     4h     <h  	   Hh     Rh     Xh     `h  
   hh     sh     �h     �h     �h     �h     �h     �h     �h  	   �h                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-09-27 04:52+0000
Last-Translator: Ivica  Kolić <ikoli@yahoo.com>
Language-Team: Croatian (http://www.transifex.com/xfce/exo/language/hr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 %s [opcije] --build-list [[ime datoteka]...]
 exo-open --launch TIP [PARAMETAR...] --build-list Parsiraj (ime, datoteka) parove
 --extern Generiraj eksterne simbole
 --launch TIP [PARAMETAR...] Pokreni preferiranu aplikaciju TIP 
s neobaveznim parametrima PARAMETAR, gdje je
TIP jedna od idućih vrijednosti. --name=identifikator C ime makroa/varijable
   --output=filename Piše generirani csource u specificiranu datoteku
 --static Generiraj statične simbole
 --strip-comments Ukloni komentare iz XML datoteka
 --strip-content Obriši sadržaj čvorova iz XML datoteka
 --working-directory MAPA Zadana radna mapa za aplikacije
kada se koristi opcija --launch. -?, --help Ispiši ovu poruku pomoći i izađi -V, --version Ispiši informacije o verziji i izađi   -V, --version    Ispiši informacije o verziji i izađi
   -h, --help        Ispiši ovu poruku i izađi
 Webpreglednik - Zadani Web preglednik.
 Čitačpošte - Zadani čitač pošte.
 Upravitelj datotekama - Zadani upravitelj datotekama.
 Emulatorterminala - Zadani emulator terminala %s (Xfce %s)

Copyright (c) 2003-2006
os-cillation e.K. Sva prava zadržana.

Napisao Benedikt Meurer <benny@xfce.org>.

Izgrađeno pomoću Gtk+-%d.%d.%d, pokreće Gtk+-%d.%d.%d.

Molimo prijavite greške na <%s>.
 %s dolazi BEZ APSOLUTNO IKAKVOG JAMSTVA,
Smijete redistribuirati %s pod uvjetima
GNU Lesser General Public License koju možete pronaći u 
paketu izvornog koda %s.
 Ikone radnje Aktivno Boja ruba aktivne stavke Active item fill color Indeks aktivne stavke  Boja teksta aktivne stavke Sve datoteke Sve Ikone Animacije Dugme birača programa Ikone programa Balsa Uređaj za blokiranje Brave Pregledaj datotečni sustav Pregledaj datotečni sustav za odabir prilagođene naredbe. Pregledaj web K_omentar: I_zradi Caja upravitelj datotekama Znakovni uređaj Izaberite željeni program Odaberi prilagođenog upravitelja datotekama Odaberi prilagođeni čitač pošte Izaberite prilagođeni emulator terminala Odaberi prilagođeni web preglednik Odaberi ime datoteke Chromium Claws pošta Očisti polje traženja Razmak stupaca Razmak stupaca Na_redba: Copyright (c) %s
 os-cillation e.K. Sva prava pridržana..

Napisao Benedikt Meurer <benny@xfce.org>.

 Napravi direktorij Napravi pokretač Napravi pokretač <b>%s</b> Napravi poveznicu Napravi novu datoteku za radnu površinu u danoj mapi Boja obruba stavke pokazivača Boja ispune stavke pokazivača Boja teksta stavke pokazivača Prilagodi alatnu traku... Debian osjetljivi preglednik Debian X emulator terminala Ikone uređaja Dillo Ne _prikazuj ponovno ovu poruku Povucite stavku na alatnu traku kako biste ju dodali, iz alatne trake među stavke za obrisati ju. Uredi mapu Uredi pokretač Uredi poveznicu Amblemi Emotikone Omogući pretraživanje Encompass Enlightened emulator terminala Epiphany web preglednik Evolution Izvršne datoteke FIFO Neuspjelo stvaranje  "%s".  Neuspjelo izvešenje zadanog upravitelja datotekama Neuspjelo izvršenje zadanog čitača pošte Neuspjelo izvršenje zadanog emulatora terminala Neuspjelo izvršenje zadanog Web preglednika Neuspjelo pokretanje željenog programa za kategoriju "%s". Neuspješno učitavanje sadržaja iz  "%s": %s Neuspješno učitavanje slike  "%s": Nepoznati razlog, vjerojatno oštećena slika Neuspješno otvaranje "%s". Neuspjelo otvaranje %s za pisanje Nisam uspio otvoriti URI "% s". Neuspjelo otvaranje zaslona Neuspješno otvaranje datoteke "%s": %s Neuspješno čitanje sadržaja iz "%s": %s Neuspješno čitanje datoteke "%s": %s Neuspjelo spremanje "%s". Neuspjelo postavljanje zadanog upravitelja datotekama Neuspjelo postavljanje zadanog čitača pošte Neuspjelo postavljanje zadanog emulatora terminala Neuspjelo postavljanje zadanog web preglednika Datoteka "%s" nema ključ tipa Upravitelj datoteka Ikone tipova datoteka Mjesto datoteke nije obična datoteka ili direktorij Mapa Pratite stanje GIcon ikona GNOME terminal Galeon Web preglednik Geary Google Chrome Homogeno Pozicija teksta i ikona svake stavke u odnosu jedne na drugu Icecat Icedove Iceweasel Ikona Model trake ikona Model pregled ikona Stupac ikona Ako ne zatražite --launch opciju, exo-open će otvoriti sve dane URL-ove
s njihovim zadanim URL rukovateljima. Inače, zatražite li --launch opciju,
možete odabrati koju zadanu aplikaciju želite pokrenuti, i proslijediti
dodatne parametre aplikaciji (npr. za TerminalEmulator, možete 
proslijediti retke naredbi koje bi se trebale pokrenuti u terminalu) Slike Pogrešan tip pomoćnika za "%s" Jumanji KMail Konqueror web preglednik Pokreni uobičajenog pomoćnika za TIP s neobaveznim parametrom PARAMETAR, gdje je TIP jedna od idućih vrijednosti. Pokretanje datoteka na radnoj površini nije podržano kad je %s kompajliran bez GIO-Unix značajki. Način rasporeda Links tekstualni preglednik Ikone lokacije Lynx tekstualni preglednik Čitač pošte Margina Stupac oznake Ikone izbornika Midori Model stupca koji će se pretraživati prilikom traženja stavke Model stupac se koristi za prihvaćanje apsolutnih putanja slikovne datoteke za prikaz Model stupca za uzimanje pixbuf  ikone  Model stupca za uzimanje teksta  Model stupca korišten za izvlačenje teksta ako se koriste Pango oznake Model za traku ikona Mozilla preglednik Mozilla Firefox Mozilla pošta Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nijedan program nije odabran Nije određena naredba Nije odabrana datoteka Nije odabrana datoteka/mapa Niije definiran pomoćnik za "%s". Nema ikone Broj stupaca Broj stupaca za prikaz Otvori konfiguracijski dijalog
željenih programa Opera preglednik Opcije: Orijentacija PCMan upravitelj datotekama PCManFM-Qt upravitelj datotekama Perl skripte Pixbuf stupac Molim izaberite vaš željeni upravitelj datotekama
sada i kliknite U redu za nastavak. Molim izaberite vaš željeni čitač pošte
sada  i kliknite U redu za nastavak. Molim izaberite vaš željeni emulator
terminala  i kliknite U redu za nastavak. Molim izaberite vaš željeni Web
preglednik i kliknite U redu za nastavak. Molimo prijavite bugove na  <%s>.
 Preferirani programi Željeni programi (Web preglednik, čitač pošte i emulator terminala) Unaprijed zadan URL kod izrade poveznice Unaprijed zadana naredba kod izrade pokretača Unaprijed zadan komentar kod izrade datoteke za radnu površinu Unaprijed zadana ikona kod izrade datoteke za radnu površinu Unaprijed zadano ime prilikom izrade datoteke za radnu površinu Pritisni lijevu tipku miša za promjenu odabranog programa Pregled Ispiši informacije o verziji i izađi Python skripte QTerminal Potraži uobičajenog pomoćnika za tip TYPE, gdje je TYPE jedna od slijedećih vrijednosti. QupZilla ROX-Filer RXVT Unicode Čitaj svoju poštu Drugačiji prikaz ovisno o stanju. Promjenljivog redoslijeda   Naredba ponovno pokreni Rodent upravitelj datotekama Razmak redova Razmak redova Ruby skripte Pokreni u _terminalu SOCKET ID Sakura Stupac traženja Odaberi_ikonu iz: Odaberite radni direktorij Odaberite program Odaberite ikonu Odaberi program Odaberite zadane programe za razne usluge Odaberite ovu opciju kako bi se omogućila obavijest pokretanja kada se naredba pokreće iz upravitelja datotekama ili izbornika.Ne podržava svaki program obavijesti pokretanja. Odaberi ovu opciju za pokretanje naredbe u terminalnom prozoru. Način odabira Razdjelnik Naredba  ponovno pokreni sesiju Socket upravljanja postavkama Skripte ljuske Jednostruki klik Vremensko ograničenje jednostrukog klika Utor (socket) Prostor umetnut na rubovima u pogledu ikona Prostor umetnut između ćelija stavke Prostor umetnut između stupaca mreže Prostor umetnut između redova mreže Razmak Odaberite program koju želite koristiti
 kao zadani Upravitelj datoteka za Xfce: Odaberite program koju želite koristiti
 kao zadani email čitač za Xfce: Odredite program koji želite koristiti
kao zadani emulator terminala za Xfce: Odaberite program koji želite koristiti
 kao zadani Web preglednik za Xfce: Ikone statusa Ugrađene ikone Surf Sylpheed TIP [PARAMETAR] Emulator terminala Terminator Stupac teksta Tekst za v_ažne ikone Tekst za _sve ikone GIcon ikona za prikaz. Količina razmaka između 2 stupca Razmak između 2 reda Količina vremena nakon koje će se stavka pod kursorom automatski odabrati u jednostruki-klik način korištenja  Datoteka "%s" ne sadrži podatke Slijedeći TYPEovi su podržani za --launch i --query naredbu:

  WebBrowser - Zadanii Web preglednik.
  MailReader - Zadani preglednik pošte.
  FileManager - Zadani upravitelj datotekama.
  TerminalEmulator - Zadani emulator terminala. Slijedeći TIPovi su podržani za --launch command: Ikona za prikaz. Način rasporeda Model za prikaz ikona Orijentacija trake ikona Zadani upravitelj datoteka će se koristiti za pregled sadržaja mapa. Željeni čitač pošte će se koristiti za pisanje email poruka kada se klikne na email adresu. Željeni emulator terminala će biti korišten za pokretanje naredbi koje zahtjeva okruženje naredbenog retka Zadani Web preglednik će se koristiti kod otvaranja web poveznica i za prikaz sadržaja pomoći. Način odabira Veličina ikone za prikaz u pixelima. Širina koja se koristi za svaku stavku Thunar Stil _alatne trake Upišite '%s --help' za uporabu. Tip datoteke radne površine za izradu (program ili poveznica) Nije moguće detektirati URI-shemu od "%s". Nekategorizirane ikone Nepodržana vrsta datoteke radne površine "%s" Upotreba: %s [opcije] [datoteka]
 Uporaba:  exo-open [URLs...] Koristi _startup obavijest Koristi prilagođeni program koji nije uključen u gornju listu Koristi naredbeni redak Pogled omogućuje korisniku interaktivno traženje kroz stupce Prikaz je promjenljivog redoslijeda Vimprobable2 W3M tekstualni preglednik Web preglednik Hoće li svi elementi biti jednake veličine Hoće li se stavke u prikazu aktivirati jednostrukim klikom Širina svake stavke Grupa prozora Voditelj grupe prozora Radni _direktorij: X Terminal Xfce Terminal Xfe upravitelj datotekama [DATOTEKA|MAPA] _Dodaj novu alatnu traku _Otkaži _Zatvori _Zadana radna površina _Pomoć _Ikona: _Samo ikone _Internet _Ime: _U redu _Otvori _Ostalo... _Ukloni alatnu traku _Spremi _Traži ikonu _Samo tekst _URL: _Uslužni programi aterm qtFM veličina 