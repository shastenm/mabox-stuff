��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  x  1?  2   �@  -   �@  *   A  .   6A  �   eA  -   ;B  7   iB  .   �B  4   �B  4   C  �   :C  I   �C  M   D  <   bD  6   �D  �   �D  �   �E  �   bF     G     G     !G     ;G     UG     lG  
   �G     �G     �G     �G     �G     �G  
   �G     �G     �G  .   �G     )H     :H     FH     NH  	   `H     jH     �H     �H     �H     �H     �H     �H     I     I      I     1I  
   BI  v   MI  	   �I     �I     �I     �I  $   J     (J     @J     XJ     qJ     �J     �J     �J     �J     �J  p   �J     aK     oK     K     �K     �K     �K  	   �K     �K     �K  	   �K     �K     �K     L  (   L  &   EL  ,   lL  %   �L  @   �L  *    M  L   +M     xM  !   �M     �M     �M  "   �M  (   N  %   7N     ]N  '   wN  %   �N  +   �N  $   �N  "   O     9O     FO  +   TO     �O     �O     �O     �O     �O     �O     �O     �O  P   �O     (P     /P  	   7P     AP     FP     ^P     qP  \  }P  
   �Q     �Q     �Q     R     R  ]    R  Y   ~R     �R     �R     �R     S     !S     -S     2S  
   ?S     JS  5   QS  M   �S  #   �S  #   �S  8   T     VT     tT     �T     �T     �T     �T     �T     �T     �T     �T     �T     U     U  "   9U  	   \U     fU     vU  -   �U     �U     �U     �U     �U     �U  
   V     V  >   V  A   ]V  E   �V  ?   �V      %W     FW  B   \W     �W  #   �W  (   �W  #   X  #   ,X  2   PX     �X  '   �X     �X  	   �X  H   �X     Y  	   %Y     /Y  
   <Y  +   GY     sY     �Y     �Y     �Y     �Y  
   �Y     �Y  	   �Y     �Y     �Y     Z     Z     )Z     9Z     FZ  .   SZ  �   �Z  7    [     X[     d[     k[     �[     �[     �[     �[     �[  2   �[  /   \  ,   =\  '   j\  	   �\  E   �\  D   �\  I   ']  B   q]     �]     �]     �]     �]     �]     �]  
   �]     ^     ^     /^     F^  +   _^  (   �^  Y   �^  "   _  �   1_  4   `     M`     d`     r`      �`  F   �`  b   �`  h   Va  b   �a     "b  8   .b     gb     �b     �b  (   �b  5   �b  ,   �b     ,c  $   Fc     kc     �c     �c  7   �c     �c  3   d  !   6d     Xd     ed  	   xd  5   �d  7   �d     �d     	e     e     (e  
   7e     Be     Pe     ae     me     �e     �e     �e     �e     �e     �e  
   �e     �e     �e     �e  
   �e     �e     f     
f     f     "f  	   (f     2f     8f  
   =f                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-24 05:27+0000
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/xfce/exo/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [opsjoner] --build-list [[navn fil]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (navn, fil) par
   --extern          Generer eksterne symboler
   --launch TYPE [PARAMETRE...]        Start det foretrukne programmet av
                                      TYPE med valgfrie PARAMETRE, hvor
                                      TYPE er en av følgende typer.   --name=identifikator C makroo/variabelnavn
 --output=filnavn Skriv generert c-kilde til angitt fil
   --static          Generer statiske symboler
   --strip-comments  Fjern kommentarer fra XML filer
   --strip-content   Fjern nodeinnhold fra XML filer
   --working-directory MAPPE           Standard arbeidsmappe for programmer
                                      ved bruk av --launch opsjonen.   -?, --help                          Skriv ut denne meldingen og avslutt   -V, --version                       Skriv ut versjonsinformasjon og avslutt   -V, --version     Skriv ut versjonsinformasjon og avslutt
   -h, --help        Skriv ut denne hjelpen og avslutt
   WebBrowser       - Foretrukket nettleser.
  MailReader       - Foretrukket epostleser.
 FileManager - Foretrukket filbehandler
 TerminalEmulator - Foretrukket teminalemulator. %s (Xfce %s)

Opphavsrettt (c) 2003-2006
        os-cillation e.K. Alle rettigheter reservert.

Skrevet av Benedikt Meurer <benny@xfce.org>.

Bygget med Gtk+-%d.%d.%d, kjører Gtk+-%d.%d.%d.

Rapporter feil til <%s>.
 %s kommer med ABSOLUTT INGEN GARANTI,
Du kan redistribuere kopier av %s under betingelsene i
GNU Lesser General Public Lisens som ligger i %s kildekodedistribusjonen.

 Handlingsikoner Aktiv Aktivt elements kantfarge Aktivt elements fyllfarge Aktivt elements indeks Aktivt elements tekstfarge Alle filer Alle ikoner Animasjoner Programvelgerknapp Programikoner Balsa Blokkenhet Brave Bla gjennom filsystemet Bla gjennom filsystemet for å velge kommando. Surfe på nettet K_ommentar: _Lag ny Caja Filbehandler Tegnenhet Velg foretrukket program Velg din egen filbehandler Velg din egen e-postleser Velg din egen terminalemulator Velg din egen nettleser Velg filnavn Chromium Claws Epost Tøm søkefelt Kolonnemellomrom Kolonnemellomrom Komm_ando. Copyright (c) %s
        os-cillation e.K. Alle rettigheter reservert.

Skrevet av Benedikt Meurer <benny@xfce.org>.

 Lag mappe Lag ny snarvei Lag ny snarvei <b>%s</b> Lag ny lenke Lag en ny desktop-fil i gitt katalog Pekerelements kantfarge Pekerelements fyllfarge Pekerelements tekstfarge Tilpass verktøylinje ... Debian Sensible Browser Debian X Terminalemulator Enhetsikoner Dillo _Ikke vis denne meldingen igjen Dra et element til verktøylinjen for å legge til, eller fra verktøylinjen til elementlisten for å fjerne det Rediger mappe Rediger snarvei Rediger lenke Emblemer Smileys Tillat søk Encompass Enlightened Terminalemulator Epiphany nettleser Evolution Kjørbare filer FIFO Klarte ikke lage «%s». Klarte ikke starte standard filbehandler Klarte ikke starte standard epostleser Klarte ikke starte standard terminalemulator Klarte ikke starte standard nettleser Klarte ikke å starte foretrukket program for kategorien «%s». Klarte ikke laste innholdet fra «%s»: %s Klarte ikke laste bilde «%s»: Ukjent årsak, antakeligvis er filen skadet. Klarte ikke åpne «%s». Klarte ikke åpne %s for skriving Klarte ikke åpne URI «%s». Klarte ikke åpne skjerm Klarte ikke åpne filen «%s»: %s Klarte ikke tolke innholdet i «%s»: %s Klarte ikke lese fra filen «%s»: %s Klarte ikke lagre «%s». Klarte ikke sette standard filbehandler Klarte ikke sette standard epostleser Klarte ikke sette standard terminalemulator Klarte ikke sette standard nettleser Filen «%s» har ingen typenøkkel Filbehandler Filtypeikoner Filplassering er ikke en fil eller en mappe Mappe Følg tilstand GIkon GNOME terminal Galeon nettleser Geary Google Chrome Homogen Hvordan tekst og ikoner for hvert element er posisjonert i forhold til hverandre Icecat Icedove Iceweasel Ikon Ikonverktøylinjemodell Ikonvisningsmodell Ikonkolonne Hvis du ikke angir --launch opsjonen vil exo-open åpne alle spesifiserte
URL-er med deres foretrukne URL-håndterere. Hvis du spesifierer --launch
opsjonen kan du velge hvilket foretrukket program du vil kjøre og i tillegg
sende parametre til programmet (f.eks. for terminalemulatoren kan du sende
kommandoene som skal kjøres i terminalvinduet). Bildefiler Ugyldig hjelpertype "%s" Jumanji KMail Konqueror nettleser Start standard hjelper av TYPE med valgfritt PARAMETER, hvor TYPE er en av følgende verdier. Oppstart av desktop filer er ikke støttet når %s er kompilert uten GIO-Unix funksjoner. Utseendemodus Links tekstnettleser Plasseringsikoner Lynx tekstnettleser E-postleser Marg Merkekolonne Menyikoner Midori Modellkolonne å søke gjennom ved søk etter element Modellkolonne brukt til å motta absolutt sti til billedfilen som skal tegnes Modellkolonne som pixbuf hentes fra Modellkolonne some tekst hentes fra Modellkolonne brukt for å hente tekst om Pango benyttes Modell for ikonverktøylinjen Mozilla nettleser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ingen program valgt Ingen kommando er angitt Ingen fil er valgt Ingen fil/mappe er angitt Ingen hjelper definert for «%s». Uten ikon Antall kolonner Vis antall kolonner Åpne oppsettsvindu for foretrukne programmer Opera nettleser Valg: Orientering PCMan Filbehandler PCManFM-Qt Filbehandler Perlskript Pixbufkolonne Velg din foretrukne filbehandler
og klikk OK for å fortsette. Velg din foretrukne e-postleser nå
og klikk OK for å fortsette. Velg din foretrukne teminalemulator nå
og klikk OK for å fortsette. Velg din foretrukne nettleser nå
og klikk OK for å fortsette. Rapporter programfeil til <%s>.
 Foretrukne programmer Foretrukne programmer (Nettleser, e-postleser og terminalemulator) Standard URL for nye lenker Standard kommando for nye snarveier Standard kommentar for nye desktop-filer Standard ikon for nye desktop-filer Standard navn for nye desktop-filer Bruk venstre museknapp for å endre valgt program. Forhåndsvisning Skriv ut versjonsinformasjon og avslutt Pythonscript QTerminal Start standardhjelper av TYPE, hvor TYPE er en av følgende verdier:åå QupZilla ROX-Filer RXVT Unicode Les e-post Tegn forskjellig basert på valgt tilstand. Kan forandre rekkefølge Omstartskommando Rodent Filbehandler Radmellomrom Radmellomorom Rubyskript Kjør i _terminal SOKKEL-ID Sakura Søkekolonne Velg _ikon fra: Velg arbeidsmappe Velg et program Velg et ikon Velg program Velg standardprogram for forskjellige oppgaver Velg dette valget for å aktivere oppstartsvarsel når kommandoen kjøres fra filbehandleren eller fra menyen. Ikke alle programmer støtter oppstartsvarsel. Velg denne for å kjøre kommandoen i et terminalvindu. Velgermodus Skille Øktens omstartskommando Innstillingsbehandlersokkel Skallskript Enkeltklikk Tidsgrense for enkeltklikk sokkel Mellomrom som legges langs kantene for ikonvisning Mellomrom som brukes mellom celler i et element Mellomrom som plasseres mellom kolonnegitter Mellomrom benyttet mellom rutenettrader Mellomrom Velg programmet du ønsker å bruke som
standard filbehandler i Xfce: Velg programmet du ønsker å bruke som
standard e-postleser i Xfce: Velg programmet du ønsker å bruke som
standard terminalemulator i Xfce: Velg programmet du ønsker å bruke som
standard nettleser i Xfce: Statusikoner Ymse ikoner Surf Sylpheed TYPE [PARAMETER] Terminalemulator Terminator Tekstkolonne Tekst for _viktige ikoner Tekst for _alle ikoner GIkonet som skal tegnes. Mengden rom mellom to påfølgende kolonner Mengden rom mellom to påfølgende rader Tiden det tar før et element under musepekeren blir valgt automatisk i enkeltklikkmodus. Filen «%s» inneholder ingen data Følgende TYPE-r er støttet for --launch og --query kommandoene:

  WebBrowser       - Ønsket nettleser.
  MailReader       - Ønsket e-postleser.
 FileManager - Ønsket filbehandler.
 TerminalEmulator - Ønsket teminalemulator. Følgende typer er støttet for --launch kommandoen: Ikonet som skal tegnes Utseendemodus Modellen for ikonvisning Orientering av ikonverktøylinje Den foretrukne filbehandleren vil brukes til å utforske mappeinnhold. Den foretrukne e-postleseren vil brukes til å skrive e-poster når du klikker på e-postadresser. Den foretrukne terminalemulatoren vil brukes til å kjøre kommandoer som krever et kommandolinjemiljø. Den foretrukne nettleseren vil brukes til å åpne hyperlenker og til å vise hjelpedokumentasjon. Velgermodus Størrelsen på ikonet som skal tegnes angitt i piksler. Bredde brukt for hvert element Thunar Verktøylinje_stil Skriv '%s --help' for bruksinstruksjoner Type desktop-fil som skal lages (program eller lenke) Klarte ikke detektere URI-schema for «%s». Ikke kategoriserte ikoner Ikke støttet desktop-filtype «%s» Bruk: %s [opsjoner] [fil]
 Bruk: exo-open [url'er...] Bruk _oppstartsvarsel Bruk selvvalgt program som ikke ligger i listen ovenfor Bruk kommandlinjen Tillat bruker å søke gjennom kolonner interaktivt Visning kan forandre rekkefølge  Vimprobable2 W3M tekstnettleser Nettleser Om alle underoppføringer skal være samme størrelse Om elementene i visningen kan aktiveres med enkeltklikk Bredde for hvert element Vindusgruppe Vindusgruppeleder Arbei_dsmappe: X Terminal Xfce Terminal Xfe Filbehandler [FIL|MAPPE] _Legg til ny verktøylinje _Avbryt _Lukk _Skrivebordstandard _Hjelp _Ikon: Bare _ikoner _Internett _Navn: _OK _Åpne _Andre ... _Fjerne verktøylinje _Lagre _Søkeikon: Bare _tekst _Url: _Verktøy aterm qtFM størrelse 