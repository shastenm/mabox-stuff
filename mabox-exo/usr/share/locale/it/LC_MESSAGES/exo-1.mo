��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  w  1?  9   �@  /   �@  4   A  +   HA  �   tA  1    B  I   2B  +   |B  4   �B  <   �B  z   C  M   �C  P   �C  B   4D  =   wD  �   �D  �   gE  �   RF     G     (G  %   /G  *   UG     �G  %   �G     �G     �G  
   �G  )   �G     H     ,H     2H     HH     NH  @   eH     �H  
   �H     �H     �H     �H  "   �H  .   I  .   GI  4   vI  )   �I     �I     �I  
   �I     J     "J     ;J  	   TJ  u   ^J     �J     �J     �J     
K  5   K     RK  !   oK     �K  (   �K     �K  "   �K     L     (L  '   .L  �   VL     �L     �L     M     %M     -M     6M  	   FM  "   PM     sM  	   �M     �M     �M     �M  3   �M  3   �M  8   (N  .   aN  D   �N  ,   �N  j   O     mO  "   �O     �O     �O  #   �O  .   
P  $   9P     ^P  5   xP  5   �P  :   �P  0   Q  4   PQ     �Q     �Q  =   �Q     �Q     �Q     R     	R     R     ,R     2R     @R  H   IR     �R     �R  	   �R     �R     �R     �R     �R  �   S     �T  "   �T     �T     �T     �T  }   �T  g   nU     �U     �U     V     V     3V     DV     LV     aV     pV  :   wV  `   �V  =   W  1   QW  K   �W      �W     �W      X     X     X     1X     6X     =X     FX      YX     zX     �X  .   �X  "   �X     �X     Y  !   Y  ?   AY     �Y     �Y     �Y     �Y     �Y     �Y     �Y  P   �Y  P   GZ  V   �Z  K   �Z  &   ;[     b[  P   y[  .   �[  /   �[  3   )\  0   ]\  /   �\  O   �\  	   ]  *   ]     C]  	   T]  N   ^]     �]  	   �]     �]     �]  >   �]     ^     (^     ;^     S^     k^     �^     �^  	   �^     �^     �^     �^  "   �^     _     #_     8_  :   S_  �   �_  P   F`     �`  
   �`  !   �`  "   �`     �`     a     a     6a  ,   =a  +   ja  ,   �a  *   �a  
   �a  _   �a  _   Yb  e   �b  Z   c     zc     �c     �c     �c     �c     �c  
   �c     �c     �c     d      +d  %   Ld  #   rd  u   �d     e  !  +e  8   Mf     �f     �f     �f  &   �f  X   g  k   \g  x   �g  y   Ah     �h  2   �h  )   i     /i  "   6i  >   Yi  <   �i  .   �i     j  (   j     9j     Xj     uj  ;   �j     �j  C   �j     $k     =k     Jk     _k  :   kk  E   �k     �k     l     l     :l     Ol     [l     ml     �l  )   �l     �l     �l     �l     �l     �l     �l  	   m     m     m     m  	    m  !   *m     Lm     Sm     am     mm  	   sm     }m     �m  
   �m                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-27 06:46+0000
Last-Translator: Emanuele Petriglia <transifex@emanuelepetriglia.com>
Language-Team: Italian (http://www.transifex.com/xfce/exo/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
           %s [opzioni] --build-list [[nome del file]...]
           exo-open --launch TIPO [PARAMETRI...]   --build-list      Analizza le coppie (nome, file)
   --extern          Genera simboli esterni
   --launch TIPO [PARAMETRI...]      Lancia le applicazioni preferite di
TIPO con PARAMETRI opzionali, dove
TIPO è uno dei valori seguenti.   --name=identifier Nome della macro/variabile C
   --output=filename Scrive il file csource generato nel file specificato
   --static          Genera simboli statici
   --strip-comments  Rimuove i commenti dai file XML
   --strip-content   Rimuove i contenuti nodali dai file XML
   --working-directory CARTELLA        Cartella di lavoro predefinita per le
applicazioni quando si usa l'opzione --launch.   -?, --help                         Stampa questo messaggio di aiuto ed esce   -V, --version                       Stampa le informazioni di versione ed esce   -V, --version     Mostra le informazioni sulla versione ed esce
   -h, --help        Mostra questo messaggio di aiuto ed esce
   WebBrowser - Browser web preferito.
 MailReader - Lettore di email preferito.
 FileManager - Gestore dei file preferito.
 TerminalEmulator - Emulatore di terminale preferito. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Tutti i diritti riservati.

Scritto da Benedikt Meurer <benny@xfce.org>.

Compilato con Gtk+-%d.%d.%d, eseguito con Gtk+-%d.%d.%d.

Segnalare eventuali problemi su <%s>.
 %s viene distribuito senza NESSUNA GARANZIA.
È possibile ridistribuire copie di %s secondo i termini
della licenza GNU Lesser General Public License presente nel
pacchetto dei sorgenti di %s.

 Icone delle azioni Attivo Colore del bordo dell'elemento attivo Colore di riempimento dell'elemento attivo Indice dell'elemento attivo Colore del testo dell'elemento attivo Tutti i file Tutte le icone Animazioni Pulsante per la scelta delle applicazioni Icone delle applicazioni Balsa Dispositivo a blocchi Brave Esplora il file system Espolora il file system per scegliere un comando personalizzato. Esplora la rete C_ommento: C_rea Gestore dei file Caja Dispositivo a caratteri Scegliere l'applicazione preferita Selezionare un gestore dei file personalizzato Selezionare un lettore di email personalizzato Selezionare un emulatore di terminale personalizzato Selezionare un browser web personalizzato Selezionare il nome del file Chromium Claws Mail Pulisci il campo di ricerca Spaziatura della colonna Spaziatura delle colonne Co_mando: Copyright (c) %s
        os-cillation e.K. Tutti i diritti riservati.

Scritto da Benedikt Meurer <benny@xfce.org>.

 Crea cartella Crea avviatore Crea avviatore <b>%s</b> Crea collegamento Crea un nuovo file desktop nella cartella specificata Colore del bordo del cursore Colore di riempimento del cursore Colore del testo del cursore Personalizza la barra degli strumenti... Debian Sensible Browser Emulatore di terminale X di Debian Icone dei dispositivi Dillo _Non visualizzare più questo messaggio Trascinare un elemento sulle barre degli strumenti in alto per aggiungerlo, dalle barre degli strumenti alla tabella degli elementi per rimuoverlo. Modifica cartella Modifica avviatore Modifica collegamento Emblemi Emoticon Abilita ricerca Encompass Emulatore di terminale Enlightened Browser web Epiphany Evolution File eseguibili FIFO Impossibile creare "%s". Impossibile avviare il gestore dei file predefinito Impossibile avviare il lettore di email predefinito Impossibile avviare l'emulatore di terminale predefinito Impossibile avviare il browser web predefinito Impossibile eseguire l'applicazione preferita per la categoria "%s". Impossibile caricare i contenuti da "%s": %s Impossibile caricare l'immagine "%s": ragione sconosciuta, probabilmente file dell'immagine è dannegiato. Impossibile aprire "%s". Impossibile aprire %s in scrittura Impossibile aprire l'URI "%s". Impossibile aprire lo schermo Impossibile aprire il file "%s": %s Impossibile analizzare i contenuti di "%s": %s Impossibile leggere il file "%s": %s Impossibile salvare "%s". Impossibile impostare il gestore dei file predefinito Impossibile impostare il lettore di email predefinito Impossibile impostare l'emulatore di terminale predefinito Impossibile impostare il browser web predefinito Il file "%s" non ha una chiave che specifica il tipo Gestore dei file Icone del tipo di file La posizione del file non è un file regolare od una cartella Cartella Segui lo stato GIcon Terminale GNOME Browser web Galeon Geary Google Chrome Omogeneo Come il testo e l'icona di ogni elemento sono posizionati reciprocamente Icecat Icedove Iceweasel Icona Modello della barra delle icone Modello della vista a icone Colonna dell'icona Se non si specifica l'opzione --launch, exo-open aprirà tutti gli URL
specificati con i loro rispettivi gestori preferiti. Se invece si specifica
l'opzione --launch, sarà possibile scegliere quale applicazione preferita si
desidera eseguire, passandole anche parametri addizionali (per esempio
per l'emulatore di terminale è possibile indicare il comando che dovrebbe
 essere eseguito nel terminale stesso). File immagine Tipo di assistente "%s" non valido Jumanji KMail Browser web Konqueror Avvia l'assistente predefinito di tipo TYPE con il parametro facoltativo PARAMETER, dove TYPE assume uno dei seguenti valori. L'avvio dei file desktop non è supportato quando %s è compilato senza le caratteristiche di GIO-Unix. Modalità di disposizione Browser testuale Links Icone della posizione Browser testuale Lynx Lettore di email Margine Colonna di marcatura Icone del menu Midori Modello di colonna utilizzata per cercare fra gli elementi Modello di colonna usata per recuperare il percorso assoluto di un file immagine da visualizzare Colonna del modello usata per recuperare il pixbuf dell'icona Colonna del modello usata per recuperare il testo Modello di colonna usato per ottenere il testo se si usa il marcatore Pango Modello per la barra delle icone Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nessuna applicazione selezionata Nessun comando specificato Nessun file selezionato Non è stato specificato alcun file o cartella Nessun aiutante definito per "%s". Nessuna icona Numero di colonne Numero di colonne da visualizzare Apre la finestra di configurazione delle
applicazioni preferite Opera Browser Opzioni: Orientamento Gestore dei file PCMan Gestore dei file PCManFM-Qt Script di Perl Colonna Pixbuf Scegliere il proprio gestore dei file
preferito e fare clic su OK per procedere. Scegliere il proprio lettore di email
preferito e fare clic su OK per procedere. Scegliere il proprio emulatore di terminale
preferito e fare clic su OK per procedere. Scegliere il proprio browser web
preferito e fare clic su OK per procedere. Segnalare eventuali problemi su <%s>.
 Applicazioni preferite Applicazioni preferite (browser web, lettore di email ed emulatore di terminale) URL predefinito quando si crea un collegamento Comando predefinito quando si crea un avviatore Commento predefinito quando si crea un file desktop Icona predefinita quando si crea un file desktop Nome predefinito quando si crea un file desktop Premere il pulsante sinistro del mouse per cambiare l'applicazione selezionata. Anteprima Stampa le informazioni di versione ed esce Script di Python QTerminal Interroga l'aiutate predefinito di TYPE, dove TYPE è uno dei seguenti valori. QupZilla ROX-Filer RXVT Unicode Leggi le email Visualizza in modo diverso a seconda dello stato di selezione. Riordinabile Comando di riavvio Gestore dei file Rodent Spaziatura tra le righe Spaziatura delle righe Script di Ruby Esegui nel _terminale ID SOCKET Sakura Colonna di ricerca Selezionare l'_icona da: Selezionare una cartella di lavoro Selezionare un'applicazione Selezionare un'icona Selezionare l'applicazione Selezionare le applicazioni predefinite per i vari servizi Selezionare questa opzione per abilitare la notifica di avvio quando il comando è eseguito dal gestore dei file o dal menu. Non tutte le applicazioni supportano la notifica di avvio. Selezionare questa opzione per eseguire il comando in una finestra di terminale. Modalità selezione Separatore Comando di riavvio della sessione Socket del gestore di impostazioni Script per la Shell Singolo clic Pausa per il singolo clic Socket Spazio inserito ai bordi della vista a icone Spazio inserito tra le celle di un elemento Spazio inserito tra le colonne della griglia Spazio inserito tra le righe della griglia Spaziatura Specificare l'applicazione che si intende utilizzare
come gestore dei file predefinito di Xfce: Specificare l'applicazione che si intende utilizzare
come lettore di email predefinito di Xfce: Specificare l'applicazione che si intende utilizzare
come emulatore di terminale predefinito di Xfce: Specificare l'applicazione che si intende utilizzare
come browser web predefinito di Xfce: Icone dello stato Fornitura di icone Surf Sylpheed TYPE [PARAMETER] Emulatore di terminale Terminator Colonna del testo Testo per le _icone importanti Testo per tutte _le icone L'oggetto GIcon da visualizzare. Lo spazio tra due colonne consecutive Lo spazio tra due righe consecutive Il tempo prima che l'elemento sotto il puntatore del mouse venga selezionato automaticamente in modalita singolo clic Il file "%s" non contiene dati I seguenti TYPE sono utilizzabili per i comandi --launch e --query:

  WebBrowser         - Il browser web preferito.
  MailReader           - Il lettore di email preferito.
  FileManager         - L'esploratore dei file preferito.
  TerminalEmulator  - L'emulatore di terminale preferito. I seguenti TIPI sono supportati per il comando --launch: L'icona da visualizzare. La modalità di disposizione Il modello per la vista a icone L'orientamento della barra delle icone Il gestore dei file preferito che verrà usato per esplorare i contenuti delle cartelle. Il lettore email preferito che verrà utilizzato per comporre email quando si clicca su un indirizzo email. L'emulatore di terminale preferito che verrà usato per eseguire i comandi che richiedono un ambiente a riga di comando. Il browser Web preferito che verrà usato per aprire i collegamenti ipertestuali e per visualizzare i contenuti di aiuto. La modalità selezione La dimensione in pixel dell'icona da visualizzare. La larghezza utilizzata per ogni elemento Thunar _Stile della barra degli strumenti Digitare '%s --help' per ottenere le informazioni di utilizzo. Tipo di file desktop da creare (applicazione o collegamento) Impossibile determinare lo schema URI di "%s". Altre icone Tipo di file desktop non supportato "%s" Utilizzo: %s [opzioni] [file]
 Utilizzo: exo-open [URLs...] Usa noti_fica di avvio Usa un'applicazione personalizzata non inclusa nell'elenco. Usa la linea di comando Consente all'utente di cercare nelle colonne in maniera interattiva La vista è riordinabile Vimprobable2 Browser testuale W3M Browser web Se i figli dovrebbero essere tutti della stessa dimensione Se gli elementi nella vista possono essere attivati con singoli click Larghezza per ogni elemento Gruppo di finestre Finestra principale del gruppo Cartella di la_voro: Terminale X Terminale di Xfce Gestore dei file Xfe [FILE|CARTELLA] _Aggiungi una nuova barra degli strumenti A_nnulla _Chiudi _Predefinito della scrivania A_iuto _Icona: Solo _icone _Internet Nom_e: _Ok _Apri Altr_o... _Rimuovi la barra degli strumenti _Salva C_erca icona: Solo _testo _URL: _Utilità aterm qtFM dimensione 