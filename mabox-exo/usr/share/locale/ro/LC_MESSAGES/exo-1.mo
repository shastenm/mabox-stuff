��    &     L	  �  |      �  2   �  -   �  -     ,   @  �   m  *   Q  >   |  ,   �  3   �  8     �   U  F   �  H   3  7   |  5   �  �   �  �   �  �   �     4     A     H     a     x     �  	   �  	   �  
   �     �     �     �     �               2         P   	   _      i      q      �      �      �      �   !   �      !     (!     8!  
   A!     L!     _!     n!  	   }!  o   �!     �!     "     "     2"  0   >"     o"     �"     �"     �"     �"     �"     �"     #  b   #     t#     �#  	   �#     �#  	   �#     �#  	   �#     �#     �#  	   �#     $     $     $  &   /$  %   V$  +   |$  %   �$  9   �$  %   %  H   .%     w%     �%     �%     �%     �%  $   �%     &     9&  "   N&  !   q&  '   �&  !   �&     �&     �&     '  0   '     E'     L'     Y'     _'     n'     �'     �'     �'  H   �'     �'     �'  	   �'     (     (     (     '(  h  3(     �)     �)     �)     �)     �)  i   �)  W   O*     �*     �*     �*     �*     �*     �*     �*  
   +     +  :   +  J   U+  2   �+  +   �+  <   �+     <,     S,     c,     s,     �,     �,     �,     �,     �,     �,     �,     �,     �,     -     /-     7-     I-  4   f-     �-     �-     �-     �-     �-     �-  F   �-  E   3.  K   y.  E   �.     /     (/  G   ?/     �/  '   �/  +   �/  (   �/  (   $0  ;   M0     �0  "   �0     �0  	   �0  L   �0     1  	   #1     -1     :1  0   J1     {1     �1     �1     �1     �1     �1     �1  	   �1     �1     �1      2     2     .2     D2     S2  0   f2  �   �2  ;   93     u3  	   �3     �3     �3     �3     �3     �3     �3  5   �3  0   +4  +   \4  )   �4     �4  I   �4  H   5  N   M5  H   �5     �5     �5     �5      6     6  
   #6     .6     :6     T6     h6  3   }6  0   �6  r   �6     U7    t7  ;   �8     �8     �8     �8     9      9  )   39     ]9     z9     �9     �9  4   �9  (   �9     
:  "   :     A:     ]:     w:  A   �:     �:  8   �:     !;     5;     B;     S;  0   _;  A   �;     �;     �;     �;     <  
   <     &<     4<     E<     S<     f<     n<     u<     �<     �<     �<  	   �<     �<     �<     �<  	   �<     �<     �<     �<  
   �<     �<  
   �<     =     
=     =  �  =  6   �>  +   �>  @   ?  1   C?  �   u?  1   b@  D   �@  1   �@  <   A  E   HA  �   �A  B   ,B  ?   oB  8   �B  ;   �B  �   $C  �   �C  �   �D     gE     �E  *   �E  )   �E     �E  (   �E      F     1F  	   BF     LF     gF     ~F     �F     �F  !   �F  A   �F     
G     G  	   (G     2G     MG     mG  #   �G     �G  $   �G     �G     H     (H  
   1H     <H     ZH     sH  
   �H  {   �H     I     'I     ;I     YI  2   iI     �I  !   �I     �I      �I     J     8J     SJ     mJ  �   sJ     �J     K     ,K     @K     HK     ]K  	   rK     |K     �K  	   �K     �K     �K     �K  1   �K  ,   L  2   IL  '   |L  @   �L  ;   �L  s   !M     �M  -   �M  &   �M     
N  ,   *N  :   WN  (   �N     �N  2   �N  -   O  3   9O  (   mO  ,   �O     �O  $   �O  ;   �O     8P     >P     NP     TP     eP     xP     ~P     �P  J   �P     �P     �P  	   �P  	   �P     Q      Q     =Q  o  RQ     �R  %   �R     S     
S     S  g   &S  i   �S     �S     T     T     8T     NT     ]T     eT     |T     �T  J   �T  f   �T  <   OU  4   �U  W   �U     V     7V     KV     [V     hV     |V     �V     �V     �V     �V     �V     �V  &    W  0   'W     XW     iW     {W  9   �W     �W  	   �W  	   �W     �W     X     %X  L   5X  G   �X  M   �X  B   Y  )   [Y     �Y  3   �Y  #   �Y  .   �Y  5   Z  6   UZ  /   �Z  6   �Z     �Z  &   [     )[  	   <[  Y   F[     �[  	   �[     �[     �[  5   �[     	\     \     +\     H\     b\     |\     �\  	   �\     �\     �\     �\     �\     ]     ]     5]  7   K]  �   �]  H   Q^     �^  	   �^      �^  !   �^     �^  	   
_      _     5_  7   <_  ,   t_  '   �_  (   �_  	   �_  X   �_  S   U`  Y   �`  N   a     Ra     ga     la     ua     �a  
   �a     �a  "   �a     �a     �a  .   b  /   Bb  m   rb  1   �b  �   c  ;   d     Hd     ^d  %   pd     �d     �d  1   �d  *   �d     #e     *e  8   Be  2   {e  0   �e     �e  ?   �e  #   9f      ]f  "   ~f  =   �f     �f  M   �f     Jg     jg     wg  	   �g  C   �g  K   �g     &h     Fh     Wh     uh     �h     �h     �h     �h  #   �h  
   �h  	   i     i     "i     *i     6i  	   Ei     Oi     Vi  	   Zi     di     pi  
   �i     �i  
   �i     �i  
   �i     �i     �i     �i     �   M        �      �     �           q            :       �      S   %       _   >   �   r       z   m   �       �       W       �   �   ^   �   �   �   �   �      �   �   `   �       7           �   =           �   �          �   �              �   .       �      8   �   A                 �       �      	   l   �                           3       �         h   �           �   �   &       !  $  <      �   �   I       U   �   P   �       �           j       �   2       �   D           �   w   C   �   �           $   |       "   �   �       T   �   �   �   4       &      �       e   �         k   \   +               �   �       @                         	        b   �      �   �   �          �   �   �      �   -     c   �   �       J   a      ~       f   R   �   y   K   �   �   /   �   }   O              [   H          �     �   �   �   �             g   0     #   )       �   v       �      �   %      �   �   �       u        ;       (          �   #  F   �   �   �              �          �     �   �   �   �   N   5      �   �   "  �       1   G       �         �   �   9   ?   �            �   i             *   t   Y   �      s   Z      �   �     Q   d        E   L   �   �   '   {   �       n   �       o       �               �   �           �       �   �                 6   �     �           ,   x       �   !           �      ]   B   �   �       
   �   �       �       p      �   �   �   V   �   �      
  �   �         �           �      �                X   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Romanian (http://www.transifex.com/xfce/exo/language/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
        %s [opțiuni] --build-list [[nume fișier]...]
        exo-open --launch TIP [PARAMETRI...]   --build-list      Prelucrează perechile de nume și fișiere
   --extern          Generează simboluri externe
   --launch TIP [PARAMETRI...]         Pornește programul preferat de tipul TIP
                                      cu parametrii opționali PARAMETRI, unde
                                      TIP are una dintre valorile de mai jos.   --name=identificator de macro ori variabilă C
 --output=filename Scrie csource generat către fișierul specificat
   --static          Generează simboluri statice
   --strip-comments  Șterge comentariile din fișierele XML
   --strip-content   Șterge conținutul nodurilor din fișierele XML
   --working-directory DIRECTOR        Director de lucru implicit pentru programe
                                      când se utilizează comanda --launch.   -?, --help                          Arată acest mesaj de ajutor   -V, --version                       Arată versiunea curentă   -V, --version     Arată versiunea curentă și ieși
   -h, --help        Arată acest mesaj de ajutor și ieși
   WebBrowser       - Navigatorul preferat.
  MailReader       - Clientul de mail preferat.
  FileManager      - Managerul de fișiere preferat.
  TerminalEmulator - Emulatorul de terminal preferat. %s (Xfce %s)

Drepturi de autor © 2003-2006
        os-cillation e.K. Toate drepturile rezervate.

Scris de Benedikt Meurer <benny@xfce.org>.

Compilat cu Gtk+-%d.%d.%d, pornit cu Gtk+-%d.%d.%d.

Raportați orice probleme la <%s>.
 %s nu vine cu ABSOLUT NICIO GARANȚIE.
Puteți redistribui copii ale %s în termenii
licenței GNU LGPL, disponibilă în pachetul cu sursele %s.

 Iconițele pentru acțiuni Active Culoare pentru marginea elementelor active Culoare de umplere pentru elemente active Index de elemente active Culoarea textului pentru elemente active Toate fișierele Toate iconițele Animații Buton selector de programe Iconițele programelor Balsa Dispozitiv de tip «block» Brave Gestionați fișierele din sistem Parcurgeți sistemul de fișiere pentru a alege o altă comandă. Navigați pe net C_omentariu: _Creează Managerul de fișiere Caja Dispozitiv de tip «character» Alegeți un program preferat Alegeți un alt manager de fișiere Alegeți un alt client de mail Alegeți un alt emulator de terminal Alegeți un alt navigator Alegeți un nume de fișier Chromium Claws Mail Curăță câmpul de căutare Spațiere între coloane Spațiere între coloane Co_mandă: Drepturi de autor © %s
        os-cillation e.K. Toate drepturile rezervate.

Scris de Benedikt Meurer <benny@xfce.org>.

 Creați un director Creați un lansator Creați un lansator <b>%s</b> Creați un link Creează un nou fișier desktop în directorul dat Culoarea marginii cursorului Culoarea de umplere pentru cursor Culoarea textului pentru cursor Personalizați bara cu unelte… Navigatorul standard Debian Terminalul standard Debian Iconițele de dispozitive Dillo Trageți un element deasupra unei bare cu unelte pentru a-l adăuga, ori din bara cu unelte între celelalte elemente pentru a-l scoate. Editare pentru director Editare pentru lansator Editare pentru link Embleme Iconițele simbolice Activează căutarea Encompass Terminalul Enlightened Navigatorul Epiphany Evolution Fișierele executabile FIFO Nu s-a putut crea „%s”. Nu s-a putut porni managerul de fișiere implicit Nu s-a putut porni clientul de mail implicit Nu s-a putut porni emulatorul de terminal implicit Nu s-a putut porni navigatorul implicit Nu s-a putut lansa programul preferat pentru categoria „%s”. Nu s-a putut încărca conținutul fișierului „%s”: %s Dintr-un motiv necunoscut, nu s-a putut încărca imaginea „%s”, probabil că fișierul de tip imagine e corupt Nu s-a putut deschide „%s”. Nu s-a putut deschide „%s” pentru scriere Nu s-a putut deschide URI-ul „%s”. Nu s-a putut deschide displayul Nu s-a putut deschide fișierul „%s”: %s Nu s-a putut prelucra conținutul fișierului „%s”: %s Nu s-a putut citi fișierul „%s”: %s Nu s-a putut salva „%s”. Nu s-a putut defini managerul de fișiere implicit Nu s-a putut defini clientul de mail implicit Nu s-a putut defini emulatorul de terminal implicit Nu s-a putut defini navigatorul implicit Fișierul „%s” nu are o cheie „type” Manager de fișiere Iconițele pentru tipuri de fișiere Calea fișierului nu este un fișier ori director obișnuit Dosar Urmează starea GIcon Terminalul GNOME Navigatorul Galeon Geary Google Chrome Omogen Modul de poziționare relativă a textului și iconiței fiecărui element Icecat Icedove Iceweasel Iconiță Model de bară cu iconițe Model de vizualizare cu iconițe Coloană cu iconițe Dacă nu specificați comanda --launch, exo-open va deschide URL-urile
specificate cu programele preferate. Dacă specificați comanda --launch,
puteți selecta ce programe preferate doriți să porniți și puteți transmite
parametri suplimentari acelui program (de ex. pentru TerminalEmulator
puteți transmite o comandă de executat în acel terminal la pornire). Fișierele de tip imagine Tipul asocierii „%s” este nevalid Jumanji KMail Navigatorul Konqueror Lansează programul TIP cu parametrul opțional PARAMETRU, unde TIP are una dintre următoarele valori: Nu există suport pentru lansarea de fișiere desktop când %s este compilat fără facilități GIO-Unix Mod de afișare Navigatorul text Links Iconițele pentru locuri Navigatorul text Lynx Client de mail Margine Coloană de tip markup Iconițele pentru meniuri Midori Model de coloană pentru căutare când se face căutarea într-un element Model de coloană utilizat pentru a obține calea completă pentru fișierul de tip imagine de afișat Coloană model utilizată pentru extragerea iconiței pixbuf Coloană model utilizată pentru extragerea textului Model de coloană utilizat pentru extragerea textului când se utilizează markup Pango Model pentru bara cu iconițe Navigatorul Mozilla Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Navigatorul Netscape Nu ați selectat niciun program Nu s-a specificat o comandă Niciun fișier nu e selectat Nu s-a specificat un fișier ori dosar Nu a fost definit niciun ajutor pentru " 1 %s ". Fără iconiță Număr de coloane Număr de coloane de afișat Deschide fereastra de configurare
a programelor preferate Navigatorul Opera Opțiuni: Orientare Managerul de fișiere PCMan Scripturile Perl Coloană pixbuf Selectați managerul de fișiere preferat
și alegeți OK pentru a continua. Selectați clientul de mail preferat
și alegeți OK pentru a continua. Selectați emulatorul de terminal preferat
și alegeți OK pentru a continua. Selectați navigatorul preferat
și alegeți OK pentru a continua. Raportați eventualele probleme la <%s>.
 Programe preferate Programe preferate (navigator, client de mail etc.) URL predefinit la crearea unui link Comandă predefinită la crearea unui lansator Comentariu predefinit la crearea unui fișier desktop Iconiță predefinită la crearea unui fișier desktop Nume predefinit la crearea unui fișier desktop Cu un clic stânga puteți schimba programul selectat. Previzualizare Arată detaliile de versiune și ieși Scripturile Python QTerminal Solicitați ajutorul standard de tip TYPE, unde TYPE este una dintre următoarele valori. QupZilla ROX-Filer RXVT Unicode Citiți-vă mailul Afișează diferit în funcție de starea selecției. Reordonabil Comandă de repornire Managerul de fișiere Rodent Spațiere între rânduri Spațiere între rânduri Scripturile Ruby Pornește în _terminal ID SOCKET Sakura Coloană de căutare Selectați _iconița dintre: Selectați un director de lucru Selectați un program Selectați o iconiță Selectați un program Selectați programele implicite pentru anumite servicii Selectați această opțiune pentru a activa notificarea la pornire atunci când comanda este executată din managerul de fișiere sau din meniu. Nu toate programele au suport pentru notificare la pornire. Selectați această opțiune pentru a executa comanda într-un terminal. Mod de selectare Separator Comandă de repornire a sesiunii Socket al managerului opțiunilor Scripturile shell Clic unic Limită de timp pentru clic unic Socket Spațiu adăugat la marginile vizualizării de iconițe Spațiu inserat între celulele unui element Spațiu inserat între coloanele grilei Spațiu inserat între rândurile grilei Spațiere Specificați ce program doriți să utilizați
ca manager de fișiere implicit în Xfce: Specificați ce program doriți să utilizați
ca client de mail implicit în Xfce: Specificați ce program doriți să utilizați
ca emulator de terminal implicit în Xfce: Specificați ce program doriți să utilizați
ca navigator implicit în Xfce: Iconițele de status Surf Sylpheed TIP [PARAMETRU] Emulator de terminal Terminator Coloană de text Text pentru iconițele i_mportante Text pentru to_ate iconițele Elementul GIcon de afișat. Spațiu liber între două coloane consecutive Spațiu liber între două rânduri consecutive Durata minimă pentru selectarea automată a elementului aflat sub cursor când se utilizează un singur clic Fișierul „%s” nu conține niciun fel de date Următoarele TYPE-uri sunt suportate pentru comenzile --launch și --query:

WebBrowser - Web Browser preferat.
MailReader - Cititorul de mesaje preferat.
FileManager - Managerul de fișiere preferat.
TerminalEmulator - Emulatorul Terminal preferat. Următoarele valori TIP sunt suportate de comanda --launch: Iconiță de afișat. Modul de afișare Model pentru vizualizarea cu iconițe Orientarea barei cu iconițe Modul de selectare Dimensiunile în pixeli ale iconiței de afișat. Lățime utilizată pentru fiecare element Thunar _Stilul barei cu unelte Încercați „%s --help” pentru detalii de utilizare. Tip de fișier desktop de creat (program sau link) Nu s-a putut detecta schema URI pentru „%s”. Iconițele necategorisite Nu există suport pentru acest tip de fișier desktop: „%s” Utilizare: %s [opțiuni] [fișier]
 Utilizare: exo-open [URL-uri...] _Utilizează notificare la pornire Utilizați un program ce nu este inclus în lista de mai sus. Utilizați linia de comandă Mod de vizualizare ce permite utilizatorului să caute interactiv în coloane Vizualizarea este reordonabilă Vimprobable2 Navigatorul text W3M Navigator Specifică dacă toți copiii ar trebui să fie de aceeași mărime Specifică dacă elementele vizualizării pot fi activate cu un singur clic Lățime pentru fiecare element Grup de ferestre Lider al grupului de ferestre _Director de lucru: Terminalul Xterm Terminal Xfce Managerul de fișiere Xfe [FIȘIER|DIRECTOR] _Adăugați o nouă bară cu unelte _Renunță _Închide Stilul _desktopului _Ajutor _Iconiță: Doar _iconițe _Internet _Nume: _OK _Deschide Altce_va... Ște_rgeți bara cu unelte _Salvează _Căutare în iconițe: Doar _text _URL: _Utilitare aterm qtFM mărime 