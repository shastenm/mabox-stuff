��         |  g  �      x  2   y  -   �  -   �  ,     �   5  *     ,   D  3   q  8   �  �   �  F   u  H   �  7     5   =  �   s  �   :  �        �     �     �     �            	   *  	   4  
   >     I     d     v     |     �  2   �     �  	   �     �     �          "     ?  !   [     }     �     �  
   �     �     �     �  	   �  o   �     h     y     �     �  0   �     �     �           '      <      T      o   b   |      �      �   	   �      !  	   !     !  	   &!     0!     N!  	   c!     m!     ~!     �!  &   �!  %   �!  +   �!  %   "  9   9"  %   s"  H   �"     �"     �"     #     .#     E#  $   b#     �#     �#  "   �#  !   �#  '   �#  !   &$     H$     b$     o$  0   $     �$     �$     �$     �$     �$     �$  H   �$     A%  	   I%     S%     X%     g%  h  w%     �&     �&     '     '  i   !'  W   �'     �'     �'     (     (     #(     /(     6(  
   D(     O(  :   V(  2   �(  +   �(  <   �(     -)     D)     T)     d)     q)     �)     �)     �)     �)     �)     �)     �)     �)     *     *     *  4   ;*     p*     ~*     �*     �*     �*     �*  F   �*  E   +  K   N+  E   �+     �+     �+  G   ,     \,  '   |,  +   �,  (   �,  (   �,  ;   "-     ^-  "   f-     �-     �-     �-  0   �-     �-     �-     .     .     .     '.  	   8.     B.     P.     c.     ~.     �.     �.  0   �.  �   �.  ;   �/     �/  	   �/     �/     �/     0     0     )0     >0  5   E0  0   {0  +   �0  )   �0     1  I   
1  H   T1  N   �1  H   �1     52     B2     K2     \2     n2     z2     �2     �2  3   �2  0   �2  r   "3     �3  ;   �3     �3     4     4     04     O4  )   b4     �4     �4     �4     �4  4   �4  (   5     95  "   M5     p5     �5     �5  A   �5     6  8   6     P6     d6     u6  0   �6  A   �6     �6     7     7     )7  
   =7     H7     V7     d7     w7     7     �7     �7     �7     �7     �7     �7     �7  	   �7     �7     �7  
   �7     �7     �7      8  �  8  8   �9  ,   �9  5   �9  -   %:  �   S:  3   .;  /   b;  6   �;  4   �;  �   �;  N   �<  M   �<  <   6=  1   s=  �   �=  �   t>  �   \?     �?     @     @  %   .@     T@     l@  
   �@     �@     �@     �@     �@     �@     �@     �@  >   A     FA     ZA  	   gA     qA     �A  (   �A     �A     �A     B     "B     <B  
   EB     PB     mB     ~B  	   �B  w   �B     C     'C     ;C     YC  6   hC     �C  $   �C     �C     D     D     2D     PD  r   `D     �D     �D     �D  	   
E     E     !E  	   6E      @E     aE  	   ~E     �E     �E     �E  3   �E  0   �E  4   &F  3   [F  ;   �F  '   �F  [   �F     OG  #   hG     �G     �G  !   �G  "   �G  "   
H     -H  4   GH  1   |H  5   �H  4   �H     I     7I     KI  8   ]I     �I     �I     �I     �I     �I     �I  >   �I     *J  	   2J     <J     BJ     WJ  H  kJ     �K      �K     �K     �K  r   L  _   zL     �L     �L     M     M     *M     ;M     @M     WM     hM  1   oM  7   �M  0   �M  C   
N     NN     cN     sN     �N     �N     �N     �N     �N     �N     �N     �N     O     O  
   9O     DO     SO  6   nO     �O     �O     �O     �O     �O     �O  R   �O  O   QP  S   �P  R   �P  (   HQ     qQ  S   �Q  !   �Q  )   �Q  4   %R  .   ZR  3   �R  D   �R     S  (   S     =S     LS     YS  *   hS     �S     �S     �S     �S     �S     �S  	   �S      T     T     -T     LT     _T     oT  :   �T  �   �T  S   xU     �U     �U     �U  #   V     )V     9V     JV     bV      iV      �V      �V     �V     �V  P   �V  M   EW  Q   �W  P   �W     6X     EX     NX     _X     tX     �X     �X     �X  *   �X  '   �X  f   Y  !   �Y  #   �Y     �Y     �Y     �Y     
Z     !Z  '   2Z     ZZ     pZ     wZ  >   �Z  6   �Z  &   �Z     &[  .   :[  #   i[     �[  $   �[  0   �[     \  7   \     O\     g\     |\  )   �\  >   �\     �\  
   ]     ]     /]     B]     P]     `]     m]     �]  	   �]     �]     �]     �]     �]     �]     �]     �]     �]     �]     ^     ^     (^     .^     4^        �           u   �   �   �          �   g       t   J   A   \   �   b       K       �   Q   �       E   �   "   �   )     �   �   S   |   �   Y      }             �   �   C   s   �       �   �      �       c   w   �   �       �   	  +   2   �   �   �   h         �              a   �   �          <   9   I   ,           (   �   �                   �   z   �       �           x       �      #       �   ^      �      �   �     �   	   �       i   �      �   �          �   �   �   �   �   �   �       �       �   L   F       *   Z      !   [              l       �       �         B   �   N   �   �   e   �   /       �   W   j   &   �   �           X   �         �   �   �           �   �   �       '   �   3   �   �   k   �      �   �   �           
  �      `   
       M   �   �             �   �       D   ?       ~       �       �   �           >   ;   �   V   �   �   �       �       H      �       �   P   �   �   �       �   4                     1          �   -           �   8               q   R   �   �   m               {   7     .   �       �       G   %          �   �   �      0          :       �   _       �   5           �           �       d   �   �   �   �   �   �   �   �   �   o   n   ]   �   �      =   �   6       f             �                  �   �          $   @   �   �   �             r      v   p   U   y           �       T       �   �   O           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Homogeneous How the text and icon of each item are positioned relative to each other Icedove Iceweasel Icon Icon Bar Model Icon View Model If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Sylpheed TYPE [PARAMETER] Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Name: _OK _Open _Other... _Remove Toolbar _Search icon: _Text only _URL: aterm size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Latvian (http://www.transifex.com/xfce/exo/language/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
        %s [opcijas] --build-list [[faila nosaukums]...]
        exo-open --launch TIPS [PARAMETRI...]   --build-list      Parsē (nosaukums, fails) pārus
   --extern          Veidot ārējos simbolus
   --launch TIPS [PARAMETRI...]       Palaist vēlamo TIPA lietotni
                                      ar neobligātiem PARAMETRIEM, kur TIPS
                                      ir kāda no sekojošām vērtībām.   --name=identifikātors, C makro/mainīgā vārds
   --static           Veidot statiskos simbolus
   --strip-comments  Dzēš komentārus no XML failiem
   --strip-content   Dzēš mezglu saturu XML failos
   --working-directory DIREKTORIJA       Noklusētā darba direktorija priekš lietotnes,
                                      kad izmanto --lunch opciju.   -?, --help                          Izdrukā šo palīdzības ziņu un iziet   -V, --version                       Izdrukā versijas informāciju un iziet   -V, --version     Izdrukā versijas informāciju un iziet
   -h, --help         Izdrukā šo ziņu un iziet
   WebBrowser       - Vēlamais tīmekļa pārlūks.
  MailReader       - Vēlamais pasta lasītājs.
  FileManager       - Vēlamais failu pārvaldnieks.
  TerminalEmulator - Vēlamais termināla emulators. %s (Xfce %s)

Autortiesības (c) 2003-2006
        os-cillation e.K. Visas tiesības paturētas.

Autors Benedikt Meurer <benny@xfce.org>.

Būvēts ar Gtk+-%d.%d.%d, skrien Gtk+-%d.%d.%d.

Lūdzu, ziņojiet par kļūdām uz <%s>.
 %s ir BEZ JEBKĀDĀM GARANTIJĀM,
Jūs varat izplatīt %s zem GNU Lesser General Public License
nosacījumiem, kuri ir atrodami pirmavota pakotnē %s 


 Darbību ikonas Aktīvs Aktīvā vienuma robežu krāsa Aktīvā vienuma aizpildījuma krāsa Aktīvā vienuma indeks Aktīvā vienuma teksta krāsa Visi faili Visas ikonas Animācijas Lietotņu izvēles poga Lietotņu ikonas Balsa Bloka iekārta Pārlūkot failu sistēmu Pārlūkot faila sistēmu, lai norādītu izmantojamo komandu. Pārlūkot Tīmekli K_omentārs: Iz_veidot Rakstzīmju iekārta Norādiet vēlamo lietotni Norādiet pielāgoto failu pārvaldnieku Norādiet pasta lasītāju Norādiet termināla emulatoru Norādiet tīmekļa pārlūku Norādiet faila nosaukumu Chromium Claws Mail Notīrīt meklēšanas lauku Kolonnu atstarpe Kolonnu atstarpes Kom_anda: Autortiesības (c) %s
        os-cillation e.K. Visas tiesības paturētas.

Autors Benedikt Meurer <benny@xfce.org>.

 Izveidot direktoriju Izveidot palaidēju Izveidot palaidēju <b>%s</b> Izveidot saiti Izveidot jaunu darbvirsmas failu, dotajā direktorijā Kursora vienuma robežu krāsa Kursora vienuma aizpildījuma krāsa Kursora vienuma teksta krāsa Pielāgot rīkjoslu... Debian Sensible pārlūks Debian X termināla emulators Ierīču ikonas Velciet vienumu uz augšējām rīkjoslām, lai pievienotu, vai no rīkjoslām, uz vienumu tabulas, lai aizvāktu. Labot direktoriju Rediģēt palaidēju Rediģēt saiti Emblēmas Emocijzīmes Ieslēgt meklēšanu Encompass Enlightened termināla emulators Epiphany tīmekļa pārlūks Evolution Izpildāmie faili Rinda (FIFO) Neizdevās izveidot "%s". Neizdevās izpildīt noklusēto Failu pārvaldnieku Neizdevās izpildīt noklusēto Pasta lasītāju Neizdevās izpildīt noklusēto Termināla emulatoru Neizdevās izpildīt noklusēto Tīmekļa pārlūku Neizdevās palaist vēlamo aplikāciju no kategorijas "%s". Neizdevās ielādēt saturu no "%s": %s Neizdevās ielādēt attēlu "%s": Iemesls nav zināms, iespējams attēla fails ir bojāts Neizdevās atvērt "%s". Neizdevās atvērt %s rakstīšanai Neizdevās atvērt URI "%s". Neizdevās atvērt displeju Neizdevās atvērt failu "%s": %s Neizdevās parsēt saturu "%s": %s Neizdevās nolasīt failu "%s": %s Neizdevās saglabāt "%s" Neizdevās uzstādīt noklusēto failu pārvaldnieku Neizdevās uzstādīt noklusēto pasta lasītāju Neizdevās uzstādīt noklusēto termināla emulatoru Neizdevās uzstādīt noklusēto tīmekļa pārlūku Failam "%s" nav tipa atslēga Faila pārvaldnieks Faila tipa ikonas Fila atrašanās vieta nav parasts fails vai direktorija Mape Sekot stāvoklim GIkona GNOME termināls Galeon tīmekļa pārlūks Viendabīgs Kā teksts un ikona tiek novietoti attiecībā viens pret otru Icedove Iceweasel Ikona Ikonu joslas modelis Ikonu skata modelis Ja jūs nenorādīsiet --lunch opciju, exo-open atvērs visus norādītos
URL ar to vēlamajiem URL apdarinātājiem. Ja, jūs norādiet --lunch
opciju, jūs varēsiet izvēlēties vēlamo lietotni, un padot papildus
parametrus lietotnei (piemēram, TerminalEmulator, jūs varat
padot komandlīniju, kuru izpildīt terminālī). Attēlu faili Nederīgs palīdzības tips "%s" KMail Konqueror tīmekļa pārlūks Palaist noklusēto palīdzības TIPU ar neobligātajiem PARAMETRIEM, kur TIPS ir viena no sekojošām vērtībām. Darbavirsmas faila palaišana netiek atbalstīta, kad %s ir kompilēts bez GIO-Unix iespējām. Izkārtojuma režīms Links teksta pārlūks Vietu ikonas Lynx teksta pārlūks Pasta lasītājs Mala Iezīmēšanas kolonna Izvēlņu ikonas Midori Modeļa kolonna meklēšanai, kad meklē vienumos Izmantota modeļa kolonna, lai iegūtu ikonas pixbuf no Izmantota modeļa kolonna, lai iegūtu tekstu no Modeļa kolonna, lai iegūtu tekstu, ja izmanto Pango iezīmēšanu Modelis ikonu joslai Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nav norādīta neviena lietotne Nav norādīta komanda Neviens fails nav atlasīts Nav norādīts fails/mape Nav ikonas Kolonnu skaits Attēlojamo kolonnu skaits Atvērt Vēlamo lietotņu
konfigurācijas dialoga logu Opera Browser Opcijas: Novietojums PCMan failu pārvaldnieks Perl skripti Pixbuf kolonna Lūdzu norādiet savu vēlamo failu pārvaldnieku
un apstipriniet, lai turpinātu. Lūdzu norādiet savu vēlamo pasta lasītāju
un apstipriniet, lai turpinātu. Lūdzu norādiet savu vēlamo termināla
emulatoru un apstipriniet, lai turpinātu. Lūdzu norādiet savu vēlamo tīmekļa
pārlūku un apstipriniet, lai turpinātu. Lūdzu ziņojiet par kļūdām uz <%s>.
 Vēlamās lietotnes Vēlamās lietotnes (tīmekļa pārlūks, pasta lasītājs un termināla emulators) Noklusētais URL, kad veido saiti Noklusētā komanda, kad veido palaidēju Noklusētais komentārs, kad veido darbvirsmas failu Noklusētā ikona, kad veido darbvirsmas failu Noklusētais nosaukums, kad veido darbvirsmas failu Nospiediet kreiso peles taustiņu, lai mainītu izvēlēto lietotni. Priekšskatījums Izdrukāt versijas informāciju un iziet Python skripti RXVT Unicode Lasīt e-pastu Attēlot atkarībā no atlases stāvokļa. Pārkārtojams Atiestates komanda Rindu atstarpe Rindu atstarpes Ruby skripti Palaist _terminālī SOKETA ID Meklēšanas kolonna Izvēlēties _ikonu no: Izvēlēties darba direktoriju Norādiet lietotni Norādiet ikonu Norādiet lietotni Norādiet noklusētās lietotnes, dažādiem pakalpojumiem Izvēlaties šo opciju, lai ieslēgtu sāknēšanas paziņošanu, kad komanda tiek palaista no failu pārvaldnieka, vai izvēlnes. Ne visas lietotnes atbalsta sāknēšanas paziņošanu. Izvēlaties, šo opciju, ja vēlaties, lai komanda tiek izpildīta termināla logā Atlases režīms Atdalītājs Sesijas atiestates komanda Uzstādījumi soketu pārvaldniekam Čaulas skripti Viens klikšķis Viena klikšķa noildze Ligzda Atstarpe, pie ikonu skata malām Atstarpe, starp vienuma šūnām Atstarpe starp režģa kolonnām Atstarpe starp režģa rindām Atstarpe Norādiet lietotni, kuru vēlaties izmantot,
kā noklusēto failu pārvaldnieku: Norādiet lietotni, kuru vēlaties izmantot,
kā noklusēto pasta lasītāju: Norādiet lietotni, kuru vēlaties izmantot,
kā noklusēto termināla emulatoru: Norādiet lietotni, kuru vēlaties izmantot,
kā noklusēto tīmekļa pārlūku: Statusa ikonas Sylpheed TIPS [PARAMETRI] Termināla emulators Teksta kolonna Tekstu svarī_gajām ikonām Tekstu _visām ikonām Attēlojamā GIcon. Atstarpe, starp divām secīgām kolonnām Atstarpe starp divām secīgām rindām Laika daudzums, pēc kura vienums zem kursora, tiks automātiski iekrāsots, viena klikšķa režīmā Fails "%s" nesatur nekādus datus --lunch komandas atbalstītie TIPI: Attēlojamā ikona. Izkārtojuma režīms Modelis ikonu skatam Ikonjoslas novietojums Atlases režīms Attēlojamās ikonas izmērs pikseļos. Katra vienuma platums Thunar Rīkjoslas _stils Ievadiet "%s --help", lai izdrukātu lietošanas instrukcijas. Veidojamā darbvirsmas faila tips (lietotne vai saite) Nevar noteikt URI-shēmu priekš "%s". Negrupētās ikonas Darbvirsmas faila tips "%s" netiek atbalstīts Izmantošana: %s [opcijas] [fails]
 Lietošana: exo-open [URLi...] Izmantot _sāknēšanas paziņošanu Izmantot lietotni, kura nav iekļauta sarakstā. Izmantot komandrindu Skats ļauj lietotājiem interaktīvi meklēt kolonnās Skats ir pārkārtojams W3M teksta pārlūks Tīmekļa pārlūks Vai visiem bērniem būt vienāda izmēra Vai skatā esošie vienumi ir aktivizējami ar vienu klikšķi Katra vienuma platums Logu grupa Logu grupas līderis Darba direktorija: X terminālis Xfce termināls [FAILS|MAPE] _Pievienot jaunu rīkjoslu A_tcelt _Aizvērt Noklusētā darbvirsma _Palīdzība _Ikona: Tikai _ikonas _Nosaukums: _Labi _Atvērt _Cits... _Noņemt rīkjoslu Meklēt ikonu: Tikai _tekstu _URL: aterm izmērs 