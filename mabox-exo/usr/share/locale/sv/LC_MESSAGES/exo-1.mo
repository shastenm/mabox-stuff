��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  h  1?  1   �@  ,   �@  *   �@  .   $A  �   SA  ,   0B  :   ]B  /   �B  7   �B  7    C  �   8C  U   �C  N   D  =   lD  D   �D  �   �D    �E  �   �F     �G     �G     �G      �G     �G     �G  
   H     'H     3H     ?H     SH     aH  
   gH     rH     xH  A   �H     �H     �H     �H     �H     I     I     .I     MI  "   lI     �I     �I     �I  
   �I     �I     �I     �I  
    J  x   J     �J     �J     �J     �J  1   �J     K      #K     DK     aK     xK     �K     �K     �K  !   �K  �   �K     cL     tL     �L     �L     �L     �L  	   �L     �L     �L  	    M     
M     M      M  8   ?M  9   xM  8   �M  3   �M  K   N  1   kN  Y   �N  !   �N  -   O  (   GO  #   pO  *   �O  1   �O  )   �O      P  =   <P  >   zP  A   �P  <   �P      8Q     YQ     fQ  1   tQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q  D   R     JR     QR  	   YR     cR     hR     |R  
   �R  D  �R  	   �S     �S     �S     T     
T  b   !T  X   �T     �T     �T      U     U     "U     /U     8U  
   EU     PU  8   WU  Y   �U  >   �U  6   )V  I   `V     �V     �V     �V     �V     �V     W     W     W     W     0W     CW     YW     hW  %   �W  
   �W     �W     �W  6   �W     X     #X     /X     ;X     OX     gX     sX  M   �X  M   �X  Q   Y  L   oY     �Y     �Y  D   �Y     0Z  (   NZ  '   wZ  "   �Z  "   �Z  4   �Z     [  (   +[     T[  	   b[  G   l[     �[  	   �[     �[     �[  2   �[     \     )\     :\     O\     \\     i\     u\  	   �\     �\  
   �\     �\     �\     �\     �\     �\  *   �\  �   %]  L   �]     ^     *^     6^  !   V^  
   x^  
   �^     �^     �^  -   �^  &   �^  /   _  ,   4_  	   a_  N   k_  N   �_  R   	`  M   \`     �`     �`     �`     �`     �`     �`  
   �`  
    a     a     %a     <a  /   Oa  ;   a  g   �a  !   #b  �   Eb  4   Dc     yc     �c     �c     �c  \   �c  n   *d  j   �d  h   e     me  1   ~e  &   �e     �e     �e  "   �e  4   f  3   Lf     �f  #   �f      �f  !   �f     �f  A   g     [g  >   qg     �g     �g     �g     �g  '   �g  4    h     Uh     mh     {h     �h  
   �h     �h     �h  
   �h     �h     �h     �h     i     i     $i     +i  	   :i     Di     Ki     Oi  	   Wi     ai     vi     }i     �i     �i     �i     �i     �i     �i                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-09-13 18:50+0000
Last-Translator: Stefan Höök <stefan.hook@gmail.com>
Language-Team: Swedish (http://www.transifex.com/xfce/exo/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [flaggor] --build-list [[namn fil]...]
        exo-open --launch TYP [PARAMETRAR...]   --build-list      Tolka (namn, fil) par
   --extern          Generera externa symboler
   --launch TYP [PARAMETRAR...]        Kör det föredragna programmet för TYP
                                      med frivilliga PARAMETRAR, där TYP är
                                      ett av följande värden.   --name=identifierare C-makro/variabelnamn
 --output=filename Skriv mottagen csource till angiven fil
   --static          Generera statiska symboler
   --strip-comments Ta bort kommentarer från XML-filer
   --strip-content Ta bort nodinnehåll från XML-filer
   --working-directory KATALOG         Standardarbetskatalog för program
                                      när flaggan --launch används.   -?, --help                          Skriv ut det här hjälpmeddelandet och avsluta   -V, --version                       Skriv ut versionsinformation och avsluta   -V, --version     Skriv ut versionsinformation och avsluta
   -h, --help        Skriv ut det här hjälpmeddelandet och avsluta
   WebBrowser       - Den föredragna webbläsaren.
  MailReader       - Den föredragna e-postklienten.
  FileManager      - Den föredragna filhanteraren.
  TerminalEmulator - Den föredragna terminalemulatorn. %s (Xfce %s)

Copyright © 2003-2006
        os-cillation e.K. Alla rättigheter är reserverade.

Skrivet av Benedikt Meurer <benny@xfce.org>.

Byggt med Gtk+-%d.%d.%d, kör Gtk+-%d.%d.%d.

Rapportera fel till <%s>.
Skicka synpunkter på översättningen till <tp-sv@listor.tp-sv.se>.
 %s levereras UTAN NÅGON FORM AV GARANTIER,
Du kan distribuera kopior av %s under villkoren för
GNU Lesser General Public License som kan hittas i
källkodspaketet för %s.

 Åtgärdsikoner Aktiv Kantfärg för aktivt objekt Fyllnadsfärg för aktivt objekt Index för aktiva objekt Textfärg för aktivt objekt Alla filer Alla ikoner Animeringar Programväljarknapp Programikoner Balsa Blockenhet Brave Bläddra i filsystemet Bläddra genom filsystemet för att välja ett anpassat kommando. Surfa på webben K_ommentar: S_kapa Filhanteraren Caja Teckenenhet Välj föredraget program Välj en anpassad filhanterare Välj en anpassad e-postklient Välj en anpassad terminalemulator Välj en anpassad webbläsare Välj filnamn Chromium Claws Mail Töm sökfältet Kolumnmellanrum Kolumnmellanrum Komm_ando: Copyright © %s
        os-cillation e.K. Alla rättigheter reserverade.

Skrivet av Benedikt Meurer <benny@xfce.org>.

 Skapa katalog Skapa programstartare Skapa programstartare <b>%s</b> Skapa länk Skapa en ny skrivbordsfil i den angivna katalogen Kantfärg för markörobjekt Fyllnadsfärg för markörobjekt Textfärg för markörobjekt Anpassa verktygsrad... Debians sensible-browser Debian X-terminalemulator Enhetsikoner Dillo Visa _inte detta  meddelande igen Dra ett objekt till verktygsraderna ovan för att lägga till det, från verktygsraderna till objekttabellen för att ta bort det. Redigera katalog Redigera programstartare Redigera länk Emblem Känsloikoner Aktivera sökning Encompass Enlightened terminalemulator Webbläsaren Epiphany Evolution Körbara filer FIFO Misslyckades med att skapa "%s". Misslyckades med att köra den föredragna filhanteraren Misslyckades med att köra den föredragna e-postklienten Misslyckades med att starta föredragen terminalemulator Misslyckades med att starta föredragen webbläsare Misslyckades med att starta det föredragna programmet för kategorin "%s". Misslyckades med att läsa innehållet i "%s": %s Misslyckades med att läsa in bilden "%s": Okänd anledning, antagligen en skadad bildfil Misslyckades med att öppna "%s". Misslyckades med att öppna %s för skrivning Misslyckades med att öppna URI:en "%s". Misslyckades med att öppna display Misslyckades med att öppna filen "%s": %s Misslyckades med att tolka innehållet i "%s": %s Misslyckades med att läsa filen "%s": %s Misslyckades med att spara "%s". Misslyckades med att ställa in den föredragna filhanteraren Misslyckades med att ställa in den föredragna e-postklienten Misslyckades med att ställa in den föredragna terminalemulatorn Misslyckades med att ställa in den föredragna webbläsaren Filen "%s" saknar en type-nyckel Filhanterare Filtypsikoner Filens plats är inte en vanlig fil eller katalog Katalog Följ tillstånd GIcon GNOME-terminal Webbläsaren Galeon Geary Google Chrome Homogena Hur texten och ikonen i varje objekt placeras relativt till varandra Icecat Icedove Iceweasel Ikon Modell för ikonrad Ikonvymodell Ikonkolumn Om du inte anger --launch kommer exo-open öppna alla angivna URL:er med
deras föredragna URL-hanterare. Annars, om du anger --launch, kan du välja
vilken av de föredragna programmen du vill köra, och de kan ge extra parametrar
till program (t.ex. kan du ange kommandot som ska köras i terminalen
med TerminalEmulator). Bildfiler Ogiltig hjälpartyp "%s" Jumanji KMail Webbläsaren Konqueror Kör standardhjälparen för TYP med frivilliga PARAMETRAR, där TYP är ett av följande värden. Körning av skrivbordsfiler stöds inte när %s är kompilerad utan GIO-Unix-funktioner. Layoutläge Textwebbläsaren Links Platsikoner Textwebbläsaren Lynx E-postklient Marginal Markupkolumn Menyikoner Midori Modellkolumn att söka i genom vid sökning efter objekt Modellkolumn använd för att hämta den absoluta sökvägen till en bildfil att behandla Modellkolumn som används för att hämta ikonens pixbuf från Modellkolumn som används för att hämta texten från Modellkolumn som används för att hämta texten om Pango-markup används Modell för ikonraden Webbläsaren Mozilla Mozilla Firefox Mozilla e-post Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Inget program valt Inget kommando angavs Ingen fil vald Ingen fil/katalog angavs Ingen hjälpare definierad för "%s". Ingen ikon Antal kolumner Antal kolumner att visa Öppna konfigurationsdialogen
för Föredragna program Webbläsaren Opera Alternativ: Orientering Filhanteraren PCMan PCManFM-Qt Filhanterare Perl-skript Pixbuf-kolumn Välj den filhanterare som du föredrar
och tryck på OK för att fortsätta. Välj den e-postklient som du föredrar
och tryck på OK för att fortsätta. Välj den terminalemulator som du
föredrar och tryck på OK för att fortsätta. Välj den webbläsare som du
föredrar och tryck på OK för att fortsätta. Rapportera fel till <%s>.
 Föredragna program Föredragna program (webbläsare, e-postklient och terminalemulator) Standard-URL för nya länkar Standardkommando för ny programstartare Standardkommentar för ny skrivbordsfil Standardikon för ny skrivbordsfil Standardnamn för ny skrivbordsfil Vänsterklicka för att ändra det valda programmet. Förhandsvisning Skriv ut versionsinformation och avsluta Python-skript QTerminal Fråga standardhjälpen om TYPE, då TYPE är ett av följande värden. QupZilla ROX-Filer RXVT Unicode Läs din e-post Rita ut olika baserat på markeringens tillstånd. Omarrangeringsbar Omstartskommando Filhanteraren Rodent Radmellanrum Radmellanrum Ruby-skript Kör i _terminal UTTAGS-ID Sakura Sökkolumn Välj _ikon från: Välj en arbetskatalog Välj ett program Välj en ikon Välj program Välj standardprogram för olika tjänster Välj det här alternativet för att aktivera startnotifiering när kommandot körs från filhanteraren eller menyn. Inte alla program har stöd för startnotifiering. Välj det här alternativet för att köra kommandot i ett terminalfönster. Markeringsläge Avgränsare Omstartskommando för sessionen Uttag för inställningshanterare Skalskript Enkelklick Tidsgräns för enkelklick Uttag Mellanrum som infogas vid kanterna av ikonvyn Mellanrum mellan cellerna i ett objekt Mellanrum som infogas mellan kolumner i rutnät Mellanrum som infogas mellan rader i rutnät Mellanrum Välj det program som du vill använda
som föredragen filhanterare för Xfce: Välj det program som du vill använda
som föredragen e-postklient för Xfce: Välj det program som du vill använda
som föredragen terminalemulator för Xfce: Välj det program som du vill använda
som föredragen webbläsare för Xfce: Statusikoner Standardikoner Surf Sylpheed TYP [PARAMETER] Terminalemulator Terminator Textkolumn Text för _viktiga ikoner Text för _alla ikoner GIcon att rita ut. Storleken på mellanrummet mellan två kolumner Storleken på mellanrummen mellan två efterföljande rader Tiden det tar innan objektet som är under muspekaren automatiskt kommer att väljas i enkelklicksläge Filen "%s" innehåller inget data Följande TYPEs srtöds för --launch och --query kommandona:

  WebBrowser       - Föredragen webbläsare.
  MailReader       - Föredraget e-postprogram.
  FileManager      - Föredragen filhanterare.
  TerminalEmulator - Föredragen terminalemulator. De följande TYPerna stöds för kommandot --launch: Ikonen att rita ut. Layoutläget Modellen för ikonvyn Orienteringen för ikonraden Den föredragna filhanteraren kommer att användas för att bläddra i innehållet i mappar. Den föredragna e-postläsaren kommer att användas för att skriva e-brev när du klickar på e-postadresser. Den föredragna terminalemulatorn kommer att användas för att köra kommandon som kräver CLI-omgivning. Den föredragna webbläsaren kommer att användas för att öppna hyperlänkar och visa hjälpinnehåll. Markeringsläget Storleken för ikonen att rita ut, i bildpunkter. Bredden som används för varje objekt Thunar _Stil på verktygsrad Ange "%s --help" för användning. Typ av skrivbordsfil att skapa (program eller länk) Det gick inte att identifiera URI-schema för "%s". Okategoriserade ikoner Skrivbordsfiltypen "%s" stöds inte Användning: %s [flaggor] [fil]
 Användning: exo-open [URL:er...] Använd _startsnotifiering Använd ett anpassat program som inte finns i ovanstående lista. Använd kommandoraden Vyn tillåter användaren att interaktivt söka genom kolumner Vyn är omarrangeringsbar Vimprobable2 Textwebbläsaren W3M Webbläsare Huruvida alla barn ska ha samma storlek Huruvida objekten i vyn kan aktiveras med enkelklick Bredd för varje objekt Fönstergrupp Fönstergruppledare Arbets_katalog: X-terminal Xfce-terminalen Filhanteraren Xfe [FIL|MAPP] _Lägg till en ny verktygsrad _Avbryt _Stäng _Skrivbordets standard _Hjälp _Ikon: Endast _ikoner _Internet _Namn: _OK _Öppna _Annat... _Ta bort verktygsrad _Spara _Sök efter ikon: Endast _text _URL: Ver_ktyg aterm qtFM storlek 