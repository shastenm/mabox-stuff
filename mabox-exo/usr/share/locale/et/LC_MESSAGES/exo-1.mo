��         �  u  l      @  2   A  -   t  -   �  ,   �  �   �  *   �  ,     3   9  8   m  �   �  F   =  H   �  7   �  5     �   ;  �     �   �     �     �     �     �     �     �  	   �  	   �  
             ,     >     D     Q  2   h     �  	   �     �     �     �     �     �       !   5     W     s     �  
   �     �     �     �  	   �  o   �     B      S      c      }   0   �      �      �      �      !     !     .!     I!     V!  b   \!     �!     �!  	   �!     �!  	   �!     �!  	   "     "     ."  	   C"     M"     ^"     c"  &   z"  %   �"  +   �"  %   �"  9   #  %   S#  H   y#     �#     �#     �#     $     %$  $   B$     g$     �$  "   �$  !   �$  '   �$  !   %     (%     B%     O%  0   _%     �%     �%     �%     �%     �%     �%  H   �%     !&     (&  	   0&     :&     ?&     N&  h  ^&     �'     �'     �'     �'     �'  i   (  W   z(     �(     �(     �(      )     )     )     %)  
   3)     >)  :   E)  2   �)  +   �)  <   �)     *     3*     C*     S*     `*     t*     y*     �*     �*     �*     �*     �*     �*     �*     �*     +  4   *+     _+     m+     v+     �+     �+     �+  F   �+  E   �+  K   =,  E   �,     �,     �,  G   -     K-  '   k-  +   �-  (   �-  (   �-  ;   .     M.  "   U.     x.  	   �.     �.     �.  0   �.     �.     �.     �.     /     /     '/     4/  	   E/     O/     V/     d/     w/     �/     �/     �/  0   �/  �   �/  ;   �0     �0  	   �0     �0     
1     "1     01     =1     R1  5   Y1  0   �1  +   �1  )   �1     2  I   2  H   h2  N   �2  H    3     I3     V3     _3     p3     �3     �3     �3     �3  3   �3  0   4  r   64     �4  ;   �4     5     5     (5     D5     c5  )   v5     �5     �5     �5     �5  4   �5  (   $6     M6  "   a6     �6     �6     �6  A   �6     7  8   +7     d7     x7     �7  0   �7  A   �7     8     8     )8     =8  
   Q8     \8     j8     {8     �8     �8     �8     �8     �8     �8     �8  	   �8     �8     �8  	   �8     �8     9  
   9     9  
   %9     09     69  a  ;9  3   �:  )   �:  ,   �:  ,   (;  �   U;  +   R<  /   ~<  7   �<  8   �<  �   =  C   �=  N   �=  8   E>  7   ~>  �   �>  �   u?  �   e@     �@     A     A     2A     MA     cA     A     �A     �A     �A     �A     �A  
   �A     �A  1   �A     +B  	   7B     AB     FB  
   WB     bB      �B  "   �B  '   �B      �B     C     C     %C     2C     OC     \C     iC  z   qC     �C     �C     D     *D  +   8D     dD     ~D     �D     �D     �D     �D     E     E  |   E     �E     �E     �E  	   �E  
   �E     �E  	   �E     F     !F  	   7F     AF     TF     YF  0   xF  2   �F  7   �F  0   G  @   EG  &   �G  Z   �G     H  '   'H  "   OH     rH  '   �H  /   �H  (   �H  #   I  /   1I  1   aI  ,   �I  /   �I  #   �I     J      J  0   7J     hJ     nJ     J     �J     �J     �J  @   �J     �J     �J  	   K     K     K     "K  �  4K     �L     �L     �L     �L     M  l   M  W   �M     �M     �M      N     N     &N     5N     :N     KN     ]N  >   dN  (   �N      �N  G   �N     5O     LO     aO     qO     �O     �O     �O     �O     �O     �O     �O     �O  (   P     .P     ;P     GP  -   `P     �P     �P     �P     �P     �P     �P  I   �P  L   'Q  P   tQ  J   �Q  %   R     6R  L   LR  !   �R  (   �R  /   �R  -   S  ,   BS  ;   oS     �S     �S     �S  	   �S     �S     �S  /   T     2T     ET     \T     oT     {T     �T     �T     �T     �T     �T     �T     �T     �T     	U     U  4   ,U  �   aU  5   V  
   HV     SV     \V     zV     �V     �V     �V     �V  #   �V  %   �V     W  $   8W     ]W  E   bW  G   �W  B   �W  F   3X     zX     �X     �X     �X  
   �X     �X     �X     �X  #   Y  !   +Y  `   MY      �Y  *   �Y     �Y     Z     Z     0Z     AZ     VZ     oZ     �Z     �Z  .   �Z  4   �Z  ,   [     =[  )   O[  !   y[     �[  !   �[  =   �[     \  D   ,\     q\     �\     �\  1   �\  7   �\     ]     *]     7]     J]  
   Z]     e]     s]     �]     �]     �]     �]     �]     �]     �]     �]  	   �]     �]     ^     ^     ^     '^     7^     E^  	   K^     U^     [^     M   5       ]     �   �   �           c   �          �   ~   �         r   �      �           D   �   W   f   i   L   C   �       v   �               .                              �   �   �   �   �   �   �   e             �                   �   �   7   �       ?         �   �   �       �         9   �     �   �                 �   �   �      ,       �   �              _      z   �   �   �   �   �   �   �          �   |   �   h         =   /               �       �       >   +          �   �       �   �   
            �   [       &           �   6   w           �   }   {   G   �       P               l   �   �     �       Z       �      R   x   �   �     �       �   �   (       �   �   8   �           S   J      *   u           �           �       �   �   �   �   �           �   U   �   �         I       �       �   �     �   m   V   H   g     �   ^   �   	      �          p   K   �       �       1   �   �   �   A   �   @   E   �       #      s   �         �       n   �   �   T   �           O       -   �   �   �      0   '   �       Q   �   t               �          
            �   �   !   ;           �   �   �   <   �      �   b   2   �   "   �   o   �   �           N     �   X   $   �     �       �       q   4           �       :   �   F       �   Y   �       �   a   	      j      �       �               3       %   B   `          �              \   )   y   �   d   �   k       �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Sylpheed TYPE [PARAMETER] Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _Open _Other... _Remove Toolbar _Search icon: _Text only _URL: _Utilities aterm size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Estonian (http://www.transifex.com/xfce/exo/language/et/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [omadused] --build-list [[nimi fail]...]
 exo-open --launch TÜÜP [PARAMEETRID...]   --build-list      Otsi (nimi, fail) paare
   --extern          Tekita välised märgid
   --launch TÜÜP [PARAMEETRID...]       Käivita eelistatav rakendus
                                      TÜÜP koos mittekohustuslike parameetritega (PARAMEETER), kus
                                      TÜÜP on üks järgnevatest väärtustest.   --name=väärtus    C makro/muutuja nimi
   --static          Tekita staatilised märgid
   --strip-comments  Eemalda kommentaarid XML failidest
   --strip-content   Eemalda sõlmede sisu XML failidest
   --working-directory KATALOOG        Vaikimisi töökataloog rakendustele
                                      kui kasutatakse --launch valikut.   -?, --help                          Trüki see abiteade ja välju   -V, --version                       Trüki versiooni informatsioon ja välju   -V, --version     Versiooni trükkimine ja väljumine
   -h, --help        Abiteate trükkimine ja väljumine
   WebBrowser       - Eelistatud veebisirvija.
  MailReader       - Eelistatud e-posti lugeja.
  FileManager      - Eelistatud failihaldur.
  TerminalEmulator - Eelistatud terminaliemulaator. %s (Xfce %s)

Autoriõigus (c) 2003-2006
       os-cillation·e.K.·Kõik õigused kaitstud.

Kirjutanud·Benedikt·Meurer·<benny@xfce.org>.

Ehitatud kasutades Gtk+-%d.%d.%d, jooksmas Gtk+-%d.%d.%d.

Vigadest andke teada aadressil <%s>.
 %s on ILMA ÜHEGI TAGATISETA,
Te võite jagada koopiaid %s-ist GNU Vähem Üldise
Avaliku Litsentsi tingimustel, mille leiate
%s lähtekoodi pakist.

 Tegevuste ikoonid Aktiivne Aktiivse eseme serva värv Aktiivse eseme täitevärv Aktiivse eseme indeks Aktiivse eseme teksti värv Kõik failid Kõik ikoonid Animatsioonid Rakenduse valimise nupp Rakenduste ikoonid Balsa Plokkseade Sirvi failisüsteemi Kohandatud käsu valimiseks sirvi failisüsteemi. Sirvi veebi _Märkus: _Loo Caja failihaldur Märkseade Eelistatud rakenduse valimine Kohandatud failihalduri valimine Kohandatud e-posti lugeja valimine Kohandatud terminaliemulaatori valimine Kohandatud veebisirvija valimine Vali failinimi Chromium Claws e-post Otsingu teksti tühjendamine Tulpade vahe Tulpade vahe _Käsk: Autoriõigus (c) %s
       os-cillation·e.K.·Kõik õigused kaitstud.

Kirjutanud·Benedikt·Meurer·<benny@xfce.org>.

 Kataloogi loomine Käivitaja loomine Loo käivitaja <b>%s</b> Viida loomine Antud kataloogi uue töölaua faili loomine Kursori eseme serva värv Kursori eseme täitevärv Kursori elemendi teksti värv Kohanda tööriistariba... Debiani Sensible brauser Debiani X terminaliemulaator Seadmete ikoonid Dillo Lohista element ülalpool asetsevatele tööriistaribadele, et see lisada; eemaldamiseks lohista elementide tabelist välja. Kataloogi muutmine Käivitaja muutmine Viida muutmine Embleemid Emotikonid Otsimise lubamine Encompass Enlightened terminaliemulaator Epiphany veebibrauser Evolution Täidetavad failid FIFO „%s” loomine ebaõnnestus. Vaikimisi failihalduri käivitamine ebaõnnestus Vaikimisi e-posti lugeja käivitamine ebaõnnestus Vaikimisi terminaliemulaatori käivitamine ebaõnnestus Vaikimisi veebisirvija käivitamine ebaõnnestus Jaotise „%s” eelistatud rakenduse käivitamine ebaõnnestus. „%s” sisu avamine ebaõnnestus: %s Pildi „%s” laadimine ebaõnnestus: põhjust ei tea, pildifail on tõenäoliselt vigane „%s” avamine ebaõnnestus. %s avamine kirjutamiseks ei õnnestunud URI „%s” avamine ebaõnnestus. Kuva avamine ebaõnnestus Faili „%s” avamine ebaõnnestus: %s Faili „%s” sisu sõelumine ebaõnnestus: %s Faili „%s” lugemine ebaõnnestus: %s „%s” salvestamine ebaõnnestus. Vaikimisi failihalduri määramine ebaõnnestus Vaikimisi e-posti lugeja määramine ebaõnnestus Vaikimisi terminali määramine ebaõnnestus Vaikimisi veebisirvija määramine ebaõnnestus Failil „%s” ei ole võtme liiki Failihaldur Failitüüpide ikoonid Faili asukoht ei ole tavaline fail või kataloog Kaust Oleku järgimine GIcon GNOME Terminal Galeon veebibrauser Ühtlane Kuidas iga elemendi tekst ja ikoon üksteise suhtes paigutatakse Icecat Icedove Iceweasel Ikoon Ikooniriba mudel Ikoonivaate mudel Kui te ei täpsusta --launch seadistust, siis avab exo-open kõik määratud
URL-id nende eelistatavate URL-i käsitlejatega. Kui te aga määratlete --launch
seadistuse, siis te saate valida, millist eelistatud rakendust te soovite käivitada
ja te saate ka määrata täiendavaid parameetreid (näiteks: terminaliemulaatori
jaoks saate te määrata käsurea, mida tuleb käivitada terminali käivitamisel). Pildifailid Vigane aitaja liik „%s” Jumanji KMail Konqueror veebibrauser Käivita vaikimisi TÜÜBIGA aitaja valikulise PARAMEETRIGA, kus tüüp on üks järgnevatest väärtustest. Töölaua failide käivitamist ei toetata, kui %s on kompileeritud GIO-Unix omadusteta. Paigutusviis Links tekstibrauser Asukohtade ikoonid Lynx tekstibrauser E-posti lugeja Piir Märgistuse tulp Menüüde ikoonid Midori Mudeli tulp, mis otsitakse läbi, kui esemest midagi otsitakse Mudeli tulp, kust ikooni pixbuf saadakse Mudeli tulp, kust tekst saadakse Mudeli tulp, kust tekst saadakse, kui kasutatakse Pango märgistuskeelt Mudel ikooniriba jaoks Mozilla veebibrauser Mozilla Firefox Mozilla e-post Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Rakendust pole valitud Käsku pole määratud Ühtegi faili pole valitud Ühtegi faili ega kausta pole määratud Ikoon puudub Tulpade arv Näidatavate tulpade arv Ava eelistatud rakenduste
seadistuste dialoog Opera veebibrauser Valikud: Suund PCMan failihaldur Perli skriptid Pixbuf-tulp Palun vali eelistatav failihaldur
ja klõpsa jätkamiseks "Sobib" nupule. Palun vali eelistatav e-posti lugeja
ja klõpsa jätkamiseks "Sobib" nupule. Palun vali eelistatav terminaliemulaator
ja klõpsa jätkamiseks "Sobib" nupule. Palun vali eelistatav veebibrauser
ja klõpsa jätkamiseks "Sobib" nupule. Palun teata vigadest aadressil <%s>.
 Eelistatud rakendused Eelistatavad rakendused (veebibrauser, e-posti lugeja ja terminaliemulaator) Ettemääratud URL viite loomisel Ettemääratud käsk käivitaja loomisel Ettemääratud märkus töölaua faili loomisel Ettemääratud ikoon töölaua faili loomisel Ettemääratud nimi töölaua faili loomisel Valitud rakenduse vahetamiseks klõpsa vasakut hiireklahvi. Eelvaade Trüki versioon ja välju Pythoni skriptid ROX-Filer RXVT Unicode Loe e-posti Esitatakse teistmoodi vastavalt valiku olekule. Ümberjärjestatav Taaskäivitamise käsk Rodent failihaldur Ridade vahe Ridade vahe Ruby skriptid Käivitatakse _terminalis SOKLI ID Sakura Otsimise tulp Ikooni _asukoht: Töökataloogi valimine Rakenduse valimine Ikooni valimine Rakenduse valimine Vaikimisi rakenduste valimine erinevatele teenustele Selle valikuba saab käivitusmärguande sisse lülitada, kui käsk failihaldurist või menüüst käivitatakse. Mitte kõigil rakendustel pole töölemineku märguande toetust. Selle märkimisel käivitatakse käsk terminaliaknas. Valikuviis Eraldaja Seansi taaskäivitamise käsk Seadistuste halduri sokkel Käsurea skriptid Üks klõps Ühe klõpsu aegumine Sokkel Ikoonivaate äärtesse jäetav ruum Elemendi lahtrite vahele jäetav ruum Ruum ruudustiku tulpade vahel Ruudustiku ridade vahele jääv ruum Vahe Määra rakendus, mida soovid vaikimisi
Xfce failihaldurina kasutada: Määra rakendus, mida soovid vaikimisi
Xfce e-posti lugejana kasutada: Määra rakendus, mida soovid vaikimisi
Xfce terminalina kasutada: Määra rakendus, mida soovid vaikimisi
Xfce veebibrauserina kasutada: Olekute ikoonid Sylpheed TÜÜP [PARAMEETER] Terminal Tekstitulp Tekst tä_htsatele ikoonidele Tekst _kõikidele ikoonidele Kasutatav GIcon. Ruum kahe järjestikuse tulba vahel Ruum kahe järjestikuse rea vahel Aeg, pärast mida hiirekursori all paiknev element valitakse ühe klõpsu režiimis automaatselt Fail „%s” ei sisalda andmeid --launch käsk toetab järgnevaid TÜÜPE: Kasutatav ikoon. Paigutusviis Mudel ikoonivaate jaoks Ikooniriba suund Valiku tegemise viis Ikooni suurus pikslites. Iga eseme jaoks kasutatav laius Thunar Tööriistariba _laad Trüki „%s --help” kasutusjuhise saamiseks Loodava töölaua faili tüüp (rakendus või viide) Pole võimalik tuvastada „%s” URI-skeemi Jaotamata ikoonid Toetamata töölaua faili tüüp „%s” Kasutamine: %s [omadused] [fail]
 Kasutamine: exo-open [URLid...] Kasutataks_e käivitusmärguannet Mõne muu rakenduse kasutamine, mida ülemises loetelus pole. Kasuta käsurida Vaade võimaldab kasutajal otsida interaktiivselt kõigist tulpadest Vaade on ümberjärjestatav W3M tekstibrauser Veebisirvija Kas kõik alamesemed peaksid olema sama suurusega Kas vaates olevaid asju saab ühe klõpsuga aktiveerida Laius igale esemele Akende rühm Akende rühma juht Töök_ataloog: X Terminal Xfce terminal Xfe failihaldur [FAIL|KAUST] _Lisa uus tööriistariba _Loobu _Sulge _Töölaua vaikimisi valik A_bi _Ikoon: Ainult _ikoonid _Internet _Nimi: _Ava _Muu... _Eemalda tööriistariba Ikooni _otsing: _Ainult tekst _URL: _Vahendid aterm suurus 