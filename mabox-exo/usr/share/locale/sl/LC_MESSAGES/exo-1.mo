��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  2   �@  %   A  -   )A  -   WA  �   �A  *   B  >   1B  ,   pB  7   �B  8   �B  `   C  6   oC  :   �C  )   �C  9   D  �   ED  �   �D  �   �E     gF     tF     |F  !   �F     �F  !   �F     �F  	   G  	   G     G     7G     GG     MG     ^G     dG  ;   �G     �G     �G     �G     �G     �G     H  '   (H  !   PH  '   rH  $   �H     �H     �H  
   �H     �H     �H     I     I  m   "I     �I     �I     �I     �I  2   �I     J     4J     TJ     tJ     �J     �J     �J     �J  "   �J  o    K  
   pK     {K     �K     �K     �K     �K  	   �K      �K     �K  	   L     L     +L     0L  4   ML  .   �L  7   �L  3   �L  <   M  %   ZM  V   �M     �M  &   �M     N  #   8N  '   \N  ,   �N  #   �N     �N  G   �N  2   ;O  ;   nO  F   �O      �O     P     'P  2   :P     mP     rP     P     �P     �P     �P     �P     �P  M   �P     Q     Q  	   'Q     1Q     7Q     NQ     gQ  1  wQ     �R     �R     �R     �R     �R  _   S  R   dS     �S     �S     �S     �S     T     T     T     2T     ?T  3   FT  5   zT  <   �T  6   �T  U   $U     zU     �U     �U     �U     �U     �U     �U     �U     �U     V     V     /V     JV     dV  
   �V     �V     �V  -   �V     �V  
   �V     W     W     %W     =W     JW  L   YW  H   �W  L   �W  I   <X  $   �X     �X  N   �X  *   Y  -   =Y  7   kY  5   �Y  3   �Y  B   Z  	   PZ     ZZ     oZ  	   ~Z  ]   �Z     �Z  	   �Z     �Z     [  (   [     F[     W[     j[     �[     �[     �[     �[     �[     �[     �[     �[     �[     \      \     /\  2   @\  �   s\  2   ]     C]     O]     V]  "   q]     �]     �]     �]     �]  -   �]  %   ^  "   (^  !   K^     m^  Y   t^  S   �^  Y   "_  V   |_     �_     �_     �_     �_     �_     `  
    `     +`     =`     V`     m`      �`      �`  Z   �`  !   a  �   Aa  +   $b     Pb     eb     ub     �b  G   �b  w   �b  Q   jc  `   �c     d  '   )d  !   Qd     sd     zd  1   �d  G   �d  (   e     4e  &   Ee  #   le     �e     �e  8   �e     f  <   f     Xf     rf     f     �f  %   �f  ;   �f     g     !g     .g     Ag  
   Pg     [g     ig     �g     �g  
   �g     �g     �g     �g     �g     �g     �g     �g     �g     h  
   h     h     1h     9h     Fh     Uh     bh     oh     uh     zh                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-19 07:48+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/xfce/exo/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
        %s [options] --build-list [[name file]...]
 exo-open --launch TIP [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Ustvari eksterne simbole
 --launch TIP [PARAMETERs...] Zaženi želeno aplikacijo
 TIP z izbranimi PARAMETERs, kjer je
 TIP ena izmed sledečih vrednosti.   --name=identifier C macro/variable name
 --output=filename Zapiši generiran csource v podano datoteko
   --static          Ustvari statine simbole
   --strip-comments  Odstrani komentarje iz XML datotek
   --strip-content   Remove node contents from XML files
 --working-directory DIRECTORY Privzeti delovni imenik za aplikacije
 ob uporabi --launch opcije.  	

-?, --help Natisni sporočilo pomoči in izstopi   -V, --version                       Podatki o različici   -V, --version     Podatki o različici
   -h, --help        Natisni to sporoilo pomoi in izstopi
 WebBrowser - želeni spletni brskalnik.
 MailReader - želeni bralnik pošte.
 FileManager - želeni upravljalnik datotek.
 TerminalEmulator - želeni terminalski emulator. %s (Xfce %s)

Copyright (c) 2003-2006
 os-cillation e.K. Vse pravice pridržane.

Napisal Benedikt Meurer <benny@xfce.org>.

Zgrajeno z Gtk+-%d.%d.%d, teče na Gtk+-%d.%d.%d.

Prosim, prijavite hroše na <%s>.
 %s je na uporabo BREZ KAKRNEKOLI GARANCIJE,
kopije %s lahko razpeujete pod pogoji
GNU Lesser General Public License, ki jih lahko najdete v
izvornem paketu %s.

 Ikone dejanj Dejaven Barva obrobe dejavnih predmetov Barva polnila dejavnih predmetov  Seznam dejavnih predmetov Barva besedila dejavnih predmetov Vse datoteke Vse ikone Animacije Gumb za izbiranje aplikacij Ikone aplikacij Balsa Blokiraj napravo Brave Brskajte po datotečnem sistemu Prebrskajte datotečni sistem za izbor prilagojenega ukaza. Brskaj po spletu _Opomba: U_stvari Caja upravljalnik datotek Naprava znakov Izberite želeno aplikacijo Izberi prilagojeni upravljalnik datotek Izberi prilagojeni bralnik pošte Izberi prilagojeni terminalski emulator Izberi prilagojeni spletni brskalnik Izberi ime datoteke Chromium Claws Mail Poisti iskalno polje Razmik stolpca Razmik stolpcev _Ukaz: Copyright (c) %s
        os-cillation e.K. Vse pravice pridrane.

Napisal Benedikt Meurer <benny@xfce.org>.

 Ustvari mapo Ustvari zaganjalnik Ustvarite zaganjalec <b>%s</b> Ustvarite povezavo Ustvarite novo namizno datoteko v izbranem imeniku Barva obrobe predmeta kazalca Barva polnila elementov kazalca Barva besedila predmeta kazalca Prilagodi orodno vrstico... Debian Sensible brskalnik Debianov X terminalski emulator Ikone naprav Dillo Tega sporočila _ne prikaži več. Povleci predmet na opravilno vrstico, da bi ga dodal, ali snemi predmet z opravilne vrstice da bi ga odstranil. Uredi mapo Uredi zaganjalnik Uredite povezavo Simboli ustveni simboli Omogoči iskanje Encompass Enlightened terminalski emulator Epiphany spletni brskalnik Evolution Izvedljive datoteke FIFO Napaka pri ustvarjanju "%s". Napaka pri izvajanju želenega upravljalnika datotek Napaka pri izvajanju želenega bralnika pošte Napaka pri izvajanju želenega terminalskega emulatorja Napaka pri izvajanju želenega spletnega brskalnika Napaka pri zaganjanju želene aplikacije za "%s" kategorijo. Napaka pri nalaganju vsebine "%s": %s Napaka pri nalaganju slike "%s": Razlog neznan, najverjetneje okvara slikovne datoteke Napaka pri odpiranju "%s". Napaka pri odpiranju %s za zapisovanje Napaka pri odpiranju URI "%s". Napaka pri odpiranju prikazovalnika Neuspeh pri odpiranju datoteke "%s": %s Napaka pri razčlenjevanju vsebine "%s": %s  Napaka pri branju datoteke "%s": %s Napaka pri shranjevanju "%s". Med nastavljanjem privzetega upravljalnika datotek je prišlo do napake Napaka pri nastavljanju privzetega bralnika pošte Napaka pri nastavljanju privzetega terminalskega emulatorja Med nastavljanjem privzetega spletnega brskalnika je prišlo do napake Datoteka "%s" nima vrste ključa Upravljalnik datotek Ikone vrst datotek Lokacija datoteke ni običajna datoteka ali imenik Mapa Sledi stanju GIcon GNOME terminal Galeon spletni brskalnik Geary Google Chrome Homogeno Kako sta besedilo in ikona vsakega predmeta postavljena v medsebojni relaciji Icecat Icedove Iceweasel Ikona Vzorec ikonske vrstice Vzorec ikonskega pogleda Stolpec z ikono Če ne navedete --launch opcije, bo exo-open odprl vse navedene
URLje v njihovih izbranih URL upravljalcih. Če pa navedete --launch
opcijo, lahko izberete zaželeno aplikacijo za zagon in
dodate parametre v aplikacijo (npr. za TerminalEmulator
lahko dodate ukazno vrstico, ki se naj zažene v terminalu). Slikovne datoteke Nepravilen pomočnik tipa "%s" Jumanji KMail Konqueror spletni brskalnik Zaženi privzeti pomočnik TIP z iz izbirnim PARAMETER, kjer je TIP ena od sledečih vrednosti. Zaganjanje namiznih datotek ni podprto, ko je %s kompiliran brez GIO-Unix funkcij. Nain postavitve Links besedilni brskalnik Ikone lokacij Lynx besedilni brskalnik Bralnik pošte Stopnja Oznaevalni stolpec Ikone menija Midori Vzorec stolpca za iskanje med iskanjem med predmeti Stolpec modela z absolutno potjo do slikovne datoteke Vzorec stolpca uporabljenega za pridobitev pixbuf ikone z/iz Vzorec stolpca uporabljenega za pridobitev besedila iz Vzorec stolpca, uporabljenega za pridobitev besedila, e ta uporablja Pango oznaevanje Vzorec za ikonsko vrstico Mozillin brskalnik Mozilla Firefox Mozillina pošta Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ni izbrane aplikacije Ni določenega ukaza Nobena datoteka ni izbrana Ni navedene datoteke/mape Za "%s" pomočnik ni določen. Brez ikone tevilo stolpcev tevilo stolpcev za prikaz Odprite zaželeno nastavitveno
okno aplikacij Opera brskalnik Možnosti: Usmerjenost PCMan brskalnik datotek PCManFM-Qt File Manager Skripti Perl Pixbuf stolpec Prosim, izberite želen upravljalnik datotek
in kliknite OK za nadaljevanje. Prosim, izberite želen brskalnik pošte
in kliknite OK za nadaljevanje. Prosim, izberite želen terminalski
emulator in kliknite OK za nadaljevanje. Prosim, izberite želen spletni
brskalnik in kliknite OK za nadaljevanje. Prosimo, sporočite napake na <%s>.
 Priljubljene aplikacije Želene aplikacije (spletni brskalnik, bralnik pošte in terminalski emulator) Prednastavljen URL ob ustvarjanju povezave Prednastavljen ukaz ob ustvarjanju zaganjalca Prednastavljen komentar ob ustvarjanju datoteke namizja Prednastavljena ikona ob ustvarjanju datoteke namizja Prednastavljeno ime ob ustvarjanju datoteke namizja Pritisnite na levi gumb miške za spreminjanje izbrane aplikacije. Predogled Podatki o različici Skripti Python QTerminal Poizvedi v privzetem pomočniku za TIP, pri čemer ima lahko TIP eno od naslednjih vrednosti. QupZilla ROX-Filer RXVT Unicode Preberite svojo pošto Upodobi razlino, glede na stanje izbora. Prerazporedljivo Ponovno zaeni ukaz Rodent brskalnik datotek Razmik vrstic Razmik vrstic Skripti Ruby Zaženi v _terminalu ID PRIKLJUČKA Sakura Stolpec iskanja Izberi _ikono iz: Izberite delovno mapo Izberite program Izberite ikono Izberite program Izberite privzete aplikacije za različne storitve Izberite to opcijo za omogočanje zagonskega obvestila ob zagonu ukaza v upravljalniku datotek ali meniju. Zagonskega obvestila ne podpira vsaka aplikacija. Izberite to opcijo za zagon ukaza v oknu terminala Nain izbora Loilec Ukaz za ponoven zagon seje Priključek upravljalca nastavitev Skripti ukazne lupine Enojni klik Zakasnitev enojnega klika Vti Razmik, vstavljen na robove ikonskega pogleda Razmik, vstavljen med celice predmeta Razmik, vstavljen med mree stolpca Razmik, vstavljen med mree vrstic Razmik Navedite aplikacijo, ki jo želite uporabiti
kot privzeti upravljalnik datotek v Xfce-ju: Navedite aplikacijo, ki jo želite uporabiti
kot privzeti bralnik pošte v Xfce-ju: Navedite aplikacijo, ki jo želite uporabiti
kot privzeti terminalski emulator v Xfce-ju: Navedite aplikacijo, ki jo želite uporabiti
kot privzeti spletni brskalnik v Xfce-ju: Ikone statusa Zaloge ikon Surf Sylpheed TIP [PARAMETER] Terminalski emulator Terminator Besedilni stolpec Besedilo _pomembnih ikon Besedilo _Za vse ikone Gicon za upodobitev. Razmak med zaporednima stolpcema Razmak med zaporednima vrsticama Koliina asa po katerem bo predmet pod kazalcem mikeizbran samodejno v nainu enojnega klika Datoteka "%s" ne vsebuje podatkov Za -launch in -query ukaze so podprti naslednji TIPi;

WebBrowser - Privzeti spletni brskalnik.
MailReader - Privzeti bralnik pošte.
FileManager - Privzeti upravljalnik datotek.
TerminalEmulator - Privzeti emulator terminala. Naslednji TIPi so podprti za --launch ukaz: Ikona za upodobitev. Nain postavitve Vzorec za ikonski pogled Usmerjenost ikonske vrstice Za brskanje po vsebini map bo uporabljen privzeti upravljalnik datotek. Za pisanje elektronske pošte, kadar kliknete na elektronski naslov, bo uporabljen privzeti bralnik elektronske pošte. Za izvajanje ukazov v ukazni vrstici, boo uporabljen privzeti emulator terminala. Za odpiranje spletnih povezav in prikazovanje pomoči, bo uporabljen privzeti spletni brskalnik. Nain izbora Velikost ikon za upodobitev, v pikslih. irina uporabljena za vsak predmet Thunar I_zgled orodne vrstice Za navodila o uporabi zaženite ukaz "%s --help". Tip namizne datoteke, ki jo želite ustvariti (aplikacija ali povezava) Napaka pri zaznavanju URI-sheme za "%s". Nerazvrene ikone Nepodprta  vrsta datoteke namizja "%s" Uporaba: %s [možnosti] [datoteka]
 Uporaba: exo-open [URL-ji ...] Uporabi obvestilo ob _zagonu Uporabi prirejeno aplikacijo, ki ni navedena na seznamu. Izberi ukazno vrstico Pogled dovoljuje uporabniku interaktivno iskanje med stolpci Pogled je prerazporedljiv Vimprobable2 W3M besedilni brskalnik Spletni brskalnik e naj bojo otroci vsi enake velikosti Ali so lahko predmeti v pogledu aktivirani z enojnim klikom irina za vsak predmet Skupina oken Vodja skupine oken _Delovna mapa: X terminal Xfce terminal Xfce brskalnik datotek [DATOTEKA|MAPA] _Dodaj novo opravilno vrstico _Prekliči _Zapri _Privzeto za namizje _Pomoč I_kona: _Samo ikone _Splet _Ime: _V redu _Odpri _Ostalo... _Odstrani orodno vrstico _Shrani _Poii ikono: Samo _Besedilo Naslov _URL: _Pripomočki aterm qtFM velikost 