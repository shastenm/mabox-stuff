��    $     <	  �  \      p  2   q  -   �  -   �  ,      �   -  *     ,   <  3   i  8   �  �   �  F   m  H   �  7   �  5   5  �   k  �   2  �        �     �     �     �     �       	   "  	   ,  
   6     A     \     n     t     �     �  2   �     �  	   �     �     �                 2      O   !   k      �      �      �   
   �      �      �      �   	   �   o   !     x!     �!     �!     �!  0   �!     �!     	"      "     7"     L"     d"     "     �"  b   �"     �"     #  	   #     #  	   $#     .#  	   <#     F#     d#  	   y#     �#     �#     �#  &   �#  %   �#  +   �#  %   )$  9   O$  %   �$  H   �$     �$     %     +%     D%     [%  $   x%     �%     �%  "   �%  !   �%  '   &  !   <&     ^&     x&     �&  0   �&     �&     �&     �&     �&     �&     '     '     '  H   "'     k'     r'  	   z'     �'     �'     �'     �'  h  �'     )     ))     B)     J)     P)  i   f)  W   �)     (*     4*     G*     V*     h*     t*     {*  
   �*     �*  :   �*  J   �*  2   !+  +   T+  <   �+     �+     �+     �+     �+     ,     ,     ,     !,     *,     =,     U,     j,     {,     �,     �,     �,     �,  4   �,     -     *-     3-     ?-     R-     j-     w-  F   �-  E   �-  K   .  E   ^.     �.     �.  G   �.      /  '   @/  +   h/  (   �/  (   �/  ;   �/     "0  "   *0     M0  	   \0     f0  	   o0     y0     �0  0   �0     �0     �0     �0     �0     1     1     1  	   -1     71     >1     L1     _1     z1     �1     �1  0   �1  �   �1  ;   �2     �2  	   �2     �2     �2     
3     3     %3     :3  5   A3  0   w3  +   �3  )   �3     �3  I   4  H   P4  N   �4  H   �4     15     >5     C5     L5     ]5  
   o5     z5     �5     �5     �5  3   �5  0   �5  r   .6     �6  ;   �6     �6     7      7     <7     [7  )   n7     �7     �7     �7     �7  4   �7  (   8     E8  "   Y8     |8     �8     �8  A   �8     9  8   #9     \9     p9     }9     �9  0   �9  A   �9     :     !:     .:     B:  
   V:     a:     o:     �:     �:     �:     �:     �:     �:     �:     �:  	   �:     �:     �:     �:  	   �:     �:     ;     ;  
   #;     .;  
   4;     ?;     E;     J;  j  O;  -   �<  -   �<  %   =  (   <=  �   e=  -   6>  ,   d>  5   �>  5   �>  �   �>  H   �?  M   �?  <   @  5   [@  �   �@  �   <A  �   B     �B     �B     �B     �B     �B     C  
   #C  	   .C     8C     DC     WC     cC     iC     rC     xC  .   �C     �C     �C     �C     �C     �C     D     D     1D     JD     hD     �D     �D     �D     �D     �D     �D  
   �D  s   �D  	   PE     ZE     iE     �E  '   �E     �E     �E     �E     F     F     7F     PF     \F  p   bF     �F     �F     �F      G     G     G  	   G     %G     AG  	   TG     ^G     nG     sG  )   �G  '   �G  ,   �G  &   
H  ;   1H  *   mH  O   �H     �H  !    I     "I     >I     WI  '   wI  $   �I     �I  (   �I  &   J  +   -J  %   YJ     J     �J     �J  )   �J     �J     �J     �J     �J     K     K     #K     1K  I   9K     �K     �K  	   �K     �K     �K     �K  
   �K  Q  �K  
   )M     4M     MM     UM     [M  X   oM  Z   �M     #N     3N     HN     XN  
   lN     wN     |N     �N     �N  5   �N  U   �N  '   )O  (   QO  9   zO     �O     �O     �O     �O      P     P     P      P     )P     <P     PP     hP     yP  "   �P  	   �P     �P     �P     �P     Q     Q     Q     )Q     <Q  
   TQ     _Q  A   lQ  ?   �Q  F   �Q  =   5R     sR     �R  :   �R     �R  #   �R  (   S  #   DS  #   hS  1   �S     �S  '   �S     �S  	   T     T  	   T     !T     .T  %   >T     dT     ~T     �T     �T     �T  
   �T     �T  	   �T     �T     �T     �T     	U     U     *U     7U  '   CU  �   kU  :   V     FV     RV     YV      nV     �V     �V     �V     �V  4   �V  4   �V  /   /W  $   _W  	   �W  D   �W  B   �W  F   X  A   ]X  
   �X     �X     �X     �X     �X  
   �X     �X     �X     Y     Y  *   8Y  (   cY  Y   �Y     �Y  0   Z     5Z     NZ     `Z      xZ     �Z  3   �Z  (   �Z     [     	[  (   [  7   E[  '   }[     �[  "   �[     �[     �[     \  6   (\     _\  =   q\     �\     �\     �\  	   �\  1   �\  4   "]     W]     p]     }]     �]  
   �]     �]     �]     �]     �]     �]     �]      ^     ^     ^     "^  
   .^     9^     @^     D^  
   J^     U^     j^     q^     ~^     �^  	   �^     �^     �^     �^        F   �   �   �       4       _   }   c   $          h           �      
    �               �   �   �   a     �   z   r               B          k   �   �       �   �   �           "   �   �   m           �   Q   �   v       R   	       �   .   �   [           �   �   �   �       N   �       �          �   �   �       �       �   <       +   -   �   ]   i   \             d   �   
             P       (   �   �              2      �              �         `   �   �   �      9   L   !  �   �   �     K   �           �   G   �       6        1   �       �   �        "  �      �   �           �   H   �   V   o   ;   �   �     �   !           �               �   7       �   �          �   I   l   T   �          �   �   �   )        �     �       �       �   w     �   u   C         �      �              �   @   S     �   A      �   �   �   �       �   #      D           �       �       �                 �   '   ?   �   �             p   �         s             $                %   {   �   �         >   �        X               �     g   :      �   �   	      �   �   �   b   �       ~   =   e   �   |          &   y       q                       �   �      8   j   M   *   O   �       �   3           Y          �                     t           f   5   �       W   �   �   �   E   �   �   0     �   �   J       #   �   �   �   �   �   Z   �   U       �   ,   n         /   x   �   �                �   �   �           ^   �   �   �   �   �              %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:22+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Norwegian Nynorsk (http://www.transifex.com/xfce/exo/language/nn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nn
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [val] --build-list [[namn fil]...]
        exo-open --launch TYPE [Parametrar...]   --build-list      Tolk (namn, fil)
   --extern          Lag eksterne symbol
   --launch TYPE [PARAMETRAR...]        Start standardprogrammet av
                                      TYPE med valfrie PARAMETRAR, der
                                      TYPE er ein av følgjande typar.   --name=identifikator C makroo/variabelnamn
   --static          Generer statiske symbol
   --strip-comments  Fjern kommentarar frå XML-filer
   --strip-content   Fjern nodeinnhald frå XML-filer
   --working-directory MAPPE           Standard arbeidsmappe for program
                                      ved bruk av valet --launch.   -?, --help                          Skriv ut denne meldinga og avslutt   -V, --version                       Skriv ut versjonsinformasjon og avslutt   -V, --version     Skriv ut versjonsinformasjon og avslutt
   -h, --help        Skriv ut denne hjelpa og avslutt
   WebBrowser       - Standard nettlesar.
  MailReader       - Standard epostlesar.
 FileManager       - Standard filutforskar
 TerminalEmulator - Standard teminalprogram. %s (Xfce %s)

Opphavsrettt (c) 2003-2006
        osar-cillation e.K. Alle rettar reservert.

Skrive av Benedikt Meurer <benny@xfce.org>.

Bygd med Gtk+-%d.%d.%d, køyrar Gtk+-%d.%d.%d.

Meld feil til <%s>.
 %s har ABSOLUTT INGEN GARANTI,
Du kan spreia kopiar av %s under vilkåra i
GNU Lesser General Public Lisens som ligg i %s kjeldekodedistribusjonen.

 Handlingsikon Aktiv Kantfarge på aktivt element Fyllfarge på aktivt element Aktivt element indeks Tekstfarge på aktivt element Alle filer Alle ikon Animasjonar Programveljarknapp Programikon Balsa Blokkfil Brave Bla gjennom filsystemet Bla gjennom filsystemet for å velja kommando. Surf på nettet K_ommentar: _lag ny Caja filutforskar Teikn-eining Vel standardprogram Vel din eigen filutforskar Vel din eigen epostlesar Vel ditt eige terminalprogram Vel din eigen nettlesar Vel filnamn Chromium Claws Epost Tøm søkjefelt Spaltemellomrom Spaltemellomrom Komm_ando. Copyright (c) %s
        osar-cillation e.K. Alle rettar reserverte.

Skrive av Benedikt Meurer <benny@xfce.org>.

 Lag mappe Lag ny snarveg Lag ny snarveg <b>%s</b> Lag ny lenkje Lag ein ny desktop-fil i gjeven katalog Kantfarge på spesielle element Fyllfarge på peikaren Tekstfarge på peikar Tilpass verktøylinja ... Debian Sensible Browser Debian X-terminalprogram Einingsikon Dillo Dra eit element til verktøylinja for å leggje til, eller frå verktøylinja til elementlista for å fjerna det Rediger mappe Rediger snarveg Rediger lenkje Emblem Smileys Tillat søk Encompass Enlightened terminalprogram Epiphany nettlesar Evolution Kjørbare filer FIFO Klarte ikkje lage "%s". Klarte ikkje starta standard filutforskar Klarte ikkje starta standard epostlesar Klarte ikkje starta standard terminalprogram Klarte ikkje starta standard nettlesar Klarte ikkje å starta standardprogram for kategorien "%s". Klarte ikkje lasta innhaldet frå "%s": %s Klarte ikkje lasta bilete "%s": Ukjend årsak, sannsynlegvis er fila øydelagd. Klarte ikkje opna "%s". Klarte ikkje opna %s for skriving Klarte ikkje opna URI "%s". Klarte ikkje opna skjerm Klarte ikkje opna fila "%s": %s Klarte ikkje tolke innhaldet i "%s": %s Klarte ikkje lese frå file "%s": %s Klarte ikkje lagre "%s". Klarte ikkje setja standard-filutforskar Klarte ikkje setje standard epostlesar Klarte ikkje setja standard-terminalprogram Klarte ikkje setje standard nettlesar Fila "%s" har ingen typenøkkel Filutforskar Filtypeikon Filplassering er ikkje ei fil eller mappe Mappe Følg tilstand GIkon GNOME terminal Galeon nettlesar Geary Google Chrome Homogen Korleis tekst og ikon for kvart element er plassert i høve til kvarandre Icecat Icedove Iceweasel Ikon Ikon-verktøylinjemodell Ikonvisingsmodell Ikonspalte Viss du ikkje bruker valet --launch vil exo-open opna alle spesifiserte
Url'ar med deira førvalde Url-handterarar. Viss du bruker valet --launch
kan du velja kva for eit standardprogram du vil køyra og i tillegg
senda parametrar til programmet (td. for terminalprogrammet kan du senda
kommandoane som skal køyrast i terminalvindauga). Bildefiler Ugyldig hjelpertype "%s" Jumanji KMail Konqueror nettlesar Start standardhjelpar av TYPE med valfritt PARAMETER, der TYPE er ein av desse verdiane. Oppstart av desktop-filer er ikkje støtta når %s er kompilert utan GIO-Unix- funksjonar. Utformingsmodus Links tekstnettlesar Plasseringsikon Lynx tekstnettlesar Epostlesar Marg Markeringsspalte Menyikon Midori Modellspalte å søkja gjennom ved søk etter element Modellspalte brukt for å henta den absolutte stien til ei biletfil som skal teiknast Modellspalte som pixbuf blir henta frå Modellspalte som teksten blir henta frå Modellspalte brukt for å hente tekst om Pango blir nytta Modell for ikonverktøylinja Mozilla nettlesar Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ingen program valde Ingen kommando er nemnd Inga fil er vald Inga fil/mappe er nemnd Ingen hjelpar definert for «%s». Utan ikon Kor mange spalter Kor mange spalter å visa Opna standard 
oppsettsdialog Opera nettlesar Val: Orientering PCMan filutforskar PCManFM-Qt filutforskar Perlskript Pixbufspalte Vel favoritt-filutforskaren din no
og klikk OK for å halda fram. Vel favoritt-epostlesaren din no
og klikk OK for å halda fram. Vel favoritt-terminalprogrammet ditt no
og klikk OK for å halda fram. Vel favorittnettlesaren din no
og klikk OK for å halda fram. Meld programfeil til <%s>.
 Standardprogram Standardprogram (nettlesar, epostlesar og terminalprogram) Standard Url for nye lenkjer Standard kommando for nye snarvegar Standard kommentar for nye desktop-filer Standard ikon for nye dekstop-filer Standard namn for nye desktop-filer Bruk venstre museknapp for å endra valt program. Førehandsvising Skriv ut versjonsinformasjon og avslutt Pythonscript QTerminal QupZilla ROX-filer RXVT Unicode Les eposten din Teikn ulikt basert på vald tilstand. Kan forandre rekkjefølgd Omstart kommando Rodent filutforskar Radmellomrom Radmellomorom Rubyskript Køyr i _terminal SOKKEL-ID Sakura Søkjespalte Vel _ikon frå: Vel arbeidsmappe Vel eit program Vel eit ikon Vel program Vel standardprogram for ulike oppgåver Vel dette valet for å slå på oppstartsvarsel når kommandoen blir køyrt frå filutforskaren eller frå menyen. Ikkje alle program støttar oppstartsvarsel. Vel denne for å køyra kommandoen i eit terminalvindauge. Veljarmodus Skilje Auka omstartkommando Sokkel for innstillingshandterar Skallskript Enkeltklikk Enkeltklikk tidsgrense sokkel Mellomrom som blir lagt langs kanten på ikonvisinga Mellomrom som blir brukt mellom celler i eit element Mellomrom som blir plassert mellom spaltegitter Mellomrom nytta mellom rutenettrader Mellomrom Vel programmet du ynskjer å bruka som
standard filutforskar i Xfce: Vel programmet du ynskjer å bruka som
standard epostlesar i Xfce: Vel programmet du ynskjer å bruka som
standard teminalprogram i Xfce: Vel programmet du ynskjer å bruka som
standard nettlesar i Xfce: Statusikon Surf Sylpheed TYPE [PARAMETER] Terminalprogram Terminator Tekstspalte Tekst for _viktige ikon Tekst for _alle ikon GIkonet som skal teiknast. Mengda rom mellom to påfølgjande spalter Mengda rom mellom to påfølgjande rader Tida det tek før eit element under musepeikaren blir valt automatisk i enkeltklikkmodus. Fila "%s" inneheld ingen data Desse typane er støtta for --launch kommandoen: Ikonet som skal teiknast Utformingsmodusen Modellen for ikonvising Orientering av ikonverktøylinje Veljarmodus Biletpunkt-storleiken på ikonet som skal teiknast. Breidda som blir brukt på kvart element Thunar Verktøylinje_stil Skriv '%s --help' for bruksinstruksjonar Type desktop-fil som skal lagast (program eller lenkje) Klarte ikkje finna URI-schema for "%s". Ikkje kategoriserte ikon Ikkje støtta desktop-filtype "%s" Bruk: %s [val] [fil]
 Bruk: exo-open [url'er...] Bruk _oppstartsvarsel Bruk sjølvvalt program som ikkje ligg i lista ovanfor Bruk kommandlinja Gje brukaren løyve til å søkja gjennom spalter interaktivt Visinga kan sorterast  Vimprobable2 W3M tekstnettlesar Nettlesar Om alle underoppføringar skal vere same storleik Om elementa i visinga kan aktiverast med enkeltklikk Breidd for kvart element Vindusgruppe Vindauge gruppeleder Arbei_dsmappe: X Terminal Xfce-terminal Xfe filutforskar [FIL|MAPPE] _legg til ny verktøylinje Av_bryt L_ukk _skrivebordstandard _Hjelp _ikon: Berre _ikon _Internett _namn: _OK _Opna _Andre ... _Fjern verktøylinje _Lagra _søkjeikon: Berre _tekst _URL: _Verktøy aterm qtFM storleik 