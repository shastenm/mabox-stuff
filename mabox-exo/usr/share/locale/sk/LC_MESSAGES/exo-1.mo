��    '     T	  �  �      �  2   �  -   �  -   "  ,   P  �   }  *   a  >   �  ,   �  3   �  8   ,  �   e  F   �  H   C  7   �  5   �  �   �  �   �  �   �     D     Q     X     q     �     �  	   �  	   �  
   �     �     �     �                    2   -      `   	   o      y      �      �      �      �      �   !   �      !     8!     H!  
   Q!     \!     o!     ~!  	   �!  o   �!     "     "     ("     B"  0   N"     "     �"     �"     �"     �"     �"     #     #  b   !#     �#     �#  	   �#     �#  	   �#     �#  	   �#     �#     �#  	   $     $     #$     ($  &   ?$  %   f$  +   �$  %   �$  9   �$  %   %  H   >%     �%     �%     �%     �%     �%  $   &     ,&     I&  "   ^&  !   �&  '   �&  !   �&     �&     '     '  0   $'     U'     \'     i'     o'     ~'     �'     �'     �'  H   �'     �'     (  	   	(     (     (     '(     7(  h  C(     �)     �)     �)     �)     �)  i   �)  W   _*     �*     �*     �*     �*     �*     +     
+  
   +     #+  :   *+  J   e+  2   �+  +   �+  <   ,     L,     c,     s,     �,     �,     �,     �,     �,     �,     �,     �,     �,     
-     #-     ?-     G-     Y-  4   v-     �-     �-     �-     �-     �-     �-  F   �-  E   C.  K   �.  E   �.     /     8/  G   O/     �/  '   �/  +   �/  (   0  (   40  ;   ]0     �0  "   �0     �0  	   �0  L   �0     *1  	   31     =1     J1  0   Z1     �1     �1     �1     �1     �1     �1     �1  	   �1     �1     2     2     #2     >2     T2     c2  0   v2  �   �2  ;   I3     �3  	   �3     �3     �3     �3     �3     �3     �3  5   4  0   ;4  +   l4  )   �4     �4  I   �4  H   5  N   ]5  H   �5     �5     6     6     6     6     -6  
   ?6     J6     V6     p6     �6  3   �6  0   �6  r   �6     q7    �7  ;   �8     �8     �8     9     9     <9  )   O9     y9     �9     �9     �9  4   �9  (   �9     &:  "   ::     ]:     y:     �:  A   �:     �:  8   ;     =;     Q;     ^;     o;  0   {;  A   �;     �;     <     <     #<  
   7<     B<     P<     a<     o<     �<     �<     �<     �<     �<     �<  	   �<     �<     �<     �<  	   �<     �<     �<     �<  
   =     =  
   =      =     &=     +=  �  0=  9   �>  +   ?  6   B?  ,   y?  �   �?  :   �@  =   �@  /   A  9   <A  :   vA  �   �A  M   NB  Q   �B  @   �B  <   /C  �   lC  �   `D  �   BE     F     F  !   'F  "   IF     lF      �F     �F     �F  	   �F     �F     �F     G     G     G     "G  <   ?G     |G     �G  
   �G     �G     �G  '   �G  '   �G  %   #H  (   IH  ,   rH     �H     �H  
   �H      �H     �H     �H  	   I  s   I     �I     �I     �I     �I  0   �I  9   J  9   OJ  7   �J     �J     �J     �J     K     $K  j   *K     �K     �K     �K     �K  	   �K     �K  	   �K     �K  !   L  	   ;L     EL     [L     `L  4   ~L  4   �L  7   �L  ;    M  H   \M  )   �M  i   �M     9N  $   VN      {N     �N  &   �N  (   �N  )   O     5O  5   RO  5   �O  8   �O  <   �O  &   4P     [P     mP  =   �P  
   �P     �P     �P     �P     �P     Q     #Q  
   1Q  F   <Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q  {  �Q     `S     rS     �S     �S  "   �S  �   �S  ^   BT     �T     �T     �T     �T     �T     U     U     %U     2U  9   9U  [   sU  9   �U  1   	V  Q   ;V     �V     �V     �V     �V     �V     �V     �V     �V     �V  "   W     4W     RW  -   qW     �W     �W     �W     �W  6   �W     .X  
   4X     ?X     KX     cX     wX  W   �X  W   �X  Z   6Y  ^   �Y  !   �Y     Z  a   /Z  0   �Z  3   �Z  7   �Z  3   .[  4   b[  >   �[     �[  +   �[     \  	   !\  j   +\     �\  	   �\     �\     �\  ,   �\     �\     
]      ]     9]     H]     Y]     m]     �]     �]     �]     �]     �]     �]     �]     �]  1   
^  �   <^  A   �^     9_     H_  "   T_     w_     �_     �_  +   �_     �_  2   �_      `  3   ?`  2   s`  	   �`  X   �`  V   	a  Y   `a  ]   �a     b     %b     6b     ;b     Db     Tb  
   ib     tb      �b     �b     �b  5   �b  4   c  U   Nc  $   �c    �c  F   �d  !   e     7e     Je     ee     ~e  &   �e  &   �e     �e     �e  7   �e  C   4f  "   xf     �f  &   �f  #   �f  "   �f  "   g  H   >g     �g  B   �g     �g     h     h     h  '   8h  G   `h     �h     �h     �h     �h     �h     
i     i     /i     Ci  	   ci  
   mi     xi  
   �i     �i  
   �i  	   �i     �i     �i  	   �i     �i     �i  	   �i     j  	   j      j  
   -j     8j     >j  	   Cj     �   M        �     �     �           q            :       �      S   %       _   >   �   r       z   m   �       �       W       �   �   ^   �   �   �   �   �      �   �   `   �       7           �   =           �   �          �   �           !  �   .       �      8   �   A                 �       �      	   l   �                           3       �         h   �           �   �   &       "  %  <      �   �   I       U   �   P   �       �           j       �   2       �   D          	  �   w   C   �   �           $   |       "   �   �       T   �   �   �   4       '      �       e   �         k   \   +               �   �       @                         
        b   �      �   �   �          �   �   �      �   -     c   �   �       J   a      ~       f   R   �   y   K   �   �   /   �   }   O              [   H          �     �   �   �   �           g   0     #   )       �   v       �      �   &      �   �   �       u        ;       (          �   $  F   �   �   �              �          �     �   �   �   �   N   5      �   �   #  �       1   G       �         �   �   9   ?   �      �       �   i             *   t   Y   �      s   Z      �   �     Q   d        E   L   �   �   '   {   �       n   �       o       �               �   �           �       �   �                 6   �     �           ,   x       �   !           �      ]   B   �   �       
   �   �       �       p      �   �   �   V   �   �        �            �           �      �                X   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-01-09 12:39+0000
Last-Translator: Jose Riha <jose1711@gmail.com>
Language-Team: Slovak (http://www.transifex.com/xfce/exo/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
        %s [možnosti] --build-list [[názov súboru]...]
        exo-open --launch TYP [PARAMETRE...]   --build-list      Parsovacie páry (názov, súbor)
   --extern          Generuje extern symboly
   --launch TYP [PARAMETRE...]         Spustí uprednostňovanú aplikáciu typu
                                      TYP s voliteľnými PARAMETRAMI, kde
                                      TYP je jedna z hodnôt uvedených nižšie.   --name=identifier Názov premennej alebo makra jazyka C
 --output=filename Zapísať vygenerovaný csource do súboru
   --extern          Generuje statické symboly
   --strip-comments  Odstráni komentáre zo súborov XML
   --strip-content   Odstráni obsah uzlov zo súborov XML
   --working-directory ADRESÁR       Predvolený pracovný adresár pre aplikácie
                                      pri použití prepínača --launch.   -?, --help                          Zobrazí tohto pomocníka a ukončí sa   -V, --version                       Zobrazí informácie o verzii a ukončí sa   -V, --version     Zobrazí informáciu o verzii a ukončí sa
   -h, --help        Zobrazí tohto pomocníka a ukončí sa
   WebBrowser       - Uprednostňovaný internetový prehliadač.
  MailReader       - Uprednostňovaný e-mailový klient.
  FileManager       - Uprednosťňovaný správca súborov.
  TerminalEmulator - Uprednostňovaný emulátor terminálu. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Všetky práva vyhradené.

Napísal Benedikt Meurer <benny@xfce.org>.

Zostavené s Gtk+-%d.%d.%d, spustené s Gtk+-%d.%d.%d.

Prosíme, chyby hláste na <%s>.
 %s NEPOSKYTUJE ŽIADNU ZÁRUKU,
Kópie programu %s môžete šíriť pod podmienkami licencie
the GNU Lesser General Public License, ktorej znenie nájdete
priložené v zdrojovom balíčku aplikácie %s.

 Ikony akcií Aktívny Farba okraja aktívnych položiek Farba výplne aktívnych položiek Index aktívnych položiek Farba textu aktívnych položiek Všetky súbory Všetky ikony Animácie Tlačidlo pre výber aplikácie Ikony aplikácií Balsa Blokové zariadenie Brave Prehliadať systém súborov Prehliadať systém súborov pre výber vlastného príkazu. Prehliadať internet _Komentár: _Vytvoriť Správca súborov Caja Znakové zariadenie Vyberte si uprednostňovanú aplikáciu Vyberte si vlastného správcu súborov Vyberte si vlastný e-mailový klient Vyberte si vlastný emulátor terminálu Vyberte si vlastný internetový prehliadač Zvoľte názov súboru Chromium Claws Mail Vymazať pole pre vyhľadávanie Šírka stĺpca Rozostup stĺpcov _Príkaz: Copyright (c) %s
        os-cillation e.K. Všetky práva vyhradené.

Napísal Benedikt Meurer <benny@xfce.org>.

 Vytvoriť adresár Vytvoriť spúšťač Vytvoriť spúšťač <b>%s</b> Vytvoriť odkaz Vytvoriť nový súbor plochy v danom priečinku Farba okrajov položiek, nad ktorými sa nachádza kurzor Farba výplne položiek, nad ktorými sa nachádza kurzor Farba textu položiek, nad ktorými sa nachádza kurzor Upravenie panela nástrojov... Debian Sensible Browser Debian X Terminal Emulator Ikony zariadení Dillo Položku pridajte na panel nástrojov pretiahnutím z tabuľky nástrojov. Opačným postupom ju odoberte. Upraviť adresár Upraviť spúšťač Upraviť odkaz Emblémy Emotikony Povoliť hľadanie Encompass Enlightened Terminal Emulator Epiphany internetový prehliadač Evolution Spustiteľné súbory FIFO Nepodarilo sa vytvoriť "%s". Nepodarilo sa spustiť predvolený správca súborov Nepodarilo sa spustiť predvolený e-mailový klient Nepodarilo sa spustiť predvolený emulátor terminálu Nepodarilo sa spustiť predvolený internetový prehliadač Nepodarilo sa spustiť uprednostňovanú aplikáciu pre kategóriu "%s". Nepodarilo sa načítať obsah z "%s": %s Nepodarilo sa načítať obrázok "%s": Neznámy dôvod, pravdepodobne je súbor s obrázkom poškodený. Nepodarilo sa otvoriť "%s". Nepodarilo sa otvoriť %s pre zápis Nepodarilo sa otvoriť URI "%s". Nepodarilo sa otvoriť displej Nepodarilo sa otvoriť súbor "%s": %s Nepodarilo sa spracovať súbor "%s": %s Nepodarilo sa prečítať súbor "%s": %s Nepodarilo sa uložiť "%s". Nepodarilo sa nastaviť predvolený správca súborov Nepodarilo sa nastaviť predvolený e-mailový klient Nepodarilo sa nastaviť predvolený emulátor terminálu Nepodarilo sa nastaviť predvolený internetový prehliadač Súbor "%s" nemá žiadny kľúč typu Správca súborov Ikony typov súborov Umiestnenie súboru nie je platným súborom alebo adresárom Priečinok Nasledovať stav Ikona typu GIcon GNOME Terminál Galeon internetový prehliadač Geary Google Chrome Homogénne Ako sú text a ikony každej položky navzájom relatívne umiestnené Icecat Icedove Iceweasel Ikona Model ikonového pohľadu Model ikonového pohľadu Stĺpec ikony Ak nezadáte možnosť prepínača --launch, exo-open otvorí všetky špecifikované
URL adresy aplikáciou zvolenou podľa tvaru adresy URL. Ak však zadáte možnosť --launch,
budete môcť vybrať, ktorú aplikáciu chcete spustiť a zadať dodatočné parametre
pre aplikácie (napr. pre TerminalEmulator môžete zadať príkaz, ktorý sa vykoná v
novom okne terminálu). Súbory obrázkov Neplatný typ pomocníka "%s" Jumanji KMail Konqueror internetový prehliadač Spustiť predvoleného pomocníka typu TYP s voliteľným PARAMETRom, kde TYP môže nadobúdať jednu z nasledujúcich hodnôt. Spúšťanie súborov plochy nie je podporované ak je %s skompilovaný bez funkcií GIO-Unix. Režim rozloženia Links textový prehliadač Ikony umiestnení Lynx textový prehliadač E-mailový klient Okraj Označiť stĺpec Ikony ponuky Midori Model stĺpca pre vyhľadávanie pri hľadaní v položke Modelový stĺpec použitý na získanie absolútnej cesty k súboru vykresleného obrázka Stĺpec modelu používaný pre získanie pixbufu ikony z Stĺpec modelu používaný pre získanie textu z Stĺpec modelu používaný pre získanie textu, ak sa používajú značky Pango Model pre ikonový pohľad Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nebola vybraná žiadna aplikácia Nebol zadaný žiadny príkaz Nie je vybraný žiadny súbor Nebol zadaný žiadny súbor alebo priečinok Žiaden pomocník pre "%s" Žiadna ikona Počet stĺpcov Počet stĺpcov pre zobrazenie Otvoriť dialógové okno
Uprednostňované aplikácie Opera Možnosti: Orientácia PCMan správca súborov Skripty jazyka Perl Stĺpec pixbuf Vyberte si prosím svoj uprednostňovaný
správca súborov a kliknite na tlačidlo OK. Vyberte si prosím svoj uprednostňovaný
e-mailový klient a kliknite na tlačidlo OK. Vyberte si prosím svoj uprednostňovaný
emulátor terminálu a kliknite na tlačidlo OK. Vyberte si prosím svoj uprednostňovaný
internetový prehliadač a kliknite na tlačidlo OK. Prosíme, hláste chyby na <%s>.
 Uprednostňované aplikácie Uprednostňované aplikácie (internetový prehliadač, e-mailový klient a emulátor terminálu) Prednastavená adresa URL pri vytváraní odkazu Prednastavený príkaz pri vytváraní spúšťača Prednastavený komentár pri vytváraní súboru plochy Prednastavená ikona pri vytváraní súboru plochy Prednastavený názov pri vytváraní súboru plochy Stlačte ľavé tlačidlo myši pre zmenu vybranej aplikácie. Náhľad Zobrazí informácie o verzii a ukončí sa Skripty jazyka Python QTerminal Dotázať sa predvoleného pomocníka typu TYP, kde TYP môže nadobúdať jednu z nasledujúcich hodnôt. QupZilla ROX-Filer RXVT Unicode Prečítať poštu Vykresľovať odlišne podľa stavu výberu. Preusporiadateľné Reštartovať príkaz Správca súborov Rodent Výška riadka Rozostup riadkov Skripty jazyka Ruby Spustiť v _termináli SOKET ID Sakura Stĺpec hľadania Vybrať _ikonu z: Vyberte pracovný adresár Vyberte si aplikáciu Vyberte ikonu Vyberte aplikáciu Vyberte predvolenú aplikáciu pre rôzne služby Zvoľte túto voľbu pre povolenie oznámenia pri spustení, ak je príkaz spustený zo správcu súborov alebo z hlavnej ponuky. Nie každá aplikácia podporuje oznámenie pri štarte. Zvoľte túto možnosť pre spustenie príkazu v okne terminálu. Režim výberu Oddeľovač Príkaz na znovu spustenia sedenia Soket správcu nastavenia Shellové skripty Jednoduché kliknutie Vypršanie limitu pre jednoduché kliknutie Soket Priestor vložený na okraje ikonového zobrazenia Priestor medzi bunkami položiek Priestor, ktorý sa vkladá medzi mriežku stĺpcov Priestor, ktorý sa vkladá medzi mriežku riadkov Rozostupy Spresnite aplikáciu, ktorú chcete použiť
ako predvoleného správcu súborov v Xfce: Spresnite aplikáciu, ktorú chcete použiť
ako predvolený e-mailový klient v Xfce: Spresnite aplikáciu, ktorú chcete použiť
ako predvolený emulátor terminálu v Xfce: Spresnite aplikáciu, ktorú chcete použiť
ako predvolený internetový prehliadač v Xfce: Ikony stavov Základné ikony Surf Sylpheed TYP [PARAMETER] Emulátor terminálu Terminator Textový stĺpec Text pri _dôležitých ikonách Text pri _všetkých ikonách Ikona GIcon pre vykresľovanie Priestor medzi dvoma po sebe nasledujúcimi stĺpcami Priestor medzi dvoma po sebe nasledujúcimi riadkami Doba, po ktorej pod kurzorom myši v režime jednoduchého kliknutia sa označí sama Súbor "%s" neobsahuje žiadne dáta Pre príkazy --launch a --query sú podporované tieto TYPy:

WebBrowser - Preferovaný internetový prehliadač.
MailReader - Preferovaná čítačka e-mailov.
FileManager - Preferovaný správca súborov.
TerminalEmulator - Preferovaný emulátor terminálu. Pri použití prepínača --launch sú podporované nasledujúce typy: Ikona určená pre vykresľovanie Režim rozloženia Model pre ikonový pohľad Orientácia panela ikon. Režim výberu Veľkosť vykreslenej ikony v pixloch. Šírka použitá pre každú položku Thunar Štýl panela _nástrojov Informácie o použití získate príkazom '%s --help'. Typ vytváraného súboru pracovnej plochy (aplikácia alebo odkaz) Nemožno nájsť schému URI "%s". Nezaradené ikony Nepodporovaný typ súboru plochy "%s" Použitie: %s [možnosti] [súbor]
 Použitie: exo-open [AdresyURL...] Použiť oznámenie pri _spustení Použiť vlastnú aplikáciu, ktorá nie je uvedená v zozname vyššie. Použiť príkazový riadok Pohľad umožní užívateľovi interaktívne hľadať v stĺpcoch Pohľad je preusporiadaný Vimprobable2 W3M Text Browser Internetový prehliadač Či majú potomkovia rovnakú veľkosť Či môžu byť zobrazené položky aktivované jednoduchým kliknutím Šírka pre každú položku Skupina okien Vedúca skupina okien Pracovný a_dresár: X Terminál Terminál Xfce Správca súborov Xfe [SÚBOR|PRIEČINOK] _Pridať nový panel nástrojov _Zrušiť _Zatvoriť Predvolená _pracovná plocha _Pomocník _Ikona Iba _ikony _Internet _Názov: _OK _Otvoriť _Ostatné... _Odobrať panel nástrojov _Uložiť Náj_sť ikonu: Iba _text _Adresa URL: _Nástroje aterm qtFM veľkosť 