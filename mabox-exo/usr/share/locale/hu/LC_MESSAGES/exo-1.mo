��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  N  1?  7   �@  1   �@  7   �@  9   "A  �   \A  ,   MB  O   zB  :   �B  B   C  L   HC  �   �C  I   ;D  R   �D  A   �D  8   E  �   SE  �   4F  �    G     �G     �G     �G     H     0H     CH     ]H     jH     vH     �H     �H     �H     �H     �H     �H  ?   �H     "I     2I     @I     NI     `I  (   sI      �I     �I  &   �I  "   J     %J     ;J     DJ     TJ  
   kJ     vJ  	   �J  m   �J     �J     K  $   3K     XK  8   rK  %   �K  )   �K  $   �K  "    L  '   CL     kL     �L     �L  )   �L  z   �L     @M     YM     tM  	   �M     �M     �M  	   �M     �M     �M  	    N     
N     "N  #   'N  ;   KN  8   �N  A   �N  =   O  Q   @O  6   �O  e   �O  &   /P     VP  *   vP  $   �P  /   �P  8   �P  -   /Q     ]Q  :   |Q  7   �Q  @   �Q  <   0R  0   mR     �R     �R  0   �R     �R     �R     	S     S     S     5S     ;S     IS  ?   RS     �S     �S  	   �S     �S     �S     �S  
   �S  {  �S     XU  +   dU     �U     �U     �U  ~   �U  m   6V     �V     �V     �V     �V  	   �V     W     W     W     'W  :   .W  d   iW  K   �W  9   X  b   TX     �X     �X     �X     �X     Y     Y     Y     %Y     .Y     AY     `Y     vY     �Y  2   �Y  
   �Y     �Y  "   �Y  >   Z     YZ     nZ  	   ~Z     �Z     �Z     �Z     �Z  \   �Z  Y   2[  b   �[  ^   �[  0   N\     \  N   �\  4   �\  9   "]  >   \]  7   �]  7   �]  9   ^  
   E^  ,   P^     }^  	   �^  n   �^     _  	   _     _     ,_  6   =_     t_     �_     �_     �_     �_     �_     �_     �_     `     	`     `     4`     Q`     m`     ~`  G   �`  �   �`  O   �a     �a     b  #   b     7b     Tb     ib  "   ~b     �b  *   �b  $   �b  &   �b  "   !c     Dc  W   Mc  T   �c  \   �c  Y   Wd     �d     �d     �d     �d     �d     �d  
   e     e     e     9e     Qe  1   ke  ,   �e  k   �e  *   6f  ,  af  ;   �g     �g     �g     �g     h  V   &h  T   }h  r   �h  c   Ei     �i  ,   �i  (   �i     j     j  4   1j  C   fj  0   �j     �j  -   �j  $   "k      Gk  $   hk  ?   �k     �k  9   �k     l     6l     Cl     ]l  (   ll  7   �l     �l     �l     �l     m      m     ,m     ;m     Lm     Zm     xm  
   �m     �m     �m     �m     �m  	   �m     �m     �m     �m  
   �m     �m     n     n     (n     6n     <n     Mn     Sn     Xn                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-28 16:20+0000
Last-Translator: Gábor P.
Language-Team: Hungarian (http://www.transifex.com/xfce/exo/language/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [kapcsolók] --build-list [[név, fájl]…]
        exo-open --launch TÍPUS [PARAMÉTEREK…]   --build-list      (név, fájl) párok feldolgozása
   --extern          Külső szimbólumok előállítása
   --launch TÍPUS [PARAMÉTEREK…]       Az adott TÍPUSÚ alapértelmezett
                                      alkalmazás indítása a PARAMÉTEREKKEL,
                                      ahol a TÍPUS a következő értékek egyike.   --name=azonosító C makró/változónév
 --output=fájlnév Előállított csource forrás kiírása a megadott fájlba
   --static          Statikus szimbólumok előállítása
   --strip-comments  Megjegyzések eltávolítása XML fájlokból
   --strip-content   Csomópont tartalmának eltávolítása XML fájlokból
   --working-directory KÖNYVTÁR      Alkalmazások alapértelmezett munkakönyvtára
                                    a --launch kapcsolóval való indításkor.   -?, --help                          Ezen súgó kiírása és kilépés   -V, --version                       Verzióinformációk kiírása és kilépés   -V, --version     Verzióinformációk kiírása és kilépés
   -h, --help        Ezen súgó kiírása és kilépés
   WebBrowser       - Az alapértelmezett webböngésző.
  MailReader       - Az alapértelmezett levelező.
  FileManager      - Az alapértelmezett fájlkezelő.
  TerminalEmulator - Az alapértelmezett terminálemulátor. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Minden jog fenntartva.

Írta: Benedikt Meurer <benny@xfce.org>.

Készült a Gtk+-%d.%d.%d használatával, fut: Gtk+-%d.%d.%d.

A hibákat ezen a címen jelentse: <%s>.
 A(z) %s programra NINCS SEMMILYEN GARANCIA.
A(z) %s másolatai továbbterjeszthetők a GNU Lesser General Public License
 feltételei szerint, amely megtalálható a(z) %s forráscsomagban.

 Műveletikonok Aktív Aktív elem szegélyszíne Aktív elem kitöltési színe Aktív elem indexe Aktív elem szövegszíne Minden fájl Minden ikon Animációk Alkalmazásválasztó gomb Alkalmazásikonok Balsa Blokkeszköz Brave Fájlrendszer tallózása A fájlrendszer tallózása egyéni parancs kiválasztásához. Webböngészés _Megjegyzés: _Létrehozás Caja fájlkezelő Karakteres eszköz Válasszon alapértelmezett alkalmazást Válasszon egyéni fájlkezelőt Válasszon egyéni levelezőt Válasszon egyéni terminálemulátort Válasszon egyéni webböngészőt Válasszon fájlnevet Chromium Claws levelező Keresőmező törlése Oszlopköz Oszloptávolság _Parancs: Copyright (c) %s
        os-cillation e.K. Minden jog fenntartva.

Írta: Benedikt Meurer <benny@xfce.org>.

 Könyvtár létrehozása Indítóikon létrehozása <b>%s</b> indítóikon létrehozása Hivatkozás létrehozása Új asztali fájl létrehozása a megadott könyvtárban Kurzornál lévő elem szegélyszíne Kurzornál lévő elem kitöltési színe Kurzornál lévő elem szövegszíne Eszköztár személyre szabása… Debian környezetérzékeny böngésző Debian X terminálemulátor Eszközikonok Dillo _Ne jelenítse meg újra ezt az üzenetet Húzzon egy elemet a fenti eszköztárakra a felvételéhez, az eszköztárakról az elemtáblába az eltávolításához. Könyvtár szerkesztése Indítóikon szerkesztése Hivatkozás szerkesztése Matricák Hangulatjelek Keresés engedélyezése Encompass Enlightened terminálemulátor Epiphany webböngésző Evolution Végrehajtható fájlok FIFO „%s” létrehozása meghiúsult. Az alapértelmezett fájlkezelő végrehajtása meghiúsult Az alapértelmezett levelező végrehajtása meghiúsult Az alapértelmezett terminálemulátor végrehajtása meghiúsult Az alapértelmezett webböngésző végrehajtása meghiúsult A kategória („%s”) alapértelmezett alkalmazásának indítása meghiúsult. A(z) „%s” tartalmának betöltése meghiúsult: %s A(z) „%s” kép betöltése meghiúsult: az ok ismeretlen, valószínűleg megsérült a képfájl A(z) „%s” megnyitása meghiúsult. A %s nem nyitható meg írásra Az URI („%s”) megnyitása meghiúsult. A képernyő megnyitása meghiúsult A(z) „%s” fájl megnyitása meghiúsult: %s A(z) „%s” tartalmának feldolgozása meghiúsult: %s A(z) „%s” fájl olvasása meghiúsult: %s „%s” mentése meghiúsult. Az alapértelmezett fájlkezelő beállítása meghiúsult Az alapértelmezett levelező beállítása meghiúsult Az alapértelmezett terminálemulátor beállítása meghiúsult Az alapértelmezett webböngésző beállítása meghiúsult A(z) „%s” fájl nem tartalmaz típus kulcsot Fájlkezelő Fájltípus-ikonok A fájlhely nem szabályos fájl vagy könyvtár Mappa Állapot követése GIcon GNOME terminál Galeon webböngésző Geary Google Chrome Homogén A szöveg és az ikon hogyan helyezkednek el egymáshoz képest Icecat Icedove Iceweasel Ikon Ikonsávmodell Ikonnézet-modell Ikonoszlop Ha nem adja meg a --launch kapcsolót, az exo-open az összes megadott URL címet
megnyitja az alapértelmezett URL kezelővel. Egyébként a --launch kapcsoló
megadásakor kiválaszthatja a futtatandó alapértelmezett alkalmazást és további
paramétereket adhat meg az alkalmazásnak (például a TerminalEmulator
esetén átadhatja a terminálban futtatandó parancssort). Képfájlok Érvénytelen segédprogramtípus: „%s” Jumanji KMail Konqueror webböngésző Az alapértelmezett segédprogram elindítása a TÍPUSHOZ, az elhagyható PARAMÉTERREL, ahol a TÍPUS a következők egyike. Az asztali fájlok indítása nem támogatott, ha a(z) %s a GIO-Unix szolgáltatások nélkül van fordítva. Elrendezésmód Links szöveges böngésző Hely ikonok Lynx szöveges böngésző Levelező Margó Jelölőkódoszlop Menüikonok Midori Elemek keresésekor a modell ezen oszlopában kell keresni A modellnek a megjelenítendő képfájl abszolút útvonalának lekérdezéséhez használt oszlopa A modellnek az ikon pixbuf értékének lekérdezéséhez használt oszlopa A modellnek a szöveg lekérdezéséhez használt oszlopa A modellnek a szöveg lekérdezéséhez használt oszlopa Pango jelölőkódok használata esetén Az ikonsáv modellje Mozilla webböngésző Mozilla Firefox Mozilla levelező Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nincs kiválasztva alkalmazás Nincs megadva parancs Nincs fájl kiválasztva Nincs megadva fájl/mappa Nincs segédprogram meghatározva ehhez: „%s”. Nincs ikon Oszlopok száma A megjelenítendő oszlopok száma Az Alapértelmezett alkalmazások
beállítóablak megnyitása Opera webböngésző Beállítások: Tájolás PCMan fájlkezelő PCManFM-Qt fájlkezelő Perl-parancsfájlok Pixbuf oszlop Válassza ki az alapértelmezett fájlkezelőt,
és nyomja meg az OK gombot a folytatáshoz. Válassza ki az alapértelmezett levelezőt,
és nyomja meg az OK gombot a folytatáshoz. Válassza ki az alapértelmezett terminálemulátort,
és nyomja meg az OK gombot a folytatáshoz. Válassza ki az alapértelmezett webböngészőt,
és nyomja meg az OK gombot a folytatáshoz. A hibákat a következő címen jelentse: <%s>.
 Alapértelmezett alkalmazások Alapértelmezett alkalmazások (webböngésző, levelező, terminálemulátor) Előre beállított URL hivatkozás létrehozásakor Előre beállított parancs indítóikon létrehozásakor Előre beállított megjegyzés asztali fájl létrehozásakor Előre beállított ikon asztali fájl létrehozásakor Előre beállított név asztali fájl létrehozásakor Kattintson a kiválasztott alkalmazás módosításához. Előnézet Verzióinformációk kiírása és kilépés Python-parancsfájlok QTerminal Az adott TÍPUSÚ alapértelmezett segédprogram lekérdezése, ahol a TÍPUS a következők értékek egyike. QupZilla ROX-Filer RXVT Unicode E-mail olvasása Megjelenítés a kijelölés állapotától függően. Átrendezhető Újraindítási parancs Rodent fájlkezelő Sorköz Sortávolság Ruby-parancsfájlok _Terminálban fusson FOGLALATAZONOSÍTÓ Sakura Keresési oszlop _Ikon választása innen: Válasszon munkakönyvtárat Válasszon egy alkalmazást Válasszon ikont Válasszon alkalmazást Alapértelmezett alkalmazások kiválasztása számos szolgáltatáshoz Válassza ezt a lehetőséget indítási értesítés használatához a parancs fájlkezelőből vagy a menüből való futtatásakor. Nem minden alkalmazás támogatja az indítási értesítést. Válassza ezt a lehetőséget a parancs terminálablakban való futtatásához. Kijelölési mód Elválasztó Munkamenet újraindítási parancsa Beállításkezelő foglalat Shell-parancsfájlok Egyszeres kattintás Egyszeres kattintás időkorlátja Foglalat Az ikonnézet szélei közötti távolság Az elem cellái közötti távolság Rácsoszlopok közé szúrt távolság A rács sorai közötti távolság Térköz Adja meg az Xfce alapértelmezett fájlkezelőjeként
használni kívánt alkalmazást: Adja meg az Xfce alapértelmezett levelezőjeként
használni kívánt alkalmazást: Adja meg az Xfce alapértelmezett terminálemulátoraként
használni kívánt alkalmazást: Adja meg az Xfce alapértelmezett webböngészőjeként
használni kívánt alkalmazást: Állapotikonok Gyári ikonok Surf Sylpheed TÍPUS [PARAMÉTER] Terminál emulátor Terminator Szövegoszlop Szöveg a _fontos ikonokhoz Szöveg min_den ikonhoz A megjelenítendő GIcon. Két egymás melletti oszlop közötti távolság Két egymás utáni sor közötti távolság Az egérkurzor alatti elem ennyi idő után automatikusan kijelölésre kerül egyszeres kattintás módban A(z) „%s” fájl nem tartalmaz adatokat A következő TÍPUSOK támogatottak a --launch és --query parancsoknál:

  WebBrowser       - Az alapértelmezett webböngésző.
  MailReader       - Az alapértelmezett levelező.
  FileManager      - Az alapértelmezett fájlkezelő.
  TerminalEmulator - Az alapértelmezett terminálemulátor. A következő TÍPUSOK támogatottak a --launch parancshoz: A megjelenítendő ikon. Az elrendezési mód Az ikonnézet modellje Az ikonsáv tájolása A mappák tartalmának tallózása az alapértelmezett fájlkezelővel fog történni. E-mail címekre kattintva az alapértelmezett levelező nyílik meg levélíráshoz. A parancssori környezetet igénylő parancsok futtatásához az alapértelmezett terminál emulátor nyílik meg. A hiperhivatkozások és a súgótartalmak az alapértelmezett webböngészővel fognak megjelenni. A kijelölés módja A megjelenítendő ikon mérete képpontban. Az egyes elemekhez használt szélesség Thunar Eszköztár _stílusa Segítségért adja ki az „%s --help” parancsot. Létrehozandó asztali fájl típusa (alkalmazás vagy hivatkozás) A(z) („%s”) URI-sémája nem ismerhető fel. Kategorizálatlan ikonok Nem támogatott asztalifájl-típus: „%s” Használat: %s [kapcsolók] [fájl]
 Használat: exo-open [URL-ek…] In_dítási értesítés használata Egyéni alkalmazás használata, amely nincs a fenti listában. Parancssor használata A felhasználók interaktívan kereshetnek az oszlopokban A nézet átrendezhető Vimprobable2 W3M szöveges böngésző Webböngésző A gyermekek azonos méretűek legyenek-e A nézet elemei aktiválhatók-e egyetlen kattintással Az egyes elemek szélessége Ablakcsoport Ablakcsoport vezetője M_unkakönyvtár: X terminál Xfce terminál Xfe fájlkezelő [FÁJL|MAPPA] Új _eszköztár hozzáadása _Mégse _Bezárás _Környezet alapértelmezése _Súgó _Ikon: _Csak ikonok _Internet _Név: _OK M_egnyitás _Egyéb… _Eszköztár eltávolítása _Mentés Ikon _keresése: Csak _szöveg _URL: Segé_dprogramok aterm qtFM méret 