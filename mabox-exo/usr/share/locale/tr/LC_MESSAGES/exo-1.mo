��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  e  1?  7   �@  /   �@  :   �@  ,   :A  �   gA  -    B  G   .B  /   vB  :   �B  G   �B  �   )C  A   �C  H   D  7   ZD  ;   �D  �   �D    �E    �F     �G     �G     �G     �G     H     %H     <H     JH     XH     hH     �H     �H     �H     �H     �H  4   �H     �H     I  	   I     I     1I     CI  "   bI  !   �I  )   �I  (   �I     �I     
J  
   J     J     6J     QJ     cJ  �   kJ     �J     K     K     7K  7   KK     �K     �K     �K      �K     �K  !   L     2L     FL     LL     iL     �L     �L     M  	   )M     3M     CM  	   YM  $   cM     �M  	   �M     �M     �M     �M  "   �M  ,   N  6   5N  4   lN  ?   �N  1   �N  N   O     bO  $   uO     �O     �O     �O  0   �O     P     ;P  ,   OP  +   |P  3   �P  2   �P  '   Q     7Q     IQ  +   ^Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q  
   �Q  ]   �Q     GR     NR  	   VR     `R     fR     |R     �R  �  �R     ET     WT     tT     |T     �T  b   �T  ]   �T     \U     kU     ~U     �U     �U     �U     �U     �U     �U  ?   �U  Y    V  G   zV  D   �V  _   W     gW     �W     �W     �W     �W     �W     �W     �W     �W     �W     X     X     ,X  %   KX  	   qX     {X     �X  8   �X     �X     �X     Y     Y     ,Y     IY     XY  c   fY  e   �Y  n   0Z  m   �Z  "   [     0[  a   J[  G   �[  =   �[  I   2\  D   |\  C   �\  @   ]  	   F]  "   P]     s]  
   �]  _   �]     �]  	   �]     ^     ^  ,   $^     Q^     i^     �^     �^     �^     �^     �^  
   �^     �^     �^     _     _     :_     N_     __  ;   o_  �   �_  H   q`     �`  
   �`      �`     �`     a     a     +a     Ga  6   La  /   �a     �a     �a     �a  L   �a  X   Fb  `   �b  _    c     `c     pc     c     �c     �c     �c  
   �c     �c     �c     �c     d  :   !d  :   \d  u   �d     e  *  +e  1   Vf     �f     �f     �f  &   �f  [   �f  q   Og  R   �g  o   h     �h  9   �h  $   �h     �h     �h  +   i  H   :i  0   �i  !   �i  *   �i  $   j     &j  %   Ej  2   kj     �j  M   �j  "   k     (k     5k     Fk  -   ^k  7   �k     �k     �k     �k     �k     l     l     (l     >l     Ll     jl     rl     yl     �l     �l     �l  
   �l     �l     �l     �l  
   �l     �l     �l      m     m     m     +m  	   @m     Jm     Om                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-29 12:42+0000
Last-Translator: Demiray Muhterem <mdemiray@msn.com>
Language-Team: Turkish (http://www.transifex.com/xfce/exo/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
        %s [seçenekler] --build-list [[dosya adı]...]
        exo-open --launch TÜR [PARAMETREler...]   --build-list      Ayrıştırma (isim, dosya) çiftleri
   --extern          Dış simgeler oluştur
   --launch TÜR [PARAMETREler...]      TÜR'e karşılık gelen uygulamayı,
                                      seçilen parametrelere göre başlat.   --name=tanıtıcı C makro/değişken ismi
   --output=dosyadı Oluşturulan csource belirtilen dosyaya yazılsın
   --static          Durağan simgeler oluştur
   --strip-comments  Yorumları XML dosyalarından kaldır
   --strip-content   Düğüm bileşenlerini XML dosyalarından kaldır
   --working-directory DIRECTORY       --launch seçeneğinin kullanıldığı, uygulamaların
                                        öntanımlı çalışma dizini.   -?, --help                          Bu mesajı yazdır ve çık   -V, --version                       Sürüm bilgisini yazdır ve çık   -V, --version     Sürüm bilgisini yazdır ve çık
   -h, --help        Bu yardım mesajını yazdır ve çık
   WebBrowser       - Tercih edilen İnternet Tarayıcısı.
  MailReader       - Tercih edilen Posta Okuyucusu.
  FileManager      -Tercih edilen Dosya Yöneticisi.
  TerminalEmulator - Tercih edilen Uçbirim Öykünücüsü. %s (Xfce %s)

Telif hakkı (c) 2003-2006
        os-cillation e.K. Tüm hakları saklıdır.

Benedikt Meurer tarafından yazılmıştır, <benny@xfce.org>.

Gtk+-%d.%d.%d ile inşa edilmiştir, Gtk+-%d.%d.%d ile çalıştırılıyor.

Hataları lütfen <%s> adresine bildirin.
 %s HİÇBİR GARANTİ İLE gelmez,
"GNU Lesser General Public License" isimli lisansın %s altındaki
sözleşme şartlarına uygun bir biçimde, kopyalarını dağıtabilirsiniz.
Bu %s kaynak paketi, GNU Lesser General Public License ile
lisanslanmıştır.

 Eylem Simgeleri Etkin Etkin öge kenar rengi Etkin öge dolgu rengi Etkin öge içeriği Etkin öge metin rengi Tüm Dosyalar Tüm Simgeler Canlandırmalar Uygulama Seçici Düğmesi Uygulama Simgeleri Balsa Blok Aygıtı Brave Dosya sistemini tara. Özel bir komut seçmek için, dosya sistemini tara. Web'de gezin Y_orum: Oluştu_r Caja Dosya Yöneticisi Karakter Aygıtı Tercih Edilen Uygulamayı Seç Özel bir Dosya Yöneticisi seçin Özel bir Eposta Okuyucusu seçin Özel bir Uçbirim Öykünücüsü seçin Özel bir İnternet Tarayıcısı seçin Dosya ismi seç Chromium Claws Mail Arama alanını temizle Dikeç Aralıklandırması Dikeç aralığı Kom_ut: Copyright (c) %s
        os-cillation e.K. Tüm hakları saklıdır.

Benedikt Meurer tarafından yazılmıştır, <benny@xfce.org>.

 Dizin Oluştur Başlatıcı Oluştur Başlatıcı Oluştur <b>%s</b> Bağlantı Oluştur Verilen dizinde yeni bir masaüstü dosyası oluşturun İmleç ögesi kenar rengi İmleç ögesi dolgu rengi İmleç ögesi metin rengi Araç Çubuğunu Özelleştir... Debian Duyarlı Tarayıcı Debian X Uçbirim Öykünücüsü Sürücü Simgeleri Dillo Bu mesajı tekrar _gösterme Bir ögeyi eklemek için onu araç çubuklarının üzerine; kaldırmak içinse araç çubuklarından dışarıya sürükleyin. Dizini Düzenle Başlatıcıyı Düzenle Bağlantıyı Düzenle Semboller Duygu Simgeleri Aramayı Etkinleştir Encompass Enlightened Uçbirim Öykünücüsü Epiphany Web Tarayıcı Evolution Çalıştırılabilir Dosyalar FIFO "%s" oluşturulamadı. Dosya Yöneticisi başlatılamadı Öntanımlı eposta okuyucu başlatılamadı Öntanımlı Uçbirim Öykünücüsü başlatılamadı Öntanımlı internet tarayıcısı başlatılamadı Tercih edilen uygulama, "%s" sıralamasına göre açılamadı. "%s" ögesinden içerik yükleme başarısız: %s "%s" kalıbı yüklenemedi: Bilinmeyen neden, büyük olasılıkla bozuk dosya "%s" açılamadı. %s, yazılacak şekilde açılamadı URI"%s" açma başarısız. Görüntü açılamadı "%s" dosyası açılamadı: %s "%s" içeriğinin imla denetimi başarısız: %s "%s" dosyası okunamadı: %s "%s" kaydedilemedi. Öntanımlı Dosya Yöneticisi ayarlanamadı Öntanımlı Eposta Okuyucusu ayarlanamadı Öntanımlı Uçbirim Öykünücüsü ayarlanamadı Öntanımlı İnternet Tarayıcısı ayarlanamadı "%s" dosyası tür anahtarı içermiyor Dosya Yöneticisi Dosya Tipi Simgeleri Dosya yeri düzenli dosya veya dizin değil Dizin Durumu izle GSimgesi GNOME Uçbirim Galeon Web Tarayıcı Geary Google Chrome Homojenlik Her bir ögenin metin ve simgesinin birbiriyle ilişkili olarak nasıl konumlandırılacağı Icecat Icedove Iceweasel Simge Simge çubuğu modeli Simge Görünüm Modeli Simge dikeci Eğer --launch seçeneğini belirlemezseniz, exo-open tüm belirtilmiş bağlantı
adreslerini, kendi tercih ettiği bağlantı işleyicileri ile açacak. Ama bu
seçeneği belirtirseniz, hangi uygulamayla beraber açmak istediğinizi ve
istediğiniz uygulamayla ilgili parametreleri de seçebilirsiniz (örneğin Uçbirim
Öykünücüsü için, uçbirimde çalıştırılmış biçimde komut satırını geçmek gibi). Kalıp Dosyaları Geçersiz yardım tipi, "%s" Jumanji KMail Konqueror Web Tarayıcı Aşağıdaki TİPlerden birinin öntanımlı yardımcısını, seçenekli PARAMETRE'lerle başlat. %s GIO-Unix özellikleri olmadan derlendiğinde Masaüstü dosyası başlatma desteklenmiyor. Yerleşim kipi Links Text Browser Konum Simgeleri Lynx Text Browser Eposta Okuyucusu Kenar Biçimleme dikeci Menü Simgeleri Midori Ögeler arasında arama yapmak için kullanılacak model dikeci Gerçeklenecek bir resim dosyasının tam yolunu getirmek için kullanılan model sütunu Pixbuf simgesini yeniden düzenlemek amacıyla kullanılan model dikeci Metin formunu yeniden düzenlemek amacıyla kullanılan model dikeci Pango biçimlemesi kullanılırken metni yeniden düzenlemek amacıyla kullanılan model dikeci Simge çubuğu için model Mozilla Tarayıcı Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Seçili uygulama yok Belirlenmiş komut yok Seçili dosya yok Belirlenmiş dosya / dizin yok "%s" için yardımcı tanımlanmadı. Simge yok Dikeçlerin sayısı Gösterilecek dikeç sayısı Tercih Edilen Uygulamaları aç
Yapılandırma penceresi Opera Tarayıcı Seçenekler: Uyumlaştırma PCMan Dosya Yöneticisi PCManFM-Qt Dosya Yöneticisi Perl Betikleri Pixbuf dikeci Lütfen şimdi tercih ettiğiniz Dosya Yöneticisi seçin
ve devam etmek için TAMAM'ı tıklayın. Lütfen şimdi tercih ettiğiniz Eposta Okuyucusu'nu seçin
ve devam etmek için TAMAM'ı tıklayın. Lütfen şimdi tercih ettiğiniz Uçbirim Öykünücüsü'nü seçin
ve devam etmek için TAMAM'ı tıklayın. Lütfen şimdi tercih ettiğiniz İnternet Tarayıcısı'nı seçin
ve devam etmek için TAMAM'ı tıklayın. Lütfen hataları bildirin: <%s>.
 Tercih Edilen Uygulamalar Tercih Edilen Uygulamalar (İnternet Tarayıcısı, Eposta Okuyucusu ve Uçbirim Öykünücüsü) Bir bağlantı oluşturulurken, önceden belirlenmiş bağlantı adresi Bir başlatıcı oluşturulurken, önceden belirlenmiş komut Bir masaüstü dosyası oluşturulurken, önceden belirlenmiş açıklama Bir masaüstü dosyası oluşturulurken, önceden belirlenmiş simge Bir masaüstü dosyası oluşturulurken, önceden belirlenmiş isim Seçili uygulamayı değiştirmek için sol fare tuşuna basın. Önizleme Sürüm bilgisini yazdır ve çık Python Betikleri QTerminali TYPE varsayılan yardımcısı için sorgu, burada TYPE aşağıdaki değerlerden birini alır. QupZilla ROX-Filer RXVT Unicode E-postanızı okuyun Seçim durumuna göre farklılaştırma yap. Yeniden düzenlenebilir Yeniden başlatma komutu Rodent Dosya Yöneticisi Dizeç Aralığı Dizeç aralığı Ruby Betikleri _Uçbirimde çalıştır YUVA ID'si Sakura Arama Dikeci S_imgeyi şuradan seç : Çalışma dizinini seçin Bir Uygulama Seçin Bir simge seçin Uygulama seçin Çeşitli servisler için öntanımlı uygulamaları seçin Komut, dosya yöneticisi veya menüden çalışırken sistem açılış bildirimini etkinleştirmek için bu seçeneği seçin; fakat her uygulama, sistem açılış bildirimini desteklememektedir. Komutu uçbirim penceresinde çalıştırmak için bu seçeneği seçin. Seçim kipi Ayırıcı Oturumu yeniden başlatma komutu Ayarlar yöneticisi yuvası Kabuk Betikleri Tek Tıklama Tek Tıklama Zaman Aşımı Yuva Simge görüntüsünün kenarlarına eklenmiş boşluk Bir ögenin hücreleri arasına eklenen boşluk Izgaradaki dikeç aralıkları Izgaradaki dizeç aralıkları Aralık Xfce için Dosya Yöneticisi olarak
kullanmak üzere, bir uygulama belirtin: Xfce için öntanımlı Eposta Okuyucusu olarak
kullanmak üzere, bir uygulama belirtin: Xfce için öntanımlı Uçbirim Öykünücüsü olarak
kullanmak üzere, bir uygulama belirtin: Xfce için öntanımlı İnternet Tarayıcısı olarak
kullanmak üzere, bir uygulama belirtin: Durum Simgeleri Temel Simgeler Surf Sylpheed TİP [PARAMETRE] Uçbirim Öykünücüsü Terminator Metin dikeci Öne_mli Simgeler için Metin Tüm Simgeler için _Metin Oluşturulacak Gsimgesi. Birbirini izleyen iki dikeç arasındaki boşluğun boyutu Birbirini izleyen iki dizeç arasındaki boşluğun boyutu Tek tıklama kipinde olduğunda fare imlecinin altındaki ögenin otomatik olarak seçilmesinden sonraki geçen zaman "%s" dosyası veri içermiyor --launch ve --query komutları için aşağıdaki TÜRLER desteklenmektedir:

  WebBrowser       - Tercih edilen Web Tarayıcısı.
  MailReader       - Tercih edilen Posta Okuyucusu.
  FileManager      - Tercih edilen Dosya Yöneticisi.
  TerminalEmulator - Tercih edilen Uçbirim Öykünücüsü. --launch komutu tarafından desteklenen TÜR'ler: Oluşturulacak simge. Yerleşim kipi Simge görünümü için model Simge çubuğunun uyumlaştırılması Tercih edilen Dosya Yöneticisi, dizinlerin içeriğine göz atmak için kullanılacaktır. Tercih edilen posta okuyucu, e-posta adreslerini tıkladığınızda e-posta oluşturmak için kullanılacaktır. Tercih edilen Uçbirim aracı, komutları çalıştırmak için kullanılacaktır. Tercih edilen Web Tarayıcı, köprüler açmak ve yardım içeriğini görüntülemek için kullanılacaktır. Seçim kipi Farklılaştırılacak simgenin piksel cinsinden değeri. Her öge için kullanılan genişlik Thunar Araç Çubuğu _Düzeni Kullanım bilgisi için '%s --help' yazın. Oluşturulacak masaüstü dosyasının türü (Uygulama veya Bağlantı) "%s" ögesinin URI-planını taramak olanaksız. Sınıflandırılmamış Simgeler Desteklenmeyen masaüstü dosya tipi: "%s" Kullanım: %s [seçenekler] [dosya]
 Kullanım : exo-open [URLs...] _Sistem açılış bildirimini kullan Yukarıdaki listede yer almayan bir uygulama seç. Komut satırını kullan Kullanıcıların dikeçler arasından etkileşimli arama yapmasına izin ver Görüntü yeniden düzenlenebilir Vimprobable2 W3M Text Browser İnternet Tarayıcısı Tüm alt menü boyutlarının eşitlik durumu Görünen ögeler için etkinleştirilmiş tek tıklama Öge genişliği Pencere grubu Pencere grubu öncüsü Çalışma dizini: X Uçbirimi Xfce Uçbirim Xfe Dosya Yöneticisi [FILE|FOLDER] Yeni bir _araç çubuğu ekle _İptal K_apat Öntanımlı M_asaüstü _Yardım S_imge: Sadece S_imgeler _İnternet İs_im: _Tamam _Aç _Diğer... A_raç Çubuğunu Kaldır _Kaydet _Arama simgesi: Sadece Me_tin Bağl_antı: _Yardımcı Araçlar bir terim qtFM boyut 