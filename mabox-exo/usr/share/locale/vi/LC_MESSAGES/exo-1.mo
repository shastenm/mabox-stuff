��    �      �  �   <      �  -   �  F   '  H   n  �   �  �   ~  �   Q            	     	        )     D     V     \     s  	   �     �     �     �     �     �  !   �          :     J  
   S  	   ^  o   h     �     �                -     3  	   B     L  	   T     ^  	   l     v     �  	   �     �     �  &   �  %     +   (  %   T  %   z     �     �     �     �          $  "   9  !   \  '   ~  !   �     �     �     �     �     �               "  	   *     4     9     E     M     S     i     |     �     �     �  
   �     �     �     �     �     �                         +     C     T     m     u     �     �     �     �     �     �  F   �  E   .  K   t  E   �          #  G   :  ;   �     �  "   �     �  	   �                    /     C     P  	   a     k     r     �     �     �     �  0   �  �   �  ;   �     �  	   �     �            5   &     \     d     q     z     �     �     �     �     �               4     ;     W     k     �  A   �     �  8   �     /     @     L     `  
   m     x     �     �     �     �     �     �     �     �  	   �     �     �  	   �              
         *   
   0      ;      A   `  F   '   �!  4   �!  3   "  �   8"  �   +#  �   &$     %     4%     C%     S%     i%     �%     �%     �%     �%     �%     �%      �%     &  1   ,&  +   ^&  8   �&  '   �&     �&     '     
'     "'  �   *'     �'     �'  %   �'     (     +(     1(     B(     T(     j(     �(  	   �(  +   �(     �(  	   �(     �(     )  >   %)  5   d)  B   �)  1   �)  ,   *     <*     T*  +   t*  %   �*  '   �*     �*  >   +  5   F+  B   |+  1   �+     �+      ,  
   .,     9,     Q,     j,     �,     �,  	   �,     �,     �,     �,     �,     �,     �,     -     %-     D-  	   W-     a-     ~-     �-     �-     �-     �-     �-     �-     �-     �-  (   .  )   0.  -   Z.     �.  
   �.     �.     �.     �.     �.  !   �.     /  d   '/  [   �/  h   �/  W   Q0     �0     �0  `   �0  D   @1     �1  %   �1     �1  	   �1     �1     �1     �1  "   2     ;2     M2  	   g2     q2     x2     �2     �2     �2     �2  9   �2  �   !3  J   �3     :4     M4  )   `4     �4     �4  [   �4     5     5     45     =5     ]5  '   n5      �5  (   �5  $   �5     6  "   6     ;6  %   B6  +   h6  %   �6  *   �6  W   �6     =7  `   V7     �7     �7     �7     8     8     !8      98     Z8     h8     �8     �8     �8     �8     �8  	   �8     �8     �8  	   �8     �8     9     "9     /9     @9     M9     S9     n   
   F       �   �   C   I   �   �       A   c           @      �          �   	   �   "   6          �                         �   K   |       J   �   �   �   �   H   �   B       L   V          �      ]       7       �   �   x   �   h       �   `   D           v          �      �   /   �   ^   �   .      z   [   �       -       $      i   M           l              a       }   w   �       Y   \           �       �   p   �   N   �      Q           {   4   �               u   3      �              �   �      9   =          _   �   !   O      *      g   j   �   P   S   �       �           �   �   5       �              m       Z   ~   �   +       )   �   k   &      <   8       %   �      �   E       �   ;           W   T   ?   y   f          t   e   ,   2      �   0              q      �       R   �   #       d   X       �   :       o   r   �   b          >           '       U   �          s   G      1   (           exo-open --launch TYPE [PARAMETERs...]   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active All Files All Icons Application Chooser Button Application Icons Balsa Browse the file system Browse the web C_omment: C_reate Caja File Manager Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Edit Directory Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to load contents from "%s": %s Failed to open "%s". Failed to open %s for writing Failed to open display Failed to open file "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File Manager File Type Icons Folder Follow state GNOME Terminal Galeon Web Browser Icecat Icedove Iceweasel Icon Image Files Jumanji KMail Konqueror Web Browser Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Menu Icons Midori Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No file selected No file/folder specified No icon Number of columns Number of columns to display Opera Browser Options: Orientation PCMan File Manager Perl Scripts Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts ROX-Filer RXVT Unicode Read your email Restart command Rodent File Manager Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Shell Scripts Single Click Space which is inserted at the edges of the icon view Spacing Status Icons Sylpheed Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The file "%s" contains no data The orientation of the iconbar The selection mode The width used for each item Thunar Type '%s --help' for usage. Uncategorized Icons Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively W3M Text Browser Web Browser Width for each item Window group X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Help _Icon: _Icons only _Internet _Name: _Open _Other... _Remove Toolbar _Search icon: _Text only _URL: _Utilities aterm size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Nguyên Bình <crziter@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/xfce/exo/language/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
  exo-open --launch TYPE [PARAMETERs...] -?, --help In tin nhắn trợ giúp này và thoát -V, --version In thông tin phiên bản và thoát Trình duyệt - Trình duyệt ưa thích.
Trình đọc thư - Trình đọc thư ưa thích.
Trình quản lý tệp tin - Trình quản lý tệp tin ưa thích
Trình giả lập dòng lệnh - Trình giả lập dòng lệnh ưa thích. %s (Xfce %s)

Bản quyền (c) 2003-2006
os-cillation e.K. Mọi quyền đã được bảo lưu.

Được viết bởi Benedikt Meurer <benny@xfce.org>.

Tạo bằng Gtk+-%d.%d.%d, đang chạy Gtk+-%d.%d.%d.

Hãy thông báo lỗi tới <%s>.
 %s không đi kèm với bất cứ đảm bảo nào,
Bạn có thể phân phối lại bản sao của %s dưới điều khoản của
Giấy phép công cộng GNU bản rút gọn, có thể tìm thấy tại
gói nguồn của %s.

 Biểu tượng hành động Hoạt động Mọi tệp tin Mọi biểu tượng Nút lựa chọn ứng dụng Biểu tượng ứng dụng Balsa Duyệt hệ thống tệp tin Duyệt web _Bình luận: _Tạo Trình quản lý tệp tin Caja Chọn ứng dụng ưa thích Chọn một trình duyệt tệp tin tùy thích Chọn một trình đọc thư tùy thích Chọn một trình giả lập dòng lệnh tùy thích Chọn một trình duyệt tùy thích Chọn tên tệp tin Chromium Thư điện tử Claws _Lệnh Bản quyền (c) %s
os-cillation e.K. Mọi quyền đã được bảo lưu.

Được viết bởi Benedikt Meure <benny@xfce.org>.

 Tùy biến thanh công cụ Debian Sensible Browser Trình giả lập dòn lệnh Debian Biểu tượng thiết bị Dillo Sửa thư mục Sửa liên kết Biểu tượng phụ Biểu tượng cảm xúc Kích hoạt tìm kiếm Encompass Trình giả lập dòng lệnh Enlightened Trình duyệt Epiphany Evolution Tệp tin thực thi Tạo "%s", thất bại. Chạy trình quản lý tệp tin mặc định, thất bại Chạy trình đọc thư mặc định, thất bại Chạy trình giả lập dòng lệnh mặc định, thất bại Chạy trình duyệt mặc định, thất bại Nạp nội dung từ "%s": %s, thất bại Mở "%s", thất bại Mở %s để ghi, thất bại Mở chế độ hiển thị, thất bại Mở tệp tin "%s": %s, thất bại Đọc tệp tin "%s": %s, thất bại Lưu "%s", thất bại. Đặt trình quản lý tệp tin mặc định, thất bại Đặt trình đọc thư mặc định, thất bại Đặt trình giả lập dòng lệnh mặc định, thất bại Đặt trình duyệt mặc định, thất bại Trình quản lý tệp tin Biểu tượng loại tệp tin Thư mục Theo dõi tình trạng Dòng lệnh của GNOME Trình duyệt Galeon Icecat Icedove Iceweasel Biểu tượng Tệp tin hình ảnh Jumanji KMail Trình duyệt Konqueror Trình duyệt văn bản Links Biểu tượng địa điểm Trình duyệt văn bản Lynx Trình đọc thư Căn lề Biểu tượng thực đơn Midori Trình duyệt Mozilla Mozilla Firefox Thư điện tử Mozilla Mozilla Thunderbird Mutt NXterm Nautilus Trình duyệt Netscape Không ứng dụng nào được chọn Không có tệp tin nào được chọn Không có tệp / thư mục nào phù hợp Không biểu tượng Số cột Số cột để hiển thị Trình duyệt Opera Tùy chọn: Hướng Trình quản lý tệp tin PCMan Kịch bản Perl Hãy chọn trình quản lý tệp tin ưa thích ngay
bây giờ và nhấn OK để tiếp tục. Hãy chọn trình đọc thư ưa thích ngay
bây giờ và nhấn OK để tiếp tục. Hãy chọn trình giả lập dòng lệnh ưa thích ngay
bây giờ và nhất OK để tiếp tục. Hãy chọn trình duyệt ưa thích ngay
bây giờ và nhấn OK để tiếp tục. Hãy báo lỗi đến <%s>.
 Ứng dụng ưa thích Ứng dụng ưa thích (Trình duyệt, Trình đọc thư và Trình giả lập dòng lệnh) Nhấn chuột trái để thay đổi ứng dụng được chọn. Xem trước In thông tin phiên bản và thoát Kịch bản Python ROX-Filer RXVT Unicode Đọc thư điện tử Lệnh khởi động lại Trình quản lý tệp tin Rodent Kịch bản Ruby Chạy trong dòng lệnh SOCKET ID Sakura Tìm kiếm cột Chọn _biểu tượng từ: Chọn ứng dụng Chọn biểu tượng Chọn ứng dụng Chọn ứng dụng mặc định cho nhiều dịch vụ Dùng tùy chọn này để bật tính năng thông báo khởi động khi lệnh được chạy từ trình quản lí hoặc menu. Không phải mọi ứng dụng đều hỗ trợ tính năng này. Chọn tùy chọn này để chạy lệnh trong cửa sổ dòng lệnh. Chế độ chọn Phần ngăn cách Lệnh khởi động lại phiên chạy Kịch bản Shell Nháy đơn Khoảng trống được chèn tại viền của chế độ hiển thị biểu tượng Khoảng trống Biểu tượng trạng thái Sylpheed Trình giả lập dòng lệnh Cột văn bản Chữ cho biểu tượng _quan trọng Chữ cho _mọi biểu tượng Tệp tin "%s" không chứa dữ liệu Hướng của thanh biểu tượng Chế độ chọn Độ rộng dùng cho mỗi mục Thunar Gõ "%s --help" để xem cách dùng Biểu tượng chưa được phân loại Cách sử dụng: exo-open [URLs...] Sử dụng thông báo khi _khởi chạy Sử dụng ứng dụng tùy chọn không được liệt kê trong danh sách trên. Sử dụng dòng lệnh Hiển thị cho phép người dùng tìm kiếm thông qua các cột một cách tương tác Trình duyệt văn bản W3M Trình duyệt Độ rộng cho mỗi mục Nhóm cửa sổ Dòng lệnh X Dòng lệnh của Xfce Trình quản lý tệp tin Xfce [FILE|FOLDER] Thêm thanh _công cụ mới _Huỷ bỏ Đóng Trợ giúp  _Biểu tượng Chỉ _biểu tượng _Internet _Tên: _Mở _Khác... _Gỡ bỏ thanh công cụ _Tìm biểu tượng: Chỉ _chữ Đường _dẫn T_iện ích aterm cỡ 