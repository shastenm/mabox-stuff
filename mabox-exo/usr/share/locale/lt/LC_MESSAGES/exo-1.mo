��    ,     |	  �  �         2   !  -   T  -   �  ,   �  �   �  *   �  >   �  ,   +  3   X  8   �  �   �  F   \  H   �  7   �  5   $  �   Z  �   !  �   �     �     �     �     �     �     �  	      	      
   %      0      K      ]      c      p      v   2   �      �   	   �      �      �      �      !     !!     >!  !   Z!     |!     �!     �!  
   �!     �!     �!     �!  	   �!  o   �!     g"     x"     �"     �"  0   �"     �"     �"     #     &#     ;#     S#     n#     {#  b   �#     �#     �#  	   $     $  	   $     $  	   +$     5$     S$  	   h$     r$     �$     �$  &   �$  %   �$  +   �$  %   %  9   >%  %   x%  H   �%     �%     �%     &     3&     J&  $   g&     �&     �&  "   �&  !   �&  '   '  !   +'     M'     g'     t'  0   �'     �'     �'     �'     �'     �'     �'     �'     (  H   (     Z(     a(  	   i(     s(     x(     �(     �(  h  �(     *     *     1*     9*     ?*  i   U*  W   �*     +     #+     6+     E+     W+     c+     j+  
   x+     �+  :   �+  J   �+  2   ,  +   C,  <   o,     �,     �,     �,     �,     �,     -     	-     -     -     ,-     D-     Y-     j-     �-     �-     �-     �-  4   �-     .     .     ".     ..     A.     Y.     f.  F   t.  E   �.  K   /  E   M/     �/     �/  G   �/     0  '   /0  +   W0  (   �0  (   �0  ;   �0     1  "   1     <1  	   K1  L   U1     �1  	   �1     �1     �1  0   �1     2     2     2     32     ?2     K2     X2  	   i2     s2     z2     �2     �2     �2     �2     �2  0   �2  �   3  ;   �3     �3  	   4     4     .4     F4     T4     a4     v4  5   }4  0   �4  +   �4  )   5     :5  I   B5  H   �5  N   �5  H   $6     m6     z6     �6     �6     �6     �6  
   �6     �6     �6     �6     �6  3   7  0   E7  r   v7     �7    8  ;   9     U9     i9     y9     �9  J   �9  [   �9  \   [:  T   �:     ;  )    ;     J;     g;     n;     };  4   �;  (   �;     �;  "   <     .<     J<     d<  A   ~<     �<  8   �<     =     "=     /=     @=  0   L=  A   }=     �=     �=     �=     �=  
   >     >     !>     2>     @>     S>     [>     b>     s>     y>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   �>     �>  
   �>     �>     �>     �>  �  ?  9   �@  .   A  4   7A  3   lA  �   �A  3   B  N   �B  2   C  9   5C  <   oC  �   �C  I   AD  B   �D  2   �D  D   E  �   FE  �    F  �   G     �G     �G  #   �G  %   �G     	H  !   &H     HH     TH  
   fH     qH     �H     �H     �H     �H     �H  >   �H     I     1I     >I     GI     _I  #   tI  ,   �I  *   �I  0   �I  ,   !J     NJ     lJ  
   uJ     �J     �J     �J  	   �J  y   �J     NK     `K     rK     �K  5   �K  "   �K  $   �K      L     >L     \L     wL     �L     �L  �   �L     @M     RM     dM     uM  	   ~M     �M  	   �M  !   �M     �M  	   �M     �M     �M     N  2   N  /   RN  4   �N  2   �N  ?   �N  )   *O  Y   TO     �O     �O     �O     P  #   P  &   AP  '   hP     �P  3   �P  0   �P  5   Q  3   HQ  #   |Q     �Q     �Q  /   �Q     �Q     R     R     R     +R     ER     KR  
   YR  ?   dR     �R     �R  	   �R  
   �R     �R     �R      S  =  S     TT  %   fT     �T     �T     �T  k   �T  \   #U     �U     �U     �U     �U     �U  	   �U     �U     
V     V  &   #V  Q   JV  7   �V  ,   �V  L   W     NW     jW     }W     �W     �W     �W     �W     �W     �W     �W     �W     X     X  1   9X     kX     }X     �X  8   �X     �X     �X     Y     Y     *Y     HY     XY  V   iY  T   �Y  Z   Z  V   pZ  &   �Z     �Z  W   [  6   ^[  ;   �[  H   �[  G   \  I   b\  P   �\  
   �\  )   ]     2]  	   D]  W   N]     �]  	   �]     �]     �]  ?   �]     ^     $^     @^     Z^     p^     �^     �^     �^     �^     �^     �^     �^     _     !_     9_  4   O_  �   �_  U   8`     �`  	   �`  "   �`     �`     �`      a  !   a     <a  2   Ca  +   va  ,   �a  ,   �a     �a  R   	b  O   \b  U   �b  R   c     Uc     jc     �c     �c     �c     �c  
   �c     �c     �c     �c     d  .   'd  -   Vd  t   �d     �d    e  ,   *f     Wf     qf     �f     �f  I   �f  k   
g  s   vg  ]   �g     Hh  +   [h     �h     �h     �h  )   �h  -   �h  (   i     Di  -   ai  %   �i     �i     �i  L   �i     ?j  Q   Xj     �j     �j     �j     �j  ;   �j  E   0k     vk     �k     �k     �k     �k     �k     �k     �k  "   l     3l  	   ?l     Il  	   bl     ll     yl     �l     �l     �l     �l     �l     �l  
   �l     �l     �l     m  
   m     m     m     $m                 7   Y   G             �   �   �   "   �   �           )   �   r              	     p   m      �     �     =   5       �     �           �           �       �   '  Z   �   �     J     c   @   �               -         �   �   �   �   �   �       v   �   Q   '             j           t   u     �      �   y   �           P       �   �   �   �         �   �           |   �      �   �       �   �   +      �   *       �   �                      T   �   �             s     N      �         ;   g      �   �   �       �         X   )          W   �                   B           h   \           �   l   3   �   �      U       �   �   &  �   �   #   C   �   �   k       �   �   M   9              ~          �       O   �   �   2     ]       4   D   �   1       �      �             	  �   d             E   �   �      �   i   R      S   �     �   A   a       $   e   z     0   
  H   �      .      �       �   L       �       �   :     q            �   b   _       (       �       *          >   ?   �   F   �   �   �   �           }       {   �   <   �       "  $  V           %   �   �     �   �   (      8   �   �   %      �   �   �   �       !  �         ^   �   `             �   x           �   !       �     �   �   �       f          �   �              w   �   #      �   �   �     [       +   �       �       �         /       &   �     �   �   K   n   �           I   �   �       
   ,         �   �   ,  �       6               �             �   o          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-20 22:52+0000
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/xfce/exo/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
        %s [parametrai] --build-list [[vardas failas]...]
        exo-open --launch TIPAS [PARAMETRAI...]   --build-list      Apdoroti (vardas, failas) poras
   --extern          Generuoti išorinius simbolius
   --launch TIPAS [PARAMETRAI...]       Paleisti pageidaujamą TIPO programą
                                      su nebūtinais PARAMETRAIS, kur
                                      TIPAS yra viena iš šių reikšmių.   --name=identifikatorius C macro/kintamojo vardas
   --output=failo_pavadinimas Rašyti sugeneruotą csource į nurodytą failą
   --static          Generuoti statinius simbolius
   --strip-comments  Pašalinti komentarus iš XML failų
   --strip-content   Pašalinti mazgo turinį iš XML failų
   --working-directory KATALOGAS       Numatytasis darbinis katalogas programoms
                                      naudojant --launch parametrą.   -?, --help                Parodyti šį pagalbos pranešimą ir išeiti   -V, --version                       Parodyti versiją ir išeiti   -V,  --version     Parodyti versiją ir išeiti
   -h, --help          Parodyti šį pagalbos pranešimą ir išeiti
   WebBrowser       - Pageidaujama saityno naršyklė.
  MailReader       - Pageidaujama pašto programa.
  FileManager      - Pageidaujama failų tvarkytuvė.
  TerminalEmulator - Pageidaujamas terminalo emuliatorius. %s (Xfce %s)

Autorinės teisės (c) 2003-2006
        os-cillation e.K. Visos teisės saugomos.

Sukūrė Benedikt Meurer <benny@xfce.org>.

Sukompiliuota su Gtk+-%d.%d.%d, veikia Gtk+-%d.%d.%d.

Apie klaidas praneškite <%s>.
 %s platinama BE JOKIŲ GARANTIJŲ,
Gali platinti kopijas %s pagal
GNU Lesser General Public License sąlygas, kurias galite rasti
%s pradinio kodo pakete.

 Veiksmų piktogramos Aktyvus Pasirinkto elemento rėmelio spalva Pasirinkto elemento užpildymo spalva Pasirinkto elemento indeksas Pasirinkto elemento teksto spalva Visi failai Visos piktogramos Animacijos Programos pasirinkimo mygtukas Programų piktogramos Balsa Blokuoti įrenginį Brave Naršyti failų sistemą Naršyti failų sistemą pasirinktinės komandos pasirinkimui. Naršyti internete K_omentaras: Suku_rti Caja failų tvarkytuvė Aprašyti įrenginį Pasirinkite pageidaujamą programą Pasirinkite pasirinktinę failų tvarkytuvę Pasirinkite pasirinktinę pašto programą Pasirinkite pasirinktinį terminalo emuliatorių Pasirinkite pasirinktinę saityno naršyklę Pasirinkite failo pavadinimą Chromium Claws Mail Išvalyti paieškos eilutę Tarpai tarp stulpelių Tarpai tarp stulpelių Kom_anda: Autorinės teisės (c) %s
        os-cillation e.K. Visos teisės saugomos.

Sukūrė Benedikt Meurer <benny@xfce.org>.

 Sukurti katalogą Sukurti leistuką Sukurti leistuką <b>%s</b> Sukurti nuorodą Sukurti nurodytame kataloge naują darbalaukio failą Žymeklio elemento rėmelio spalva Žymeklio elemento užpildymo spalva Žymeklio elemento teksto spalva Tinkinti įrankių juostą... Debian Sensible naršyklė Debian X Terminalo Emulatorius Įrenginių piktogramos Dillo Užtempkite elementą ant aukščiau esančios įrankių juostos tam, kad pridėtumėte, o pašalinimui - nutempkite nuo įrankių juostos. Taisyti katalogą Taisyti leistuką Taisyti nuorodą Emblemos Jaustukai Įjungti paiešką Encompass Enlightened Terminalo Emulatorius Epiphany saityno naršyklė Evolution Vykdomieji failai FIFO Nepavyko sukurti „%s“. Nepavyko paleisti numatytosios failų tvarkytuvės Nepavyko paleisti numatytosios pašto programos Nepavyko paleisti numatytojo terminalo emuliatoriaus Nepavyko paleisti numatytosios saityno naršyklės Nepavyko paleisti pageidaujamos programos kategorijai „%s“. Nepavyko įkelti turinio iš „%s“: %s Nepavyko įkelti paveikslo „%s“: nežinoma priežastis, tikriausiai sugadintas failas Nepavyko atverti „%s“. Nepavyko atverti %s rašymui Nepavyko atverti URI „%s“. Nepavyko atverti ekrano Nepavyko atverti failo „%s“: %s Nepavyko apdoroti „%s“ turinio: %s Nepavyko perskaityti failo „%s“: %s Nepavyko įrašyti „%s“. Nepavyko nustatyti numatytosios failų tvarkytuvės Nepavyko nustatyti numatytosios pašto programos Nepavyko nustatyti numatytojo terminalo emuliatoriaus Nepavyko nustatyti numatytosios saityno naršyklės Failas „%s“ neturi tipo įrašo Failų tvarkytuvė Failų tipų piktogramos Failo vieta nėra paprastas failas ar katalogas Aplankas Sekti būseną GIcon GNOME terminalas Galeon saityno naršyklė Geary Google Chrome Vienalytis Kaip tekstas ir piktogramos išsidėstę vienas kito atžvilgiu Icecat Icedove Iceweasel Piktograma Piktogramų juostos modelis Piktogramų rodinio modelis Piktogramos stulpelis Jei nenurodysite --launch parametro, exo-open atvers visus nurodytus URL
su jų numatytomis programomis. Jei nurodysite --launch parametrą, galėsite
pasirinkti pageidaujamą programą kurią norėsite vykdyti ir programai perduoti
parametrus (t.y. terminalo emuliatoriui galėsite perduoti komandą kurią vykdyti). Paveikslų failai Netinkamas pagelbiklio tipas „%s“ Jumanji KMail Konqueror saityno naršyklė Paleisti numatytąjį TIPO pagelbiklį su nebūtinu PARAMETRU, TIPAS gali būti viena iš šių reikšmių. Darbalaukio failų paleidimas nepalaikomas, kai %s yra sukompiliuota be GIO-Unix galimybės. Išdėstymo būdas Links tekstinė naršyklė Vietų pikrogramos Lynx tekstinė naršyklė Pašto programa Paraštė Žymėjimo stulpelis Meniu piktogramos Midori Stulpelio modelis pagal kurį ieškoti Modelio stulpelis, naudojamas absoliutaus, atvaizduojamo paveikslo kelio, gavimui Modelio stulpelis iš kurio gaunamas piktogramos pixbuf Modelio stulpelis iš kurio gaunamas tekstas Modelio stulpelis naudojamas gauti tekstui, kai naudojamas Pango žymėjimas Piktogramų juostos modelis Mozilla naršyklė Mozilla Firefox Mozilla paštas Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nepasirinkta jokia programa Nenurodyta komanda Nepasirinktas failas Nenurodytas failas/aplankas Nėra apibrėžta jokio pagelbiklio, skirto "%s". Nėra piktogramos Stulpelių skaičius Rodomų stulpelių skaičius Atverti pageidaujamų programų
konfigūracijos dialogą Opera naršyklė Parametrai: Orientacija PCMan failų tvarkytuvė PCManFM-Qt failų tvarkytuvė Perl scenarijai Pixbuf stulpelis Pasirinkite pageidaujamą failų tvarkytuvę
ir spustelėkite Gerai, norėdami tęsti. Pasirinkite pageidaujamą pašto programą
ir spustelėkite Gerai, norėdami tęsti. Pasirinkite pageidaujamą terminalo emuliatorių
ir spustelėkite Gerai, norėdami tęsti. Pasirinkite pageidaujamą saityno
naršyklę ir spustelėkite Gerai, norėdami tęsti. Prašome apie klaidas pranešti <%s>.
 Pageidaujamos programos Pageidaujamos programos (Saityno naršyklė, Pašto programa ir terminalo emuliatorius) Išankstinių nustatymų rinkinio URL kuriant nuorodą Išankstinių nustatymų rinkinio komanda kuriant leistuką Išankstinių nustatymų rinkinio komentaras, darbalaukio failo kūrimui Išankstinių nustatymų rinkinio piktograma darbalaukio failo kūrimui Išankstinių nustatymų rinkinio pavadinimas, darbalaukio failo kūrimui Paspauskite kairįjį pelės mygtuką, norėdami pakeisti pasirinktą programą. Peržiūra Parodyti versijos informaciją ir išeiti Python scenarijai QTerminal Užklausti numatytąjį TIPO pagelbiklį, kuriame TIPAS yra viena iš šių reikšmių. QupZilla ROX-Filer RXVT Unicode Skaityti paštą Atvaizduoti skirtingai, atsižvelgiant į pasirinkimo būseną. Pertvarkomas Paleisti komandą iš naujo Rodent failų tvarkytuvė Tarpai tarp eilučių Tarpai tarp eilučių Ruby scenarijai Paleisti _terminale LIZDO ID Sakura Paieškos stulpelis Pasirinkite _piktogramą iš: Pasirinkite darbinį katalogą Pasirinkite programą Pasirinkite piktogramą Pasirinkite programą Pasirinkite numatytas programas įvairioms tarnyboms Pasirinkite šį parametrą, norėdami įjungti paleidimo pranešimą, kai komanda paleidžiama iš failų tvarkytuvės ar meniu. Ne visos programos palaiko paleidimo pranešimą. Pasirinkite šį parametrą, jei norite, kad komanda būtų paleista terminalo lange. Pasirinkimo veiksena Skirtukas Seanso paleidimo iš naujo komanda Nustatymų tvarkytuvės lizdas Shell scenarijai Vienkartis spustelėjimas Vienkarčio spustelėjimo trukmė Lizdas Tarpas, įterpiamas piktogramų rodinio kraštuose Tarpas, įterpiamas tarp elemento langelių Tarpas įterpiamas tarp grotelių stulpelių Tarpas, įterpiamas tarp grotelių eilučių Išretinimas Pasirinkite programą, kurią naudosite kaip
numatytąją failų tvarkytuvę Xfce: Pasirinkite programą kurią naudosite kaip
numatytąją pašto programą XFce: Pasirinkite programą kurią naudosite kaip
numatytąjį terminalo emuliatorių XFce: Pasirinkite programą, kurią naudosite kaip
numatytąją saityno naršyklę Xfce: Būsenos piktogramos Įprastinės piktogramos Surf Sylpheed TIPAS [PARAMETRAS] Terminalo emuliatorius Terminator Teksto stulpelis Tekstas _svarbioms piktogramoms Tekstas _visoms piktogramoms GIcon atvaizduoti. Tarpas tarp dviejų greta esančių stulpelių Tarpas tarp dviejų greta esančių eilučių Laiko tarpas po kurio elementas po pelės žymekliu bus automatiškai pasirinktas vienkarčio spustelėjimo veiksena Faile „%s“ nėra duomenų Komandoms --launch ir --query yra palaikomi šie TIPAI:

  WebBrowser       - Pageidaujama saityno naršyklė.
  MailReader       - Pageidaujama pašto programa.
  FileManager      - Pageidaujama failų tvarkytuvė.
  TerminalEmulator - Pageidaujamas terminalo emuliatorius. Šie tipai yra palaikomi --launch parametro: Piktograma atvaizdavimui. Išdėstymo būdas Piktogramų rodinio modelis Piktogramų juostos orientacija Pageidaujama failų tvarkytuvė bus naudojama aplankų turiniui naršyti. Pageidaujama pašto programa, jums spustelėjus ant el. pašto adreso, bus naudojama el. laiškams rašyti. Pageidaujamas terminalo emuliatorius bus naudojamas komandoms, kurioms reikia komandų eilutės aplinkos, vykdymui. Pageidaujama saityno naršyklė bus naudojama nuorodų atvėrimui ir žinyno turiniui rodyti. Parinkimo veiksena Piktogramos dydis pikseliais atvaizdavimui. Kiekvieno elemento plotis Thunar Įrankių juostos _stilius Bandykite „%s --help“ dėl naudojimo. Darbalaukio failo tipas (programa ar nuoroda) Nepavyko nustatyti URI schemos „%s“. Nekategarizuotos piktogramos Nepalaikomas darbalaukio failo tipas „%s“ Naudojimas: %s [parametrai] [failas]
 Naudojimas: exo-open [URL...] Naudoti pa_leidimo pranešimą Naudoti pasirinktinę programą kurios nėra aukščiau esančiame sąraše. Naudoti komandų eilutę Rodinys leidžia naudotojams gali vykdyti interaktyvią paiešką tarp stulpelių Rodinys yra pertvarkomas Vimprobable2 W3M Text Browser Saityno naršyklė Ar visi antriniai objektai turėtų būti to paties dydžio Ar elementas rodinyje gali būti aktyvuotas vienkarčiu spustelėjimu Kiekvieno elemento plotis Langų grupė Langų grupės lyderis Darbinis _katalogas: X Terminalas Xfce Terminalas Xfe failų tvarkytuvė [FAILAS|APLANKAS] Pridėti _naują įrankių juostą _Atsisakyti _Užverti _Darbalaukio numatytasis _Žinynas P_iktograma: Tik _piktogramos _Internetas Pa_vadinimas: _Gerai _Atverti _Kita... Ša_linti įrankių juostą Į_rašyti _Ieškoti piktogramos: Tik _tekstas _URL: _Reikmenys aterm qtFM dydis 