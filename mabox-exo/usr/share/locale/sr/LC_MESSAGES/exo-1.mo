��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  Q   �@  ;   DA  U   �A  A   �A  -  B  L   FC  f   �C  A   �C  S   <D  b   �D  �   �D  g   �E  e   EF  T   �F  I    G    JG  _  ^H    �I     �J  
   �J  +   �J  5   &K  &   \K  +   �K     �K     �K     �K  (   �K     L  
   9L     DL     ZL  4   gL  q   �L     M     ,M     ?M  *   OM     zM  0   �M  M   �M  A   N  O   YN  G   �N  .   �N      O     1O  *   MO     xO     �O     �O  �   �O     rP     �P  )   �P     �P  [   �P  5   PQ  ?   �Q  5   �Q  3   �Q  *   0R  >   [R     �R     �R  3   �R  �   �R     �S     �S     �S     T     T     +T     IT  8   \T  .   �T     �T     �T     �T  5    U  ^   6U  R   �U  b   �U  Z   KV  c   �V  J   
W  �   UW  3   �W  >   X  @   \X  6   �X  G   �X  G   Y  I   dY  3   �Y  ^   �Y  R   AZ  `   �Z  \   �Z  6   R[  !   �[  *   �[  \   �[     3\     D\     Z\     x\  *   �\     �\     �\     �\  �   �\     x]     �]     �]     �]  $   �]  *   �]     ^  �  ;^     �`  ;   �`     *a     9a  /   Ia  �   ya  �   -b     �b  ,   �b     !c  ,   ;c     hc  
   ~c     �c  !   �c     �c  e   �c  x   8d  Y   �d  K   e  �   We  )   �e  #   f     0f     Pf  #   lf     �f     �f     �f  #   �f  &   �f  (   g  *   <g  ;   gg  7   �g     �g     �g  3   h  ]   ?h  !   �h     �h     �h  ,   �h  ,   i     @i     ^i  �   zi  �    j  �   �j  �   bk  +   l     0l  }   Pl  T   �l  `   #m  x   �m  v   �m  r   tn  h   �n     Po  ?   _o     �o     �o  �   �o     np     }p     �p  '   �p  T   �p  %   *q  2   Pq  .   �q     �q     �q     �q  %   r     ,r     Hr     Ur  $   sr  .   �r  !   �r  !   �r  !   s  O   -s  @  }s  y   �t     8u     Pu  ?   cu  :   �u     �u     �u  #   v     >v  a   Uv  P   �v  P   w  N   Yw     �w  �   �w  �   Tx  �   �x  �   �y     z     5z     Sz     bz     oz  #   �z     �z     �z  ,   �z  (   	{  8   2{  <   k{  8   �{  �   �{  :   �|  s  �|  S   [~  )   �~     �~  5   �~  *   -  �   X  �   �  �   ��  �   D�     �  L   �  )   h�  
   ��  '   ��  7   ł  j   ��  V   h�  #   ��  Q   �  ?   5�  ,   u�  >   ��  v   �  .   X�  }   ��  7   �     =�  #   O�     s�  e   ��  i   ��  $   c�     ��  "   ��  %   Ň     �  "   �  /   +�  #   [�  .   �     ��     ��  4   ̈     �     �     �     7�     I�     V�     c�     q�  '   ��     ��     ��     ؉     �     ��  
   
�  
   �      �                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-16 22:43+0000
Last-Translator: Саша Петровић <salepetronije@gmail.com>
Language-Team: Serbian (http://www.transifex.com/xfce/exo/language/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
        %s [могућности] --build-list [[назив датотеке]...]
        exo-open --launch ВРСТА [ОДРЕДНИЦе...]   --build-list      Рашчлањује (назив, датотека) парове
   --extern          Образује спољне симболе
   --launch ВРСТА [ПАРАМЕТРИ...]       Покреће омиљени програм
                                      ВРСТЕ са могућим ОДРЕДНИЦАМ, где је
                                      ВРСТА једна од следећих вредности.   --name=одредник назива Ц макроа/променљиве
   --output=filename Исписује створени извор у одређену датотеку
   --static          Образује сталне симболе
   --strip-comments  Уклања напомене из ИксМЛ датотека
   --strip-content   Уклања садржаје чворова из ИксМЛ датотека
   --working-directory ДИРЕКТОРИЈУМ    Основни радни директоријум за програме
                                      приликом коришћења могућности „--launch“.   -?, --help                          Исписује ову поруку помоћи и излази   -V, --version                       Исписује податке о издању и излази   -V, --version     Исписује податке о издању и излази
   -h, --help        Исписује ову поруку и излази
   WebBrowser       — Омиљени веб прегледник.
  MailReader       — Омиљени читач поште.
  FileManager      — Омиљени управник датотека.
  TerminalEmulator — Омиљени опонашач терминала. %s (Иксфце %s)

Ауторска права (c) 2003-2006
        ос-цилација е.К. Сва права су задржана.

Написао је Бенедик Мејер <benny@xfce.org>.

Изграђено употребом Гтк+-%d.%d.%d, ради на Гтк+-%d.%d.%d.

Молим пријавите грешке на <%s>.
 %s долази БЕЗ ИКАКВОГ ЈЕМСТВА
Можете да расподељујете примерке %s под одредбама
Гнуове мање опште јавне лиценце коју можете пронаћи
у %s изворном пакету.

 Сличице радње Радно Боја ивице радне ставке Боја испуњавања радне ставке Садржај радне ставке Боја писма радне ставке Све датотеке Све сличице Анимације Дугме бирача програма Сличице програма Балса Блок уређај Храбар Разгледајте систем датотека Разгледајте систем датотека да изаберете произвољну наредбу. Прегледајте веб На_помена: _Направи Управник датотека Цаја Знаковни уређај Изаберите омиљени програм Изаберите произвољног управника датотека Изаберите произвољног читача поште Изаберите произвољног опонашача терминала Изаберите произвољног веб прегледника Изаберите назив датотеке Кромијум Клавсова пошта Очистите поље претраге Размак стубаца Размак стубаца На_редба: Ауторска права (c) %s
        ос-цилација е.К. Сва права су задржана.

Написао Бенедикт Мејер <benny@xfce.org>.

 Направи фасциклу Направи покретач Направи покретач <b>%s</b> Направи везу Прави нову датотеку радне површи у датој фасцикли Боја ивице ставке показивача Боја испуњавања ставке показивача Боја писма ставке показивача Прилагодите траку прибора... Разумни читач Дебијана Опонашач Икс терминала за Дебијан Сличице уређаја Дило Не_ приказуј више ову поруку Превуците ставку на траку прибора изнад да је додате, или са траке прибора на табелу ставки да је уклоните. Уреди фасциклу Уреди покретач Уреди везу Обележја Изрази лица Укључи претрагу Обухватач Опонашач терминала Просвећења Прегледник веба Спознаја Еволуција Извршне датотеке ФИФО Нисам успео да направим „%s“. Нисам успео да извршим основног управника датотека Нисам успео да извршим основног читача поште Нисам успео да покренем основног опонашача терминала Нисам успео да извршим основног прегледника веба Нисам успео да покренем омиљени програм за врсту „%s“. Нисам успео да учитам садржаје са „%s“: %s Нисам успео да учитам слику „%s“: Разлог није познат, вероватно неисправан запис Нисам успео да отворим „%s“. Нисам успео да отворим %s за писање Нисам успео да отворим адресу „%s“. Нисам успео да отворим приказ Нисам успео да отворим датотеку „%s“: %s Нисам успео да обрадим садржаје „%s“: %s Нисам успео да прочитам датотеку „%s“: %s Нисам успео да сачувам „%s“. Нисам успео да подесим основног управника датотека Нисам успео да подесим основног читача поште Нисам успео да подесим основног опонашача терминала Нисам успео да подесим основног прегледника мреже Датотека „%s“ нема кључ врсте Управник датотека Сличице врсте датотеке Место датотеке није редовна датотека или фасцикла Фасцикла Прати стање Гномова сличица Гномов терминал Прегледник веба Галија Гири Гуглов Гром Уједначено Како се писмо и сличица сваке ставке постављају једно у односу на друго Ледено маче Ледени голуб Ледена ласица Сличица Модел траке сличице Начин прегледа сличице Колона сличица Ако не наведете могућност „--launch“, ексо-опен ће отворити све наведене
адресе са њиховим омиљеним руковаоцима адреса. Другачије, ако наведете
могућност „--launch“, можете да изаберете који омиљени програм желите да
покренете, и да проследите додатне одреднице програму (тј. за „TerminalEmulator“
можете да проследите наредбу која би требала да буде покренута у терминалу). Датотеке слика Неисправна врста помоћника „%s“ Џуманџи К‑пошта К‑освајач веб прегледник Покреће основног помоћника ВРСТЕ са могућом ОДРЕДНИЦОМ, где је ВРСТА једна од следећих вредности. Покретање датотека радне површи није подржано када је „%s“ преведен без ГИО-Јуникс-оих могућности. Распоред Линксов прегледач писма Сличице места Линксов прегледач писма Читач поште Оквир Стубац ознаке Сличице изборника Мидори Стубац модел за претрагу приликом претраге кроз ставке Колона која се користе за потпуну путању датотека слика за приказ Стубац начина приказа из које се извлачи сличица Стубац у моделу из којег се извлачи писмо Стубац у моделу из којег се извлачи писмо уколико се користе Пангове ознаке Модел за траку сличице Мозилин прегледник Мозила Фајерфокс Мозилина пошта Мозилин Тандербирд Мут Ениксов терминал Наутилус Нетскејп навигатор Није изабран програм Није наведена наредба Датотека није изабрана Није наведена датотека/фасцикла Није одређен помоћник за „%s“. Нема сличице Број стубаца Број стубаца за приказивање Отворите прозорче за подешавање
омиљених програма Оперин прегледник Могућности: Усмерење Управник датотека ПЦМен PCManFM-Qt управник датотека Перлова скрипта Стубац сличице Молим сада изаберите вашег омиљеног управника
датотека и кликните „У реду“ да наставите. Молим сада изаберите вашег омиљеног читача
поште и кликните „У реду“ да наставите. Молим сада изаберите вашег омиљеног опонашача
терминала и кликните „У реду“ да наставите. Молим сада изаберите вашег омиљеног прегледника
веба и кликните „У реду“ да наставите. Грешке пријавите на <%s>.
 Омиљени програми Омиљени програми (прегледник веба, читач поште и опонашач терминала) Адреса претподешавања приликом стварања везе Наредба претподешавања приликом стварања покретача Напомена претподешавања приликом стварања датотеке радне површи Сличица претподешавања приликом стварања датотеке радне површи Назив претподешавања приликом стварања датотеке радне површи Притисните лево дугме миша да измените означени програм. Преглед Исписује податке о издању и излази Питонова скрипта Куов терминал Упитује подразумеваног помагача ВРСТЕ, где је ВРСТА једна од следећих вредности. Купзила РОКС-Фајлер РИксВТ Уникод Читајте своју е-пошту Исцртава различито засновано на стању избора. Променљиви редослед Наредба поновног покретања Управник датотека Родент Размак редова Размак редова Рубијева скрипта Покрени у _терминалу ЛБ ПРИКЉУЧНИЦЕ Сакура Стубац претраге Изабери _сличицу из: Изаберите радну фасциклу Изаберите програм Изаберите сличицу Изаберите програм Изаберите основне програме за разне услуге Изаберите ову могућност да укључите обавештења о покретању када се наредба покрене из управника датотека или из изборника. Не подржавају сви програми обавештења о покретању. Изаберите ову могућност да покренете наредбу у прозору терминала. Начин избора Раздвајач Наредба поновног покретања сесије Подешава управника прикључнице Скрипта шкољке Једноструки клик Време за један клик Прикључница Размак који се убацује на крајевима прегледа сличица Размак који се убацује између ћелија ставке Размак који се убацује између стубаца мреже Размак који се убацује између редова мреже Размаци Наведите програм који желите да користите
као основног управника датотека за Иксфце: Наведите програм који желите да користите
као основног читача поште за Иксфце: Наведите програм који желите да користите
као основног опонашача терминала за Иксфце: Наведите програм који желите да користите
као основног веб прегледника за Иксфце: Сличице стања Основне сличице Једрење Силфид ВРСТА [ОДРЕДНИЦА] Опонашач терминала Терминал Стубац писма натпис за _важне сличице натпис за _све сличице Гномова сличица за исцртавање. Размак између два суседна ступца Размак између два суседна реда Износ времена након кога ће ставка под показивачем миша бити изабрана када се користи покретање једним кликом Датотека „%s“ не садржи податке Следеће ВРСТЕ су подржане за наредбе упита --launch и --query:

  WebBrowser       - Омиљени прегледач веба.
  MailReader       - Омиљени читач е-поште.
  FileManager      - Омиљени управник датотека.
  TerminalEmulator - Омиљени опонашач терминала. Следеће ВРСТЕ су подржане за наредбу „--launch“: Сличица за исцртавање. Начин распореда Начин преглед помоћу сличица Усмерење траке сличице Омиљени управник датотека ће се користити за преглед садржаја фасцикли. Омиљени читач поште ће се користити за писање електронске поште када кликнете на адресу е-поште. Омиљени опонашач терминала ће се користити за извршавање наредби које захтевају наредбено сучеље. Омиљени програм за преглед мрежних веза ће се користити за отварање супервеза и приказ садржаја помоћи. Начин избора Величина сличице за исцртавање у тачкама. Ширина за сваку ставку Тунар _Изглед траке прибора Упишите „%s --help“ за коришћење. Врста датотеке радне површи за стварање (програм или веза) Нисам успео да откријем образац адресе за „%s“. Неодређене сличице Неподржана врста датотеке радне површи „%s“ Употреба: %s [могућности] [датотека]
 Употреба: exo-open [адресе...] Користи обавештења при _покретању Користите произвољни програм који се не налази на горњем списку. Користите линију наредби Преглед омогућава корисницима да траже међудејствујући кроз ступце Дозвољена је измена редоследа Вимогући2 В3М прегледач писма Прегледник мреже Да ли садржани елементи треба да буду сви исте величине Да ли ставке у прегледу могу бити покренуте једним кликом Ширина сваке ставке Група прозора Вођа групе прозора Радни _директоријум: Икс-ов терминал Иксфце-ов терминал Управник датотека Иксфе-а [ДАТОТЕКА|ФАСЦИКЛА] _Додај нову траку прибора _Откажи _Затвори _Основни изглед радне површи _Помоћ _Сличица: само _сличице _Интернет На_зив: _У реду _Отвори _Друго... _Уклони траку прибора _Сачувај _Потражи сличицу: само _писмо _Адреса: _Прибор атерм Кутфм величина 