��         	            2   	  -   <  -   j  ,   �  �   �  *   �  ,   �  3     8   5  �   n  F     H   L  7   �  5   �  �     �   �  �   �     M     Z     a     z     �     �  	   �  	   �  
   �     �     �                 2   0     c  	   r     |     �     �     �     �     �  !   �           ;      K   
   T      _      r      �   	   �   o   �      
!     !     +!     E!  0   Q!     �!     �!     �!     �!     �!     �!     "     "  b   $"     �"     �"  	   �"     �"  	   �"     �"  	   �"     �"     �"  	   #     #     &#     +#  &   B#  %   i#  +   �#  %   �#  9   �#  %   $  H   A$     �$     �$     �$     �$     �$  $   
%     /%     L%  "   a%  !   �%  '   �%  !   �%     �%     
&     &  0   '&     X&     _&     l&     r&     �&     �&     �&  H   �&     �&     �&  	   '     '     '     $'     4'  h  @'     �(     �(     �(     �(     �(  i   �(  W   \)     �)     �)     �)     �)     �)      *     *  
   *      *  :   '*  J   b*  2   �*  +   �*  <   +     I+     `+     p+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,      ,     (,     :,  4   W,     �,     �,     �,     �,     �,     �,  F   �,  E   $-  K   j-  E   �-     �-     .  G   0.     x.  '   �.  +   �.  (   �.  (   /  ;   >/     z/  "   �/     �/  	   �/     �/  	   �/     �/     �/  0   �/     0     +0     ;0     O0     [0     g0     t0  	   �0     �0     �0     �0     �0     �0     �0     �0  0   
1  �   ;1  ;   �1     2  	   (2     22     J2     b2     p2     }2     �2  5   �2  0   �2  +    3  )   ,3     V3  I   ^3  H   �3  N   �3  H   @4     �4     �4     �4     �4     �4     �4     �4     �4     5  3   5  0   J5  r   {5     �5  ;   6     I6     ]6     m6     �6     �6  )   �6     �6     7     	7     7  4   47  (   i7     �7  "   �7     �7     �7     �7  A   8     [8  8   p8     �8     �8     �8     �8  0   �8  A   9     Z9     n9     {9     �9  
   �9     �9     �9     �9     �9     �9     �9     �9     :     :     :  	   ':     1:     8:     <:  	   B:     L:     \:     b:  
   p:     {:  
   �:     �:     �:     �:  _  �:  8   �;  .   5<  ;   d<  *   �<  �   �<  1   �=  *   �=  8   >  =   A>  �   >  M   ?  M   f?  <   �?  ;   �?  �   -@  �    A  �   �A     �B     �B  #   �B  $   �B     �B  $   C     8C     JC  
   XC     cC     }C     �C     �C     �C  E   �C     D  
   D     D     #D     ?D     PD  5   oD  ,   �D  2   �D  0   E     6E     NE     WE     cE     {E     �E  
   �E  o   �E     F     $F     5F     PF  9   ]F     �F     �F     �F     �F  "   G     8G     UG     cG  d   iG     �G     �G     �G     H     H     H  	   )H     3H     SH  	   nH     xH     �H     �H  6   �H  -   �H  3   I  1   GI  8   yI  1   �I  a   �I  !   FJ     hJ     �J     �J  /   �J  &   �J  0   K     JK  7   dK  .   �K  4   �K  2    L  #   3L     WL     nL  E   �L     �L     �L     �L     �L     �L     M  
   "M  >   -M     lM     sM  	   {M     �M     �M     �M     �M  a  �M     'O      9O     ZO     bO     hO  `   �O  e   �O     KP     [P     uP     �P     �P     �P     �P     �P     �P  <   �P  W   Q  7   mQ  6   �Q  O   �Q     ,R     @R     VR     fR     tR     �R     �R     �R     �R     �R     �R     �R     �R     S     'S      6S  6   WS     �S     �S     �S     �S     �S     �S  V   �S  M   GT  S   �T  Q   �T     ;U     YU  Q   qU  4   �U  7   �U  7   0V  5   hV  4   �V  6   �V  
   
W  &   W     <W  	   MW     WW  	   `W     jW     wW  ;   �W     �W     �W     �W     X     X     &X     5X  
   KX     VX     ]X     nX     �X     �X     �X     �X  7   �X  �   Y  9   �Y     �Y  	   �Y     Z     !Z     >Z     NZ     ZZ     rZ  0   {Z  4   �Z  )   �Z  +   [     7[  Z   >[  Q   �[  W   �[  Q   C\     �\     �\     �\     �\     �\     �\      �\     ]     !]  6   7]  5   n]  _   �]     ^  ;   #^     _^     t^     �^     �^     �^  )   �^  /   �^     $_     +_  !   A_  ;   c_  $   �_     �_  -   �_  %   `     1`      O`  A   p`     �`  N   �`     a     6a     Ca     [a  &   ma  ?   �a     �a     �a     �a     b     %b     1b     @b     \b     pb     �b     �b     �b  	   �b     �b     �b  	   �b     �b     �b     �b     �b     c     c     c     1c     Bc  
   Jc     Uc     [c     `c     �   �   �   �   �   m   J                  8   	     U                       l   �       Q   a     �   �   �   �   D       �       �            �         �       2   �   �   �   �   �         �                %   �           9      5   #       s   �       =   �   G   {       >   X   �   Y   /         �   �           	               �         �          �       �     L      6       P   K   �       �   !   �      H       �   7   .   �   �   �       �   :       �   ;   N   �       }   +   �   �   �   �   �       i     R       S   ]   �   �     �   )   z       �   y   ~   Z   �   �   �   �   [   u       |                   e   �   �   
    �   r   �       �   �   �       �   g       �     \                            O          �   t   o          �   (   �   �   M       �   @       F   �   1            �   f   �   �       V           �              �   `          �   �   �   I   �   �                         0   �   �   W                 C     �   <           �              ^       �   �       �   k   �      '   E              �   4       �       ?        �   �   �   v   �   w   _   x   h   �      �           �   T   �   3   �   q       �   &       �   �   �                 ,   �   �     �   �   A   �   �      �       �           �       B       �   �   �   �   �   �   j   �       "   �         b   
       �   n   d   -   �       $   c     �   p             �   �       �       �         �       �   �               *           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Basque (http://www.transifex.com/xfce/exo/language/eu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [aukerak] --build-list [[izen fitxategia]...]
        exo-open --launch MOTA [PARAMETROAK...]   --build-list      Bikoteak (izena, fitxategia) analizatu
   --extern          Kanpo sinboloak sortu
   --launch MOTA [PARAMETROak...]       Abiarazi MOTA motako aplikazio
                                       gogokoena PARAMETROak parametroez non
                                      MOTA hauetako balio bat izan behar da.   --name=identifikatzailea C makro/aldagai izena
   --static          Sinbolo finkoak sortu
   --strip-comments  Iruzkinak kendu XML fitxategietatik
   --strip-content   Ezabatu nodo edukiak XML fitxategietatik
   --working-directory DIREKTORIOA       Lehenetsiriko lan direktorioa aplikazioentzat
                                      --launch aukera erabiltzean.   -?, --help                          Laguntza testua hau bistarazi eta irten   -V, --version                       Bertsio xehetasunak bistarazi eta irten   -V, --version     Bertsio xehetasunak bistarazi eta irten
   -h, --help        Laguntza testu hau bistarazi eta irten
   WebBrowser       - Hobetsiriko Web Nabigatzailea.
  MailReader       - Hobetsiriko Posta Bezeroa.
  FileManager      - Hobetsiriko fitxategi kudeatzailea.
  TerminalEmulator - Hobetsiriko Terminal Emuladorea. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Benedikt Meurerek <benny@xfce.org> idatzia.

Gtk+-%d.%d.%d erabiliaz eraikia, Gtk+-%d.%d.%d erabiliaz exekutatua.

Programa erroreen berri hemen eman: <%s>.
 %s INONGO BERMERIK GABE banatzen da.
%s kopiak partekatu ditzakezu %s iturburu paketean aurki daitekeen
"GNU Lesser General Public License" lizentziaren arauetan.

 Ekintza ikonoa Gaitua Gaitutako elementuaren ertz kolorea Gaitutako elementuaren barne kolorea Gaituriko elementu indizea Gaitutako elementuaren testu kolorea Fitxategi guztiak Ikono guztiak Animazioak Aplikazio hautatze botoia Aplikazio Ikonoak Balsa Bloke gailua Arakatu fitxategi sistema Pertsonalizatutako komando bat aukeratzeko fitxategi-sistema arakatu. Nabigatu web-a Iru_zkina: So_rtu CAja fitxategi kudeatzailea Karaktere gailua Hobetsiriko aplikazioa hautatu Pertsonalizatutako fitxategi kudeatzaile bat aukeratu Pertsonalizatutako posta bezero bat aukeratu Pertsonalizatutako terminal emuladore bat aukeratu Pertsonalizatutako web nabigatzaile bat aukeratu Fitxategi izena hautatu Chromium Claws posta Garbitu bilaketa eremua Zutabe tartea Zutabe tartea Kom_andoa: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Benedikt Meurer-ek <benny@xfce.org> idatzia.

 Sortu direktorioa Sortu abiarazlea Sortu <b>%s</b> abiarazlea Sortu lotura Emandako direktorioan idazmahai fitxategi berri bat sortu Kurtsore elementu ertz kolorea Kurtsore elementu barne kolorea Kurtsore elementu testu kolorea Pertsonalizatu tresna-barra... Debian lehenetsiriko nabigatzailea Debian X terminal emuladorea Gailu ikonoak Dillo Elementu bat tresna barra mugitu eta bertan gehitu, tresna-barratik kanpora mugitu berau ezabatzeko. Editatu direktorioa Editatu abiarazlea Editatu lotura Ikurrak Irriabartxoak Gaitu bilaketa Encompass Enlightened terminal emuladorea Epiphany web nabigatzailea Evolution Fitxategi exekutagarriak FIFO Huts "%s" sortzerakoan. Huts lehenetsiriko fitxategi kudeatzailea exekutatzean Huts lehenetsiriko posta bezeroa exekutatzean Huts lehenetsiriko terminal emuladorea exekutatzean Huts lehenetsiriko web nabigatzailea exekutatzean Huts "%s" ataleko hobetsiriko aplikazioa irekitzerakoan. Huts "%s" fitxategiko edukiak irakurtzerakoan: %s Huts egin du "%s" irudia kargatzerakoan: Errore ezezaguna, ziurrenik hondaturiko irudi fitxategia Huts egin du "%s" irekitzerakoan. Huts %s idazteko idazterakoan Huts "%s" URIa irekitzerakoan. Huts pantaila irekitzerakoan Huts egin du "%s" fitxategia irekitzerakoan: %s Huts "%s" edukiak analizatzerakoan: %s Huts egin du "%s" fitxategia irakurtzerakoan: %s Huts "%s" gordetzerakoan. Huts lehenetsiriko fitxategi kudeatzailea ezartzerakoan Huts lehenetsiriko posta bezeroa ezartzerakoan Huts lehenetsiriko terminal emuladorea ezartzerakoan Huts lehenetsiriko web nabigatzailea ezartzerakoan "%s" fitxategiak ez du mota gakorik Fitxategi kudeatzailea Fitxategi mota ikonoak Fitxategi kokapena ez da ez fitxategi erregular bat ez direktorio bat Karpeta Jarraitu egoera Gikonoa GNOME terminala Galeon web nabigatzailea Google Chrome Homogeneoa Elementu bakoitzaren testu eta ikonoaren kokapena bestearekiko Icecat Icedove Iceweasel Ikonoa Ikono barra modeloa Ikono ikuspegi modeloa Ikono zutabea Ez baduzu --launch aukera zehazten, exo-open URL guztiak URL kudeatzaile gogokoenaz
irekitzen saiatuko da. Baina --launch komandoa
zehaztu ezkero zein gogoko aplikazio abiaraztea
aukera dezakezu, eta horri parametro gehigarriak
bidali diezazkiokezu (adib. terminal emuladoreari
terminal batean ireki beharrezko egiten duen parametroa ipini diezaiokezu). Irudi fitxategiak "%s" laguntzaile mota baliogabea Jumanji KMail Konqueror web nabigatzailea Abiarazi MOTA laguntzailea aukerako PARAMETRO honez, non MOTA hauetako balio bat izan daitekeen. Idazmahai fitxategi abiarazleak ez dira onartzen %s ez dagoenean GIO_Unix ezaugarriekin konpilaturik. Aurkezpen modua Links testu nabigatzailea Kokapen Ikonoak Lynx testu nabigatzailea Posta bezeroa Marjina Markatu zutabea Menu ikonoak Midori Elementuen artean bilatzerakoan erabiliko den zutabe modeloa Errenderizatzeko irudi fitxategiaren bide osoa eskuratzeko erabiliko den zutabe modeloa Ikono pixbuf-a eskuratzeko erabiliko den zutabe modeloa Testua hortik eskuratzeko erabiliko den zutabe modeloa Pango markatzea erabiltzen bada testua eskuratzeko erabiliko den zutabe modeloa Ikono barra modeloa Mozilla nabigatzailea Mozilla Firefox Mozilla posta Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ez da aplikaziorik hautatu Ez da komandoa ezarri Ez da fitxategirik hautatu Ez da fitxategi/karpeta ezarri Ikonorik ez Zutabe kopurua Bistaratuko diren zutabe kopurua Ireki Xfce 4 hobetsiriko aplikazio
konfigurazio leihoa Opera nabigatzailea Aukerak: Orientazioa PCM fitxategi kudeatzailea Perl Script-ak Pixbuf zutabea Mesedez hobetsiriko fitxategi kudeatzailea ezarri
eta ados sakatu aurrera jarraitzeko. Mesedez hobetsiriko posta bezeroa ezarri
eta Ados sakatu aurrera jarraitzeko. Mesedez hobetsiriko terminal emuladorea ezarri
eta Ados sakatu aurrera jarraitzeko. Mesedez hobetsiriko web nabigatzailea ezarri
eta Ados sakatu aurrera jarraitzeko. Erroreen berri eman <%s>-ra.
 Hobetsiriko aplikazioak Hobetsiriko aplikazioa (web nabigatzailea, eposta bezero eta terminal emuladorea) Idazmahai fitxategi bat sortzean URL-a aurrez ezarri Idazmahai fitxategi bat sortzean komandoa aurrez ezarri Idazmahai fitxategi bat sortzean azalpena aurrez ezarri Idazmahai fitxategi bat sortzean ikonoa aurrez ezarri Idazmahai fitxategi bat sortzean izena aurrez ezarri Saguaren eskuineko botoia sakatu aplikazioa aldatzeko. Aurrebista Bertsio argibideak bistarazi eta irten Python Script-ak QTerminal QupZilla ROX-Filer RXVT Unicode Irakurri zure posta Errendatu modu desberdinean hautapenaren egoeraren arabera. Berrantolagarria Berrabiarazi komandoa Rodent fitxategi kudeatzailea Lerro tartea Lerro tartea Ruby Script-ak Abiarazi _terminalean SOCKET IDa Sakura Bilaketa zutabea Hautatu _ikonoa hemendik: Hautatu lan direktorioa Hautatu aplikazio bat Hautatu ikono bat Hautatu aplikazioa Zenbait erabilpenerako lehenetsiriko aplikazioak ezarri Komandoa fitxategi kudeatzailearen edo menuaren bitartez abiaraztean abiarazte berri ematea gaitu. Abiarazte berri ematea ez dute aplikazio guztiek onartzen. Aukera hau hautatu komandoa terminal batetan abiarazteko. Aukeratze modua Bereizlea Saio berrabiarazte komandoa Ezarpen kudeatzaile socket-a Shell Script-ak Klik bakana Klik bakan denbora muga Socket-a Ikono ikuspegiaren ertzetan txertatuko den lekua Elementu baten gelaxkan artean txertatuko den tartea Sareta zutabeen artean sartuko den tartea Sareta lerroen artean txertatuko den tartea Tartea Xfce-ren lehenetsiriko fitxategi kudeatzaile bezala erabili
nahi duzun aplikazioa zehaztu: Xfce-ren lehenetsiriko posta bezero bezala erabili
nahi duzun aplikazioa zehaztu: Xfce-ren lehenetsiriko terminal emuladore bezala erabili
nahi duzun aplikazioa zehaztu: Xfce-ren lehenetsiriko nabigatzaile bezala erabili
nahi duzun aplikazioa zehaztu: Egoera Ikonoak Surf Sylpheed MOTA [PARAMETROA] Terminal emuladorea Testu zutabea Testua Ikono ga_rrantzitsuentzat Testua Ikono guztientz_at Errendatzeko GIkonoa. Jarraian dauden bi zutabe artean txertatuko den tartea Jarraian dauden bi lerro artean txertatuko den tartea Klik bakaneko moduan kurtsorearen azpiko elementua automatikoki aukeratu pasa behar den denbora "%s" fitxategiak ez du daturik Hurrengo MOTA hauek onartzen dira --launch komandoarentzat: Errendatzeko ikonoa. Aurkezpen modua Ikono ikuspegiaren modeloa Ikono-barraren orientazioa Aukeratze modua Errendatzeko ikonoaren tamaina pixeletan. Elementu bakoitzarentzat erabiliko den zabalera Thunar Tresna-barra e_stiloa Idatzi '%s --help' erabilerarako. Sortu beharreko idazmahai fitxategia (Aplikazio edo Lotura) Ezin da "%s"ren URI-eskema antzeman. Kategoria gabeko Ikonoak '"%s" idazmahai fitxategi mota ez da onartzen Erabilera: %s [aukerak] [fitxategia]
 Erabilera: exo-open [URLs...] A_biarazte berri emateak erabili Beheko zerrendan ez dagoen aplikazio pertsonalizatu bat aukeratu. Erabili komando lerroa Ikuspegiak erabiltzaileari zutabetan interaktibori bilatzeko aukera ematen dio Ikuspegia berrantolagarria da Vimprobable2 W3M testu nabigatzailea Web nabigatzailea Semeak tamaina berdina izan behar duen Honela ikuspegiko elementuak klik bakan batez aktibatu daitezke Elementu bakoitzaren zabalera Leiho taldea Leiho talde nagusia Lan _direktorioa: X terminala Xfce Terminala Xfce Fitxategi kudeatzailea [FITXATEGI|KARPETA] Tresna-barra berri_a gehitu Ut_zi It_xi Lehenetsiriko i_dazmahaia _Laguntza _Ikonoa: _Ikonoak bakarrik _Internet _Izena: Ad_os _ireki _Besteak... Ezabatu t_resna-barra _Gorde _Bilaketa ikonoa: _Testua bakarrik _URL-a: _Lanabesak aterm qtFM tamaina 