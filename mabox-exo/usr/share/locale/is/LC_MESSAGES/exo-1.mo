��    %     D	  �  l      �  2   �  -   �  -     ,   0  �   ]  *   A  ,   l  3   �  8   �  �     F   �  H   �  7   -  5   e  �   �  �   b  �   5     �     �     �          )     ;  	   R  	   \  
   f     q     �     �     �     �     �  2   �        	               "      4      E      b         !   �      �      �      �   
   �      �      !     !  	   .!  o   8!     �!     �!     �!     �!  0   �!      "     9"     P"     g"     |"     �"     �"     �"     �"     �"  	   �"     �"  	   �"     �"  	   	#     #     1#  	   F#     P#     a#     f#  &   }#  %   �#  +   �#  %   �#  9   $  %   V$  H   |$     �$     �$     �$     %     (%  $   E%     j%     �%  "   �%  !   �%  '   �%  !   	&     +&     E&     R&  0   b&     �&     �&     �&     �&     �&     �&     �&     �&  H   �&     8'     ?'  	   G'     Q'     V'     e'     u'     �'     �'     �'     �'     �'  i   �'     4(     @(     S(     b(     t(     �(     �(  
   �(     �(  :   �(  J   �(  2   -)  +   `)  <   �)     �)     �)     �)      *     *     !*     &*     -*     6*     I*     a*     v*     �*     �*     �*     �*  4   �*     +     +     #+     /+     B+     Z+     g+  F   u+  E   �+  K   ,  E   N,     �,     �,  G   �,     -  '   0-  +   X-  (   �-  (   �-  ;   �-     .  "   .     =.  	   L.  L   V.     �.  	   �.     �.     �.  0   �.     /     /      /     4/     @/     L/     Y/  	   j/     t/     {/     �/     �/     �/     �/     �/  0   �/  ;    0     \0  	   k0     u0     �0     �0     �0     �0     �0  5   �0  0   1  +   C1  )   o1     �1  I   �1  H   �1  N   42  H   �2     �2     �2     �2     �2     �2     3  
   3     !3     -3     G3     [3  3   p3  0   �3     �3    �3  ;   5     A5     U5     e5     �5  J   �5  [   �5  \   G6  T   �6     �6  )   7     67     S7     Z7     i7  4   �7  (   �7     �7  "   �7     8     68     P8  A   j8     �8  8   �8     �8     9     9     ,9  0   89  A   i9     �9     �9     �9     �9  
   �9     �9     :     :     ,:     ?:     G:     N:     _:     e:     l:  	   x:     �:     �:     �:  	   �:     �:     �:     �:  
   �:     �:  
   �:     �:     �:     �:  |  �:  2   j<  .   �<  1   �<  :   �<  �   9=  -   >  9   L>  7   �>  5   �>  �   �>  E   �?  :   �?  <   @  5   E@  �   {@  �   )A  �   B     �B     �B     �B     �B      C     "C     >C     KC     ]C     lC     ~C     �C     �C     �C     �C  9   �C     �C     D  	   D     %D  
   8D     CD      [D  $   |D     �D     �D     �D     �D  
   �D     E     E     (E     9E  z   BE     �E     �E     �E     �E  1   F     7F     RF     lF     �F     �F     �F     �F     �F     �F     �F     G  
   G     G     +G  	   7G     AG     YG  	   kG     uG     �G     �G  .   �G  2   �G  6   H  )   >H  6   hH  '   �H  M   �H     I  !   -I  "   OI     rI     �I  '   �I  !   �I     �I  .   J  2   >J  +   qJ  )   �J  $   �J     �J      �J  8   K     TK     ZK     iK     oK     �K     �K     �K  	   �K  Q   �K     L     L  	   L  	   L     $L     ;L     SL  
   eL     pL     �L     �L     �L  |   �L     +M     >M     OM     oM     M  	   �M     �M     �M     �M  M   �M  \   N  <   lN  :   �N  `   �N     EO     bO     sO     �O     �O     �O     �O     �O     �O     �O     �O     �O     	P     %P     5P     DP  1   dP     �P  
   �P     �P     �P     �P     �P     �P  S   Q  Q   UQ  R   �Q  N   �Q     IR     eR  1   rR  (   �R  3   �R  <   S  ;   >S  7   zS  ;   �S  
   �S  (   �S     "T  	   2T  _   <T     �T  	   �T     �T     �T  &   �T     �T     U     U     0U     ?U     OU     ]U     rU     �U     �U     �U     �U     �U     �U     �U  G   �U  A   :V     |V  
   �V     �V     �V     �V     �V  !   �V     W  3   W  ,   NW  )   {W  (   �W     �W  O   �W  M   'X  M   uX  J   �X     Y     (Y     AY     FY     OY     aY  
   mY     xY  $   �Y     �Y     �Y  .   �Y  -   Z  #   AZ  �   eZ  8   V[     �[     �[     �[     �[  G   �[  g   C\  Q   �\  Q   �\     O]  2   []     �]     �]     �]  >   �]  @   
^  +   K^     w^  '   �^     �^     �^     �^  I   _     ]_  >   q_     �_     �_     �_     �_  1   �_  ?   `     ^`     }`     �`     �`     �`     �`     �`     �`  !   �`     a     a     a     <a     Da     Pa  	   fa     pa     xa     �a  
   �a     �a     �a     �a     �a     �a  
   �a     �a     �a     �a     �   K   �      �   �   �       �           o            9       |      Q   $       ]   =   �   p       w   k   �               U       �   �   \   �   �   �   �        �   �   ^   �       6           �   <           �   �          �   �             �   -       �      7   �   @                 �       �         j   �                   �       2       �         f   �           �   �   %           #  ;      �   �   G       S   �   N   �       �           h       �   1       �   B           �   u       �   �           #   y     !   �   �       R   �   �   }   3       %      �       c   �         i   Z   *               �   �       ?             �                   `   �      �   �   �          �   �   �      �   ,     a   �   �       H   _      {       d   P   �   v   I   �   �   .   �   z   M       
       Y   F          �     �   �   �               e   /     "   (       �   t       �      �           �   �   �       s        :   $    '          �   "  D   �   �   �              �          �     �   �   �   �   L   4   �   �   �   !  �       0   E       �          �   �   8   >   �      �       �   g             )   r   W   �      q   X      �   �     O   b        C   J   �   �   &   x   �       l   �      m                       �   �                 �   �                 5   �     �     
      +     �   �               �      [   A   �   �       	   �   �       �       n      �   �   �   T   �   �      	  �   �         �   �       ~      �                 V   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-10 11:28+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic (http://www.transifex.com/xfce/exo/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
        %s [rofar] --build-list [[skráaheiti]...]
        exo-open --launch TEGUND [VIÐFÖNG...]   --build-list      Þáttar (heiti, skrá) pör
   --extern          Útbýr ytri tákn (external symbols)
   --launch TEGUND [VIÐFÖNG...]       Ræsa valið forrit fyrir
                                      TEGUNDina með valkvæðum VIÐFÖNGum, þar sem
                                      TEGUND er eitt af eftirfarandi gildum.   --name=auðkenni Heiti á C-fjölva/breytu
   --static          Útbýr föst tákn (static symbols)
   --strip-comments  Eyðir athugasemdum úr XML-skrám
   --strip-content   Eyðir efni liða úr XML-skrám
   --working-directory MAPPA          Sjálfgefin vinnumappa fyrir forrit
                                      þegar --launch rofinn er notaður.   -?, --help                          Birtir þessa hjálp og hættir   -V, --version     Birtir útgáfuupplýsingar og hættir   -V, --version     Birtir útgáfuupplýsingar og hættir

   -h, --help        Birtir þessa hjálp og hættir

   WebBrowser       - Valinn vefvafri.
  MailReader       - Valið tölvupóstforrit.
  FileManager      - Valinn skráastjóri.
  TerminalEmulator - Valið skjáhermiforrit. %s (Xfce %s)

Höfundarréttur (c) 2003-2006
        os-cillation e.K. Allur réttur áskilinn.

Skrifað af Benedikt Meurer <benny@xfce.org>.

Byggt með Gtk+-%d.%d.%d, keyrandi Gtk+-%d.%d.%d.

Tilkynntu villur til <%s>.
 %s kemur ÁN NOKKURRAR ÁBYRGÐAR.
Þú mátt dreifa afritum af %s undir skilmálum
GNU Lesser General Public notkunarleyfisins
sem finna má í upprunapakka %s.

 Táknmyndir aðgerða Virkt Jaðarlitur á virku atriði Fyllilitur á virku atriði Skráningarvísir virks atriðis Textalitur á virku atriði Allar skrár Allar táknmyndir Myndhreyfingar Forritavalhnappur Táknmyndir forrita Balsa Blokkartæki Brave Flakka í skráakerfinu Flakka um skráakerfið til að velja sérsniðna skipun. Flakka á vefnum At_hugasemd: _Búa til Caja skráastjóri Tákntæki Veldu sérvalið forrit Velja sérsniðinn skráastjóra Velja sérsniðið tölvupóstforrit Velja sérsniðinn skjáhermi Velja sérsniðinn netvafra Veldu skráarheiti Chromium Claws Mail Hreinsa leitarsvæði Millibil dálka Bil milli dálka _Skipun: Höfundarréttur (c) %s
        os-cillation e.K. Allur réttur áskilin.

Skrifað af Benedikt Meurer <benny@xfce.org>.

 Búa til möppu Búa til ræsi Búa til ræsi <b>%s</b> Búa til tengil Búa til nýja skjáborðsskrá í gefinni möppu Jaðarlitur bendilatriðis Fyllilitur bendilatriðis Textalitur bendilatriðis Sérsníða verkfærastiku... Debian Sensible vafri Debian X skjáhermir Tækjatáknmyndir Dillo Breyta möppu Breyta ræsi Breyta tengli Táknmerki Tjáningartákn Virkja leit Encompass Enlightened skjáhermir Epiphany netvafri Evolution Keyrsluskrár FIFO Mistókst að búa til "%s". Mistókst að keyra sjálfgefinn skráastjóra Mistókst að keyra sjálfgefið tölvupóstforrit Mistókst að keyra sjálfgefinn skjáhermi (terminal) Mistókst að keyra sjálfgefinn netvafra Mistókst að keyra valið forrit fyrir flokkinn "%s". Mistókst að hlaða efni frá "%s": %s Mistókst að hlaða mynd "%s": Óþekkt ástæða, líklega skemmd myndskrá Mistókst að opna "%s" Mistókst að opna %s fyrir ritun Mistókst að opna slóðina "%s". Mistókst að opna skjá. Gat ekki opnað skrá "%s": %s Mistókst að þátta efni úr "%s": %s Mistókst að lesa skrá "%s": %s Mistókst að vista "%s". Mistókst að velja sjálfgefinn skráastjóra Mistókst að velja sjálfgefið tölvupóstforrit Mistókst að velja sjálfgefinn skjáhermi Mistókst að velja sjálfgefinn netvafra Skrá "%s" hefur engan tegundarlykil Skráastjóri Táknmyndir fyrir skráategundir Skráarstaðsetningin er hvorki venjuleg skrá né mappa Mappa Fylgja stöðu GIcon GNOME skjáhermir Galeon netvafri Geary Google Chrome Einsleitt Hvernig táknmynd og texti hvers atriðis eru staðsett miðað við hvort annað Icecat Icedove Iceweasel Táknmynd Módel táknmyndastiku Módel táknmyndasýnar Táknmyndadálkur Myndskrár Ógild tegund hjálpara "%s" Jumanji KMail Konqueror netvafri Ræsa sjálfgefið hjálparforrit fyrir TEGUNDina með valkvæðu VIÐFANGI, þar sem TEGUND er eitt af eftirfarandi gildum. Framsetningarhamur Links textavafri Táknmyndir fyrir staðsetningu Lynx textavafri Póstforrit Spássía Merkingadálkur Táknmyndir valmynda Midori Dálkur módels sem notaður er til að leita í þegar leitað er í atriði Dálkur módels sem notaður er til að sækja algilda slóð myndskrár sem á að myndgera Dálkur módels sem notaður er til að sækja táknmynd úr Dálkur módels sem notaður er til að sækja textann úr Dálkur módels sem notaður er til að sækja textann úr ef verið er að nota Pango-merkingar Módel fyrir táknmyndastiku Mozilla netvafri Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Ekkert forrit valið Engin skipun tiltekin Engin skrá valin Engin skrá/mappa tilgreind Engin táknmynd Fjöldi dálka Fjöldi dálka sem á að birta Skipta yfir í stillingaglugga
fyrir valin forrit Opera netvafri Valkostir: Stefna PCMan skráastjóri PCManFM-Qt skráastjóri Perl skriftur Pixbuf dálkur Veldu núna hvaða forrit þú vilt nota
sem skráastjóra og smelltu á 'Í lagi'. Veldu núna hvaða forrit þú vilt nota
sem póstforrit og smelltu á 'Í lagi'. Veldu núna hvaða forrit þú vilt nota
sem skipanalínu og smelltu á 'Í lagi'. Veldu núna hvaða forrit þú vilt nota
sem netvafra og smelltu á 'Í lagi'. Tilkynntu villur til <%s>.
 Valin forrit Valin forrit (vafri, póstforrit og skipanalína) Forstillta URL þegar gerður er hlekkur Forskeyta athugasemd þegar ræsir er búinn er til Forskeyta athugasemd þegar skjáborðsskrá er búin er til Forskeyta táknmynd þegar skjáborðsskrá er búin er til Forskeyta heiti þegar skjáborðsskrá er búin er til Ýttu á vinstri músarhnapp til að breyta völdu forriti. Forskoðun Birta upplýsingar um útgáfu og hætta Python skriftur QTerminal Ræsa sjálfgefið hjálparforrit fyrir TEGUND, þar sem TEGUND er eitt af eftirfarandi gildum. QupZilla ROX-Filer RXVT Unicode Lesa tölvupóstinn Myndgera misjafnlega eftir valstöðu. Endurraðanlegt Enduræsingarskipun Rodent skráastjóri Millibil raða Bil milli raða Ruby skriftur Keyra í skjá_hermi AUÐKENNI SÖKKULS - ID Sakura Leitardálkur Velja _táknmynd frá: Veldu vinnumöppu Veldu forrit Veldu táknmynd Velja forrit Veldu hvaða forrit þú vilt að tölvan noti fyrir ýmsar þjónustur Veldu þennan valkost ef þú vilt keyra skipunina í skjáhermi. Valhamur Aðgreinir Skipun til að enduræsa setu Sökkull stillingastjórnunar Skeljarskriftur Einn músarsmellur Tímamörk fyrir einn músarsmell Sökkull Bilið sem er sett á milli jaðra táknmyndasýnar Bilið sem er sett á milli reita í atriði Bilið sem er sett á milli möskvadálka Bilið sem er sett á milli möskvaraða Millibil Tilgreindu hvaða forrit þú vilt nota
sem sjálfgefinn skráastjóra í XFCE: Tilgreindu hvaða forrit þú vilt nota
sem sjálfgefið póstforrit í XFCE: Tilgreindu hvaða forrit þú vilt nota
sem sjálfgefna skipanalínu í XFCE: Tilgreindu hvaða forrit þú vilt nota
sem sjálfgefinn netvafra í XFCE: Táknmyndir fyrir stöðu Upprunalegar táknmyndir Surf Sylpheed TEGUND [VIÐFANG] Skjáhermir Terminator Textadálkur Texti fyrir _mikilvægar táknmyndir Texti fyrir _allar táknmyndir GIcon sem á að myndgera. Stærð bils milli tveggja samliggjandi dálka Stærð bils milli tveggja samliggjandi raða Skráin "%s" inniheldur engin gögn Eftirfarandi TEGUNDir má nota með --launch og --query skipunum:

  WebBrowser       - Valinn vefvafri.
  MailReader       - Valið tölvupóstforrit.
  FileManager      - Valinn skráastjóri.
  TerminalEmulator - Valið skjáhermiforrit. Eftirfarandi TEGUNDir má nota með --launch skipuninni: Táknmynd sem á að myndgera. Framsetningarhamurinn Módel fyrir táknmyndasýn Stefna táknmyndastikunnar Valinn skráastjóri verður notaður til að vafra um efni í möppum. Valið póstforrit mun vera notað til að skrifa tölvupóstskeyti þegar smellt er á netfangstengla. Valinn skjáhermir verður notaður til að keyra skipanir á skipanalínu (CLI). Valinn netvafri mun vera notaður til að opna veftengla og birta hjálparskjöl. Valhamurinn Stærð smámyndar sem á að myndgera, í dílum. Breidd notuð fyrir hvern hlut Thunar S_tíll verkfærastiku Skrifaðu '%s --help' til að sjá hvernig á að nota þetta. Tegund af skjáborðsskrá til að búa til (Forrit eða Tengil) Tókst ekki að finna URI-skema fyrir "%s". Óflokkaðar táknmyndir Óstutt tegund af skjáborðsskrá "%s" Notkun: %s [valkostir] [skrá]
 Notkun: exo-open [URLs...] Nota _tilkynningu við ræsingu Nota sérsniðið forrit sem er ekki tiltækt úr listanum hér að ofan. Nota skipanalínuna Sýn gerir notanda kleift að leita gagnvirkt í gegnum dálka Sýn er endurraðanleg Vimprobable2 W3M textavafri Netvafri Hvort afleiður skuli allar vera af sömu stærð Hvort atriðin í sýninni geti verið virkjuð með einsmellum Breidd notuð fyrir hvern hlut Gluggahópur Aðal í gluggahóp _Vinnumappa: X skjáhermir Xfce skjáhermir Xfe skráastjóri [SKRÁ|MAPPA] Bæt_a við nýrri verkfærastiku _Hætta við Lo_ka _Sjálfgefið á skjáborði _Hjálp Táknm_ynd: E_inungis táknmyndir _Internet _Heiti: Í _lagi _Opna _Annað... Fja_rlægja verkfærastiku Vi_sta _Leita að táknmynd: Einungis _texti S_lóð: N_ytjatól aterm qtFM stærð 