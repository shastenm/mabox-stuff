��    +     t	  �  �        2     -   D  -   r  ,   �  �   �  *   �  >   �  ,     3   H  8   |  �   �  F   L  H   �  7   �  5     �   J  �     �   �     �     �     �     �     �     �  	      	      
                ;      M      S      Y   2   p      �   	   �      �      �      �      �      !  !   ,!     N!     j!     z!  
   �!     �!     �!     �!  	   �!  o   �!     9"     J"     Z"     t"  0   �"     �"     �"     �"     �"     #     %#     @#     M#     S#  b   s#     �#     �#  	   �#     �#  	   $     $  	   $     '$     E$  	   Z$     d$     u$     z$  &   �$  %   �$  +   �$  %   
%  9   0%  %   j%  H   �%     �%     �%     &     %&     <&  $   Y&     ~&     �&  "   �&  !   �&  '   �&  !   '     ?'     Y'     f'  0   v'     �'     �'     �'     �'     �'     �'     �'     �'  H   (     L(     S(  	   [(     e(     j(     y(     �(  h  �(     �)     
*     #*     +*     1*  i   G*  W   �*     	+     +     (+     7+     I+     U+     \+  
   j+     u+  :   |+  J   �+  2   ,  +   5,  <   a,     �,     �,     �,     �,     �,     �,     �,     -     -     -     6-     K-     \-     u-     �-     �-     �-  4   �-     �-     .     .      .     3.     K.     X.  F   f.  E   �.  K   �.  E   ?/     �/     �/  G   �/     0  '   !0  +   I0  (   u0  (   �0  ;   �0     1  "   1     .1  	   =1  L   G1     �1  	   �1     �1     �1  0   �1     �1     2     2     %2     12     =2     J2  	   [2     e2     l2     z2     �2     �2     �2     �2  0   �2  �   3  ;   �3     �3  	   �3     4      4     84     F4     S4     h4  5   o4  0   �4  +   �4  )   5     ,5  I   45  H   ~5  N   �5  H   6     _6     l6     x6     }6     �6     �6  
   �6     �6     �6     �6     �6  3   7  0   77  r   h7     �7    �7  ;   9     G9     [9     k9     �9  J   �9  [   �9  \   M:  T   �:     �:  )   ;     <;     Y;     `;     o;  4   �;  (   �;     �;  "   �;      <     <<     V<  A   p<     �<  8   �<      =     =     !=     2=  0   >=  A   o=     �=     �=     �=     �=  
   �=     >     >     $>     2>     E>     M>     T>     e>     k>     r>  	   ~>     �>     �>     �>  	   �>     �>     �>     �>  
   �>     �>  
   �>     �>     �>     �>  e  �>  7   Y@  .   �@  5   �@  ,   �@  �   #A  4   B  M   8B  ,   �B  4   �B  :   �B  �   #C  K   �C  8   D  6   FD  0   }D  �   �D  �   �E  �   �F     >G     NG     VG     pG     �G     �G     �G     �G  
   �G     �G     H     !H     'H     -H  E   HH     �H     �H     �H     �H      �H  +   �H  !   I  &   >I     eI     �I     �I  
   �I     �I     �I     �I  	   �I  �   �I     �J     �J     �J     �J  5   �J     �J  "   K     9K     YK     pK     �K     �K     �K  !   �K  t   �K     RL     fL     xL     �L  	   �L     �L  	   �L     �L     �L  	   �L     �L     M     M  <   6M  0   sM  5   �M  -   �M  H   N  +   QN  c   }N     �N  &   �N  #   &O     JO  *   dO  ,   �O  +   �O     �O  :   P  0   CP  5   tP  -   �P     �P     �P     Q  D   &Q     kQ     qQ     ~Q     �Q     �Q     �Q     �Q     �Q  J   �Q     R     R  	   "R     ,R     3R     IR     ]R  �  nR     
T      T     <T     DT     JT  g   _T  U   �T     U     ,U     EU     YU     nU  	   {U     �U     �U     �U  8   �U  ]   �U  =   BV  5   �V  Q   �V     W     $W     7W     GW     UW     iW     nW     uW     ~W  "   �W     �W     �W     �W  +   �W  	   (X     2X     CX  >   aX     �X  	   �X     �X     �X  $   �X     Y     Y  i   (Y  a   �Y  _   �Y  X   TZ  &   �Z     �Z  L   �Z  %   ?[  )   e[  3   �[  3   �[  2   �[  Q   *\  	   |\  !   �\     �\  	   �\  R   �\     ]  	    ]     *]     6]  5   K]     �]     �]      �]     �]     �]     �]     �]     ^     ^     &^     :^     U^     u^     �^     �^  B   �^  �   _  L   �_     `     %`     -`  #   G`     k`     |`     �`     �`  /   �`  5   �`  +   a  +   Da  
   pa  a   {a  W   �a  [   5b  T   �b     �b  
   �b     c     c     c     !c  
   3c     >c  #   Nc     rc     �c  >   �c  ?   �c  u   %d  $   �d    �d  .   �e     f     (f     :f      Sf  `   tf  i   �f  c   ?g  h   �g     h  2   !h  &   Th     {h     �h  &   �h  ?   �h  *   �h     !i  &   3i  %   Zi     �i     �i  J   �i     j  S   #j     wj     �j     �j  	   �j  7   �j  D   �j     <k     Vk     ek     �k  
   �k     �k     �k     �k     �k     �k     �k     �k     l     l     #l  	   1l     ;l     Cl     Gl     Ml  
   Yl     dl     kl     }l     �l     �l     �l     �l     �l                 5   X   F             �   �   �   !   �   �           '   �   q              	     o   l      �     �     ;   3       �   
  �           �           �   �   �   &  Y   �   �     I     b   >   �               +         �   �   �   �   �   �       u   �   P   &              i           s   t     �      �   x   �           O       �   �   A   �         �   �           {   �      �   �       �   �   *      �   (       �   �           ~           S   �   �             r     M      �         9   f      �   �   �       �         W   (          V   �                  @           g   [           �   k   1   �   �      T       �   �   %  �   �   "   B   �   �   j       �   �   L   7              }          �       N   �   �   0     \       2   C       /       �      �               �   c             D   �   �      �   h   Q      R   �     �   ?   `       #   d   y     .   	  G   �   �   ,      �       �   K       �       �   8     p            �   a   ^               �       )          <   =   �   E   �   �   �   �           |       z   �   :   �       !  #  U           $   �   �     �   �   '      6   �   �   $      �   �   �   �          �         ]   �   _             �   w           �           �     �   �   �       e          �   �              v   �   "      �   �   �     Z       )   �       �       �         -       %   �     �   �   J   m   �           H   �   �       
   *         �   �   +  �       4               �            �   n          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-26 09:41+0000
Last-Translator: Besnik <besnik@programeshqip.org>
Language-Team: Albanian (http://www.transifex.com/xfce/exo/language/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [mundësi] --build-list [[emër kartele]...]
        exo-open --launch LLOJ [PARAMETËRa...]   --build-list      Përtyp çifte (emër, kartelë)
   --extern          Prodho simbole "extern"
   --launch LLOJ [PARAMETËR...]       Nis aplikacionin e parapëlqyer për
                                      LLOJIN me PARAMETËR opsional, ku
                                      LLOJI është një nga vlerat vijuese.   --name=emër makroje/ndryshoreje identifikuesi C 
   --output=emër kartele Shkruaje te kartela e treguar csource-in e prodhuar
   --static          Prodho simbole statikë
   --strip-comments  Hiq komentet prej kartelash XML
   --strip-content   Hiq lëndë nyjash prej kartelash XML
   --working-directory DREJTORI       Drejtoria parazgjedhje e punës për aplikacione
                                      kur përdoret mundësia --launch.   -?, --ndihmë                          Shtyp këtë mesazh ndihme dhe dil  -V, --version         Shtyp të dhëna versioni dhe dil  -V, --version      Shtyp të dhëna versioni dhe dil
  -h,  --help        Shtyp këtë mesazh dhe dil
   WebBrowser       - Shfletuesi i parapëlqyer.
  MailReader       - Lexuesi i parapëlqyer i postës.
  FileManager      - Përgjegjësi i parapëlqyer i Kartelave.
  TerminalEmulator - Emuluesi i parapëlqyer i Terminalit. %s (Xfce %s)

Të drejta kopjimi (c) 2003-2006
        os-cillation e.K. Tërë të drejtat të rezervuara.

Shkruar nga Benedikt Meurer <benny@xfce.org>.

Ndërtuar me Gtk+-%d.%d.%d, xhiron Gtk+-%d.%d.%d.

Ju lutemi, njoftoni të meta te <%s>.
 %s vjen ABSOLUTISHT PA GARANCI,
Mund të rishpërndani kopje të %s sipas kushteve të
licencës GNU Lesser General Public License e cila mund të
gjendet në paketën burim të %-s.

 Ikona Veprimesh Veprues Ngjyrë ane objekti aktiv Ngjyrë mbushjeje objekti aktiv Tregues objektesh aktivë Ngjyrë teksti objekti aktiv Krejt Kartelat Krejt Ikonat Animacione Buton Zgjedhësi Aplikacionesh Ikona Aplikacionesh Balsa Brave Shfletoni sistem kartelash Shfletoni sistemin e kartelave për të zgjedhur një urdhër vetjak. Shfletoni në Web K_oment: _Krijo Përgjegjësi i Kartelave Caja Zgjidhni Zbatim të Parapëlqyer Zgjidhni një Përgjegjës Kartelash vetjak Zgjidhni një Lexues Poste vetjak Zgjidhni një Emulues Terminali vetjak Zgjidhni një Shfletues vetjak Zgjidhni emër kartele Chromium Claws Mail Pastro fushë kërkimesh Hapësirë Shtyllash Hapësirë shtyllash Urdhë_r: Të drejta Kopjimi (c) %s
        os-cillation e.K. Tërë të drejtat të rezervuara.

Shkruar nga Benedikt Meurer <benny@xfce.org>.

 Krijo Drejtori Krijo Nisës Krijo Nisës <b>%s</b> Krijo Lidhje Krijo te drejtoria e dhënë kartelë të re desktopi Ngjyrë ane objekti kursori Ngjyrë mbushjeje elementi kursori Ngjyrë teksti elementi kursori Përshtatni Panelin… Shfletuesi Debian Sensible Emulues Debian Terminal X Ikona Pajisjesh Dillo _Mos e shfaq prapë këtë mesazh Për ta shtuar, tërhiqni një objekt te panelet më sipër dhe, për ta hequr, prej paneleve te tabela e objekteve. Përpunoni Drejtori Përpunoni Nisës Përpunoni Lidhje Emblema Emotikone Aktivizo Kërkimin Encompass Emulues Terminali Enlightened Shfletuesi Epiphany Evolution Kartela të Ekzekutueshmish FIFO U dështua në krijimin e "%s". U dështua në xhirimin Përgjegjësi Kartelash parazgjedhje U dështua në xhirim Lexuesi parazgjedhje Poste U dështua në xhirim Emuluesi Terminali parazgjedhje U dështua në xhirim Shfletuesi parazgjedhje U dështua në nisjen e aplikacionit parazgjedhje për kategorinë "%s". U dështua në ngarkim lënde prej "%s": %s U dështua në ngarkimin e pamjes "%s": Arsye e panjohur, ka gjasa për kartelë e dëmtuar pamjeje U dështua në hapjen e "%s". U dështua në hapjen e %s për shkrim U dështua në hapjen e URI-t "%s". Dështoi në hapje ekrani U dështua në hapjen e kartelës "%s": %s U dështua në përtypje lënde të "%s": %s U dështua në leximin e kartelës "%s": %s U dështua në ruajtjen e "%s". U dështua në caktim Përgjegjësi Kartelash parazgjedhje U dështua në caktim Lexuesi parazgjedhje Poste U dështua në caktim Emuluesi Terminali parazgjedhje U dështua në caktim Shfletuesi parazgjedhje Kartela "%s" s’ka kyç lloji Përgjegjës Kartelash Ikona Llojesh Kartelash Vendndodhja e kartelës s’është kartelë ose drejtori e rregullt Dosje Ndiq gjendje GIcon Terminal Gnome Shfletuesi Galeon Geary Google Chrome Njëtrajtshëm Si vendosen në lidhje me njëri tjetrin teksti dhe ikona e secilit objekt Icecat Icedove Iceweasel Ikonë Model Shtylle Ikonash Model Pamjeje Ikona Shtyllë ikonash Nëse s’përcaktoni mundësinë --launch, exo-open do t’i hapë krejt URL-të
e treguara me trajtuesit e tyre të parapëlqyer. Përndryshe, nëse përcaktoni
mundësinë --launch, mund të përzgjidhni cili aplikacion i parapëlqyer doni të
të xhirohet, dhe t’i kaloni aplikacionit edhe parametra shtesë (p.sh. për TerminalEmulator
mund të jepeni rreshtin e urdhrave që duhet xhiruar te terminali). Kartela Figurash Lloj i pavlefshëm ndihmësi: %s Jumanji KMail Shfletuesi Konqueror Nis ndihmësin parazgjedhje të LLOJIT me PARAMETËR opsional, ku LLOJ është një nga vlerat vijuese. Nisja e kartelave desktop nuk mbulohet kur %s është përpiluar pa veçori GIO-Unix. Mënyrë skeme Shfletues Tekst Lidhjesh Ikona Vendndodhjesh Shfletues Tekst Lynx Lexues Poste Mënjanë Shtyllë "markup" Ikona Menush Midori Model shtylle për kërkim, kur kërkohet nëpër objekt Shtyllë model e përdorur për të marrë shtegun absolut të një figure që duhet vizatuar Shtyllë model e përdorur për të pasur "pixbuf" ikone prej Shtyllë model e përdorur për të marrë tekst prej Shtyllë model e përdorur për marrjen e tekstit, nëse përdoret "markup" Pango Model për shtyllë ikonash Shfletuesi Mozilla Mozilla Firefox Posta Mozilla Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator S’ka aplikacion të përzgjedhur S’u dha urdhër S’u përzgjodh kartelë S’u tregua kartelë/dosje S’ka ndihmës të përkufizuar për "%s". Pa ikonë Numër shtyllash Numër shtyllash për shfaqje Hap dialogun e formësimit
të Aplikacioneve të Parapëlqyera Shfletuesi Opera Mundësi: Orientim Përgjegjës Kartelash PCMan Përgjegjësi PCManFM-Qt i Kartelave Programthe Perl Shtyllë "pixbuf" Ju lutemi, zgjidhni tani Përgjegjësin tuaj të Kartelave
parazgjedhje dhe klikoni OK që të vazhdohet. Ju lutemi, zgjidhni tani Lexuesin tuaj parazgjedhje
të Postës dhe klikoni OK që të vazhdohet. Ju lutemi, zgjidhni tani Emuluesin tuaj Terminal
parazgjedhje dhe klikoni OK që të vazhdohet. Ju lutemi, zgjidhni tani Shfletuesin tuaj
parazgjedhje dhe klikoni OK që të vazhdohet. Ju lutemi, njoftoni të meta te <%s>.
 Aplikacione të Parapëlqyera Aplikacione të Parapëlqyer (Shfletues, Lexues Poste dhe Emulues Terminali) URL e paracaktuar kur krijohet lidhje Urdhër i paracaktuar kur krijohet nisës Koment i paracaktuar kur krijohet kartelë desktopi Ikonë e paracaktuar kur krijohet kartelë desktopi Emër i paracaktuar kur krijohet kartelë desktopi Shtypni butonin e majtë të miut për të ndryshuar aplikacionin e përzgjedhur. Paraparje Shtyp të dhëna versioni dhe dil Programthe Python QTerminal Pyet ndihmësin parazgjedhje të LLOJIT, ku LLOJI është një nga vlerat vijuese. QupZilla ROX-Filer RXVT Unikod Lexoni email-et tuaj Vizatoje ndryshe, bazuar në gjendjen e përzgjedhur. E rirenditshme Urdhër rinisjeje Përgjegjësi i Kartelave Rodent Hapësirë Rreshtash Hapësirë rreshtash Programthe Ruby Xhiroje në t_erminal ID SOCKET-i Sakura Shtyllë Kërkimesh Përzgjidhni _ikonë prej: Përzgjidhni një drejtori pune Përzgjidhni një Aplikacion Përzgjidhni ikonë Përzgjidhni aplikacion Përzgjidhni aplikacione parazgjedhje për shërbime të ndryshëm Përzgjidheni këtë mundësi për të aktivizuar njoftim në nisje, kur urdhri xhirohet prej përgjegjësit të kartelave ose prej menuje. Jo çdo aplikacion mbulon njoftime gjatë nisjesh. Përzgjidheni këtë mundësi për xhirim urdhri në një dritare terminali. Mënyrë përzgjedhjeje Ndarës Urdhër rinisjeje sesioni "Socket" përgjegjësi rregullimesh Programthe Shell Klikim Njësh Kohëmbarim për Klikim Njësh "Socket" Hapësirë që futet në skajet e pamjes ikonë Hapësirë që futet midis dy kutish të një objekti Hapësirë që futet midis shtyllash rrjete Hapësirë që futet midis rreshtash rrjete Hapësirë Caktoni aplikacionin që doni të përdoret
si Përgjegjës Kartelash parazgjedhje për Xfce-në: Caktoni aplikacionin që doni të përdoret
si Lexues Poste parazgjedhje për Xfce-në: Caktoni aplikacionin që doni të përdoret
si Emulues Terminal parazgjedhje për Xfce-në: Caktoni aplikacionin që doni të përdoret
si Shfletues parazgjedhje për Xfce-në: Ikona Gjendjesh Ikona Stok Surf Sylpheed LLOJ [PARAMETËR] Emulues Terminali Terminator Shtyllë teksti Tekst për ikona të Rë_ndësishme Tekst për _Krejt Ikonat GIcon për t’u vizatuar. Sasia e hapësirës ndërmjet dy shtyllash të njëpasnjëshme Sasia e hapësirës ndërmjet dy rreshtash të njëpasnjëshëm Sasia e kohës pas së cilës objekti nën kursorin e miut do të përzgjidhet vetvetiu sipas mënyrës klikim njësh Kartela "%s" s’përmban të dhëna Për urdhrat --launch dhe --query mbulohen LLOJET vijuese :

  WebBrowser       - Shfletuesi i parapëlqyer.
  MailReader       - Lexuesi i parapëlqyer i Postës.
  FileManager      - Përgjegjësi i parapëlqyer i Kartelave.
  TerminalEmulator - Emuluesi i parapëlqyer i Terminalit. Për urdhrin --launch mbulohen LLOJET vijuese: Ikona për t’u vizatuar. Mënyra e skemës Modeli për pamjen ikona Drejtimi i shtyllës së ikonave Përgjegjësi i parapëlqyer i Kartelave do të përdoret për të shfletuar lëndën e dosjeve. Lexuesi i parapëlqyer i Postës do të përdoret për të hartuar email-e, kur klikoni mbi adresa email. Emuluesi i parapëlqyer i Terminalit do të përdoret për të xhiruar urdhra që lypin mjedis CLI. Shfletuesi i parapëlqyer do të përdoret për të hapur tejlidhje dhe për të shfaqur lëndë ndihme. Mënyra përzgjedhje Madhësia në piksel e ikonës që duhet vizatuar. Gjerësia e përdorur për çdo objekt Thunar _Stil Paneli Për përdorimin, shtypni '%s --help'. Lloj kartele desktopi që duhet krijuar (Aplikacion ose Lidhje) S’arrihet të pikaset URI-scheme e "%s". Ikona Pa Kategori Lloj i pambuluar kartele desktopi "%s" Përdorimi: %s [mundësi] [kartelë]
 Përdorimi: exo-open [URLra...] Përdor njoftime _nisjesh Përdor një aplikacion vetjak që s’përfshihet në listën më sipër. Përdorni rreshtin e urdhrave Pamja i lejon përdoruesit të kërkojë nëpër shtylla në mënyrë ndërvepruese Pamja është e rirenditshme Vimprobable2 Shfletues Tekst W3M Shfletues Të jenë a jo krejt pjellat në të njëjtën madhësi Nëse mund të aktivizohen a jo me klikime njëshe objektet e pamjes Gjerësi për çdo objekt Grup dritaresh Udhëheqës grupi dritaresh Drejtori _Pune: Terminal X Terminal Xfce Përgjegjës Kartelash Xfe [KARTELË|DOSJE] _Shto panel të ri _Anuloje _Mbylle Parazgjedhje _Desktopi _Ndihmë _Ikonë: Vetëm _ikona _Internet _Emër: _OK _Hape _Tjetër… _Hiq Panel _Ruaje _Kërkoni ikonë: Vetëm _tekst _URL: Të _dobishme aterm qtFM madhësi 