��    	     d  a  �      0  2   1  -   d  -   �  ,   �  �   �  ,   �  3   �  �   2  F   �  H     7   Y  5   �  �   �  �   �  �   a               %     >     U     g  	   ~  	   �  
   �     �     �     �     �     �     �  2   �     -  	   <     F     N     `     q     �     �  !   �     �            
        )     <     K  	   Z  o   d     �     �     �       0        L     a     y     �     �  b   �     
         	   '      1   	   9      C   	   Q      [      y   	   �      �      �      �   &   �   %   �   +   !  %   >!  9   d!  %   �!  H   �!     "     ""     @"     Y"     p"  $   �"     �"     �"  "   �"  !   #  '   )#  !   Q#     s#     �#     �#  0   �#     �#     �#     �#     �#     
$     $     $     *$     1$  	   9$     C$     H$     W$     c$     o$     �$     �$     �$  i   �$  W   %     n%     �%     �%     �%     �%  
   �%     �%  :   �%     &     &     )&     9&     F&     Z&     _&     f&     o&     �&     �&     �&     �&     �&     �&     �&     '  4   ,'     a'     o'     x'     �'     �'     �'     �'  F   �'  E   (  K   W(  E   �(     �(     )  G   )     e)  '   �)  +   �)  (   �)  (   *  ;   +*     g*  "   o*     �*  	   �*  L   �*     �*  	   +     +     +     (+     4+     D+     X+     d+     p+     }+  	   �+     �+     �+     �+     �+     �+     �+      ,  0   ,  �   D,  ;   �,     "-  	   1-     ;-     S-     k-     y-     �-     �-     �-  I   �-  H   �-  N   =.  H   �.     �.     �.     �.     �.     �.     /  
   /     */     6/     P/     d/    �/  ;   �0  J   �0  [   1  \   w1  T   �1     )2     <2     Y2     `2     o2  4   �2  (   �2     �2  "   �2      3     <3     V3  A   p3     �3     �3     �3     �3     �3     4     4  
   &4     14     ?4     P4     ^4     q4     y4     �4     �4     �4     �4  	   �4     �4     �4     �4  	   �4     �4     �4     �4  
   �4     �4  
   5     5     5     5  b  5  6   �6  ,   �6  6   �6  ,   7  �   J7  -   8  3   K8  �   8  0   9  @   N9  D   �9  1   �9  �   :  �   �:  �   �;     e<     x<      ~<      �<     �<      �<  
   �<     =     =  #   =     @=     X=     ^=     k=     q=  ;   �=     �=     �=     �=     �=      >  "   >  $   4>  $   Y>  (   ~>  !   �>     �>     �>  
   �>     �>     ?     &?  	   >?  n   H?     �?     �?     �?     �?  *   @  !   3@     U@     r@     �@     �@  ^   �@     A     A     5A     KA  
   TA     _A  	   pA     zA     �A  	   �A     �A     �A     �A  4   �A  4    B  8   UB  1   �B  9   �B  .   �B  T   )C     ~C  )   �C  #   �C     �C  '   D  0   ,D  $   ]D     �D  4   �D  1   �D  8   E  .   =E  (   lE     �E     �E  =   �E     F     	F     F     !F     6F     <F     JF     RF     YF  	   aF     kF     qF     �F     �F      �F     �F     �F     �F  `   �F  _   VG     �G     �G     �G     �G     H     H     H  ?   &H     fH     �H     �H     �H     �H     �H     �H     �H     �H     �H     
I      I     3I  %   SI  
   yI     �I     �I  9   �I     �I  	   �I     J     J     .J     JJ     XJ  M   kJ  N   �J  R   K  J   [K     �K     �K  N   �K  '   +L  +   SL  ,   L  -   �L  /   �L  A   
M     LM  /   UM     �M  	   �M  H   �M     �M  	   �M     �M     N     N     %N     ;N     WN     mN     �N     �N     �N     �N     �N     �N     �N     �N     O     !O  5   ;O  �   qO  E   P     `P  	   rP      |P      �P     �P     �P     �P     �P     �P  R   Q  R   [Q  V   �Q  O   R     UR     eR     uR     zR     �R     �R  
   �R     �R     �R     �R  !   �R    S  5   'T  Q   ]T  m   �T  ^   U  d   |U     �U     �U     V  !   V     :V  0   ZV  1   �V     �V  &   �V     �V     W     *W  G   GW     �W     �W     �W     �W     �W     �W     	X     X     -X     >X     WX     eX     }X     �X     �X     �X     �X     �X  	   �X  	   �X     �X     �X  	   �X     �X     Y  	   
Y  
   Y     Y  
   %Y     0Y     6Y  	   ;Y     �   �   �          �       t           9   �   �              �   @      !       �       �   V   p       �   X      u   �   �   �   �       7       �      �   3   �   �                  �   _   b   ?       �   d   	       x       �   5   �   C      �   l   �     �   +   �                 o   �       |      �   Q       �   T   �   8            J      �       ;       N   �   �      �   �   >   �   �   �   =   �   �   L   �   k   �       E   (           �         �           	  �   �   �       �   z       �           S   <       �           �       &   �       B   "   �   �       %   �   �   $   �           �   �   �   �   ,   �   �   �       
   1       �       �   �   �   �           A   �   �   *   ^     M   a           �       �           �   �   �       �       �   r   I      i      �   �               }   �   G       �       �           `   W       �   v   h   �       c   4   �   j      Y       �   �      �       �   n   �   y          6   w   �              �      �   /               f   �           -   �   �       H              '       �   �   �   [       K       q   �       R   \   0       s   �   ]   �   �   O   e          �   �      U   �   �   #   �   �         �   g   �   �   Z   P       2   m   �   )   ~   �   �   �   �   D   :   F       �       �   �   �       �      �         �              .           �   �   �   {                  %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous Icecat Icedove Iceweasel Icon Icon Bar Model Icon column Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Menu Icons Midori Model column to search through when searching through item Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line Vimprobable2 W3M Text Browser Web Browser Width for each item Window group Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-20 15:01+0000
Last-Translator: Ольга Смирнова
Language-Team: Interlingue (http://www.transifex.com/xfce/exo/language/ie/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ie
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [optiones] --build-list [[nómine file]...]
        exo-open --launch TIP [PARAMETRes...]   --build-list      Analisar pares de (nómine, file)
   --extern          Generar extern simboles
   --launch TIP [PARAMETREs...]       Lansar li preferet application de
                                      TIP con PARAMETREs facultativ, u
                                      TIP es un del sequent valores.   --static          Generate static simboles
   --strip-comments  Remover commentas ex files XML
   --working-directory DIRECTORIA       Predefinit directorial de labor por
                                      applicationes lansat per li option --launch.   -h, --help        Monstra ti-ci textu e surtir   -V, --version     Monstrar li information del version e surtir   -V, --version     Monstrar li information del version, poy surtir
   -h, --help        Monstra ti-ci textu e surtir
   WebBrowser       - Li preferet Navigator web.
  MailReader       - Li preferet Letor de e-postage.
  FileManager      - Li preferet Gerente de files.
  TerminalEmulator - Li preferet Emulator de terminal. %s (Xfce %s)

Jure editorial (c) 2003-2006
        os-cillation e.K. All rights reserved.

Scrit per Benedikt Meurer <benny@xfce.org>.

Constructet con Gtk-%d.%d.%d, executente Gtk-%d.%d.%d.

Ples raportar defectes a <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Icones de actiones Activ Color de borde del activ element Color de funde del activ element Index del activ element Color de textu del activ element Omni files Omni icones Animationes Buton del selector de applicationes Icones de applicationes Balsa Bloc aparate Brave Navigar li sistema de files Navigar li sistema de files por trovar un personal comande. Navigar li Web N_ota: C_rear Gerentiator de files Caja Caracter aparate Selecter un application predefinit Selecte un personal gerente de files Selecte un personal letor de postage Selecte un personal emulator de terminal Selecte un personal navigator Web Selecter un fil-nómine Chromium Claws Mail Vacuar li camp de sercha Interspacie de columnes Interspacie de columnes Com_ande: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Scrit per Benedikt Meurer <benny@xfce.org>.

 Crear un fólder Crear un lansator Crear un lansator <b>%s</b> Crear un ligament Crear un nov file desktop in ti-ci fólder Pesonalisar li instrumentarium... Navigator intelligent Debian Emulator de Terminal Debian X Icones de aparates Dillo Tira un element al paneles de instrumentarium por adjunter it, e ex li paneles por remover it. Redacter li directoria Modificar li lansator Modificar li ligament Emblemas Emoticones Permisser sercha Encompass Emulator de Terminal civilisat Navigator Web Epiphany Evolution Executibil files FIFO Ne successat crear «%s». Ne successat executer li Gerente de files predefinit Ne successat executer li Letor de postage predefinit Ne successat executer li Emulator de terminal predefinit Ne successat executer li Navigator Web predefinit Ne successat lansar li application predefinit por «%s». Ne successat cargar li contenete ex «%s»: %s Ne successat cargar li image «%s»: ínconosset cause, fórsan li file es corruptet Ne successat aperter «%s». Ne successat aperter «%s» por scrition. Ne successat aperter li URI «%s». Ne posset aperter un ecran Ne successat aperter li file «%s»: %s Ne successat analisar li contenete de «%s»: %s Ne successat leer li file «%s»: %s Ne successat gardar «%s» Ne successat assignar un Gerente de files predefinit Ne posset assignar li Letor de postage predefinit Ne successat assignar un Emulator de terminal predefinit Ne posset assignar li Navigator Web predefinit Li file «%s» ne have un clave «Type» Gerentiator de files Icones de tipes del file Un localisation del file ne es un regulari file o directoria. Fólder GIcon Terminal de GNOME Navigator Web Galeon Geary Google Chrome Homogen Icecat Icedove Iceweasel Icone Modelle de barra de icones Columne de icones Files de images Ínvalid tip de processor «%s» Jumanji KMail Navigator Web Konqueror Lansar li predefinit processor de TIP con PARAMETRE facultativ, u TIP es un del sequent valores. Lansament de files desktop ne es supportat quande %s esset compilat sin functiones de GIO-Unix. Navigator textual Links Icones de locs Navigator textual Lynx Letor de postage Márgine Icones de menús Midori Columne de modelle por serchar durante un sercha por un element Un modelle del barra de icones Navigator Mozilla Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Null application selectet Comande ne specificat Null file selectet Null file o fólder es providet Null processor es definit por «%s». Null icone Númere de columnes Númere de monstrat columnes Aperter li dialog de configuration
Applicationes preferet Opera Browser Optiones: Orientation Gerentiator de files PCMan Gerente de files PCManFM-QT Scriptes Perl Columne  de Pixbuf Ples selecter li Gerente de files preferet
e fa un clic sur OK por continuar. Ples selecter li Letor de postage preferet
 e fa un clic sur OK por continuar. Ples selecter li Emulator de terminal preferet
e fa un clicc sur OK por continuar. Ples selecter li Navigator Web preferet
e fa un clic sur OK por continuar. Ples raportar errores a <%s>.
 Applicationes preferet Preferet applicationes (navigator web, letor de e-post e emulator de terminal) URL predefinit por creation un ligament Comande predefinit por creation un lansator Nota predefinit por creation un file desktop Icone predefinit por creation un file desktop Nómine predefinit por creation un file desktop Presse li levul buton de mus por alterar li selectet application. Previder Monstrar li information del version, poy surtir Scriptes Python QTerminal Demande li predefinit processor de TIP, u TIP es un del sequent valores. QupZilla ROX-Filer RXVT Unicode Leer vor postage Reordinabil Comande por restartar Gerentiator de files Rodent Interspacie de ranges Interspacie de ranges Scriptes Ruby Lansar in _terminal ID DE SOCKET Sakura Columne de sercha S_electer ex: Selecter un directoria de labor Selecte un application Selecte un icone Selection del application Selecter applicationes predefinit por varie servicies Selecte ti-ci option por usar li notification de inicie quande li commande es lansat del navigator o li menú. Ne chascun application supporta notificationes de inicie. Selecte ti-ci option por lansar li comande in un fenestre de terminal Mode de selection Separator Comande por restartar li session Socket del gerente de parametres Scriptes shell Singul clicc Duration de un singul clic Socket Interspacie Selecte li application que vu vole usar
quam Gerente de files predefinit por Xfce: Selecte li application que vu vole usar
quam Letor de postage predefinit por Xfce: Selecte li application que vu vole usar
quam Emulator de terminal predefinit por Xfce: Selecte li application que vu vole usar
quam Navigator Web predefinit por Xfce: Icones de statu Standard icones Surf Sylpheed TIP [PARAMETRE] Emulator de Terminal Terminator Columne de textu Textu por i_mportant icones Textu por _omni icones Li file «%s» contene null data. Li sequent TIPes es suportat por comandes --launch e --query:

  WebBrowser       - Li preferet Navigator web.
  MailReader       - Li preferet Letor de e-postage.
  FileManager      - Li preferet Gerente de files.
  TerminalEmulator - Li preferet Emulator de terminal. Li sequent TIPes es suportat por li comande --launch: Li preferet Gerente de files va esser usat por navigar li contenete de fólderes. Li preferet Letor de postage va esser usat por composir missages quande vu fa un clic sur adresses de e-post. Li preferet Emulator de terminal va esser usat por executer comandes que besona ambientie CLI. Li preferet Navigator web va esser usat por aperter hiperligamentes e monstrar contenete de auxilie. Li mode de selection Largore de chascun element Thunar Stil de panel de _instrumentarium Tippa '%s --help' por li usage. Tip de file desktop a crear (Application o Link) Ne successar detecter li schema de URI de «%s». Íncategorisat Ínsubtenet tip «%s» de file desktop Usage: %s [optiones] [file]
 Usage: exo-open [URLs...] U_sar notification de inicie Usar un personal application que ne es disponibil in li liste ad-supra. Usar li linea de comandes Vimprobable2 Navigator textual W3M Navigator Web Largore de chascun element Gruppe de fenestres _Directoria de labor: Terminal de X Terminal de Xfce Gerentiator de files Xfe [FILE|FOLDER] _Adjuncter un nov panel _Anullar _Cluder _Predefinit del ambientie Au_xilie _Icone: Sol _icones _Internet _Nómine: _OK _Aperter _Altri... _Remover li panel _Gardar _Serchar: Sol _textu _URL: _Utensiles aterm qtFM dimension 