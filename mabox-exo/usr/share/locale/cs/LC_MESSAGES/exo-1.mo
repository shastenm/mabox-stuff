��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  �  1?  5   �@  +    A  6   LA  -   �A  �   �A  :   �B  F   �B  0   C  :   HC  :   �C  �   �C  L   ]D  N   �D  =   �D  ;   7E  �   sE  �   =F  �   %G     �G     H      
H  "   +H     NH     hH     �H     �H     �H     �H     �H     �H     �H     �H     I  =   I     [I     jI  
   wI     �I     �I  &   �I  !   �I  "   �I  $   J  %   ?J     eJ     {J  
   �J     �J     �J     �J  
   �J  q   �J     JK     ^K     tK     �K  1   �K  8   �K  9   L  6   IL     �L     �L     �L     �L     �L      �L  j   M     sM     �M     �M     �M  	   �M     �M  	   �M     �M     �M  	   N     N     1N  "   =N  3   `N  3   �N  6   �N  5   �N  @   5O  ,   vO  n   �O  "   P  -   5P  -   cP      �P  ,   �P  3   �P  ,   Q      @Q  3   aQ  3   �Q  6   �Q  6    R  &   7R     ^R     pR  <   �R     �R     �R     �R     �R     �R     S     S  
   !S  F   ,S     sS     zS  	   �S     �S     �S     �S     �S  k  �S     >U  !   GU     iU     qU     wU  y   �U  _   V     pV     �V     �V     �V     �V     �V     �V     �V     W  :   W  [   FW  :   �W  2   �W  U   X     fX     �X     �X     �X     �X     �X     �X     �X     �X  !   �X     Y     *Y  )   HY  <   rY  	   �Y     �Y     �Y  5   �Y     Z  
   #Z  	   .Z     8Z     PZ     mZ     �Z  S   �Z  @   �Z  C   %[  C   i[      �[     �[  \   �[  3   G\  7   {\  \   �\  W   ]  X   h]  :   �]     �]  (   ^     -^  	   C^  h   M^     �^  	   �^     �^     �^  +   �^     _  #   %_     I_     b_     s_     �_     �_  	   �_     �_     �_     �_     �_     �_     `     !`  1   2`  �   d`  B   &a     ia     ya  *   �a     �a     �a     �a  +   �a     b  1   %b     Wb  '   vb  2   �b  	   �b  a   �b  a   =c  d   �c  d   d     id     vd     �d     �d     �d     �d  
   �d     �d     �d     �d     e  5    e  5   Ve  ^   �e  (   �e    f  K   g     hg     �g     �g     �g  P   �g  g   h  �   �h  i   i     ui  &   �i  '   �i     �i     �i  <   �i  U   0j  "   �j     �j  0   �j     �j  "   k  %   0k  B   Vk     �k  =   �k     �k     l     !l     ;l  '   Pl  F   xl     �l     �l     �l      m     m     "m     1m     Gm     Xm     xm     �m  
   �m     �m     �m     �m  	   �m     �m     �m  
   �m     �m     �m     �m     n     n     "n  
   /n     :n     @n     En                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-08-01 19:53+0000
Last-Translator: Pavel Borecki <pavel.borecki@gmail.com>
Language-Team: Czech (http://www.transifex.com/xfce/exo/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
        %s [volby] --build-list [[název souboru]...]
        exo-open --launch TYP [PARAMETRY...]   --build-list      Parsovací páry (název, soubor)
   --extern          Vytvořit extern symboly
   --launch TYP [PARAMETRY...]         Spustí upřednostňovanou aplikaci typu
                                      TYP s volitelnými PARAMETRY, kde
                                      TYP je jedna hodnot uvedených níže.   --name=identifier Název proměnné nebo makra jazyka C
 --output=filename Zapíše vygenerovaný csource do zadaného souboru
   --static          Vytvořit statické symboly
   --strip-comments  Odstraní komentáře ze souborů XML
   --strip-content   Odstraní obsah uzlů ze souborů XML
   --working-directory ADRESÁŘ         Výchozí pracovní adresář pro aplikace
                                      při použití přepínače --launch.   -h, --help                          Zobrazí tuto nápovědu a ukončí se   -V, --version                       Zobrazí informace o verzi a ukončí se   -V, --version     Zobrazí informaci o verzi a ukončí se
   -h, --help        Zobrazí tuto nápovědu a ukončí se
   WebBrowser       - Zvolený webový prohlížeč.
  MailReader       - Zvolený e-mailový klient.
  FileManager      - Zvolený správce souborů.
  TerminalEmulator - Zvolený emulátor terminálu. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Všechna práva vyhrazena.

Napsal Benedikt Meurer <benny@xfce.org>.

Sestaveno s Gtk+-%d.%d.%d, spuštěno s Gtk+-%d.%d.%d.

Prosíme, abyste chyby hlásili na <%s>.
 %s NEPOSKYTUJE ŽÁDNOU ZÁRUKU,
Kopie programu %s můžete šířit pod podmínkami licence
the GNU Lesser General Public License, jejíž znění najdete
přiložené u zdrojového balíčku aplikace %s.

 Ikony akcí Aktivní Barva okraje aktivních položek Barva výplně aktivních položek Index aktivních položek Barva textu aktivních položek Všechny soubory Všechny ikony Animace Tlačítko pro výběr aplikace Ikony aplikací Balsa Blokové zařízení Brave Procházet systém souborů Procházet systém souborů pro výběr vlastního příkazu. Procházet web K_omentář: Vy_tvořit Správce souborů Caja Znakové zařízení Vyberte si upřednostňovanou aplikaci Zvolte vlastní správce souborů Vyberte vlastní e-mailový klient Zvolte vlastní emulátor terminálu Vyberte vlastní webový prohlížeč Zvolte název souboru Chromium Claws Mail Vymazat pole pro hledání Šířka sloupce Rozestup sloupců Přík_az: Copyright (c) %s
        os-cillation e.K. Všechna práva vyhrazena.

Napsal Benedikt Meurer <benny@xfce.org>.

 Vytvořit adresář Vytvořit spouštěč Vytvořit spouštěč <b>%s</b> Vytvořit odkaz Vytvoří nový soubor plochy v daném adresáři Barva okrajů položek, nad kterými se nachází kurzor Barva výplně položek, nad kterými se nachází kurzor Barva textu položek, nad kterými se nachází kurzor Upravit panel nástrojů… Debian Sensible Browser Debian X Terminal Emulator Ikony zařízení Dillo _Nezobrazovat tuto zprávu znovu Položku přidejte na panel nástrojů přetažením z tabulky nástrojů. Opačným postupem ji odeberte. Upravit adresář Upravit spouštěč Upravit odkaz Emblémy Emotikony Povolit hledání Encompass Enlightened Terminal Emulator Webový prohlížeč Epiphany Evolution Spustitelné soubory Fronta FIFO Nepodařilo se vytvořit „%s“. Nepodařilo se nastavit výchozí správce souborů Nepodařilo se nastavit výchozí e-mailový klient Nepodařilo se nastavit výchozí emulátor terminálu Nepodařilo se spustit výchozí webový prohlížeč Nepodařilo se spustit předvolenou aplikaci kategorie „%s“. Nepodařilo se načíst obsah z „%s“: %s Nepodařilo se načíst obrázek „%s“. Důvod není znám, pravděpodobně je soubor s obrázkem poškozen Nepodařilo se otevřít „%s“. Nepodařilo se otevřít soubor %s pro zápis Nepodařilo se otevřít adresu URI „%s“. Nepodařilo se otevřít displej Nepodařilo se otevřít soubor „%s“: %s Nepodařilo se zpracovat obsah souboru „%s“: %s Nepodařilo se vytvořit soubor „%s“: %s Nepodařilo se uložit „%s“. Nepodařilo se nastavit výchozí správce souborů Nepodařilo se nastavit výchozí e-mailový klient Nepodařilo se nastavit výchozí emulátor terminálu Nepodařilo se nastavit výchozí webový prohlížeč Soubor "%s" nemá žádný klíč typu Správce souborů Ikony typů souborů Umístění souboru není platným souborem nebo adresářem Složka Následovat stav GIcon GNOME Terminál Webový prohlížeč Galeon Geary Google Chrome Homogenní Jak jsou text a ikony každé položky navzájem relativně umístěny Icecat Icedove Iceweasel Ikona Model ikonového pohledu Model ikonového pohledu Sloupec ikony Nezadáte-li volbu --launch, exo-open otevře všechny specifikované adresy URL
aplikací zvolenou podle tvaru adresy URL. Pokud však zadáte volbu --launch,
budete moci vybrat, kterou aplikaci chcete spustit a zadat dodatečné parametry
pro aplikace (např. pro TerminalEmulator můžete zadat příkaz, který se provede
v nově otevřeném okně terminálu. Obrázky Neplatný typ nápovědy „%s“ Jumanji KMail Webový prohlížeč Konqueror Spustí výchozí nápovědu typu TYP s volitelným PARAMETRem, kde TYP může nabývat jedné z následujících hodnot. Spouštění souborů plochy není podporováno, pokud je %s zkompilován bez funkcí GIO-Unix. Režim rozvržení Textový prohlížeč Links Ikony umístění Textový prohlížeč Lynx E-mailový klient Okraj Označit sloupec Ikony nabídky Midori Model sloupce pro vyhledávání při hledání v položce Modelový sloupec použitý ke získání absolutní cesty k souboru vykresleného obrázku Sloupec modelu používaný pro získání pixbufu ikony z Sloupec modelu používaný pro získání textu z Sloupec modelu používaný pro získání textu, pokud se používají značky Pango Model pro ikonový pohled Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Nebyla vybrána žádná aplikace Nebyl zadán žádný příkaz Není vybrán žádný soubor Nebyl zadán žádný soubor nebo složka Pro položku „%s“ není definována žádná nápověda. Bez ikony Počet sloupců Počet sloupců ke zobrazení Otevřít dialogové okno
Upřednostňované aplikace Opera Možnosti: Orientace Správce souborů PCMan Správce souborů PCManFM-Qt Skripty jazyka Perl Sloupec pixbuf Nyní si vyberte upřednostňovaný
správce souborů a klikněte na tlačítko OK. Vyberte si svůj e-mailový klient
a klikněte na tlačítko OK. Vyberte si svůj emulátor terminálu
a klikněte na tlačítko OK. Vyberte si svůj webový prohlížeč
a klikněte na tlačítko OK. Prosíme, hlaste chyby na <%s>.
 Upřednostňované aplikace Upřednostňované aplikace (webový prohlížeč, e-mailový klient a emulátor terminálu) Přednastavená adresa URL při vytváření odkazu Přednastavený příkaz při vytváření spouštěče Přednastavený komentář při vytváření souboru
                              na ploše Přednastavená ikona při vytváření souboru na
                              ploše Přednastavený název při vytváření souboru na
                              ploše Klikněte levým tlačítkem pro změnu vybrané aplikace. Náhled Zobrazí informace o verzi a ukončí se Skripty jazyka Python QTerminal Pošle dotaz na výchozí nápovědu typu TYP, kde TYP může nabývat jedné z následujících hodnot. QupZilla ROX-Filer RXVT Unicode Číst poštu Vykreslovat odlišně podle stavu výběru. Přeuspořádatelný Příkaz pro opakované spuštění Správce souborů Rodent Výšky řádků Rozestup řádků Skripty jazyka Ruby Spustit v _terminálu SOCKET ID Sakura Sloupec hledání Vybrat _ikonu z: Vyberte pracovní adresář Vyberte si aplikaci Vyberte ikonu Vyberte aplikaci Vyberte si výchozí aplikace pro různé služby Zvolte tuto možnost pro povolení oznámení při spuštění, pokud je příkaz spouštěn ze správce souborů nebo z hlavní nabídky. Ne každá aplikace podporuje oznámení při startu . Zvolte tuto možnost pro spuštění příkazu v okně terminálu. Režim výběru Oddělovač Příkaz pro opakované spuštění relace Soket správce nastavení Shellové skripty Jednoduché kliknutí Vypršení limitu pro jednoduché kliknutí Socket Prostor vložený na okraje ikonového zobrazení Prostor mezi buňkami položek Prostor, který je vložen mezi sloupce Prostor, který je vložen mezi mřížku řádků Rozestupy Upřesněte aplikaci, kterou chcete použít
jako výchozí správce souborů v prostředí Xfce: Upřesněte aplikaci, kterou chcete použít
jako výchozí e-mailový klient v prostředí Xfce: Upřesněte aplikaci, kterou chcete použít
jako výchozí emulátor terminálu v prostředí Xfce: Upřesněte aplikaci, kterou chcete použít
jako výchozí webový prohlížeč v prostředí Xfce: Ikony stavů Ikony dodané výrobcem Surf Sylpheed TYP [PARAMETR] Emulátor terminálu Terminator Textový sloupec Text u _důležitých ikon Text u _všech ikon GIcon k vykreslení. Prostor mezi dvěma po sobě následujícími sloupci Prostor mezi dvěma po sobě následujícími řádky Doba, po které se položka pod kurzorem myši v režimu jednoduchého kliknutí sama označí Soubor „%s“ neobsahuje žádná data Příkazy --launch a --query podporují následující TYPY:

  WebBrowser       - Zvolený webový prohlížeč.
  MailReader       - Zvolený e-mailový klient.
  FileManager      - Zvolený správce souborů.
  TerminalEmulator - Zvolený emulátor terminálu. Při použití přepínače --launch jsou podporovány následující TYPy: Ikona určená pro vykreslení. Režim rozvržení Model pro ikonový pohled Orientace panelu ikon Upřednostňovaný správce souborů bude použit k procházení obsahu složek. Upřednostňovaná čtečka pošty bude použita pro psaní e-mailů po kliknutí na e-mailové adresy. Upřednostňovaný emulátor terminálu bude použit ke spouštění příkazů, které vyžadují prostředí příkazového řádku. Upřednostňovaný webový prohlížeč bude použit k otevření odkazů a zobrazení obsahu nápovědy. Režim výběru Velikost vykreslené ikony v pixelech. Šířka použitá pro každou položku Thunar _Styl panelu nástrojů Informace o použití získáte příkazem  „%s --help“. Typ vytvářeného souboru plochy
                              (aplikace nebo odkaz) Nelze najít schéma URI „%s“. Nezařazené ikony Nepodporovaný typ souboru spouštěče „%s“ Použití: %s [volby] [soubor]
 Použití: exo-open [AdresyURL...] Použít oznámení při _spuštění Použít vlastní aplikaci, která není uvedena v seznamu výše. Použít příkazový řádek Pohled umožní uživateli interaktivně hledat ve sloupcích Pohled je přeuspořádatelný Vimprobable2 Textový prohlížeč W3M Webový prohlížeč Zda mají mít potomci stejnou velikost Zda mohou být zobrazené položky aktivovány jednoduchým kliknutím Šířka pro každou položku Skupina oken Vedoucí skupina oken Pracovní _adresář: X Terminál Terminál Xfce Správce souborů Xfe [SOUBOR|SLOŽKA] Přid_at nový panel nástrojů _Storno _Zavřit Vý_chozí _Nápověda _Ikona: Pouze _ikony _Internet _Název: _OK _Otevřít _Ostatní… _Odebrat panel nástrojů _Uložit _Najít ikonu: Pouze _text Adresa _URL: _Nástroje aterm qtFM velikost 