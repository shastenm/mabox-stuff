��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  O  1?  2   �@  -   �@  )   �@  /   A  �   <A  ,   B  :   :B  /   uB  4   �B  5   �B  �   C  O   �C  K   �C  :   <D  >   wD  �   �D  �   �E  �   hF     G     G     G  #   5G     YG     sG  
   �G     �G     �G     �G     �G     �G  	   �G     �G     �G  ?    H     @H     PH     \H     cH  	   wH     �H  !   �H  #   �H  )   �H  #   I     /I     =I  
   FI     QI     _I     nI  
   }I  x   �I     J     J     J     5J  -   FJ     tJ  #   �J     �J     �J     �J     K      K     -K     3K  �   NK     �K     �K     �K      L  
   	L     L  	   &L     0L     ML  	   aL     kL     zL     �L  '   �L  #   �L  )   �L  #   &M  =   JM  (   �M  Y   �M     N  !   "N     DN     bN     zN  )   �N     �N     �N  +   �N  '   %O  -   MO  '   {O  $   �O     �O     �O  2   �O     P     P     -P     4P     CP     UP     [P     iP  I   qP     �P     �P  	   �P     �P     �P     �P     �P  g  
Q     rR     ~R     �R     �R     �R  [   �R  Z   S     rS     �S     �S     �S  
   �S     �S     �S  
   �S     �S  ?   �S  V   /T  :   �T  6   �T  [   �T     TU     iU     yU     �U     �U     �U     �U     �U     �U     �U     �U     �U     V  "   #V  
   FV     QV     `V  1   ~V     �V     �V     �V     �V     �V     W     W  Q   %W  M   wW  S   �W  M   X  #   gX     �X  B   �X  3   �X  5   Y  =   NY  8   �Y  8   �Y  ;   �Y     :Z  %   KZ     qZ  	   �Z  N   �Z     �Z  	   �Z     �Z     �Z  .   [     :[     S[     e[     {[     �[     �[     �[  	   �[     �[     �[     �[     �[     �[     \     \  2   (\  �   [\  D   ]     W]  	   j]     t]  "   �]     �]  
   �]     �]     �]  '   �]      ^     9^     W^     t^  @   |^  <   �^  B   �^  <   =_     z_     �_     �_     �_     �_     �_  
   �_     �_     �_     �_     `  3   (`  2   \`  m   �`      �`    a  5   ,b     bb     |b  #   �b     �b  G   �b  [   c  Y   oc  P   �c     d  2   /d  *   bd     �d     �d  !   �d  D   �d  )   e     :e  2   Qe     �e  !   �e     �e  A   �e     #f  3   7f      kf     �f     �f  
   �f  1   �f  7   �f     g     8g     Fg     Yg  
   hg     sg     �g     �g     �g  	   �g     �g     �g     �g     �g     �g  	   �g     h     h     h  
   h     !h     8h     =h  
   Oh     Zh  
   `h     kh     qh  
   vh                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-19 20:45+0000
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/xfce/exo/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [tilvalg] --build-list [[navn fil] ...]
        exo-open --launch TYPE [PARAMETRE ...]   --build-list      Tolk (navn, fil)-par
   --extern          Generér eksterne symboler
   --launch TYPE [PARAMETRE ...]       Start foretrukne program for
                                      TYPE med mulige PARAMETRE, hvor
                                      TYPE er en af følgende værdier.   --name=identifikator C-makro/variabelnavn
   --output=filnavn Skriv generede csource til angivne fil
   --static          Generér statiske symboler
   --strip-comments  Fjern kommentarer fra XML-filer
   --strip-content   Fjern knudeindhold fra XML-filer
   --working-directory DIRECTORY       Standardarbejdsmappe for programmer
                                      når tilvalget --launch bruges.   -?, --help                          Udskriv denne hjælpemeddelelse og afslut   -V, --version                       Udskriv versionsinformation og afslut   -V, --version     Udskriv versionsinformation og afslut
   -h, --help        Udskriv denne hjælpemeddelelse og afslut
   WebBrowser       - Den foretrukne webbrowser.
  MailReader       - Den foretrukne postlæser.
  FileManager      - Den foretrukne filhåndtering.
  TerminalEmulator - Den foretrukne terminalemulator. %s (Xfce %s)

Ophavsret (c) 2003-2006
        os-cillation e.K. Alle rettigheder forbeholdes.

Skrevet af Benedikt Meurer <benny@xfce.org>.

Kompileret med Gtk+-%d.%d.%d, kørende Gtk+-%d.%d.%d.

Rapportér venligst fejl til <%s>.
 %s leveres med ABSOLUT INGEN GARANTIER,
Du kan distribuere %s under betingelserne
af GNU Lesser General Public License som kan findes i
%s-kildepakken.

 Handlingsikoner Aktiv Kantfarve for aktive element Udfyldningsfarve for aktive element Indeks for aktive element Tekstfarve for aktive element Alle filer Alle ikoner Animationer Programvælgerknap Programikoner Balsa Blokenhed Brave Gennemse filsystemet Gennemse filsystemet for at vælge en brugerdefineret kommando. Gennemse webbet K_ommentar: _Opret Caja Filhåndtering Tegnenhed Vælg foretrukket program Vælg en tilpasset filhåndtering Vælg en brugerdefineret postlæser Vælg en brugerdefineret terminalemulator Vælg en brugerdefineret webbrowser Vælg filnavn Chromium Claws Mail Ryd søgefelt Kolonneafstand Kolonneafstand Komm_ando: Ophavsret (c) %s
        os-cillation e.K. Alle rettigheder forbeholdes.

Skrevet af Benedikt Meurer <benny@xfce.org>.

 Opret mappe Opret starter Opret starteren <b>%s</b> Opret henvisning Opret en ny skrivebordsfil i den valgte mappe Kantfarve for markørelement Udfyldningsfarve for markørelement Tekstfarve for markørelement Tilpas værktøjslinje ... Debian Sensible Browser Debian X Terminalemulator Enhedsikoner Dillo Vis _ikke meddelelsen igen Træk et element til værktøjslinjerne ovenfor for at tilføje det, fra værktøjslinjerne i elementtabellen for at fjerne det. Rediger mappe Rediger starter Rediger henvisning Emblemer Emotikoner Aktivér søgning Encompass Enlightened Terminalemulator Epiphany Webbrowser Evolution Kørbare filer Først ind, først ud (FIFO) Kunne ikke oprette "%s". Kunne ikke køre standardfilhåndtering Kunne ikke køre standardpostlæser Kunne ikke køre standardterminalemulator Kunne ikke køre standardwebbrowser Kunne ikke starte det foretrukne program for kategorien "%s". Kunne ikke indlæse indhold fra "%s": %s Kunne ikke indlæse billede "%s": Ukendt årsag, højst sandsynligt en ødelagt billedfil Kunne ikke åbne "%s". Kunne ikke åbne %s til skrivning Kunne ikke åbne URI'en "%s". Kunne ikke åbne skærm Kunne ikke åbne filen "%s": %s Kunne ikke fortolke indholdet af "%s": %s Kunne ikke læse fil "%s": %s Kunne ikke gemme "%s". Kunne ikke indstille standardfilhåndtering Kunne ikke indstille standardpostlæser Kunne ikke indstille standardterminalemulator Kunne ikke indstille standardwebbrowser Filen "%s" har ikke nogen typenøgle Filhåndtering Filtypeikoner Filplaceringen er ikke en regulær fil eller mappe Mappe Følg tilstand G-ikon GNOME Terminal Galeon Webbrowser Geary Google Chrome Homogen Hvordan hvert elements tekst og ikoner er placeret i forhold til hinanden Icecat Icedove Iceweasel Ikon Ikonlinjemodel Model for ikonvisning Ikonkolonne Hvis du ikke angiver tilvalget --launch, vil exo-open åbne alle angivne
URL'er med deres foretrukne URL-håndtering. Ellers, hvis du angiver tilvalget --launch
, kan du vælge hvilket foretrukket program du ønsker at køre og angive
yderligere argumenter til programmet (f.eks. for TerminalEmulator
kan du angive kommandolinje som skal køres i terminalen). Billedfiler Ugyldig hjælpertype "%s" Jumanji KMail Konqueror Webbrowser Start standardhjælpen af TYPE med mulige PARAMETRE, hvor TYPE er en af følgende værdier. Opstart af skrivebordsfiler understøttes ikke når %s er bygget uden GIO-Unix-egenskaber. Layouttilstand Links Tekstbrowser Placeringsikoner Lynx Tekstbrowser Postlæser Margen Opmærkningskolonne Menuikoner Midori Modelkolonne til at søge igennem, når der søges i et element Modelkolonnen bruges til at indhente den fulde sti for en billedfil, der skal gengives Modelkolonne som bliver brugt til at hente ikon-pixbuf fra Modelkolonne som bliver brugt til at hente teksten fra Modelkolonne som bliver brugt til at hente teksten, hvis der bliver brugt Pango-opmærkning Model for ikonlinjen Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator Intet program valgt Ingen kommando valgt Ingen fil valgt Ingen fil/mappe angivet Ingen hjælper defineret for "%s". Intet ikon Antal kolonner Antal kolonner der skal vises Åbn de foretrukne programmers
indstillingsdialog Opera Browser Indstillinger: Orientering PCMan Filhåndtering PCManFM-Qt Filhåndtering Perl-scripter Pixbuf-kolonne Vælg venligst din foretrukne filhåndtering
nu og klik på OK for at fortsætte. Vælg venligst din foretrukne postlæser
nu og klik på OK for at fortsætte. Vælg venligst din foretrukne terminalemulator
nu og klik på OK for at fortsætte. Vælg venligst din foretrukne webbrowser
nu og klik på OK for at fortsætte. Rapportér venligst fejl til <%s>.
 Foretrukne programmer Foretrukne programmer (webbrowser, postlæser og terminalemulator) Forudindstillet URL når der oprettes en henvisning Forudindstillet kommando når der oprettes en starter Forudindstillet kommentar når der oprettes en skrivebordsfil Forudindstillet ikon når der oprettes en skrivebordsfil Forudindstillet navn når der oprettes en skrivebordsfil Tryk på venstre musetast for at ændre det valgte program. Forhåndsvisning Udskriv versionsinformation og afslut Python-scripter QTerminal Efterspørg standardhjælperen af TYPE, hvor TYPE er en af følgende værdier. QupZilla ROX-Filer RXVT Unicode Læs dine e-mail Gengiv anderledes baseret på valgte tilstand. Rækkefølge kan ændres Genstartskommando Rodent Filhåndtering Rækkeafstand Rækkeafstand Ruby-scripter Kør i _terminal SOKKEL-ID Sakura Søgekolonne Vælg _ikon fra: Vælg en arbejdsmappe Vælg et program Vælg et ikon Vælg program Vælg standardprogrammer for forskellige tjenester Vælg denne indstilling for at aktivere opstartsunderretning, når kommandoen bliver kørt fra filhåndteringen eller menuen. Ikke alle programmer understøtter opstartsunderretning. Vælg denne indstilling for at køre kommandoen i et terminalvindue. Markeringstilstand Separator Kommando for sessionsgenstart Sokkel til indstillingshåndtering Skal-scripter Enkeltklik Tidsgrænse for enkeltklik Sokkel Afstand mellem kanterne ved ikonvisning Afstand mellem elementets celler Afstand mellem gitterkolonner Afstand mellem gitterrækker Afstand Angiv programmet du
vil bruge som standardfilhåndtering i Xfce: Angiv programmet du vil bruge
som standardpostlæser i Xfce: Angiv programmet du vil bruge
som standardterminalemulator i Xfce: Angiv programmet du vil bruge
som standardwebbrowser i Xfce: Statusikoner Stockikoner Surf Sylpheed TYPE [PARAMETRE] Terminalemulator Terminator Tekstkolonne Tekst til vi_gtige ikoner Tekst til _alle ikoner G-ikonet der skal gengives. Afstanden mellem to på hinanden følgende kolonner Afstanden mellem to på hinanden følgende rækker Hvor lang tid der skal gå, inden elementet under musemarkøren bliver valgt automatisk i enkeltklikstilstand Filen "%s" indeholder ingen data Følgende TYPEr understøttes af kommandoerne --launch og --query:

  WebBrowser       - Den foretrukne webbrowser.
  MailReader       - Den foretrukne postlæser.
  FileManager      - Den foretrukne filhåndtering.
  TerminalEmulator - Den foretrukne terminalemulator. Følgende TYPEr understøttes af kommandoen --launch: Ikonet der skal gengives. Layouttilstand Modellen der bruges til ikonvisning Orienteringen af ikonlinjen Den foretrukne filhåndtering bruges til at gennemse mappernes indhold. Den foretrukne postlæser bruges til at skrive e-mails, når du klikker på e-mailadresser. Den foretrukne terminalemulator bruges til at køre kommandoer som kræver et CLI-miljø. Den foretrukne webbrowser bruges til at åbne hyperlinks og vise hjælp-indhold. Markeringstilstanden Størrelsen af ikonet der skal gengives, i pixels. Bredden som bliver brugt for hvert element Thunar Værktøjslinje_stil Skriv "%s --help" for anvendelse. Typen af skrivebordsfil som skal oprettes (program eller henvisning) Kunne ikke genkende URI-skemaet for "%s". Ukategoriserede ikoner Ikke understøttet type af skrivebordsfiltype "%s" Anvendelse: %s [tilvalg] [fil]
 Anvendelse: exo-open [URL'er ...] Brug _opstartsunderretning Brug et brugerdefineret program som ikke er i ovenstående liste. Brug kommandolinjen Med "Vis" kan brugeren søge i kolonner interaktivt Visningsrækkefølge kan ændres Vimprobable2 W3M Tekstbrowser Webbrowser Om alle underelementer skal have samme størrelse Om elementerne i visningen kan aktiveres med enkeltklik Bredde for hvert element Vinduesgruppe Vinduesgruppeleder Arbejds_mappe: X Terminal Xfce Terminal Xfe Filhåndtering [FIL|MAPPE] _Tilføj ny værktøjslinje _Annuller _Luk Skrivebor_dsstandard _Hjælp _Ikon: Kun _ikoner _Internet _Navn: _OK _Åbn _Andet ... Fje_rn værktøjslinje _Gem _Søg efter ikon: Kun _tekst _URL: _Redskaber aterm qtFM størrelse 