��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  {  1?  9   �@  .   �@  4   A  ,   KA  �   xA  1   ZB  N   �B  .   �B  8   
C  =   CC  �   �C  I   D  J   cD  9   �D  8   �D  �   !E  �   �E  �   �F     �G     �G     �G  "   �G     �G     H     'H     9H     JH     VH     vH     �H     �H     �H      �H  @   �H     I     !I     /I     6I     SI  !   lI  (   �I  "   �I  %   �I      J     J     8J     AJ     YJ     rJ     �J  	   �J  y   �J     $K     5K     EK     _K  ]   oK     �K  &   �K     L  $   2L     WL      kL     �L     �L  &   �L  �   �L     VM     hM     yM     �M  	   �M     �M  	   �M      �M     �M  	   �M     �M     N     N  3   ,N  -   `N  0   �N  )   �N  ?   �N  )   )O  c   SO     �O     �O     �O     P  !    P  )   BP     lP     �P  2   �P  ,   �P  /   Q  (   2Q  -   [Q     �Q     �Q  A   �Q      R     R     R     R     ,R     AR     GR  
   UR  P   `R     �R     �R  	   �R     �R     �R  #   �R     S  �  !S     �T  !   �T     �T     U     U  �   $U  s   �U     QV     fV     V     �V     �V     �V     �V     �V     �V  4   �V  W   #W  .   {W  &   �W  I   �W     X     :X     LX     \X     vX     �X     �X     �X     �X     �X     �X     �X  '   Y  #   /Y     SY     aY     tY  _   �Y     �Y  	   Z     Z     Z  "   ;Z     ^Z     nZ  Y   |Z  S   �Z  V   *[  O   �[  '   �[     �[  P   \  &   a\  )   �\  [   �\  V   ]  T   e]  I   �]     ^  $   ^     8^  	   J^  K   T^     �^  	   �^     �^     �^  .   �^     �^     _      _     ?_     U_     j_     z_  	   �_     �_     �_     �_  #   �_     �_     `     `  4   2`  �   g`  I   Ha     �a  	   �a     �a  (   �a     �a     b     b     -b  @   4b  1   ub  %   �b  #   �b     �b  `   �b  Z   _c  ]   �c  V   d     od     �d     �d     �d     �d     �d  
   �d     �d     �d     e     e  7   5e  5   me  z   �e  !   f    @f  8   ]g     �g     �g  )   �g  "   �g  U   h  j   gh  q   �h  X   Di     �i  +   �i     �i     �i     j  (   #j  d   Lj  2   �j     �j  5   �j     0k     Nk  &   fk  <   �k     �k  J   �k     -l     Ll     Yl     pl  -   ~l  D   �l     �l     m     m     4m  
   Mm     Xm     im     �m  (   �m  	   �m     �m     �m     �m     �m     n  	   n     n     #n     'n  	   .n     8n     Vn     ^n     sn     �n     �n     �n     �n     �n                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-07-27 13:28+0000
Last-Translator: Michael Martins <michaelfm21@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/xfce/exo/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
        %s [opções] --build-list [[nome do arquivo]...]
        exo-open --launch TIPO [PARÂMETROs...]   --build-list      Analisa pares (nomes, arquivos)
   --extern          Gera símbolos externos
   --launch TIPO [PARÂMETROs...]       Executar o aplicativo preferido do
                                      TIPO com PARÂMETROs opcionais, onde
                                      TIPO é um dos valores listados abaixo   --name=Nome identificador de variável/macro C
   --output=nome_do_arquivo Grava o csource gerado para o arquivo especificado
   --static          Gera símbolos estáticos
   --strip-comments  Remove comentários de arquivos XML
   --strip-content   Remove conteúdo de nós de arquivos XML
   --working-directory DIRETÓRIO       Diretório de trabalho padrão para
                                      aplicativos ao usar a opção --launch   -?, --help                          Mostra esta mensagem de ajuda e sai   -V, --version                       Mostra informação de versão e sai   -V, --version     Mostra informação de versão e sai
   -h, --help        Mostra esta mensagem de ajuda e sai
   WebBrowser       - O navegador web preferido.
  MailReader       - O cliente de e-mail preferido.
  FileManager      - O gerenciador de arquivos preferido.
  TerminalEmulator - O emulador de terminal preferido. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. Todos os direitos reservados.

Escrito por Benedikt Meurer <benny@xfce.org>.

Compilado com Gtk+-%d.%d.%d em ambiente
de execução Gtk+-%d.%d.%d.

Por favor relatar erros para <%s>.
 %s vem com ABSOLUTAMENTE NENHUMA GARANTIA,
Você pode redistribuir cópias de %s sob os termos
da Licença Pública Geral Menor GNU que pode ser
encontrada no pacote fonte %s.

 Ícones de ação Atual Cor da borda do item atual Cor de preenchimento do item atual Índice do item atual Cor do texto do item atual Todos os arquivos Todos os ícones Animações Botão de escolha de aplicativo Ícones de aplicativos Balsa Dispositivo de bloco Brave Navegar pelo sistema de arquivos Navegue pelo sistema de arquivos para escolher um outro comando. Navegar pela web C_omentário: C_riar Gerenciador de arquivos Caja Dispositivo de caractere Escolha os aplicativos preferidos Escolha um outro gerenciador de arquivos Escolha um outro cliente de e-mail Escolha um outro emulador de terminal Escolha um outro navegador web Escolher nome do arquivo Chromium Cliente de e-mail Claws Limpar campo de pesquisa Espaçamento de coluna Espaçamento de coluna Com_ando: Copyright (c) %s
        os-cillation e.K. Todos os direitos reservados.

Escrito por Benedikt Meurer <benny@xfce.org>.

 Criar diretório Criar lançador Criar lançador <b>%s</b> Criar ligação Criar um novo arquivo de área de trabalho no
                           diretório fornecido Cor da borda do item do cursor Cor de preenchimento do item do cursor Cor do texto do item do cursor Personalizar barra de ferramentas... Navegador do Debian Emulador de terminal X do Debian Ícones de dispositivos Dillo Não mostrar esta mensagem _novamente. Arraste um item sobre as barras de ferramentas para adicioná-lo e das barras de ferramentas para a tabela de itens para removê-lo. Editar diretório Editar lançador Editar ligação Emblemas Emoticons Habilitar pesquisa Encompass Emulador de terminal Enlightened Navegador web Epiphany Evolution Arquivos executáveis FIFO Falha ao criar "%s". Falha ao executar o gerenciador de arquivos padrão Falha ao executar o cliente de e-mail padrão Falha ao executar o emulador de terminal padrão Falha ao executar o navegador web padrão Falha ao executar o aplicativo preferido para a categoria "%s". Falha ao carregar o conteúdo de "%s": %s Falha ao carregar a imagem "%s": razão desconhecida, provavelmente um arquivo de imagem corrompido Falha ao abrir "%s". Falha ao abrir %s para escrita Falha ao abrir o URI "%s". Falha ao abrir exibição Falha ao abrir o arquivo "%s": %s Falha ao analisar o conteúdo de "%s": %s Falha ao ler o arquivo "%s": %s Falha ao salvar "%s". Falha ao definir o gerenciador de arquivos padrão Falha ao definir o cliente de e-mail padrão Falha ao definir o emulador de terminal padrão Falha ao definir o navegador web padrão O arquivo "%s" não tem a chave "Type" (tipo) Gerenciador de arquivos Ícones de tipos de arquivos A localização do arquivo não é um diretório ou arquivo comum Pasta Seguir estado GIcon Terminal do GNOME Navegador web Galeon Geary Google Chrome Homogêneo Como o texto e o ícone de cada item são posicionados com relação um ao outro Icecat Icedove Iceweasel Ícone Modelo de barra de ícones Modelo de visualização de ícones Coluna de ícone Se você não especificar a opção --launch, exo-open irá abrir todas os URLs
especificados com seus tratadores de URL preferidos. Caso contrário, se você
especificar a opção --launch, você pode selecionar qual aplicativo preferido
você deseja executar e passar parâmetros adicionais para o aplicativo (isto é,
para TerminalEmulator você pode passar a linha de comando que deve ser
executada dentro do terminal). Arquivos de imagem Assistente de tipo "%s" inválido Jumanji KMail Navegador web Konqueror Iniciar o assistente padrão de TIPO com
                                    o PARÂMETRO opcional, onde TIPO é um dos
                                    valores listados mais abaixo Não há suporte para lançamento de arquivos de área de trabalho quando %s é compilado sem os recursos GIO-Unix. Modo de disposição Navegador de texto Links Ícones de locais Navegador de texto Lynx Cliente de e-mail Margem Coluna de marcação Ícones de menu Midori Coluna modelo a pesquisar quando procurando por item Coluna modelo usada para obter o caminho absoluto de um arquivo de imagem a ser exibida Coluna modelo usada para obter o ícone pixbuf Coluna modelo usada para obter o texto Coluna modelo usada para obter o texto se estiver usando marcação Pango Modelo para a barra de ícones Navegador Mozilla Mozilla Firefox Cliente de e-mail Mozilla Mozilla Thunderbird Mutt NXTerm Nautilus Navegador Netscape Nenhum aplicativo selecionado Nenhum comando especificado Nenhum arquivo selecionado Nenhum(a) arquivo/pasta especificado(a) Nenhum ajudante definido para "%s". Nenhum ícone Número de colunas Número de colunas para mostrar Abre o diálogo de configuração
                                    de aplicativos preferidos Navegador Opera Opções: Orientação Gerenciador de arquivos PCManFM Gerenciador de arquivos PCManFM-Qt Scripts em Perl Coluna pixbuf Por favor escolha o seu gerenciador de arquivos
preferido e clique em OK para prosseguir. Por favor escolha o seu cliente de e-mail
preferido e clique em OK para prosseguir. Por favor escolha o seu emulador de terminal
preferido e clique em OK para prosseguir. Por favor escolha o seu navegador web
preferido e clique em OK para prosseguir. Por favor, relate problemas para <%s>.
 Aplicativos preferidos Aplicativos preferidos (navegador web, cliente de e-mail e emulador de terminal) URL predefinido ao criar uma ligação Comando predefinido ao criar um lançador Comentário predefinido ao criar um arquivo de
                           área de trabalho Ícone predefinido ao criar um arquivo de área de
                           trabalho Nome predefinido ao criar um arquivo de área de
                           trabalho Pressione o botão esquerdo do mouse para mudar o aplicativo selecionado. Visualização Mostra informação de versão e sai Scripts em Python QTerminal Consulte o ajudante padrão de TIPO, onde TIPO é um dos seguintes valores. QupZilla ROX-Filer RXVT Unicode Ler seu e-mail Renderizar de acordo com o estado selecionado. Reordenável Comando de reinício Gerenciador de arquivos Rodent Espaçamento de filas Espaçamento de fila Scripts em Ruby Executar no _terminal ID SOCKET Sakura Coluna de pesquisa Selecionar í_cone de: Selecione um diretório de trabalho Selecione um aplicativo Selecione um ícone Selecione o aplicativo Selecione aplicativos padrão para vários serviços Selecione esta opção para habilitar a notificação de inicialização quando o comando for executado a partir do gerenciador de arquivos ou do menu. Nem todos os aplicativos tem suporte a notificação de inicialização. Selecione esta opção para executar o comando em uma janela de terminal. Modo de seleção Separador Comando de reinício da sessão Socket do gerenciador de configurações Scripts em Shell Clique único Tempo de clique único Socket Espaço que é inserido nos limites da visualização de ícones Espaço que é inserido entre células de um item Espaço que é inserido entre colunas Espaço que é inserido entre filas Espaçamento Especifique o aplicativo que você deseja usar
como gerenciador de arquivos padrão para o Xfce: Especifique o aplicativo que você deseja usar
como cliente de e-mail padrão para o Xfce: Especifique o aplicativo que você deseja usar
como emulador de terminal padrão para o Xfce: Especifique o aplicativo que você deseja usar
como navegador web padrão para o Xfce: Ícones de status Ícones padrões Surf Sylpheed TIPO [PARÂMETRO] Emulador de terminal Terminator Coluna de texto Texto para ícones i_mportantes Texto p_ara todos os ícones O GIcon a renderizar. A quantidade de espaço entre duas colunas consecutivas A quantidade de espaço entre duas filas consecutivas A quantidade de tempo após a qual o item sob o cursor do mouse será selecionado automaticamente no modo de clique único O arquivo "%s" não contém dados Os TIPOS a seguir são suportados para os comandos --launch e --query:

  WebBrowser       - O navegador web preferido.
  MailReader       - O cliente de e-mail preferido.
  FileManager      - O gerenciador de arquivos preferido.
  TerminalEmulator - O emulador de terminal preferido. Os seguintes TIPOs têm suporte para o comando --launch: O ícone a renderizar. O modo de disposição O modelo para a visualização de ícones A orientação da barra de ícones O gerenciador de arquivos preferido será usado para navegar no conteúdo das pastas. O cliente de e-mail preferido será usado para compor e-mails quando você clicar num endereço de e-mail. O emulador de terminal preferido será usado para executar comandos que requerem um ambiente de linha de comando. O navegador web preferido será usado para abrir hyperlinks e exibir conteúdo de ajuda. O modo de seleção O tamanho do ícone a renderizar em pixels. A largura usada para cada item Thunar E_stilo de barra de ferramentas Digite "%s --help" para opções de uso. Tipo de arquivo de área de trabalho para criar
                           (aplicativo ou ligação) Não foi possível detectar o esquema URI de "%s". Ícones sem categoria Tipo de arquivo de área de trabalho "%s" sem suporte Uso: %s [opções] [arquivo]
 Uso: exo-open [URLs...] Usar _notificação de inicialização Usar um aplicativo que não esteja incluído na lista acima. Usar a linha de comando Visualização permite ao usuário pesquisar pelas colunas interativamente Visualização é reordenável Vimprobable2 Navegador de texto W3M Navegador web Se os filhos devem ser todos do mesmo tamanho Se os itens da visualização podem ser ativados com cliques únicos Largura para cada item Grupo de janelas Líder do grupo de janelas _Diretório de trabalho: Terminal X Terminal do Xfce Gerenciador de arquivos do Xfce [ARQUIVO|PASTA] _Adicionar uma nova barra de ferramentas _Cancelar _Fechar Padrão de área _de trabalho Aj_uda Í_cone: Apenas í_cones _Internet _Nome: _OK _Abrir _Outro... _Remover barra de ferramentas _Salvar Ícone de pesqui_sa: Apenas _texto _URL: _Utilitários aterm qtFM tamanho 