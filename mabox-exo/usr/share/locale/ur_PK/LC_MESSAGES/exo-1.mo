��    �      |  =  �      �  2   �  -     -   2  ,   `  �   �  *   q  ,   �  3   �  8   �  �   6  F   �  H     7   ]  5   �  �   �  �   �     N     [     b     {     �     �  	   �  	   �  
   �     �     �            2     	   M     W     _     p     �  !   �     �     �  
   �            	      o   *     �     �     �  0   �               1     H     ]     u     �  b   �        	          	         *  	   8     B     `  	   u          �     �  %   �  +   �  %   �  9   $  H   ^     �     �     �     �          +  !   @  '   b  !   �     �     �     �     �     �     �     �  H         T      Y      h   h  x      �!     �!     "     "  i   ""     �"     �"     �"     �"     �"     �"     �"  
   �"     �"  :   �"  2   :#  +   m#  <   �#     �#     �#     �#     $     $     .$     3$     :$     M$     e$     z$     �$     �$     �$     �$  4   �$     %     %     '%     3%     @%  E   N%  K   �%  E   �%     &&     C&  G   Z&     �&  '   �&  +   �&  (   '  (   ?'  ;   h'     �'  "   �'     �'     �'  0   �'     (     ((     8(     D(     P(     ](  	   n(     x(     �(     �(     �(     �(  0   �(  �   )  ;   �)     �)  	   �)     �)     *     )*     7*     D*     Y*  5   `*  0   �*  +   �*  )   �*     +  H   %+  N   n+  H   �+     ,     ,     ,     -,     ?,     K,     e,  3   y,  0   �,  r   �,  ;   Q-     �-     �-     �-     �-     �-  )   �-     ).     F.     M.     \.  4   x.     �.  "   �.     �.      /     /  A   4/  8   v/     �/     �/     �/  0   �/  A   0     S0     g0     t0  
   �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     1  	   1     1  
   "1     -1     31     91  n  >1  I   �2  9   �2  M   13  @   3  �   �3  <   �4  <   �4  L   ,5  O   y5  �   �5  i   �6  d   7  W   f7  V   �7  @  8  �   V9     8:     J:  (   S:  *   |:     �:  $   �:     �:     �:     ;  '    ;     H;  
   `;     k;  X   �;     �;     �;     �;  -   <  .   9<  ;   h<  2   �<  (   �<      =     =     "=     2=  �   ?=     �=  #   �=     >  R   ">  (   u>  *   �>  $   �>  &   �>  $   ?  &   :?     a?  �   u?     c@     �@     �@     �@     �@     �@  0   �@      A     7A     OA     iA  (   nA  E   �A  O   �A  >   -B  S   lB  r   �B  &   3C  ?   ZC  )   �C  2   �C  0   �C  1   (D  C   ZD  O   �D  @   �D     /E     CE  
   \E     gE     yE      �E     �E  j   �E     -F     6F     OF  N  jF     �H  2   �H      I     I  �   +I     �I     �I     J      J     9J  
   IJ  !   TJ     vJ     �J  F   �J  h   �J  Z   GK  �   �K  $   3L     XL     rL     �L     �L     �L     �L      �L  9   �L  3   4M  %   hM  0   �M     �M     �M  7   �M  Z   $N     N     �N     �N     �N     �N  V   �N  o   1O  �   �O  $   &P     KP  k   eP  A   �P  I   Q  U   ]Q  X   �Q  Q   R  n   ^R     �R  D   �R     S     7S  g   KS     �S     �S     �S     �S     T     T     ;T     PT     bT  "   }T     �T  "   �T  P   �T    1U  Z   7V     �V     �V  #   �V  "   �V     �V     W  (   W     CW  K   LW  K   �W  B   �W  B   'X     jX  p   qX  U   �X  a   8Y     �Y     �Y     �Y     �Y     �Y     �Y  $   Z  C   7Z  C   {Z  �   �Z  h   l[     �[     �[  &   \  #   ,\     P\  <   d\  C   �\     �\  "   �\  9   ]  X   K]     �]  -   �]  -   �]  "   ^  0   :^  g   k^  o   �^  *   C_     n_     �_  F   �_  G   �_     )`     I`     [`     v`     �`     �`  +   �`     �`     �`     �`     a  
   *a     5a     Ea     Na     \a  "   ia     �a     �a     �a     �a     �   W   �       �   ?      �   .   �   }   �   <       �   �   �       H              g   )   L   +                   �       �       /   (   �   r   i   �       �   �   �   Q      �                          &      b          8       O   �   �   �           4   C   �   �   �   #   �   �   �           �   �   �   K   "   @   a   �   �       ;      	       �   �           �       n   �       �   >   Z   �   �   l   M       �           6       �      �       R   q          U       V   0      u   !           |   9   �           5   �   Y   x   �   �   h           �          2   y   
   c   E          p           B   %   7           �   �   �              ^       ]   _   �          {   �           t       T          F   �       e       �   �      �   \              X   �   �   v   �   �   k   ~   �       �   �   �       =   �               �   `   �   A            �   w              J   f   �   3       �   �   s       �   �   �   j   �   �          �      I   �   �   �   �               �   �   S   [   -   �   �      m   �   G   D       �   o   �          ,   �       �   �           �      �       �   �   '      :   �   �   �   �   �   N   $   d   �   �   1   z      �   �      �   �   �   *       �   P       �       �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
 %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Browse the file system to choose a custom command. C_omment: C_reate Character Device Choose Preferred Application Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Claws Mail Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open display Failed to open file "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File Manager File Type Icons Folder Follow state GNOME Terminal Galeon Web Browser Homogeneous How the text and icon of each item are positioned relative to each other Icon Icon Bar Model Icon View Model If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Netscape Navigator No application selected No command specified No file selected No file/folder specified No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation Perl Scripts Pixbuf column Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts RXVT Unicode Render differently based on the selection state. Reorderable Restart command Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Search Column Select _icon from: Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Sylpheed TYPE [PARAMETER] Terminal Emulator Text column Text for I_mportant Icons Text for _All Icons The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. View allows user to search through columns interactively View is reorderable W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader X Terminal Xfce Terminal [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Name: _Open _Other... _Remove Toolbar _Text only _URL: aterm size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-14 00:27+0000
Last-Translator: Xfce Bot <transifex@xfce.org>
Language-Team: Urdu (Pakistan) (http://www.transifex.com/xfce/exo/language/ur_PK/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ur_PK
Plural-Forms: nplurals=2; plural=(n != 1);
        %s [اختیارات] --بلڈ-لسٹ [[فائل کا نام]...]
        exo-کھولیں --لاؤنچ TYPE [PARAMETERs...]   --build-list      حرفی تجزی (نام, فائل) حرفی تجزیہ
   --خارجی          خارجی رموز جنریٹ کریں
   --launch TYPE [PARAMETERs...]       مجوزہ اطلاقیہ چلائیں برائے
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C ماکرو/ویریئبل کا نام
   --ساکن          ساکن رموز جنریٹ کریں
   --strip-comments  تبصرے XML فائلوں سے حذف کرتے ہیں
   --strip-content   XML فائلوں سے node مواد حذف کرتے ہیں
   --working-directory DIRECTORY       اطلاقیوں کے لیے طے شدہ ورکنگ ڈائریکٹری
                                      جب استعمال کیا جائے --لاؤنچ آپشن.   -?, --ہدایات                          ہدایت پیغام طبع کرکے برخاست کریں   -v, --ورژن                       ورژن معلومات طبع کرکے برخاست کریں   -V, --ورژن    ورژن کی معلومات پرنٹ کرکے اخراج کریں
   -h, --ہدایت        ہدایت پیغام طبع کرکے برخاست کریں
 %s (ایکسفس %s)

کاپی رائٹ (c) 2003-2006
        os-cillation e.K. تمام حقوق محفوظ ہیں.

تحریر کردہ از Benedikt Meurer <benny@xfce.org>.

تیار کردہ بمع Gtk+-%d.%d.%d, چل رہا ہے Gtk+-%d.%d.%d.

براہ مہربانی اغلاط کی اطلاع <%s> کو دیں.
 %s مکمل طور پر بغیر کسی ضمانت کے پیش کیا جاتا ہے
آپ %s کی کاپیاں زیرِ شرائط برائے
GNU جنرل پبلک لائسنس جو
%s کے مصدر پیکج موجود ہے.

 آئکن حرکت فعال فعال عنصر کا بارڈر رنگ فعال عنصر کا بھرائی رنگ فعال عنصر فہرست فعال عنصر کا متن رنگ تمام فائلیں تمام آئکن متحرکات اطلاقیہ منتخب کار بٹن اطلاقیہ آئکن بالسا بلاک ڈیوائس مختص کمانڈ کے انتخاب کے لیے فائل سسٹم براؤز کریں. تبص_رہ: ب_نائیں رموز ڈیوائس مجوزہ اطلاقیے منتخب کریں مخصوص میل ریڈر منتخب کریں مخصوص ٹرمنل  ایمولیٹر منتخب کریں مخصوص ویب براؤزر منتخب کریں فائل کا نام متعین کریں کلاؤز میل کالم خلا کالم خلا کم_انڈ: کاپی رائٹ (c) %s
        os-cillation e.K. تمام حقوق محفوظ ہیں

لکھا گیا از Benedikt Meurer <benny@xfce.org>.

 لاؤنچر بنائیں <b>%s</b> لاؤنچر بنائیں ربط بنائیں دی گئی ڈائریکٹری میں نئی ڈیسک ٹاپ فائل بنائیں کرسر عنصر کا بارڈر رنگ کرسر عنصر کا بھرائی رنگ کرسر عنصر کا متن رنگ اوزار پٹی مختص کریں... ڈیبین سینسبل براؤزر ڈبین X ٹرمنل ایمولیٹر ڈوائس آئکن عنصر کو شامل کرنے کے لیے اوپر اوزار کی پٹی میں اسے ڈریگ کرکے چھوڑیں، ختم کرنے کے لیے اوزار کی پٹی سے ڈریگ کرکے عناصر ٹیبل میں چھوڑیں. لاؤنچر مدون کریں ربط مدون کریں علامات اموٹیکنز تلاش فعال کریں اینکمپاس انلائیٹینڈ ٹرمنل ایمولیٹر اپپفنی ویب براؤزر نوول ایولوشن اطلاقی فائلیں FIFO "%s" کو بنانے میں ناکامی طے شدہ ڈاک مطالعہ گاہ چلانے میں ناکامی طے شدہ ٹرمنل ایمولیٹر متعین کرنے میں ناکامی طے شدہ ویب براؤسر چلانے میں ناکامی زمرہ "%s" کے متعلقہ اطلاقیہ کو چلانے میں ناکامی. تصویر "%s" لوڈ نہیں ہوسکی: نامعلوم وجہ، تصویر فائل خراب ہوسکتی ہے  "%s" کھولنے میں ناکامی لکھنے کے لیے %s کو کھولنے میں ناکامی منظر کھولنے میں ناکامی فائل کھولنے میں ناکامی "%s": %s فائل پڑھنے میں ناکامی "%s": %s "%s" کو محفوظ کرنے میں ناکامی طے شدہ میل ریڈر متعین کرنے میں ناکامی طے شدہ ٹرمنل ایمولیٹر متعین کرنے میں ناکامی طے شدہ براؤزر متعین کرنے میں ناکامی فائل منیجر فائل قسم آئکن فولڈر متبع حالت گنوم ٹرمنل گیلیئن ویب براؤزر ہومو جینئس کس طرح ہر عنصر کی آئکن اور متن ایک دوسرے سے متعلق جگہ لیں گے آئکن آئکن پٹی ماڈل آئکن منظر ماڈل اگر آپ نے --launch option متعین نہیں کیا تو exo-open تمام روابط کو ان
ان کے مجوزہ روابط ہینڈلر میں کھول دے گا ما سوائے اگر آپ نے لاؤنچ آپشن
متعین کیا تو آپ اپنے مجوزہ اطلاقیوں کو چلانے کے لیے منتخب کرسکتے ہیں
اور اطلاقیہ کے اضافی پرامیٹرز کو پاس کر سکتے ہیں جیسے (i.e. برائے TerminalEmulator
آپ کمانڈ لائن کو پاس کرسکتے ہیں جنہیں ٹرمنل میں چلنا چاہیے.) تصویر فائل غیرموزوں معاون کار نوعیت "%s" کے میل کنکرر ویب براؤزر نوعیت TYPE کا طے شدہ معاون کار بمع اختیاری PARAMETER کے ساتھ چلائیں، جہاں TYPE ذیل کی قدروں میں سے ایک قدر ہے خاکہ طرز روابط متن براؤزر مقام آئکن Lynx متن براؤزر میل ریڈر مارجن کالم نشان زدہ کریں مینیو آئکن میڈوری عنصر میں سے تلاشنے کے لیے نمونہ کا کالم نمونہ کالم pixbuf آئکن کو حاصل کرنے کے لیے استعمال ہوتا ہے از نمونہ کالم متن حاصل کرنے کے لیے استعمال ہوتا ہے از اگر Pango markup استعمال کیا جائے تو نمونہ کالم متن کے حصول کے لیے استعمال کیا جاتا ہے آئکن پٹی کے لیے ماڈل موزیلا براؤزر موزیلا فائر فاکس موزیلا میل موزیلا تھنڈربرڈ مُٹ نیکسٹرم نیٹ سکیپ نیویگیٹر کوئی اطلاقیہ منتخب نہیں کیا گیا کوئی کمانڈ متعین نہیں کی گئی کوئی فائل منتخب نہیں کوئی فائل/فولڈر متعین نہیں بلا آئکن کالموں کی تعداد ظاہر ہونے والے کالموں کی تعداد ترجیح شدہ اطلاقیوں کو تشکیل دینے کا مکالمہ کھولیں اوپرا براؤزر اختیارات: جہتیابی پرل سکرپٹ Pixbuf کالم اپنا مجوزہ ای میل ریڈر منتخب کریں
اور آگے بڑھیں. اپنا مجوزہ ٹرمنل ایمولیٹر منتخب
کریں اور ٹھیک ہے سے آگے بڑھیں اپنا مجوزہ ویب براؤزر منتخب کریں
اور آگے بڑھنے کے لیے ٹھیک ہے پر کلک کریں. بگ رپورٹ کریں تا <%s>.
 مجوزہ اطلاقیے مجوزہ اطلاقیے (ویب براؤزر، ای میل ریڈر، اور ٹرمنل ایمولیٹر) جب ربط بنایا جائے پہلے URL کو سیٹ کریں جب لاؤنچر بنايا جائے پہلے کمانڈ سیٹ کریں جب ڈیسک ٹاپ فائل بنائی جائے پہلے کمانڈ سیٹ کریں جب ڈیسک ٹاپ فائل بنائی جائے پہلے آئکن کو سیٹ کریں جب ڈیسک ٹاپ فائل بنائی جائے پہلے نام سیٹ کریں منتخب کردہ اطلاقیوں کو بدلنے کے لیے ماؤس کا بایاں بٹن دبائیں. معائنہ ورژن معلومات طبع کریں اور برخاست کریں پائتھن سکرپٹ RXVT یونیکوڈ انتخابی حالت کو مد نظر رکھتے ہوئے مختلف طور پر رینڈر کریں پھر قابلِ ترتیب ری سٹارٹ کمانڈ سطر خلا سطر خلا روبی سکرپٹ ٹ_رمنل میں چلائیں ساکٹ آئی ڈی تلاش کالم م_نتخب آئکن از: اطلاقیہ منتخب کریں آئکن منتخب کریں اطلاقیہ منتخب کریں مختلف خدمات کے لیے طے شدہ اطلاقیے منتخب کریں سٹارٹ اپ انتباہ کے لیے یہ آپشن منتخب کریں جب کمانڈ فائل منیجر یا مینیو سے چلائی جائے. نوٹ کریں کہ ہر اطلاقیہ سٹارٹ اپ انتباہ کی معاونت نہیں رکھتا. کمانڈ ٹرمنل ونڈو میں چلانے کے لیے آپشن منتخب کریں. انتخاب طرز جداگار نشست ری سٹارٹ کمانڈ ترتیبات منیجر ساکٹ شیل سکرپٹ ایک کلک ایک کلک کا انتہائی وقت ساکٹ خلا جو آئکن منظر کے کناروں پر ڈالا جائے گا خلا جو عنصر کے خلیے کے درمیان ڈالا جائے گا خلا جو grid کالم کے درمیان ڈالا جائے گا خلا جو grid سطور کے درمیان ڈالا جائے گا خلا استعمال کے لیے اطلاقیہ منتخب کریں
بطور طے شدہ ایکسفس میل ریڈر: ایکسفس کے لیے طے شدہ
ٹرمنل ایمولیٹر منتخب کریں: چلانے کے لیے اطلاقیہ متعین کریں
بطور Xfce طے شدہ براؤزر: حالت آئکن سیلفیڈ TYPE [PARAMETER] ٹرمنل ایمولیٹر متن کالم خاص آئکن کا متن متن برائے ت_مام آئکن دو متواتر کالم کے درمیان خلا کی مقدار دو متواتر سطور کے درمیان خلا کی مقدرا ایک کلک طرز کا وہ انتہائی وقت جس میں کسی عنصر پر ماؤس کرسر لے جانے پر وہ خود بخود منتخب ہوجائے گا مندرجہ ذیل اقسام TYPEs معاونت رکھتے ہیں --لاؤنچ کمانڈ کے لیے: آئکن برائے رینڈر. خاکہ طرز آئکن منظر کے لیے ماڈل آئکن پٹی کی جہتیابی انتخاب طرز پکسل میں رینڈر کے لیے آئکن کا حجم. چوڑائی جو ہر عنصر کے لیے استعمال ہوگی تھنر اوزار پٹی کا ان_داز استعمال کے لئے ٹائپ کریں '%s --help'. بنانے کے لیے ڈیسک ٹاپ فائل کی قسم (اطلاقیہ یا ربط) بے زمرہ آئکن فائل "%s" کی معاونت نہیں ہے استعمال: %s [اختیار] [فائل]
 استعمال: exo-open [URLs...] آغاز اط_لاعیہ استعمال کریں کوئی مختص اطلاقیہ استعمال کریں جو اوپر فہرست میں نہیں ہے. منظر صارف کو کالم کے درمیان فعال طور پر تلاش کی صلاحیت دیتا ہے منظر پھر قابلِ ترتیب ہے W3M متن براؤزر ویب براؤزر کب تمام چھوٹوں کو ایک حجم کا ہونا چاہیے ایک کلک طرز میں عناصر کس طرح فعال ہوں گے ہر عنصر کی چوڑائی ونڈو گروپ ونڈو گروپ لیڈر ایکس ٹرمنل اکسفس ٹرمنل [فائل|فولڈر] _نئی اوزار پٹی شامل کریں _مسترد _بند کریں _ڈیسک ٹاپ طے شدہ _ہدایات آئ_کن: صرف آئکن ن_ام: ک_ھولیں _دیگر... اوزار پ_ٹی حذف کریں صرف متن ربط: اٹرم حجم 