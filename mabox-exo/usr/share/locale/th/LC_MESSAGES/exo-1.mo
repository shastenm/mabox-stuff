��    -     �	  �  �      0  2   1  -   d  -   �  ,   �  �   �  *   �  >   �  ,   ;  3   h  8   �  �   �  F   l  H   �  7   �  5   4  �   j  �   1  �        �     �     �     �     �     
   	   !   	   +   
   5      @      [      m      s      �      �   2   �      �   	   �      �      �      !     !     1!     N!  !   j!     �!     �!     �!  
   �!     �!     �!     �!  	   �!  o   "     w"     �"     �"     �"  0   �"     �"     #     #     6#     K#     c#     ~#     �#     �#  b   �#     $     #$  	   1$     ;$  	   C$     M$  	   [$     e$     �$  	   �$     �$     �$     �$  &   �$  %   �$  +   %  %   H%  9   n%  %   �%  H   �%     &     ,&     J&     c&     z&  $   �&     �&     �&  "   �&  !   '  '   3'  !   ['     }'     �'     �'  0   �'     �'     �'     �'     �'     (     !(     '(     5(  H   A(     �(     �(  	   �(     �(     �(     �(     �(  h  �(     <*     H*     a*     i*     o*  i   �*  W   �*     G+     S+     f+     u+     �+     �+     �+  
   �+     �+  :   �+  J   �+  2   @,  +   s,  <   �,     �,     �,     -     -      -     4-     9-     @-     I-     \-     t-     �-     �-     �-     �-     �-     �-  4   .     ;.     I.     R.     ^.     q.     �.     �.  F   �.  E   �.  K   1/  E   }/     �/     �/  G   �/     ?0  '   _0  +   �0  (   �0  (   �0  ;   1     A1  "   I1     l1  	   {1  L   �1     �1  	   �1     �1     �1  0   2     32     ?2     O2     c2     o2     {2     �2  	   �2     �2     �2     �2     �2     �2     �2     3  0   3  �   O3  ;   �3     -4  	   <4     F4     ^4     v4     �4     �4     �4  5   �4  0   �4  +   5  )   @5     j5  I   r5  H   �5  N   6  H   T6     �6     �6     �6     �6     �6     �6  
   �6     �6     �6     7     ,7  3   A7  0   u7  r   �7     8    88  ;   I9     �9     �9     �9     �9  J   �9  [   /:  \   �:  T   �:     =;  )   P;     z;     �;     �;     �;  4   �;  (   �;     '<  "   ;<     ^<     z<     �<  A   �<     �<  8   =     >=     R=     _=     p=  0   |=  A   �=     �=     >     >     $>  
   8>     C>     Q>     b>     p>     �>     �>     �>     �>     �>     �>  	   �>     �>     �>     �>  	   �>     �>     �>     �>  
   ?     ?  
   ?     !?     '?     ,?  h  1?  S   �@  L   �@  S   ;A  F   �A  �  �A  v   �C  �   D  F   �D  h   �D  z   _E    �E  �   �F  w   |G  f   �G  {   [H  �  �H  d  sJ  W  �K  '   0M     XM  Z   kM  Q   �M  Q   N  Z   jN     �N  $   �N  '    O  0   (O  $   YO     ~O  -   �O     �O  *   �O  r   �O     VP     uP     �P  8   �P  0   �P  3   Q  Q   ?Q  H   �Q  `   �Q  K   ;R  $   �R     �R  
   �R  !   �R  <   �R  <   S     \S  }   qS  *   �S  <   T  F   WT  *   �T  �   �T  f   WU  ]   �U  f   V  B   �V  c   �V  `   *W  $   �W     �W  I   �W  $   X  *   %Y  <   PY  *   �Y  	   �Y  !   �Y  -   �Y  	   Z  N   Z  6   kZ  	   �Z  <   �Z     �Z  0   �Z  ~   [  u   �[  �   \  x   �\  �   ]  O   �]  �   ^  -   �^  I   �^  1   _  ?   O_  =   �_  L   �_  =   `  3   X`  o   �`  f   �`  ~   ca  i   �a  2   Lb  3   b  '   �b  r   �b     Nc  !   gc     �c  -   �c  4   �c     �c     �c  $   d  �   +d     �d     �d  	   �d     �d  0   �d  9   (e  $   be  s  �e     �h  ^   i     yi     �i  7   �i  F  �i  �   k  '   �k  T   �k  9   El  S   l  *   �l  !   �l  -    m     Nm     jm  �   qm  �   n  �   �n  l   Yo  �   �o  0   rp  >   �p     �p     �p     �p     q     q     q     (q  <   ;q  0   xq  3   �q  N   �q  c   ,r     �r  $   �r  H   �r  `   s  3   ~s     �s     �s  9   �s  >   t     Xt     st  �   �t  �   Yu  �   v  �   �v  O   �w  3   x  �   8x  t   y  �   �y  �   z  ~   �z  {   {  ~   �{  !   |  Q   /|     �|  	   �|  �   �|     w}  	   �}     �}  -   �}  l   �}  -   2~  9   `~  :   �~  0   �~  0        7  4   R  -   �     �  $   �  )   �  9   �  $   E�     j�  $   ��  i   ��  �  �  �   ͂  $   ��     ��  K   Ã  ]   �  $   m�     ��  H   ��     ��  i   �  �   z�  o   �  c   v�  B   چ  �   �  �   ه  �   ��  �   W�     �  *   ,�     W�     \�  0   e�  B   ��  
   ي  *   �  F   �  @   V�     ��  �   ��  ~   A�  �   ��  <   ��    �  W   ��  '   U�  '   }�  B   ��  3   �  �   �  �   ב  �   ��  �   ��  -   ��  d   ��  E   �     b�  C   i�  C   ��  �   �  Q   ��  6   ݖ  S   �  D   h�  )   ��  C   ח  �   �  -   ��  �   �  Z   q�     ̙  R   ٙ  -   ,�  �   Z�  �   �  E   ��  '   ��  ?   �  ,   \�  
   ��  ,   ��  7   ��  '   ��  L   !�     n�  
   ��  @   ��     Ν     �  (   ��  %    �     F�     U�     c�     q�  .   ��     ��      Ȟ  .   �     �  1   �     P�     V�     [�                 7   Z   H             �   �   �   "   �   �           )   �   s              	      q   n      �     �     =   5       �     �           �           �       �   (  [   �   �     K     d   @   �               -         �   �   �   �   �   �       w   �   R   '             k           u   v     �      �   z   �           Q       �   �   C   �         �   �           }   �      �   �       �   �   ,      �   *       �   �           �           U   �   �             t     O      �         ;   h      �   �   �       �         Y   *          X                  �   B           i   ]           �   m   3   �   �      V       �   �   '  �   �   #   D   �   �   l       �   �   N   9                        �       P   �   �   2     ^       4   E   �   1       �      �             
  �   e             F   �   �      �   j   S      T   �     �   A   b       $   f   {     0     I   �     .      �       �   M       �       �   :     r            �   c   `       (       �       +          >   ?   �   G   �   �   �   �           ~       |   �   <   �       #  %  W           %   �   �     �   �   )      8   �   �   &      �   �   �   �       "  �         _   �   a       	      �   y           �   !       �     �   �   �       g          �   �              x   �   $      �   �   �     \       +   �       �       �         /       &   �     �   �   L   o   �           J   �   �       
   ,         �   �   -  �       6               �          !  �   p          �   �   �   �   �           %s [options] --build-list [[name file]...]
        exo-open --launch TYPE [PARAMETERs...]   --build-list      Parse (name, file) pairs
   --extern          Generate extern symbols
   --launch TYPE [PARAMETERs...]       Launch the preferred application of
                                      TYPE with the optional PARAMETERs, where
                                      TYPE is one of the following values.   --name=identifier C macro/variable name
   --output=filename Write generated csource to specified file
   --static          Generate static symbols
   --strip-comments  Remove comments from XML files
   --strip-content   Remove node contents from XML files
   --working-directory DIRECTORY       Default working directory for applications
                                      when using the --launch option.   -?, --help                          Print this help message and exit   -V, --version                       Print version information and exit   -V, --version     Print version information and exit
   -h, --help        Print this help message and exit
   WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

Built with Gtk+-%d.%d.%d, running Gtk+-%d.%d.%d.

Please report bugs to <%s>.
 %s comes with ABSOLUTELY NO WARRANTY,
You may redistribute copies of %s under the terms of
the GNU Lesser General Public License which can be found in the
%s source package.

 Action Icons Active Active item border color Active item fill color Active item index Active item text color All Files All Icons Animations Application Chooser Button Application Icons Balsa Block Device Brave Browse the file system Browse the file system to choose a custom command. Browse the web C_omment: C_reate Caja File Manager Character Device Choose Preferred Application Choose a custom File Manager Choose a custom Mail Reader Choose a custom Terminal Emulator Choose a custom Web Browser Choose filename Chromium Claws Mail Clear search field Column Spacing Column spacing Comm_and: Copyright (c) %s
        os-cillation e.K. All rights reserved.

Written by Benedikt Meurer <benny@xfce.org>.

 Create Directory Create Launcher Create Launcher <b>%s</b> Create Link Create a new desktop file in the given directory Cursor item border color Cursor item fill color Cursor item text color Customize Toolbar... Debian Sensible Browser Debian X Terminal Emulator Device Icons Dillo Do _not show this message again Drag an item onto the toolbars above to add it, from the toolbars in the items table to remove it. Edit Directory Edit Launcher Edit Link Emblems Emoticons Enable Search Encompass Enlightened Terminal Emulator Epiphany Web Browser Evolution Executable Files FIFO Failed to create "%s". Failed to execute default File Manager Failed to execute default Mail Reader Failed to execute default Terminal Emulator Failed to execute default Web Browser Failed to launch preferred application for category "%s". Failed to load contents from "%s": %s Failed to load image "%s": Unknown reason, probably a corrupt image file Failed to open "%s". Failed to open %s for writing Failed to open URI "%s". Failed to open display Failed to open file "%s": %s Failed to parse contents of "%s": %s Failed to read file "%s": %s Failed to save "%s". Failed to set default File Manager Failed to set default Mail Reader Failed to set default Terminal Emulator Failed to set default Web Browser File "%s" has no type key File Manager File Type Icons File location is not a regular file or directory Folder Follow state GIcon GNOME Terminal Galeon Web Browser Geary Google Chrome Homogeneous How the text and icon of each item are positioned relative to each other Icecat Icedove Iceweasel Icon Icon Bar Model Icon View Model Icon column If you don't specify the --launch option, exo-open will open all specified
URLs with their preferred URL handlers. Else, if you specify the --launch
option, you can select which preferred application you want to run, and
pass additional parameters to the application (i.e. for TerminalEmulator
you can pass the command line that should be run in the terminal). Image Files Invalid helper type "%s" Jumanji KMail Konqueror Web Browser Launch the default helper of TYPE with the optional PARAMETER, where TYPE is one of the following values. Launching desktop files is not supported when %s is compiled without GIO-Unix features. Layout mode Links Text Browser Location Icons Lynx Text Browser Mail Reader Margin Markup column Menu Icons Midori Model column to search through when searching through item Model column used to retrieve the absolute path of an image file to render Model column used to retrieve the icon pixbuf from Model column used to retrieve the text from Model column used to retrieve the text if using Pango markup Model for the icon bar Mozilla Browser Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator No application selected No command specified No file selected No file/folder specified No helper defined for "%s". No icon Number of columns Number of columns to display Open the Preferred Applications
configuration dialog Opera Browser Options: Orientation PCMan File Manager PCManFM-Qt File Manager Perl Scripts Pixbuf column Please choose your preferred File Manager
now and click OK to proceed. Please choose your preferred Mail Reader
now and click OK to proceed. Please choose your preferred Terminal
Emulator now and click OK to proceed. Please choose your preferred Web
Browser now and click OK to proceed. Please report bugs to <%s>.
 Preferred Applications Preferred Applications (Web Browser, Mail Reader and Terminal Emulator) Preset URL when creating a link Preset command when creating a launcher Preset comment when creating a desktop file Preset icon when creating a desktop file Preset name when creating a desktop file Press left mouse button to change the selected application. Preview Print version information and exit Python Scripts QTerminal Query the default helper of TYPE, where TYPE is one of the following values. QupZilla ROX-Filer RXVT Unicode Read your email Render differently based on the selection state. Reorderable Restart command Rodent File Manager Row Spacing Row spacing Ruby Scripts Run in _terminal SOCKET ID Sakura Search Column Select _icon from: Select a working directory Select an Application Select an icon Select application Select default applications for various services Select this option to enable startup notification when the command is run from the file manager or the menu. Not every application supports startup notification. Select this option to run the command in a terminal window. Selection mode Separator Session restart command Settings manager socket Shell Scripts Single Click Single Click Timeout Socket Space which is inserted at the edges of the icon view Space which is inserted between cells of an item Space which is inserted between grid column Space which is inserted between grid rows Spacing Specify the application you want to use
as default File Manager for Xfce: Specify the application you want to use
as default Mail Reader for Xfce: Specify the application you want to use
as default Terminal Emulator for Xfce: Specify the application you want to use
as default Web Browser for Xfce: Status Icons Stock Icons Surf Sylpheed TYPE [PARAMETER] Terminal Emulator Terminator Text column Text for I_mportant Icons Text for _All Icons The GIcon to render. The amount of space between two consecutive columns The amount of space between two consecutive rows The amount of time after which the item under the mouse cursor will be selected automatically in single click mode The file "%s" contains no data The following TYPEs are supported for the --launch and --query commands:

  WebBrowser       - The preferred Web Browser.
  MailReader       - The preferred Mail Reader.
  FileManager      - The preferred File Manager.
  TerminalEmulator - The preferred Terminal Emulator. The following TYPEs are supported for the --launch command: The icon to render. The layout mode The model for the icon view The orientation of the iconbar The preferred File Manager will be used to browse the contents of folders. The preferred Mail Reader will be used to compose emails when you click on email addresses. The preferred Terminal Emulator will be used to run commands that require a CLI environment. The preferred Web Browser will be used to open hyperlinks and display help contents. The selection mode The size of the icon to render in pixels. The width used for each item Thunar Toolbar _Style Type '%s --help' for usage. Type of desktop file to create (Application or Link) Unable to detect the URI-scheme of "%s". Uncategorized Icons Unsupported desktop file type "%s" Usage: %s [options] [file]
 Usage: exo-open [URLs...] Use _startup notification Use a custom application which is not included in the above list. Use the command line View allows user to search through columns interactively View is reorderable Vimprobable2 W3M Text Browser Web Browser Whether the children should be all the same size Whether the items in the view can be activated with single clicks Width for each item Window group Window group leader Working _Directory: X Terminal Xfce Terminal Xfe File Manager [FILE|FOLDER] _Add a new toolbar _Cancel _Close _Desktop Default _Help _Icon: _Icons only _Internet _Name: _OK _Open _Other... _Remove Toolbar _Save _Search icon: _Text only _URL: _Utilities aterm qtFM size Project-Id-Version: Exo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-27 07:22+0000
Last-Translator: Theppitak Karoonboonyanan <theppitak@gmail.com>
Language-Team: Thai (http://www.transifex.com/xfce/exo/language/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
        %s [ตัวเลือก] --build-list [[ชื่อ แฟ้ม]...]
        exo-open --launch ชนิด [พารามิเตอร์...]   --build-list      แจงคู่ลำดับ (ชื่อ, แฟ้ม)
   --extern          สร้างสัญลักษณ์ extern
   --launch ชนิด [พารามิเตอร์...]         เรียกโปรแกรมหลักตาม "ชนิด" ที่กำหนด
                                      โดยอาจระบุ "พารามิเตอร์" ประกอบ
                                      โดย "ชนิด" เป็นค่าใดค่าหนึ่งที่อธิบายข้างท้าย   --name=ชื่อ         ชื่อของแมคโครหรือตัวแปรภาษาซี
   --output=filename เขียนซอร์สโค้ดภาษา C ที่สร้างลงในแฟ้มที่กำหนด
   --static          สร้างสัญลักษณ์ static
   --strip-comments  ตัดหมายเหตุออกจากแฟ้ม XML ต่างๆ
   --strip-content   ตัดเนื้อหาของโหนดออกจากแฟ้ม XML ต่างๆ
   --working-directory ไดเรกทอรี        ไดเรกทอรีทำงานโดยปริยายสำหรับโปรแกรมต่างๆ
                                      เมื่อใช้ตัวเลือก --launch   -?, --help                          แสดงข้อความวิธีใช้นี้และจบการทำงาน   -V, --version                       แสดงข้อมูลรุ่นและจบการทำงาน   -V, --version     แสดงข้อมูลรุ่นและจบการทำงาน
   -h, --help        แสดงข้อความวิธีใช้นี้และจบการทำงาน
   WebBrowser       - โปรแกรมหลักสำหรับท่องเว็บ
  MailReader       - โปรแกรมหลักสำหรับอ่านเมล
  FileManager      - โปรแกรมหลักสำหรับจัดการแฟ้ม
  TerminalEmulator - โปรแกรมหลักสำหรับจำลองเทอร์มินัล %s (Xfce %s)

Copyright (c) 2003-2006
        os-cillation e.K. All rights reserved.

เขียนโดย Benedikt Meurer <benny@xfce.org>.

ประกอบสร้างโดยใช้ Gtk+-%d.%d.%d, กำลังทำงานโดยใช้ Gtk+-%d.%d.%d

หากพบปัญหา กรุณารายงานที่ <%s>
 %s ไม่มีการรับประกันใดๆ
คุณสามารถแจกจ่ายสำเนาของ %s ต่อได้ ภายใต้เงื่อนไขของ
GNU Lesser General Public License ซึ่งสามารถพบได้ในแพกเกจซอร์สของ
%s

 ไอคอนการกระทำ แอคทีฟ สีเส้นขอบสำหรับรายการที่แอคทีฟ สีพื้นสำหรับรายการที่แอคทีฟ ดัชนีของรายการที่แอคทีฟอยู่ สีข้อความสำหรับรายการที่แอคทีฟ ทุกแฟ้ม ไอคอนทั้งหมด ภาพเคลื่อนไหว ปุ่มเลือกโปรแกรม ไอคอนโปรแกรม Balsa อุปกรณ์แบบบล็อค Brave ท่องดูระบบแฟ้ม ท่องดูระบบแฟ้มเพื่อเลือกกำหนดคำสั่งเอง ท่องดูเว็บ ห_มายเหตุ: _สร้าง โปรแกรมจัดการแฟ้ม Caja อุปกรณ์แบบอักขระ เลือกโปรแกรมหลักๆ เลือกกำหนดโปรแกรมจัดการแฟ้ม เลือกกำหนดโปรแกรมอ่านเมล เลือกกำหนดโปรแกรมจำลองเทอร์มินัล เลือกกำหนดโปรแกรมท่องเว็บ ตั้งชื่อแฟ้ม Chromium Claws Mail ล้างช่องค้น ช่องไฟระหว่างคอลัมน์ ช่องไฟระหว่างคอลัมน์ คำ_สั่ง: Copyright (c) %s
        os-cillation e.K. All rights reserved.

เขียนโดย Benedikt Meurer <benny@xfce.org>.

 สร้างไดเรกทอรี สร้างตัวเรียกโปรแกรม สร้างตัวเรียกโปรแกรม <b>%s</b> สร้างจุดเชื่อม สร้างแฟ้มเดสก์ท็อปรายการใหม่ในไดเรกทอรีที่กำหนด สีเส้นขอบสำหรับรายการที่เคอร์เซอร์ สีพื้นสำหรับรายการที่เคอร์เซอร์ สีข้อความสำหรับรายการที่เคอร์เซอร์ ปรับแต่งแถบเครื่องมือ... โปรแกรมท่องเว็บตามเกณฑ์ของเดเบียน โปรแกรมจำลองเทอร์มินัลของเดเบียน ไอคอนอุปกรณ์ Dillo ไ_ม่ต้องแสดงข้อความนี้อีก ลากรายการไปวางในแถบเครื่องมือเพื่อเพิ่มรายการ หรือลากจากแถบเครื่องมือมายังตารางรายการเพื่อลบรายการ แก้ไขไดเรกทอรี แก้ไขตัวเรียกโปรแกรม แก้ไขจุดเชื่อม ตรา ไอคอนอารมณ์ เปิดใช้การค้นหา Encompass โปรแกรมจำลองเทอร์มินัล Enlightened โปรแกรมท่องเว็บ Epiphany Evolution แฟ้มที่เรียกทำงานได้ FIFO สร้าง "%s" ไม่สำเร็จ เรียกทำงานโปรแกรมจัดการแฟ้มปริยายไม่สำเร็จ เรียกทำงานโปรแกรมอ่านเมลปริยายไม่สำเร็จ เรียกทำงานโปรแกรมจำลองเทอร์มินัลปริยายไม่สำเร็จ เรียกทำงานโปรแกรมท่องเว็บปริยายไม่สำเร็จ เรียกทำงานโปรแกรมหลักที่เลือกไว้สำหรับหมวด "%s" ไม่สำเร็จ โหลดเนื้อหาจาก "%s" ไม่สำเร็จ: %s โหลดภาพ "%s" ไม่สำเร็จ: ไม่ทราบสาเหตุ แฟ้มภาพอาจจะเสียหาย เปิด "%s" ไม่สำเร็จ เปิด %s เพื่อเขียนไม่สำเร็จ เปิด URI "%s" ไม่สำเร็จ เปิดดิสเพลย์ไม่สำเร็จ เปิดแฟ้ม "%s" ไม่สำเร็จ: %s แจงเนื้อหาของ "%s" ไม่สำเร็จ: %s อ่านแฟ้ม "%s" ไม่สำเร็จ: %s บันทึก "%s" ไม่สำเร็จ กำหนดโปรแกรมจัดการแฟ้มปริยายไม่สำเร็จ กำหนดโปรแกรมอ่านเมลปริยายไม่สำเร็จ กำหนดโปรแกรมจำลองเทอร์มินัลปริยายไม่สำเร็จ กำหนดโปรแกรมท่องเว็บปริยายไม่สำเร็จ แฟ้ม "%s" ไม่มีคีย์ type โปรแกรมจัดการแฟ้ม ไอคอนชนิดแฟ้ม ตำแหน่งแฟ้มไม่ใช่แฟ้มปกติหรือไดเรกทอรี โฟลเดอร์ วาดตามสถานะ GIcon เทอร์มินัลของ GNOME โปรแกรมท่องเว็บ Galeon Geary Google Chrome ขนาดสม่ำเสมอ วิธีการจัดตำแหน่งของข้อความเทียบกับไอคอนของแต่ละรายการ Icecat Icedove Iceweasel ไอคอน โมเดลของแถบไอคอน โมเดลของมุมมองไอคอน คอลัมน์ไอคอน ถ้าคุณไม่ระบุตัวเลือก --launch แล้ว exo-open จะเปิด URL ทั้งหมดที่ระบุมา
ด้วยโปรแกรมที่เลือกไว้สำหรับจัดการ URL นั้นๆ มิฉะนั้น ถ้าคุณระบุตัวเลือก --launch
คุณก็สามารถเลือกโปรแกรมที่จะเรียกทำงานได้ และสามารถเพิ่มพารามิเตอร์
ให้กับโปรแกรมดังกล่าวได้ด้วย (เช่น สำหรับ TerminalEmulator
คุณสามารถกำหนดบรรทัดคำสั่งที่จะเรียกทำงานในเทอร์มินัลได้) แฟ้มรูปภาพ ชนิด "%s" ของตัวช่วยเรียก ไม่ถูกต้อง Jumanji KMail โปรแกรมท่องเว็บ Konqueror เรียกตัวช่วยเรียกปริยายของ "ชนิด" ที่ระบุ โดยอาจเพิ่ม "พารามิเตอร์" ประกอบได้ โดย "ชนิด" ต้องเป็นค่าใดค่าหนึ่งต่อไปนี้ ไม่รองรับการเรียกทำงานแฟ้มเดสก์ท็อปถ้า %s ไม่ได้คอมไพล์ให้รองรับ GIO-Unix โหมดการจัดวาง โปรแกรมท่องเว็บโหมดข้อความ Links ไอคอนตำแหน่งที่ตั้ง โปรแกรมท่องเว็บโหมดข้อความ Lynx โปรแกรมอ่านเมล ระยะกั้นขอบ คอลัมน์มาร์กอัป ไอคอนเมนู Midori หมายเลขคอลัมน์ในโมเดลที่จะใช้ค้นหาเมื่อสั่งค้นหาในรายการ หมายเลขคอลัมน์ในโมเดลที่จะใช้ดึงพาธเต็มของรูปภาพที่จะวาดแสดง หมายเลขคอลัมน์ในโมเดลที่จะดึงข้อมูล pixbuf ของไอคอน หมายเลขคอลัมน์ในโมเดลที่จะดึงข้อความ หมายเลขคอลัมน์ในโมเดลที่ใช้ดึงข้อความซึ่งใช้มาร์กอัปแบบ Pango โมเดลของแถบไอคอน โปรแกรมท่องเว็บของ Mozilla Mozilla Firefox Mozilla Mail Mozilla Thunderbird Mutt NXterm Nautilus Netscape Navigator ไม่ได้เลือกโปรแกรมใด ไม่ได้ระบุคำสั่ง ไม่ได้เลือกแฟ้มใด ไม่ได้ระบุแฟ้มหรือโฟลเดอร์ ไม่ได้กำหนดโปรแกรมช่วยสำหรับ "%s" ไว้ ไม่มีไอคอน จำนวนคอลัมน์ จำนวนของคอลัมน์ที่จะแสดง เปิดกล่องโต้ตอบเลือกโปรแกรมหลักๆ โปรแกรมท่องเว็บ Opera ตัวเลือก: แนววาง โปรแกรมจัดการแฟ้ม PCMan โปรแกรมจัดการแฟ้ม PCManFM-Qt สคริปต์ Perl คอลัมน์ pixbuf กรุณาเลือกโปรแกรมจัดการแฟ้มที่คุณชอบ
แล้วคลิก "ตกลง" เพื่อดำเนินการต่อ กรุณาเลือกโปรแกรมอ่านเมลที่คุณชอบ
แล้วคลิก "ตกลง" เพื่อดำเนินการต่อ กรุณาเลือกโปรแกรมจำลองเทอร์มินัลที่คุณชอบ
แล้วคลิก "ตกลง" เพื่อดำเนินการต่อ กรุณาเลือกโปรแกรมท่องเว็บที่คุณชอบ
แล้วคลิก "ตกลง" เพื่อดำเนินการต่อ หากพบปัญหา กรุณารายงานที่ <%s>
 เลือกโปรแกรมหลักๆ เลือกโปรแกรมหลักๆ (โปรแกรมท่องเว็บ, โปรแกรมอ่านเมล และโปรแกรมจำลองเทอร์มินัล) กำหนด URL เตรียมไว้เมื่อสร้างแฟ้มเดสก์ท็อป กำหนดคำสั่งเตรียมไว้เมื่อสร้างแฟ้มเดสก์ท็อป กำหนดหมายเหตุเตรียมไว้เมื่อสร้างแฟ้มเดสก์ท็อป กำหนดไอคอนเตรียมไว้เมื่อสร้างแฟ้มเดสก์ท็อป กำหนดชื่อเตรียมไว้เมื่อสร้างแฟ้มเดสก์ท็อป กดเมาส์ปุ่มซ้ายเพื่อเปลี่ยนโปรแกรมที่เลือก ภาพตัวอย่าง แสดงข้อมูลรุ่นและจบการทำงาน สคริปต์ Python QTerminal สอบถามโปรแกรมช่วยปริยายสำหรับ "ชนิด" เมื่อ "ชนิด" คือค่าใดค่าหนึ่งต่อไปนี้ QupZilla ROX-Filer RXVT Unicode อ่านอีเมลของคุณ วาดข้อความแตกต่างกันตามสถานะการเลือก เปลี่ยนลำดับได้ คำสั่งเริ่มวาระใหม่ โปรแกรมจัดการแฟ้ม Rodent ช่องไฟระหว่างแถว ช่องไฟระหว่างแถว สคริปต์ Ruby ทำงานในเ_ทอร์มินัล หมายเลขซ็อกเก็ต Sakura คอลัมน์ค้นหา เลือกไ_อคอนจาก: เลือกไดเรกทอรีทำงาน เลือกโปรแกรม เลือกไอคอน เลือกโปรแกรม เลือกโปรแกรมปริยายสำหรับบริการต่างๆ เลือกตัวเลือกนี้หากต้องการเปิดใช้การแจ้งเหตุขณะเริ่มเมื่อเรียกคำสั่งจากโปรแกรมจัดการแฟ้มหรือจากเมนู ไม่ใช่ทุกโปรแกรมที่จะรองรับการแจ้งเหตุขณะเริ่ม เลือกตัวเลือกนี้หากต้องการเรียกทำงานคำสั่งในหน้าต่างเทอร์มินัล โหมดการเลือก ขีดคั่น คำสั่งสำหรับเริ่มวาระใหม่ ซ็อกเก็ตของโปรแกรมจัดการค่าตั้ง เชลล์สคริปต์ คลิกเดียว กำหนดเวลาในโหมดคลิกเดียว ซ็อกเก็ต ช่องว่างที่แทรกที่ขอบของมุมมองไอคอน ช่องว่างที่แทรกระหว่างช่องต่างๆ ของรายการหนึ่งๆ ช่องว่างที่แทรกระหว่างคอลัมน์ของตาราง ช่องว่างที่แทรกระหว่างแถวของตาราง ช่องไฟระหว่างช่องตาราง ระบุโปรแกรมที่คุณต้องการใช้
เป็นโปรแกรมจัดการแฟ้มปริยายสำหรับ Xfce: ระบุโปรแกรมที่คุณต้องการใช้
เป็นโปรแกรมอ่านเมลปริยายสำหรับ Xfce: ระบุโปรแกรมที่คุณต้องการใช้
เป็นโปรแกรมจำลองเทอร์มินัลปริยายสำหรับ Xfce: ระบุโปรแกรมที่คุณต้องการใช้
เป็นโปรแกรมท่องเว็บปริยายสำหรับ Xfce: ไอคอนสถานะ ไอคอนสำเร็จรูป Surf Sylpheed ชนิด [พารามิเตอร์] โปรแกรมจำลองเทอร์มินัล Terminator คอลัมน์ข้อความ ข้อความสำหรับไอคอน_สำคัญ ข้อความสำหรับทุ_กไอคอน GIcon ที่จะวาด ช่องว่างที่แทรกระหว่างคอลัมน์ที่ติดกันของตาราง ช่องว่างที่แทรกระหว่างแถวที่ติดกันของตาราง ระยะเวลาที่จะคอยก่อนที่จะเลือกรายการใต้ตัวชี้เมาส์โดยอัตโนมัติในโหมดคลิกเดียว แฟ้ม "%s" ไม่มีข้อมูลใดๆ รองรับ "ชนิด" ต่อไปนี้สำหรับคำสั่ง --launch และ --query:

  WebBrowser       - โปรแกรมหลักสำหรับท่องเว็บ
  MailReader       - โปรแกรมหลักสำหรับอ่านเมล
  FileManager      - โปรแกรมหลักสำหรับจัดการแฟ้ม
  TerminalEmulator - โปรแกรมหลักสำหรับจำลองเทอร์มินัล คำสั่ง --launch รองรับ "ชนิด" ต่อไปนี้: ไอคอนที่จะวาด โหมดการจัดวาง โมเดลสำหรับมุมมองไอคอน แนววางของแถบไอคอน โปรแกรมจัดการแฟ้มหลักจะใช้ในการท่องดูเนื้อหาภายในโฟลเดอร์ต่างๆ โปรแกรมอ่านเมลหลักจะใช้ในการเขียนอีเมลเมื่อคุณคลิกที่อยู่อีเมล โปรแกรมจำลองเทอร์มินัลหลักจะใช้ในการเรียกคำสั่งที่ต้องอาศัยสภาพแวดล้อมแบบบรรทัดคำสั่ง โปรแกรมท่องเว็บหลักจะใช้เปิดไฮเพอร์ลิงก์และใช้แสดงเนื้อหาคู่มือวิธีใช้โปรแกรมต่างๆ โหมดของการเลือก ขนาดของไอคอนที่จะวาด ในหน่วยพิกเซล ความกว้างของแต่ละรายการ Thunar รูปแ_บบของแถบเครื่องมือ สั่ง '%s --help' เพื่อดูวิธีใช้ ชนิดของแฟ้มเดสก์ท็อปที่จะสร้าง (โปรแกรมหรือจุดเชื่อม) ไม่สามารถตรวจหา scheme ของ URI ของ "%s" ไอคอนไม่มีหมวดหมู่ ไม่รองรับแฟ้มเดสก์ท็อปชนิด "%s" วิธีใช้: %s [ตัวเลือก] [แฟ้ม]
 วิธีใช้: exo-open [URLs...] ใช้การแจ้งเหตุขณะเ_ริ่ม ใช้โปรแกรมที่กำหนดเองซึ่งไม่รวมอยู่ในรายชื่อข้างต้น ใช้บรรทัดคำสั่ง ให้ผู้ใช้สามารถค้นหาข้อมูลในคอลัมน์แบบโต้ตอบได้ มุมมองสามารถสับเปลี่ยนลำดับได้ Vimprobable2 โปรแกรมท่องเว็บโหมดข้อความ W3M โปรแกรมท่องเว็บ กำหนดว่าช่องตารางต่างๆ ควรมีขนาดเท่ากันหมดหรือไม่ กำหนดว่ารายการต่างๆ ในมุมมองสามารถเรียกใช้ได้ด้วยคลิกเดียวหรือไม่ ความกว้างของแต่ละรายการ กลุ่มหน้าต่าง ผู้นำของกลุ่มหน้าต่าง ไดเรกทอรีทำ_งาน: X Terminal เทอร์มินัลของ Xfce โปรแกรมจัดการแฟ้ม Xfe [แฟ้ม|โฟลเดอร์] เ_พิ่มแถบเครื่องมือแถบใหม่ _ยกเลิก ปิ_ด ค่า_ปริยายของเดสก์ท็อป _วิธีใช้ ไ_อคอน: ไ_อคอนเท่านั้น _อินเทอร์เน็ต _ชื่อ: _ตกลง _เปิด _อื่นๆ... _ลบแถบเครื่องมือ _บันทึก _ค้นหาไอคอน: _ข้อความเท่านั้น _URL: เ_ครื่องมือทั่วไป aterm qtFM ขนาด 