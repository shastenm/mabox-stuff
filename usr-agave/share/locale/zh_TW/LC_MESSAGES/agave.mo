��    +      t  ;   �      �  ,   �  "   �     	       	          *  !   K     m     y     �     �     �     �     �  -   �  
   (  	   3     =     U     l     �     �     �     �     �  -   �        *        8     J     R  
   Y     d     s     y          �     �     �     �     �     �  �  �  -   r  !   �     �     �     �  $   �     	  	   &	     0	     E	     R	     b	     u	     �	  $   �	     �	     �	     �	     �	     
     
  	   -
     7
     >
     K
  $   ^
     �
  '   �
     �
     �
     �
     �
     �
                    *     <     N     f     {  %   �         (                          )      &                +                 	                  %                                    #       "               *       
      $   !   '                            A colorscheme designer for the GNOME desktop Add the current color to favorites Add to Favorites Agave Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Enter a new name: Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Rename Color Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Rename Color _Saturate Scheme _Save Scheme... translator-credits Project-Id-Version: Agave 0.4.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-12-24 05:23+0800
Last-Translator: Cheng-Wei Chien <e.cwchien@gmail.com>
Language-Team: Cheng-Wei Chien <e.cwchien@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Country: TAIWAN
X-Poedit-Language: Chinese
X-Poedit-SourceCharset: utf-8
X-Generator: Pootle 0.10.1
 GNOME 桌面環境的色彩配置設計工具 將目前色彩加入我的最愛 加入我的最愛 Agave 類比 請選擇一種色彩和配置型別 清除我的最愛色彩列表 互補色 降低飽和度 (_e) 減少亮度 降低飽和度 透過雙擊選取 匯出我的最愛 (_x) 請輸入新名稱： 匯出我的最愛為 GIMP 色盤檔 我的最愛 (_v) 我的最愛 亂數產生色彩 產生色彩配置檔 增加亮度 增高飽和度 同色系 色盤 專案網站 移除所選色彩 從我的最愛中移除所選色彩 重新命名色彩 將目前配置另存為 GIMP 色盤檔 分離互補色 四角互補 三色 全部清除 (_C) 色彩變暗 (_D) 編輯 (_E) 檔案 (_F) 說明 (_H) 色彩加亮 (_L) 亂數產生 (_R) 重新命名色彩 (_R) 增高飽和度 (_S) 儲存配置檔 (_S) Cheng-Wei Chien
<e.cwchien@gmail.com> 