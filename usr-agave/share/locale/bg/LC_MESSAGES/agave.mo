��            )         �  ;   �  "   �       	   !      +  !   L     n     z     �  
   �     �     �     �     �          "     2  -   B     p     x  
        �     �     �     �     �     �     �     �     �  !  �  {     G   �  *   �       2     N   H     �  *   �  2   �       7     7   L  ,   �  4   �     �  5   	  ,   9	  K   f	     �	     �	  '   �	  /   �	     &
  	   >
     H
  /   T
     �
  -   �
  '   �
    �
               
       	                                                                                                                  A program for the GNOME desktop for generating colorschemes Add the current color to favorites Add to Favorites Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements Decrease the brightness Decrease the saturation Fa_vorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Project Website Remove Selected Remove the selected color from your favorites Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Saturate Scheme _Save Scheme translation-credits Project-Id-Version: colorscheme
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-09-21 12:36+0300
Last-Translator: Rostislav "zbrox" Raykov <zbrox@i-space.org>
Language-Team: Bulgarian <dict@fsa-bg.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit Програма за работната среда GNOME за генериране на палитри от цветове Добавяне на текущия цвят към отметките Добавяне към отметките Аналози Изберете цвят и вид палитра Изчистване на списъка с отметки за цветове Добавки Намаляване на яркостта Намялаване на наситеността _Отметки Генериране на произволен цвят Генериране на цветови палитри Увеличаване на яркостта Увеличаване на наситеността Монохроматично Интернет страница на проекта Премахване на избраното Премахване на избрания цвят от отметките Тетради Триади _Изчистване на всичко По_тъмняване на палитрата _Редактиране _Файл _Помощ _Осветляване на палитрата П_роизволни _Наситеност на палитрата _Запазване на палитра Ростислав "zbrox" Райков <zbrox@i-space.org>

Проектът за превод на GNOME има нужда от подкрепа.
Научете повече за нас на http://gnome.cult.bg
Докладвайте за грешки на http://gnome.cult.bg/bugs 