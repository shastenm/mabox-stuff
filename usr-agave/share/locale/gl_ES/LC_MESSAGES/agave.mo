��    !      $  /   ,      �  ;   �  "   %     H  	   Y      c  !   �     �     �     �     �  
   �                /     G     _     m     }  -   �     �     �     �  
   �     �     �     �                          1     >  _  R  B   �  "   �       	   ,  %   6  !   \     ~     �     �     �  
   �     �     �               .     =     Q  (   g     �  	   �     �     �     �     �     �     �     �  
   	     	     !	  (   1	                                                                                          	   
              !                                                    A program for the GNOME desktop for generating colorschemes Add the current color to favorites Add to Favorites Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Fa_vorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Project Website Remove Selected Remove the selected color from your favorites Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Saturate Scheme _Save Scheme translation-credits Project-Id-Version: GNOME Colorscheme 0.2.2.1
Report-Msgid-Bugs-To: Jonathon Jongsma <jonathon.jongsma@gmail.com>
PO-Revision-Date: 2005-11-14 11:22+0100
Last-Translator: Xoan Sampaiño Villagra <xoansampainho@gmail.com>
Language-Team: Spanish/Spain <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit Un programa para o escritorio GNOME para xenerar esquemas de color Engadir o color actual a favoritos Engadir a favoritos Análogos Elixa unha color e un tipo de esquema Limpar a lista de cores favoritos Complementarios _Desaturar esquema Reducir o brillo Reducir a saturación _Favoritos Xenerar unha cor aleatoria Xenere esquemas de cor Aumentar o brillo Aumentar a saturación Monocromático Páxina do proyecto Eliminar seleccionado Eliminar a cor seleccionada de favoritos Complementarios partidos Tétradas Tríadas _Limpar Todo _Escurecer esquema _Editar _Arquivo A_xuda A_clarar esquema _Aleatorio _Saturar esquema _Gardar esquema Francisco Dieguez <fran.dieguez@glug.es> 