��    '      T  5   �      `  ;   a  "   �     �  	   �      �  !   �          *     =     U     m     �  -   �  
   �  	   �     �     �          #     ;     I     Q     a  -   q  *   �     �     �     �  
   �     �                         '     /     @     M  P  a  %   �  #   �     �       "     $   <     a     p     �     �     �     �  -   �  
   	  	   	     #	     <	     U	     l	     �	     �	     �	     �	  )   �	  *   �	     
     .
     7
     ?
     K
     a
     h
     p
     w
  	   �
     �
     �
     �
               %          "   
                $             #          	      !             &                                                              '                             A program for the GNOME desktop for generating colorschemes Add the current color to favorites Add to Favorites Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Saturate Scheme _Save Scheme translation-credits Project-Id-Version: GNOME Colorscheme 0.2.2.1
Report-Msgid-Bugs-To: Jonathon Jongsma <jonathon.jongsma@gmail.com>
PO-Revision-Date: 2005-11-14 11:22+0100
Last-Translator: Xisco Bonet <xiscobv@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Genera esquemas de color per al gnome Afegeix el color actual a preferits Afegeix a preferits Anàlogs Tria un color y un tipus d'esquema Neteja la llista de colors preferits Complementaris _Desatura l'esquema Redueix la brillantor Redueix la saturacció Doble click per seleccionar E_xporta preferits Exporta colors preferits com a paleta de GIMP _Preferits Preferits Genera un color aleatori Genera esquemes de color Augmenta la brillantor Augmenta la saturació Monocromàtic Paleta Pàgina del projecte Elimina seleccionat Elimina el color seleccionat de preferits Guarda esquema actual com a paleta de GIMP Complementaris dividits Tetrades Triadas _Neteja tot _Enfosqueix l'esquema _Edita _Fitxer A_juda A_clara esquema _Aleatori _Satura l'esquema _Guarda esquema Xisco Bonet <xiscobv@gmail.com> 