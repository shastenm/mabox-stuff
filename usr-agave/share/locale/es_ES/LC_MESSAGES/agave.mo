��    !      $  /   ,      �  ;   �  "   %     H  	   Y      c  !   �     �     �     �     �  
   �                /     G     _     m     }  -   �     �     �     �  
   �     �     �     �                          1     >  _  R  C   �  #   �       	   .  #   8  %   \     �     �     �     �  
   �     �     �                8     G     \  +   r     �  	   �     �     �     �     �     �     �     	  
   	     	     0	  (   A	                                                                                          	   
              !                                                    A program for the GNOME desktop for generating colorschemes Add the current color to favorites Add to Favorites Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Fa_vorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Project Website Remove Selected Remove the selected color from your favorites Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Saturate Scheme _Save Scheme translation-credits Project-Id-Version: GNOME Colorscheme 0.2.2.1
Report-Msgid-Bugs-To: Jonathon Jongsma <jonathon.jongsma@gmail.com>
PO-Revision-Date: 2005-11-14 11:22+0100
Last-Translator: Xoan Sampaiño Villagra <xoansampainho@gmail.com>
Language-Team: Spanish/Spain <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit Un programa para el escritorio GNOME para generar esquemas de color Añadir el color actual a favoritos Añadir a favoritos Análogos Elija un color y un tipo de esquema Limpiar la lista de colores favoritos Complementarios _Desaturar esquema Reducir el brillo Reducir la saturación _Favoritos Generar un color aleatorio Genere esquemas de color Aumentar el brillo Aumentar la saturación Monocromático Página del proyecto Eliminar seleccionado Eliminar el color seleccionado de favoritos Complementarios partidos Tétradas Tríadas _Limpiar Todo _Oscurecer esquema _Editar _Archivo A_yuda A_clarar esquema _Aleatorio _Saturar esquema _Guardar esquema Xoan Sampaiño <xoansampainho@gmail.com> 