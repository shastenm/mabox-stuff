��    +      t  ;   �      �  ,   �  "   �     	       	          *  !   K     m     y     �     �     �     �     �  -   �  
   (  	   3     =     U     l     �     �     �     �     �  -   �        *        8     J     R  
   Y     d     s     y          �     �     �     �     �     �    �  +   �  (        =     S     Y  $   b  %   �     �     �  	   �     �      �     	     (	  )   A	  
   k	  	   v	     �	     �	     �	     �	     �	     �	     �	     

  +    
     L
  .   \
     �
     �
  	   �
     �
     �
  	   �
     �
     �
     �
          	          ,     =         (                          )      &                +                 	                  %                                    #       "               *       
      $   !   '                            A colorscheme designer for the GNOME desktop Add the current color to favorites Add to Favorites Agave Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Enter a new name: Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Rename Color Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Rename Color _Saturate Scheme _Save Scheme... translator-credits Project-Id-Version: Agave VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-08-30 11:08+0200
Last-Translator: Sergio Durzu <pionono@gmail.com>
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit Un generatore di schemi di colore per GNOME Aggiungi il colore corrente ai preferiti Aggiungi ai Preferiti Agave Analoghi Scegli un colore e un tipo di schema Pulisci la lista dei colori preferiti Complementari Meno s_aturazione Meno lumi Diminuisce la saturazione Clicca due volte per selezionare _Esporta preferiti... Inserisci un nuovo nome: Esporta i preferiti come Palette per GIMP _Preferiti Preferiti Genera un colore random Genera schemi di colore Aumenta la luminosità Aumenta la saturazione Monocromatico Palette Homepage del progetto Elimina i selezionati Elimina il colore selezionato dai preferiti Rinomina colore Salva lo schema corrente come Palette per GIMP Dividi complementari Quadricolore Tricolore _Pulisci tutto _Meno lumonisità _Modifica _File _Aiuto Più _luminosità _Random _Rinomina colore Più _saturazione _Salva schema... sergej <pionono@gmail.com> 