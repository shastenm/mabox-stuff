��    #      4  /   L        ;   	  "   E     h  	   y      �  !   �     �     �     �     �     	  
   !  	   ,     6     N     e     }     �     �     �  -   �     �            
             ,     2     8     >     N     V     g     t    �  L   �  @   �  &   3     Z  .   i  =   �     �  &   �     	  !   (  +   J     v     �  ,   �  *   �     �  )   	     ?	     V	  !   n	  D   �	  $   �	     �	     
     $
     C
     Q
  	   _
     i
     y
     �
     �
     �
  &   �
                                         	                          !                       
                 #       "                                           A program for the GNOME desktop for generating colorschemes Add the current color to favorites Add to Favorites Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Colorscheme Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Project Website Remove Selected Remove the selected color from your favorites Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Saturate Scheme _Save Scheme translation-credits Project-Id-Version: colorscheme-0.2.2.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-11-04 15:16+0300
Last-Translator: Roxana Chernogolova <mavka@justos.org>
Language-Team: russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Приложение GNOME для создания цветовых схем Добавить текущий цвет в изобранное Добавить в избранное Похожие Выберите цвет и тип схемы Очистить список избранных цветов Colorscheme Взаимодополняющие (2) М_енее насыщенно Уменьшить яркость Уменьшить насыщенность Из_бранное Избранное Выбрать случайные цвета Создать цветовую схему Повысить яркость Повысить насыщенность Монохромные Сайт проекта Удалить выбранный Удалить выбранный цвет из избранного Взаимодполняющие (3) Четыре цвета Три цвета _Очистить список _Темнее _Правка _Файл _Справка С_ветлее Сл_учайный _Более насыщенно _Сохранить схему Roxana Chernogolova <mavka@justos.org> 