��    +      t  ;   �      �  ,   �  "   �     	       	          *  !   K     m     y     �     �     �     �     �  -   �  
   (  	   3     =     U     l     �     �     �     �     �  -   �        *        8     J     R  
   Y     d     s     y          �     �     �     �     �     �  .  �  4     &   C     j     �     �     �  !   �     �     �     �     	     	     $	     >	  3   T	     �	  
   �	     �	      �	     �	     �	     �	     
     
     )
  '   <
     d
  2   w
     �
     �
     �
     �
     �
     �
     �
             
         +     ?     R  F   f         (                          )      &                +                 	                  %                                    #       "               *       
      $   !   '                            A colorscheme designer for the GNOME desktop Add the current color to favorites Add to Favorites Agave Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Enter a new name: Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Rename Color Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Rename Color _Saturate Scheme _Save Scheme... translator-credits Project-Id-Version: Agave 0.4.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-06-16 12:06+0100
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Country: CZECH REPUBLIC
 Návrhář barevných schémat pro prostředí GNOME Přidat aktuální barvu k oblíbeným Přidat k oblíbeným Agave Analogické Vyberte barvu a typ schématu Vymazat seznam oblíbených barev Komplementární O_dsytit schéma Snížit jas Snížit sytost Vyberte dvojklikem E_xportovat oblíbené... Vložte nový název: Exportovat oblíbené barvy jako soubor palety GIMP _Oblíbené Oblíbené Generovat náhodnou barvu Vytváření barevných schémat Zvýšit jas Zvýšit sytost Monochromatické Paleta Webová stránka programu Odstranit vybrané Odstranit vybranou barvu z oblíbených Přejmenovat barvu Uložit současné schéma jako soubor palety GIMP Polokomplementární Tetrády Triády _Vymazat vše Z_tmavit schéma _Upravit _Soubor _Nápověda Z_esvětlit schéma Ná_hodně Přejmenovat ba_rvu Při_sytit schéma _Uložit schéma... Zbynek Mrkvicka <zbynek@mrkvicka.eu>
Marek Černocký <marek@manet.cz> 