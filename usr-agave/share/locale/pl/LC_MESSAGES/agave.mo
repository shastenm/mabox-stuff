��    +      t  ;   �      �  ,   �  "   �     	       	          *  !   K     m     y     �     �     �     �     �  -   �  
   (  	   3     =     U     l     �     �     �     �     �  -   �        *        8     J     R  
   Y     d     s     y          �     �     �     �     �     �  &  �  3     #   :     ^     r     x  $   �  $   �     �  #   �     �     	     %	     D	     X	  0   l	  	   �	     �	     �	     �	  
   �	     �	     �	     
     
     .
  #   ?
     c
  3   x
     �
     �
     �
  
   �
     �
     �
     �
                    "     8     D  %   _         (                          )      &                +                 	                  %                                    #       "               *       
      $   !   '                            A colorscheme designer for the GNOME desktop Add the current color to favorites Add to Favorites Agave Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Enter a new name: Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Rename Color Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Rename Color _Saturate Scheme _Save Scheme... translator-credits Project-Id-Version: GNOME Agave 0.4.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-03-12 09:10-0500
Last-Translator: Paweł Łupkowski <p_lupkowski@o2.pl
Language-Team: Polish <mailto:p_lupkowski@o2.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Projektant zestawów kolorów dla środowiska GNOME Dodaj bieżący kolor do ulubionych Dodaj do ulubionych Agave Analogiczne Wybierz kolor i typ zestawu kolorów Wyczyść listę ulubionych kolorów Dopełnienie Redukcja nasycenia kolorów zestawu Przyciemnij zestaw Zmniejsz nasycenie Kliknij podwójnie aby wybrać E_ksportuj ulubione Wpisz nową nazwę: Wyeksportuj ulubione jako paletę kolorów GIMPa U_lubione Ulubione Wygeneruj losowy kolor Wygeneruj zestaw kolorów Rozjaśnij Zwiększ nasycenie Monochromatyczny Paleta Strona domowa projektu Usuń zaznaczone Usuń zaznaczony kolor z ulubionych Zmień nazwę koloru Zapisz bieżący zestaw jako paletę kolorów GIMPa Rozdzielone dopełnienia Tetrady Triady _Wyczyść _Przyciemnij zestaw _Edycja _Plik _Pomoc Rozjaśnij zestaw _Losowo _Zmień nazwę koloru Przyciemnij _Zapisz zestaw kolorów... Paweł Łupkowski <p_lupkowski@o2.pl> 