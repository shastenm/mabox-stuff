��    +      t  ;   �      �  ,   �  "   �     	       	          *  !   K     m     y     �     �     �     �     �  -   �  
   (  	   3     =     U     l     �     �     �     �     �  -   �        *        8     J     R  
   Y     d     s     y          �     �     �     �     �     �  -  �  ?     '   M     u     �  	   �  -   �  %   �     �     �     	     &	     =	     \	     r	  D   �	     �	     �	      �	  &    
     '
     :
     R
     b
     j
     }
  0   �
     �
  E   �
       	   4     >     F     M  	   f     p     y          �     �     �     �  %   �         (                          )      &                +                 	                  %                                    #       "               *       
      $   !   '                            A colorscheme designer for the GNOME desktop Add the current color to favorites Add to Favorites Agave Analogous Choose a Color and a Scheme Type Clear the list of favorite colors Complements D_esaturate Scheme Decrease the brightness Decrease the saturation Double-click to select E_xport Favorites... Enter a new name: Export favorite colors as a GIMP Palette file Fa_vorites Favorites Generate a random color Generate color schemes Increase the brightness Increase the saturation Monochromatic Palette Project Website Remove Selected Remove the selected color from your favorites Rename Color Save current scheme as a GIMP Palette file Split-Complements Tetrads Triads _Clear All _Darken Scheme _Edit _File _Help _Lighten Scheme _Random _Rename Color _Saturate Scheme _Save Scheme... translator-credits Project-Id-Version: GNOME Agave 0.4.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-03-12 09:10-0500
Last-Translator: Yan Brodeur <yanbrodeur@videotron.ca>
Language-Team: French <mailto:yanbrodeur@videotron.ca>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un générateur d'arrangements de couleurs pour le bureau GNOME Ajouter la couleur actuelle aux favoris Ajouter aux favoris Agave Analogues Choisir une couleur et son type d'arrangement Vider la liste des couleurs favorites Compléments Dé_saturer l'arrangement Diminuer l'éclat Diminuer la saturation Double-clic pour sélectionner E_xporter les favoris Entrer un nouveau nom: Exporter les couleurs favorites en tant que palette de couleurs GIMP Fa_voris Favoris Générer une couleur aléatoire Générer des arrangements de couleurs Augmenter l'éclat Augmenter la saturation Monochromatique Palette Site Web du projet Retirer la sélection Retirer la couleur sélectionnée de vos favoris Renommer la couleur Sauvegarder l'arrangement actuel en tant que palette de couleurs GIMP Compléments divisés Tétrades Triades _Vider _Obscurcir l'arrangement _Édition _Fichier _Aide Éclaircir l'arrangement A_léatoire _Renommer la couleur Saturer l'arrangement _Sauvegarder l'arrangement... Yan Brodeur <yanbrodeur@videotron.ca> 