#!/bin/bash

CFGFILE="$HOME/.config/conky/Network_mbcolor.conkyrc"
rm /tmp/xx*
cd /tmp

csplit -s ${CFGFILE} /conky.text/


#IFACE=$(ip route get 8.8.8.8 | awk -- '{printf $5}')
IFACE=$(ip route get 8.8.8.8 | awk '{for(i=1; i<NF; i++) {if($i=="dev") dev=i+1} print $dev}')


cat <<EOF >> /tmp/xx00
conky.text = [[
\${color}NETWORK
\${color}Device \${alignr}$IFACE
\${color}IP \${alignr}\${addr $IFACE}
\${color}Download \$color0\${downspeed $IFACE} \${alignr}\${color0}\${upspeed $IFACE}\${color} Upload
\${color2}\${downspeedgraph $IFACE 30,100 -t} \${alignr} \${upspeedgraph $IFACE 30,100 -t}
\${color}Total Down \${color0}\${totaldown $IFACE} \${alignr}\${color0}\${totalup $IFACE}\${color} Total Up
\${color}\${hr}
\${execi 30 netstat -ept | grep ESTAB | awk '{print \$9}' | cut -d/ -f2 | sort | uniq -c | sort -nr}
]];
EOF

cat /tmp/xx00 > ${CFGFILE}
