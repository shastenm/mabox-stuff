#!/bin/bash
DIR="$HOME/.config/quoter"
mkdir -p "$DIR"
CNF_FILE="$DIR/quoter.cfg"
if [ ! -f $CNF_FILE ]; then
cat <<EOF > ${CNF_FILE}
quote_fontsize=12
author_fontsize=10
EOF
fi
source <(grep = $CNF_FILE)
qfsize=${quote_fontsize:-12}
afsize=${author_fontsize:-10}

export Q=$(curl -s "http://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en" | sed 's|\\||g' )
QUOTE=$(echo $Q | jq .quoteText)
AUTOR=$(echo $Q | jq .quoteAuthor | tr -d '"')
echo "${QUOTE}|${AUTOR}" >> "${DIR}"/forismatic.txt

printf "\${font Droid Sans:size=$qfsize}\${color}"
echo ${QUOTE} | fmt -80
printf "\${font Droid Sans:size=$afsize}\${alignr}\${color0}"
echo "${AUTOR}"
