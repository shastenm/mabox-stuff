#!/bin/bash
#
#    colorizer - set of tools for Mabox (auto)theming
#    Copyright (C) 2022-2023 Daniel Napora    <napcok@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

CNF_DIR="$HOME/.config/colorizer"
mkdir -p "$CNF_DIR"
CNF_FILE="$CNF_DIR/colorizer.conf"
CUSTOM_COMMANDS_FILE="$CNF_DIR/custom.csv"

if [ ! -f $CNF_FILE ]; then
cat <<EOF > ${CNF_FILE}
# User wallpaper dir
user_wallpapers_dir=~/wallpapers
# Show related tools (show or hide)
related_tools=hide
# Colorizer size: full or compact
colorizer_size=full
# Gradients: change openbox theme? (active window titlebar)
gradient_obtheme=yes
# Gradients: change menu/panels background?
gradient_menu=no
EOF
fi

source "$CNF_FILE"

case "$LANG" in
    pl*)
    REGENERATE="(Re)Generuj z kolorów tapety..."
    REGENERATEALL="Regeneruj wszystko"
    ASCONFIGURED="wg konfiguracji"
    REVERSE="odwrotnie"
    CURRENTCONF="Obecna konfiguracja"
    MODULES="Moduły"
    FONTS="Konfiguruj <b>Czcionki</b>"
    OBTHEME="Motyw <b>Openbox</b>"
    MENUPANELS="<b>Menu</b> i Panele boczne"
    COLORIZER_CONKY="<b>Conky</b> Manager i Colorizer"
    CAVA="Cava - wizualizacja dźwięku"
    TERMINALTHEME="Motyw terminala"
    EDIT_PALETTE="Edytuj paletę kolorów"
    RANDWALL="Ustaw losową tapetę"
    SYSWALLPAPER="Tapety <b>systemowe</b>"
    USERWALLPAPER="Tapety <b>użytkownika</b>"
    GRADIENTS="Gradienty"
    PRE_GRADIENTS="Gradienty"
    GRAD_OB="Stosować do motywu OpenBox?"
    GRAD_MENU="Stosować do Menu/Paneli?"
    GRAD_CONKY="Stosować do Conky?"
    SETTINGS_AND_HELP="Pomoc i Ustawienia"
    SETTINGS="Ustawienia"
    COLORIZER_SETTINGS="Ustawienia Colorizera"
        GRADIENTS_ACT="Działanie gradientów w menu gł."
    EDITCONF="Edytuj plik konfiguracyjny"
    RESET="Resetuj ustawienia Colorizera"
    YES="Tak"
    NO="Nie"
    LIGHTBG="<b>light</b> - jasne tło"
    DARKBG="<b>dark</b> - ciemne tło"
    OTHERTOOLS="Powiązane narzędzia"
    ADDLAUNCHER="Dodaj uruchamiacz Colorizera do panelu"
    DELETELAUNCHER="Usuń uruchamiacz Colorizera z panelu"
    HEIGHT="Wysokość Colorizera"
    FULL="<b>full</b> - pełna wysokość"
    COMPACT="<b>compact</b> - kompaktowa"
    OTHER_SETTINGS="Inne ustawienia"
    SHOW_RELATED="Pokazuj powiązane?"
    SHOW="<b>show</b> - pokaż"
    HIDE="<b>hide</b> - ukryj"
    EDIT_RELATED="Edytuj powiązane"
    RESET_RELATED="Resetuj powiązane"
    LXAPPEARANCE="Motyw GTK i ikony"
    THEMEMANAGER="Zarządzaj motywami"
    WALLPAPERS="Katalogi z tapetami"
    OPENDIR="Otwórz katalog w menadżerze plików"
    OPENDIR_VIEWNIOR="Otwórz katalog w przeglądarce obrazków"
    SETRANDOM="Ustaw losową"
    CHANGE="Zmień"
    USERDIR_NOT_EXIST="kalalog nie istnieje"
    CREATE_USERDIR="Utwórz katalog"
    HELP="Okno główne i Pomoc"
    RESET_COLORS="Resetuj kolory"
    RESET_CHOOSE="Resetuj..."
    RESET_SEP="Resetuj kolory dla"
    RES_OB_WD="Dekoracje okien OpenBox"
    RES_CONKY="Conky"
    RES_MENU="Menu i Paneli"
    RES_ALL="Wszystko <i>(OB, menu, conky)</i>"
    COLORIZE="Szybko Pokoloruj lub Resetuj..."
    ;;
    *)
    REGENERATE="(Re)Generate from wallpaper colors..."
    REGENERATEALL="Regenerate all"
    ASCONFIGURED="as configured"
    REVERSE="reverse"
    CURRENTCONF="Current configuration"
    MODULES="Modules"
    FONTS="Configure <b>Fonts</b>"
    OBTHEME="<b>Openbox</b> Theme"
    MENUPANELS="<b>Menus/SidePanels</b>"
    COLORIZER_CONKY="<b>Conky</b> Manager &amp; Colorizer"
    CAVA="Cava - Audio Visualizer"
    TERMINALTHEME="Terminal color scheme"
    RANDWALL="Set random wallpaper"
    SYSWALLPAPER="<b>System</b> Wallpapers"
    USERWALLPAPER="<b>User</b> Wallpapers"
    GRADIENTS="Gradients"
    PRE_GRADIENTS="Gradients"
    GRAD_OB="Apply to OpenBox theme?"
    GRAD_MENU="Apply to Menu/Panels?"
    GRAD_CONKY="Apply to Conkies?"
    SETTINGS_AND_HELP="Settings and Help"
    SETTINGS="Settings"
    COLORIZER_SETTINGS="Colorizer Settings"
    GRADIENTS_ACT="Gradients in Rootmenu actions"
    EDITCONF="Edit config file"
    RESET="Reset to defaults"
    YES="Yes"
    NO="No"
    LIGHTBG="with <b>light</b> background"
    DARKBG="with <b>dark</b> background"
    OTHERTOOLS="Related tools"
    ADDLAUNCHER="Add Colorizer launcher to panel"
    DELETELAUNCHER="Delete Colorizer from panel"
    HEIGHT="Height of Colorizer"
    FULL="full"
    COMPACT="compact"
    OTHER_SETTINGS="Other settings"
    SHOW_RELATED="Show related tools?"
    SHOW="show"
    HIDE="hide"
    EDIT_RELATED="Edit related tools"
    RESET_RELATED="Reset related tools"
    LXAPPEARANCE="GTK theme and icons"
    THEMEMANAGER="Theme manager"
    WALLPAPERS="Wallpapers directories"
    OPENDIR="Open directory in file manager"
    OPENDIR_VIEWNIOR="Open directory in Image viewer"
    SETRANDOM="Set random"
    CHANGE="Change"
    USERDIR_NOT_EXIST="directory does not exist"
    CREATE_USERDIR="Create directory"
    DIFF_CONKY_COLORS="Different colors for each Conky"
    HELP="Main Window and Help"
    RESET_COLORS="Reset Colors"
    RESET_CHOOSE="Reset..."
    RESET_SEP="Reset Colors of ..."
    RES_OB_WD="OpenBox Window Decorations"
    RES_CONKY="Conkies"
    RES_MENU="Menus / Panels"
    RES_ALL="All <i>(OB, menus, conky)</i>"
    COLORIZE="Quickly Colorize or Reset..."
    ;;
esac

if [ ! -f $CUSTOM_COMMANDS_FILE ]; then
cat <<EOF > ${CUSTOM_COMMANDS_FILE}
^sep($OTHERTOOLS)
Tint2 Menu,^pipe(jgtint2-pipe)
#$WALLPAPERS,^pipe(jgwallpaperchanger)
$TERMINALTHEME,^term(theme.sh -i2;/bin/bash)
$LXAPPEARANCE,lxappearance
$THEMEMANAGER,mb-obthemes
Color Menu,colormenu
EOF
fi

WALLPALDIR="$HOME/.cache/colorizer/palettes"
mkdir -p "$WALLPALDIR"
read WALLPATH<<< $(grep file "$HOME"/.config/nitrogen/bg-saved.cfg | cut -d'=' -f2)

NAME=${WALLPATH////_}
if [[ "${NAME}" =~ ^_home_.* ]]; then
n=${#HOME}
((n++))
NAME=${NAME:${n}}
fi


if [ ! -f "$WALLPALDIR/$NAME.clr" ]; then
convert ${WALLPATH} -resize 25% -colors 16 -unique-colors txt:- |grep -v '^#'| awk '{print substr($3,1,7)}' |pastel sort-by brightness |pastel format hex > "$WALLPALDIR/$NAME.clr"
fi
mapfile -t w < "$WALLPALDIR/$NAME.clr"


if [[ "${#w[@]}" -lt 8 ]]; then
w+=("#2e3440" "#4c566a" "#5e81ac" "#bf616b" "#b48ead" "#a3be8c" "#ebcb8b" "#d8dee9" "#eceff4")
fi

#Accent colors
plasma=("#e93a9a" "#e93d58" "#e9643a" "#e8cb2d" "#3dd425" "#00d3b8" "#3daee9" "#b875dc" "#926ee4" "#686b6f")
ubuntu=("#E95420" "#18b0b0" "#3584e4" "#0e8420" "#c748ba" "#77216f" "#c7162b" "#f4a100")
mint+=("#6cabcd" "#5b73c4" "#aa876a" "#9d9d9d" "#db9d61" "#c76199" "#8c6ec9" "#c15b58" "#c8ac69" "#5aaa9a")


out+=("^sep(Mabox Colorizer)")
out+=("  $COLORIZE,^pipe(randomizer c)")
out+=("^sep($MODULES)")
out+=("<big></big>   $FONTS,colorizer-fonts -s")
out+=("^sep()")
out+=("$OBTHEME,colorizer-ob -s")

#out+=("TINT2,^pipe(colorizer-tint2)")
out+=("$MENUPANELS,colorizer-menus -s")
out+=("$COLORIZER_CONKY,colorizer-conky -s")
out+=("^sep()")
#out+=("Systray HW monitor,^pipe(colorizer-phwmon)")

#if pgrep -f pyradio >/dev/null ;then
#    out+=("^sep()")
out+=("PyRadio,colorizer-pyradio -s")
#fi

#if pgrep -f cava >/dev/null ;then
#    out+=("^sep()")
out+=("$CAVA,colorizer-cava -s")
#fi




### TODO Rework below as submenu(edit palette and present palette with HTML helper- to keep it on desktop, allowing to copy colors from it)

##out+=("^sep()")
##out+=("  $EDIT_PALETTE,xdg-open $WALLPALDIR/$WALLPAPER.clr")


out+=("^sep($RANDWALL)")
syswallp=$(shopt -s nullglob ; set -- /usr/share/backgrounds/*.{jpg,JPG,jpeg,JPEG,png,PNG} ; echo $#)
out+=("  $SYSWALLPAPER (<small>${syswallp}</small>),nitrogen --random /usr/share/backgrounds --set-scaled --save;colorizer -s")
	if [[ -d "${user_wallpapers_dir}" ]]; then
	userwallp=$(shopt -s nullglob ; set -- $user_wallpapers_dir/*.{jpg,JPG,jpeg,JPEG,png,PNG} ; echo $#)
	out+=("  $USERWALLPAPER (<small>${userwallp}</small>),nitrogen --random ${user_wallpapers_dir} --set-scaled --save;colorizer -s")
	fi
	out+=("^sep()")
	out+=("$WALLPAPERS,^checkout(wallpapersettings)")

if [[ "$gradient_obtheme" == "yes" || "$gradient_menu" == "yes" ]];then
	[[ "$gradient_obtheme" == "yes" ]] && OBGRAD="OB" || OBGRAD=""
	[[ "$gradient_menu" == "yes" ]] && MENUGRAD="menus" || MENUGRAD=""
	out+=("^sep($GRADIENTS <small>$OBGRAD $MENUGRAD</small>)")
	out+=("<tt><span bgcolor='#16a084'> </span><span bgcolor='#43a480'> </span><span bgcolor='#5da97b'> </span><span bgcolor='#72ad77'> </span><span bgcolor='#84b171'> </span><span bgcolor='#95b66c'> </span><span bgcolor='#a5ba66'> </span><span bgcolor='#b5be61'> </span><span bgcolor='#c3c15a'> </span><span bgcolor='#d1c753'> </span><span bgcolor='#dfc94c'> </span><span bgcolor='#edce44'> </span><span bgcolor='#f4d03f'> </span></tt>  $PRE_GRADIENTS,^pipe(mbgrad)")
fi

out+=("^sep($SETTINGS)")
out+=("  $SETTINGS,^checkout(settings)")
LNG=${LANG:0:2}
if [[ -f "/usr/share/mabox-colorizer/help/$LNG.html" ]];then
out+=("  $HELP,yhtml /usr/share/mabox-colorizer/help/$LNG.html 'Mabox Colorizer'")
else
out+=("  $HELP,yhtml /usr/share/mabox-colorizer/help/en.html 'Mabox Colorizer'")
fi

### RESET ACTIONS (replaced by Randomizer)
#out+=("^sep($RESET_COLORS)")
#out+=("$RESET_CHOOSE,^checkout(resetroot)")
#out2+=("^tag(resetroot)")
#out2+=("^sep($RESET_SEP)")
#out2+=("$RES_OB_WD,obtctl resetMBcolors;colorizer -s")
#out2+=("$RES_MENU,jgctl reset;colorizer -s")
#out2+=("$RES_CONKY,conkyctl resetcolorsall;colorizer -s")
#out2+=("^sep()")
#out2+=("\"\"\"$RES_ALL\"\"\",obtctl resetMBcolors;conkyctl resetcolorsall;jgctl reset;colorizer -s")

if [[ "$related_tools" == "show" ]];then
out+=(". $CUSTOM_COMMANDS_FILE")
fi
### FUTURE: SAVE AND RESTORE
#out+=("^sep()")
#out+=("Pin current colors to wallpaper")
###

out2+=("^tag(settings)")


#out2+=("$WALLPAPERS,^checkout(wallpapersettings)")
out3+=("^tag(wallpapersettings)")
out3+=("^sep($SYSWALLPAPER ($syswallp))")
out3+=("^sep(<i>/usr/share/backgrounds</i>)")
out3+=("  $OPENDIR,xdg-open /usr/share/backgrounds")
out3+=("  $OPENDIR_VIEWNIOR,viewnior /usr/share/backgrounds")
out3+=("  $SETRANDOM,nitrogen --random /usr/share/backgrounds --set-scaled --save;colorizer -s")
out3+=("^sep($USERWALLPAPER  ($userwallp))")
if [[ -d "${user_wallpapers_dir}" ]]; then
# dir exist
out3+=("^sep(<i>${user_wallpapers_dir}</i>)")
out3+=("  $OPENDIR ,xdg-open ${user_wallpapers_dir}")
out3+=("  $OPENDIR_VIEWNIOR ,viewnior ${user_wallpapers_dir}")
out3+=("  $SETRANDOM,nitrogen --random ${user_wallpapers_dir} --set-scaled --save;colorizer -s")
out3+=("^sep()")
out3+=("  $CHANGE ($EDITCONF),xdg-open $CNF_FILE")
else
# dir not exist
out3+=("^sep(<i>${user_wallpapers_dir} - $USERDIR_NOT_EXIST</i>")
out3+=("  $CREATE_USERDIR,mkdir -p ${user_wallpapers_dir};colorizer -s")
out3+=("  $CHANGE ($EDITCONF),xdg-open $CNF_FILE")
fi
out2+=("^sep($GRADIENTS_ACT)")
[[ "$gradient_obtheme" == "yes" ]] && out2+=("<big></big>  $GRAD_OB,mb-setvar gradient_obtheme=no $CNF_FILE;colorizer -s") || out2+=("<big></big>  $GRAD_OB,mb-setvar gradient_obtheme=yes $CNF_FILE;colorizer -s")
#out2+=("$GRAD_OB [ <b>${gradient_obtheme}</b> ],^checkout(grad_ob)")
#out3+=("^tag(grad_ob")
#out3+=("^sep($GRAD_OB)")
#out3+=("$YES,mb-setvar gradient_obtheme=yes $CNF_FILE;colorizer -s")
#out3+=("$NO,mb-setvar gradient_obtheme=no $CNF_FILE;colorizer -s")

[[ "$gradient_menu" == "yes" ]] && out2+=("<big></big>  $GRAD_MENU,mb-setvar gradient_menu=no $CNF_FILE;colorizer -s") || out2+=("<big></big>  $GRAD_MENU,mb-setvar gradient_menu=yes $CNF_FILE;colorizer -s")
#out2+=("$GRAD_MENU [ <b>${gradient_menu}</b> ],^checkout(grad_menu)")
#out3+=("^tag(grad_menu")
#out3+=("^sep($GRAD_MENU)")
#out3+=("$YES,mb-setvar gradient_menu=yes $CNF_FILE;colorizer -s")
#out3+=("$NO,mb-setvar gradient_menu=no $CNF_FILE;colorizer -s")
#out2+=("$GRAD_CONKY [ <b>${gradient_conkies}</b> ],^checkout(grad_conky)")
#out3+=("^tag(grad_conky")
#out3+=("^sep($GRAD_CONKY)")
#out3+=("$YES,mb-setvar gradient_conkies=yes $CNF_FILE;colorizer -s")
#out3+=("$NO,mb-setvar gradient_conkies=no $CNF_FILE;colorizer -s")

out2+=("^sep($OTHER_SETTINGS)")
#out2+=("^sep($COLORIZER_SETTINGS)")
#if [[ -f "$HOME/.config/tint2/launcherapps/colorizer.desktop" ]]; then
#out2+=("$DELETELAUNCHER,rm ~/.config/tint2/launcherapps/colorizer.desktop;killall -SIGUSR1 tint2;colorizer -s")
#else
#out2+=("$ADDLAUNCHER,cp /usr/share/applications/colorizer.desktop ~/.config/tint2/launcherapps/;killall -SIGUSR1 tint2;colorizer -s")
#fi

out2+=("$HEIGHT [ <b>${colorizer_size}</b> ],^checkout(csize)")
out3+=("^tag(csize)")
out3+=("^sep($HEIGHT)")
out3+=("$FULL,mb-setvar colorizer_size=full $CNF_FILE;colorizer -s")
out3+=("$COMPACT,mb-setvar colorizer_size=compact $CNF_FILE;colorizer -s")
[[ "$related_tools" == "show" ]] && out2+=("<big></big>  $SHOW_RELATED,^checkout(related)") || out2+=("<big></big>  $SHOW_RELATED,^checkout(related)")
out3+=("^tag(related)")
out3+=("^sep($SHOW_RELATED)")
[[ "$related_tools" == "show" ]] && out3+=("<big>綠</big>  $SHOW,colorizer -s" "<big>祿</big>  $HIDE,mb-setvar related_tools=hide $CNF_FILE;colorizer -s") || out3+=("<big>祿</big>  $SHOW,mb-setvar related_tools=show $CNF_FILE;colorizer -s" "<big>綠</big>  $HIDE,colorizer -s") 
out3+=("^sep()")
out3+=("  $EDIT_RELATED,xdg-open $CUSTOM_COMMANDS_FILE")
out3+=("󰁯  $RESET_RELATED,rm $CUSTOM_COMMANDS_FILE;colorizer -s")
out2+=("^sep()")
out2+=("  $EDITCONF,xdg-open $CNF_FILE")
out2+=("󰁯  $RESET,rm $CNF_FILE;colorizer -s")


### RUN
if [[ "$1" == "-s" ]]; then
    . /usr/share/mb-jgtools/pipemenu-standalone.cfg
    . $HOME/.config/mabox/mabox.conf

    if [[ "$colorizer_size" == "full" ]];then
    eval $(xdotool getdisplaygeometry --shell)
    MENU_HEIGHT_MIN="$HEIGHT"
    MENU_HEIGHT_MAX=$MENU_HEIGHT_MIN
    jgtools_radius=0
    menu_margin_x="0"
    else
    MENU_VALIGN="center"
    menu_margin_x=${submenu_spacing:-0}
    fi
    
    MENU_PADDING_TOP=${jgtools_padding:-0}
    MENU_HALIGN="left"
    jgmenu_icon_size=0
    TABS=180
    
    [ $(pidof picom) ] && MENU_RADIUS=$jgtools_radius
    [ -z $jgmenu_use_borders ] && menu_border=0
    JGWIDTH=$((jgtools_padding + 270 + jgtools_padding))
    MENU_PADDING_TOP=$((jgtools_padding + 150))
    
    THUMBDIR="$HOME/.cache/colorizer/thumbs"
    mkdir -p ${THUMBDIR}
    THUMB="${THUMBDIR}/${NAME}.png"
    if [[ ! -f "$THUMB" ]]
        then
    convert ${WALLPATH} -resize 270x150^ -gravity center -extent 270x150 ${THUMB}
    fi
    # colors 
    n=0
    m=0
    X=$((jgtools_padding + 4))
    Y=$((jgtools_padding + 96))
    for i in "${w[@]}"
    do
    if [[ "$n" -lt "10" ]];then
    dots+=("@rect,,$((X + 25*n)),$((Y)),22,22,1,left,top,#FFFFFF 60,${i} 100,")
    else
    dots+=("@rect,,$((X + 25*m)),$((Y+24)),22,22,1,left,top,#FFFFFF 80,${i} 100,")
    ((m=m+1))
    fi
    ((n=n+1))

    done
    
    mkconfigfile
#THUMB="$(DISPLAY=:0 scrot -t 220x100 -o $HOME/.config/mabox/colorizer.png -e 'echo $m')"

cat <<EOF > ${MENU_ITEMS}
@icon,,$((jgtools_padding)),$((jgtools_padding)),270,150,4,left,top,,,${THUMB}

@rect,,$((jgtools_padding)),$((jgtools_padding + 114)),270,32,0,left,top,#222222 20,#222222 70,
@text,,$((jgtools_padding + 168)),$((jgtools_padding + 116)),120,30,0,left,top,#FFFFFF ,${WINCLR},<span size='10400' font_family='Ubuntu'><b>Colorizer</b></span>
$(printf '%s\n' "${dots[@]}")
#@icon,,$((jgtools_padding + 4)),$((jgtools_padding + 34)),270,120,4,left,top,,,/usr/share/mabox-colorizer/img/colorizer.png
$(printf '%s\n' "${out[@]}")
$(printf '%s\n' "${out2[@]}")
$(printf '%s\n' "${out3[@]}")
EOF

    jgmenu --config-file=${CONFIG_FILE} --csv-file=${MENU_ITEMS} 2>/dev/null
    exit 0
fi
printf '%s\n' "${out[@]}"
printf '%s\n' "${out2[@]}"
printf '%s\n' "${out3[@]}"
