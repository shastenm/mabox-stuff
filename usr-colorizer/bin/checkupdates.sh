#!/bin/bash

# check updates and show how many packeges to update in Conky

# We don't need to run checkupdates at Conky start as it takes few seconds and block Conky

CNF_DIR="$HOME/.config/colorizer/conky"
CNF_FILE="$CNF_DIR/conky.cfg"
source <(grep = $CNF_FILE)

case "$LANG" in
    pl*)
    AVAIL_UPDATES="Dostępne aktualizacje"
    UPTODATE="System jest aktualny"
    PKGS_TO_UPDATE="Pakiety do aktualizacji:"
    YAY_UPD="Użyj yay (cli)"
    PAMAC_UPD="Użyj Pamac (GUI)"
    DISABLE="Wyłącz powiadomienia"
    ;;
    es*)
    AVAIL_UPDATES="Actualizaciones disponibles"
    UPTODATE="El sistema esta actualizado"
    PKGS_TO_UPDATE="Paquetes para actualizar:"
    YAY_UPD="Actualizar con yay (cli)"
    PAMAC_UPD="Actualizar con Pamac (GUI)"
    DISABLE="Deshabilitar notificaciones"
    ;;
    *)
    AVAIL_UPDATES="Available updates"
    UPTODATE="System is up to date"
    PKGS_TO_UPDATE="Packages to update:"
    YAY_UPD="Update with yay (cli)"
    PAMAC_UPD="Update with Pamac (GUI)"
    DISABLE="Disable notifications"
    ;;
esac

case "$1" in
    -s)
    CONKYPID=$(pgrep -f sysinfo_mbcolor)
    ;;
    -g)
    CONKYPID=$(pgrep -f sysinfo_graph_mbcolor)
    ;;
    -m)
    CONKYPID=$(pgrep -f mabox_info_mbcolor)
    ;;
esac


if [ -f /tmp/"$CONKYPID" ]; then
    PKGS=$(pamac checkupdates --no-aur -q| wc -l)
    if [ "$PKGS" != "0" ]; then
        if [[ "$update_notifications" == "true" ]];then
        notify-send.sh -u critical -i mbcc "$AVAIL_UPDATES" "$PKGS_TO_UPDATE $PKGS" -o "$YAY_UPD:terminator -T '$YAY_UPD' -e yay" \
        -o "$PAMAC_UPD:pamac-manager --updates" -o "$DISABLE:mb-setvar update_notifications=false ${CNF_FILE}"
        fi
        printf "\${font}\${alignr} $AVAIL_UPDATES: \${color red}"
        echo "$PKGS"
    else
    printf "\${font}\${color}\${alignr} $UPTODATE"
    fi
else
    echo "$1" > /tmp/"${CONKYPID}"
fi
